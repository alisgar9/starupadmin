<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Azulcremas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW azulcremas AS SELECT `numeroAmericanista`,`nombre`,`paterno`,`materno`,`correo`,`estado`,`fechanac`,`direccion`,`colonia`,`municipio`,`cp`,`activo` FROM `americanista` WHERE (`numeroAmericanista` like 'AZ%');");
    }

   
}
