-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-01-2020 a las 00:46:20
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `starup`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_bancos`
--

CREATE TABLE `adm_bancos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 activo, 2 desactivo ',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_bancos`
--

INSERT INTO `adm_bancos` (`id`, `descripcion`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'BANAMEX', 1, '2019-09-26 20:38:03', '2019-09-26 20:38:03'),
(2, 'HSBC', 1, '2019-09-25 05:00:00', '2019-09-25 05:00:00'),
(3, 'Bancomer', 0, '2019-09-26 19:35:42', '2019-09-26 19:35:42'),
(4, 'BBVA', 1, '2019-09-26 19:31:00', '2019-09-26 19:31:00'),
(5, 'SANTANDER', 1, '2019-09-26 20:38:44', '2019-09-26 20:38:44'),
(6, 'Banco Azteca', 1, '2019-10-01 19:02:15', '2019-10-01 19:02:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_cargo`
--

CREATE TABLE `adm_cargo` (
  `idcargo` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 activo, 2 desactivo ',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_cargo`
--

INSERT INTO `adm_cargo` (`idcargo`, `descripcion`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'Encargado de Operaciones', 1, '2019-09-23 20:35:27', '2019-09-23 20:35:27'),
(2, 'Capturista', 0, '2019-09-23 19:14:05', '2019-09-23 19:14:05'),
(3, 'Capturista', 0, '2019-09-23 19:14:33', '2019-09-23 19:14:33'),
(4, 'Mensajero', 1, '2019-09-23 19:01:11', '2019-09-23 19:01:11'),
(5, 'Operador', 1, '2019-10-01 19:01:01', '2019-10-01 19:01:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_clasificacion`
--

CREATE TABLE `adm_clasificacion` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 activo, 2 desactivo ',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_clasificacion`
--

INSERT INTO `adm_clasificacion` (`id`, `descripcion`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'Shipping Line', 1, '2019-10-28 19:04:17', '2019-09-25 15:07:02'),
(2, 'Shipper', 1, '2019-09-25 15:31:37', '2019-09-25 15:31:37'),
(3, 'Carrier', 1, '2019-09-25 14:48:11', '2019-09-25 14:48:11'),
(4, 'Consignee', 1, '2019-09-25 15:33:43', '2019-09-25 15:33:43'),
(5, 'Customs Brokers', 1, '2019-09-25 15:37:54', '2019-09-25 15:37:54'),
(6, 'Trucker', 1, '2019-09-25 15:39:22', '2019-09-25 15:39:22'),
(7, 'Port Operator', 1, '2019-09-25 15:40:39', '2019-09-25 15:40:39'),
(8, 'AirLine', 1, '2019-11-15 18:25:19', '2019-10-28 19:04:52'),
(9, 'Inland Carrier', 1, '2019-10-28 19:06:04', '2019-10-28 19:06:04'),
(10, 'Shipping Agents', 1, '2019-10-28 19:06:04', '2019-10-28 19:06:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_contenedor`
--

CREATE TABLE `adm_contenedor` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_contenedor`
--

INSERT INTO `adm_contenedor` (`id`, `descripcion`, `activo`, `created_at`, `updated_at`) VALUES
(1, '20 ST', 1, '2019-10-04 18:27:46', '2019-10-04 18:27:46'),
(2, '20 ST', 0, '2019-10-04 19:13:31', '2019-10-04 19:13:31'),
(3, '40 ST', 1, '2019-10-04 20:27:29', '2019-10-04 20:27:29'),
(4, '40 HC', 1, '2019-10-04 20:39:30', '2019-10-04 20:39:30'),
(5, '40 RF', 1, '2019-10-04 20:41:40', '2019-10-04 20:41:40'),
(6, '40 OT', 1, '2019-10-04 20:42:20', '2019-10-04 20:42:20'),
(7, '10 ST', 1, '2019-10-04 20:43:04', '2019-10-04 20:43:04'),
(8, 'OTRO', 1, '2020-01-20 22:20:17', '2020-01-20 16:20:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_entidad`
--

CREATE TABLE `adm_entidad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `razonsocial` varchar(255) DEFAULT NULL,
  `id_clasificacion` int(10) DEFAULT NULL,
  `nombrecontacto` varchar(255) DEFAULT NULL,
  `direccion` text DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `email` varchar(40) DEFAULT 'empresa@empresa.com',
  `taxid` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_entidad`
--

INSERT INTO `adm_entidad` (`id`, `nombre`, `razonsocial`, `id_clasificacion`, `nombrecontacto`, `direccion`, `telefono`, `email`, `taxid`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ZETA FORWARDING Inc', 'ZETA FORWARDING Inc', 9, 'ZETA FORWARDING Inc', NULL, NULL, 'dta1@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(2, 'ZAGA EXPRESS', 'ZAGA EXPRESS', 9, 'ZAGA EXPRESS', NULL, NULL, 'dta2@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(3, 'XCF SA DE CV', 'XCF SA DE CV', 9, 'XCF SA DE CV', NULL, NULL, 'dta3@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(4, 'TRASNPORTES HERMANOS VILLAR', 'TRASNPORTES HERMANOS VILLAR', 9, 'TRASNPORTES HERMANOS VILLAR', NULL, NULL, 'dta4@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(5, 'TRIP MEXICANA S.A. DE C.V.', 'TRIP MEXICANA S.A. DE C.V.', 9, 'TRIP MEXICANA S.A. DE C.V.', NULL, NULL, 'dta5@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(6, 'TRANSPORTES FUGARI S.A DE C.V', 'TRANSPORTES FUGARI S.A DE C.V', 9, 'TRANSPORTES FUGARI S.A DE C.V', NULL, NULL, 'dta6@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(7, 'TRANSPORTES MC', 'TRANSPORTES MC', 9, 'TRANSPORTES MC', NULL, NULL, 'dta7@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(8, 'TRANSPORTES JUAREZ', 'TRANSPORTES JUAREZ', 9, 'TRANSPORTES JUAREZ', NULL, NULL, 'dta8@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(9, 'Sepsa Transportes S.A de C.V', 'Sepsa Transportes S.A de C.V', 9, 'Sepsa Transportes S.A de C.V', NULL, NULL, 'dta9@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(10, 'TRANSPORTES RAYMUNDO SILVA DE LA PORTILLA SA DE CV', 'TRANSPORTES RAYMUNDO SILVA DE LA PORTILLA SA DE CV', 9, 'TRANSPORTES RAYMUNDO SILVA DE LA PORTILLA SA DE CV', NULL, NULL, 'dta10@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(11, 'ROBERTO PANIAGUA ANAYA', 'ROBERTO PANIAGUA ANAYA', 9, 'ROBERTO PANIAGUA ANAYA', NULL, NULL, 'dta11@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(12, 'RIO GRANDE MACHINERY', 'RIO GRANDE MACHINERY', 9, 'RIO GRANDE MACHINERY', NULL, NULL, 'dta12@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(13, 'REGROS SA DE CV', 'REGROS SA DE CV', 9, 'REGROS SA DE CV', NULL, NULL, 'dta13@gmail.com', NULL, 1, '2019-11-15 19:39:11', '2019-11-15 19:39:11'),
(14, 'POTOSINOS', 'POTOSINOS', 9, 'POTOSINOS', NULL, NULL, 'dta14@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(15, 'PEREZ TRUCKING SA DE CV', 'PEREZ TRUCKING SA DE CV', 9, 'PEREZ TRUCKING SA DE CV', NULL, NULL, 'dta15@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(16, 'MILLHOUSE MEXICO SA DE CV', 'MILLHOUSE MEXICO SA DE CV', 9, 'MILLHOUSE MEXICO SA DE CV', NULL, NULL, 'dta16@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(17, 'MEXICOM LOGISTICS', 'MEXICOM LOGISTICS', 9, 'MEXICOM LOGISTICS', NULL, NULL, 'dta17@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(18, 'MEXPRESS TRANSPORTATION INC', 'MEXPRESS TRANSPORTATION INC', 9, 'MEXPRESS TRANSPORTATION INC', NULL, NULL, 'dta18@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(19, 'TRANSPORTES LIMON', 'TRANSPORTES LIMON', 9, 'TRANSPORTES LIMON', NULL, NULL, 'dta19@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(20, 'LANDSTAR', 'LANDSTAR', 9, 'LANDSTAR', NULL, NULL, 'dta20@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(21, 'INNOVACION LOGIKA', 'INNOVACION LOGIKA', 9, 'INNOVACION LOGIKA', NULL, NULL, 'dta21@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(22, 'FISA', 'FISA', 9, 'FISA', NULL, NULL, 'dta22@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(23, 'LOGISTICA DLA S DE RL DE CV', 'LOGISTICA DLA S DE RL DE CV', 9, 'LOGISTICA DLA S DE RL DE CV', NULL, NULL, 'dta23@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(24, 'CBI FREIGHT', 'CBI FREIGHT', 9, 'CBI FREIGHT', NULL, NULL, 'dta24@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(25, 'AZTECA PLUS SA DE CV', 'AZTECA PLUS SA DE CV', 9, 'AZTECA PLUS SA DE CV', NULL, NULL, 'dta25@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(26, 'VALOR SOLUCIONES EN TRANSPORTE Y LOGISTICA INTEGRAL S.A DE CV', 'VALOR SOLUCIONES EN TRANSPORTE Y LOGISTICA INTEGRAL S.A DE CV', 9, 'VALOR SOLUCIONES EN TRANSPORTE Y LOGISTICA INTEGRAL S.A DE CV', NULL, NULL, 'dta26@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(27, 'TRANSPORTES DE CARGA VEGETA S.A. DE C.V.', 'TRANSPORTES DE CARGA VEGETA S.A. DE C.V.', 9, 'TRANSPORTES DE CARGA VEGETA S.A. DE C.V.', NULL, NULL, 'dta27@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(28, 'SCHNELL LOGISTICS', 'SCHNELL LOGISTICS', 5, 'SCHNELL LOGISTICS', NULL, NULL, 'dta28@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(29, 'GRUCAS LOGISTICA', 'GRUCAS LOGISTICA', 5, 'GRUCAS LOGISTICA', NULL, NULL, 'dta29@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(30, 'SERVICIOS ADUANEROS DEL NORTE S.C', 'SERVICIOS ADUANEROS DEL NORTE S.C', 5, 'SERVICIOS ADUANEROS DEL NORTE S.C', NULL, NULL, 'dta30@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(31, 'ALIANZA ESTRATEGICA PORTUARIA S.A DE C.V', 'ALIANZA ESTRATEGICA PORTUARIA S.A DE C.V', 5, 'ALIANZA ESTRATEGICA PORTUARIA S.A DE C.V', NULL, NULL, 'dta31@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(32, 'HANO PANIAGUA', 'HANO PANIAGUA', 5, 'HANO PANIAGUA', NULL, NULL, 'dta32@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(33, 'LOZAK SERVICIOS LOGISTICA Y REFACCIONES S.A DE C.V', 'LOZAK SERVICIOS LOGISTICA Y REFACCIONES S.A DE C.V', 5, 'LOZAK SERVICIOS LOGISTICA Y REFACCIONES S.A DE C.V', NULL, NULL, 'dta33@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(34, 'GRUPO ADUANAL BARQUIN S.C', 'GRUPO ADUANAL BARQUIN S.C', 5, 'GRUPO ADUANAL BARQUIN S.C', NULL, NULL, 'dta34@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(35, 'ALBECA LOGISTICS SOLUTIONS', 'ALBECA LOGISTICS SOLUTIONS', 5, 'ALBECA LOGISTICS SOLUTIONS', NULL, NULL, 'dta35@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(36, 'CAPITALZEER SA DE CV', 'CAPITALZEER SA DE CV', 5, 'CAPITALZEER SA DE CV', NULL, NULL, 'dta36@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(37, 'AGENCIA ADUANAL ESQUER LUKEN, S.C.', 'AGENCIA ADUANAL ESQUER LUKEN, S.C.', 5, 'AGENCIA ADUANAL ESQUER LUKEN, S.C.', NULL, NULL, 'dta37@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(38, 'AGENCIA ADUANAL ZAMUDIO SA DE CV', 'AGENCIA ADUANAL ZAMUDIO SA DE CV', 5, 'AGENCIA ADUANAL ZAMUDIO SA DE CV', NULL, NULL, 'dta38@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(39, 'KUNDISO RED LOGISITCA S DE R.L. DE C.V.', 'KUNDISO RED LOGISITCA S DE R.L. DE C.V.', 5, 'KUNDISO RED LOGISITCA S DE R.L. DE C.V.', NULL, NULL, 'dta39@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(40, 'SINERGIA ADUANAL MANZANILLO SC', 'SINERGIA ADUANAL MANZANILLO SC', 5, 'SINERGIA ADUANAL MANZANILLO SC', NULL, NULL, 'dta40@gmail.com', NULL, 1, '2019-11-15 19:39:12', '2019-11-15 19:39:12'),
(41, 'RADAR CUSTOMS & LOGISTICS SAPI DE CV.', 'RADAR CUSTOMS & LOGISTICS SAPI DE CV.', 5, 'RADAR CUSTOMS & LOGISTICS SAPI DE CV.', NULL, NULL, 'dta41@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(42, 'GOMEZ Y ALVEZ', 'GOMEZ Y ALVEZ', 5, 'GOMEZ Y ALVEZ', NULL, NULL, 'dta42@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(43, 'VISA FREIGHT SOLUTIONS SA DE CV', 'VISA FREIGHT SOLUTIONS SA DE CV', 5, 'VISA FREIGHT SOLUTIONS SA DE CV', NULL, NULL, 'dta43@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(44, 'Zim israel navigation CO', 'Zim israel navigation CO', 1, 'Zim israel navigation CO', NULL, NULL, 'dta44@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(45, 'ZIM LINE', 'ZIM LINE', 1, NULL, NULL, NULL, 'dta45@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(46, 'Yangming marine transport corp', 'Yangming marine transport corp', 1, 'Yangming marine transport corp', NULL, NULL, 'dta46@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(47, 'Wan Hai Lines', 'Wan Hai Lines', 1, 'Wan Hai Lines', NULL, NULL, 'dta47@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(48, 'Hamburg Süd', 'Hamburg Süd', 1, 'Hamburg Süd', NULL, NULL, 'dta48@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(49, 'Sea-land service; INC', 'Sea-land service; INC', 1, 'Sea-land service; INC', NULL, NULL, 'dta49@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(50, 'PACIFIC INTERNATIONAL LINES ( PTE) LTD.', 'PACIFIC INTERNATIONAL LINES ( PTE) LTD.', 1, NULL, NULL, NULL, 'dta50@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(51, 'PACIFIC INTERNATIONAL LINES (PTE) LTD.', 'PACIFIC INTERNATIONAL LINES (PTE) LTD.', 1, NULL, NULL, NULL, 'dta51@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(52, 'Orient overseas container line INC', 'Orient overseas container line INC', 1, 'Orient overseas container line INC', NULL, NULL, 'dta52@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(53, 'Orient Overseas Container Line Ltd', 'Orient Overseas Container Line Ltd', 1, NULL, NULL, NULL, 'dta53@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(54, 'ONE LINE', 'ONE LINE', 1, NULL, NULL, NULL, 'dta54@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(55, 'Nyk lines', 'Nyk lines', 1, 'Nyk lines', NULL, NULL, 'dta55@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(56, 'NEWTRAL', 'NEWTRAL', 1, NULL, NULL, NULL, 'dta56@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(57, 'Naviomar (MXP)', 'Naviomar (MXP)', 1, NULL, NULL, NULL, 'dta57@gmail.com', NULL, 1, '2019-11-15 19:39:13', '2019-11-15 19:39:13'),
(58, 'MSC (CITI)', 'MSC (CITI)', 1, NULL, NULL, NULL, 'dta58@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(59, 'MSC (SANT) 1543', 'MSC (SANT) 1543', 1, NULL, NULL, NULL, 'dta59@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(60, 'MSC (SANT) 6382', 'MSC (SANT) 6382', 1, NULL, NULL, NULL, 'dta60@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(61, 'Mitsui O.S.K. lines', 'Mitsui O.S.K. lines', 1, 'Mitsui O.S.K. lines', NULL, NULL, 'dta61@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(62, 'MOL PACE', 'MOL PACE', 1, NULL, NULL, NULL, 'dta62@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(63, 'MSC (SANT) 1255', 'MSC (SANT) 1255', 1, NULL, NULL, NULL, 'dta63@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(64, 'Maersk lines; INC.', 'Maersk lines; INC.', 1, 'Maersk lines; INC.', NULL, NULL, 'dta64@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(65, 'INTERTEAM SA DE CV', 'INTERTEAM SA DE CV', 1, 'INTERTEAM', NULL, NULL, 'dta65@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(66, 'Hapag-lloyd A G', 'Hapag-lloyd A G', 1, 'Hapag-lloyd A G', NULL, NULL, 'dta66@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(67, 'Hyundai merchant marine (america) INC', 'Hyundai merchant marine (america) INC', 1, 'Hyundai merchant marine (america) INC', NULL, NULL, 'dta67@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(68, 'EVERGREEN (MXP) 4625', 'EVERGREEN (MXP) 4625', 1, NULL, NULL, NULL, 'dta68@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(69, 'EVERGREEN (MXP) 2859', 'EVERGREEN (MXP) 2859', 1, NULL, NULL, NULL, 'dta69@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(70, 'Evergreen Marine Corporation', 'Evergreen Marine Corporation', 1, 'Evergreen Marine Corporation', NULL, NULL, 'dta70@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(71, 'EVERGREEN (USD)', 'EVERGREEN (USD)', 1, NULL, NULL, NULL, 'dta71@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(72, 'CHARTER LINK LOGISTICS', 'CHARTER LINK LOGISTICS', 1, NULL, NULL, NULL, 'dta72@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(73, 'COSCO (MXP)', 'COSCO (MXP)', 1, NULL, NULL, NULL, 'dta73@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(74, 'COSCO (BAN)', 'COSCO (BAN)', 1, NULL, NULL, NULL, 'dta74@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(75, 'COSCO (JP)', 'COSCO (JP)', 1, NULL, NULL, NULL, 'dta75@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(76, 'COSCO', 'COSCO', 1, 'COSCO', NULL, NULL, 'dta76@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(77, 'Cargo marine containers LTD', 'Cargo marine containers LTD', 1, 'Cargo marine containers LTD', NULL, NULL, 'dta77@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(78, 'CMA-CGM', 'CMA-CGM', 1, NULL, NULL, NULL, 'dta78@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(79, 'American president lines LTD', 'American president lines LTD', 1, 'American president lines LTD', NULL, NULL, 'dta79@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(80, 'Empresa de navegacao alianca S A', 'Empresa de navegacao alianca S A', 1, 'Empresa de navegacao alianca S A', NULL, NULL, 'dta80@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(81, 'OCEAN NETWORK EXPRESS PTE. LTD.(HSBC)', 'OCEAN NETWORK EXPRESS PTE. LTD.(HSBC)', 1, NULL, NULL, NULL, 'dta81@gmail.com', NULL, 1, '2019-11-15 19:39:14', '2019-11-15 19:39:14'),
(82, 'TACA International Airlines', 'TACA International Airlines', 8, 'TACA International Airlines', NULL, NULL, 'dta82@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(83, 'SN Brussels Airlines', 'SN Brussels Airlines', 8, 'SN Brussels Airlines', NULL, NULL, 'dta83@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(84, 'DHL Aviation / European Air Transport', 'DHL Aviation / European Air Transport', 8, 'DHL Aviation / European Air Transport', NULL, NULL, 'dta84@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(85, 'Transportes Aereos Mercantiles', 'Transportes Aereos Mercantiles', 8, 'Transportes Aereos Mercantiles', NULL, NULL, 'dta85@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(86, 'ANA All Nippon Airways', 'ANA All Nippon Airways', 8, 'ANA All Nippon Airways', NULL, NULL, 'dta86@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(87, 'Mas Air Cargo', 'Mas Air Cargo', 8, 'Mas Air Cargo', NULL, NULL, 'dta87@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(88, 'LAN Airlines', 'LAN Airlines', 8, 'LAN Airlines', NULL, NULL, 'dta88@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(89, 'Iberia Airlines (IAG Cargo)', 'Iberia Airlines (IAG Cargo)', 8, 'Iberia Airlines (IAG Cargo)', NULL, NULL, 'dta89@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(90, 'Emirates', 'Emirates', 8, 'Emirates', NULL, NULL, 'dta90@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(91, 'European Aviation Air Charter', 'European Aviation Air Charter', 8, 'European Aviation Air Charter', NULL, NULL, 'dta91@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(92, 'China Southern Airlines', 'China Southern Airlines', 8, 'China Southern Airlines', NULL, NULL, 'dta92@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(93, 'Cathay Pacific Airways', 'Cathay Pacific Airways', 8, 'Cathay Pacific Airways', NULL, NULL, 'dta93@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(94, 'Cargolux Airlines', 'Cargolux Airlines', 8, 'Cargolux Airlines', NULL, NULL, 'dta94@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(95, 'COPA', 'COPA', 8, 'COPA', NULL, NULL, 'dta95@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(96, 'Air China / Air China Cargo', 'Air China / Air China Cargo', 8, 'Air China / Air China Cargo', NULL, NULL, 'dta96@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(97, 'British Airways', 'British Airways', 8, 'British Airways', NULL, NULL, 'dta97@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(98, 'Aeromexico', 'Aeromexico', 8, 'Aeromexico', NULL, NULL, 'dta98@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(99, 'Air France', 'Air France', 8, 'Air France', NULL, NULL, 'dta99@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(100, 'Air Canada', 'Air Canada', 8, 'Air Canada', NULL, NULL, 'dta100@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(101, 'AeroUnion (Aerotransporte de Carga Unión)', 'AeroUnion (Aerotransporte de Carga Unión)', 8, 'AeroUnion (Aerotransporte de Carga Unión)', NULL, NULL, 'dta101@gmail.com', NULL, 1, '2019-11-15 19:39:15', '2019-11-15 19:39:15'),
(102, 'SAN LOGISTICS LIMITED\r\n', 'SAN LOGISTICS LIMITED', 10, 'SAN LOGISTICS LIMITED', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(103, 'ORIENT OVERSEAS CONTAINER LINE\r\n', 'ORIENT OVERSEAS CONTAINER LINE\r\n', 10, 'ORIENT OVERSEAS CONTAINER LINE\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(104, 'AGUNSA L&D DE CV BANAMEX(369)\r\n', 'AGUNSA L&D DE CV BANAMEX(369)\r\n', 10, 'AGUNSA L&D DE CV BANAMEX(369)\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(105, 'AIR & SEA CONTAINERSHIPPING SRL\r\n', 'AIR & SEA CONTAINERSHIPPING SRL\r\n', 10, 'AIR & SEA CONTAINERSHIPPING SRL\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(106, 'ONE LINE\r\n', 'ONE LINE\r\n', 10, 'ONE LINE\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(107, 'AGUNSA (MXN) 018\r\n', 'AGUNSA (MXN) 018\r\n', 10, 'AGUNSA (MXN) 018\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(108, 'AGUNSA (BAN) 624\r\n', 'AGUNSA (BAN) 624\r\n', 10, 'AGUNSA (BAN) 624\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(109, 'AGUNSA (BAN) 369\r\n', 'AGUNSA (BAN) 369\r\n', 10, 'AGUNSA (BAN) 369\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(110, 'ACP LOGISTICS SAC\r\n', 'ACP LOGISTICS SAC\r\n', 10, 'ACP LOGISTICS SAC\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(111, 'TRANSPORT SERVICES & LOGISTICS JAPAN LTD\r\n', 'TRANSPORT SERVICES & LOGISTICS JAPAN LTD\r\n', 10, 'TRANSPORT SERVICES & LOGISTICS JAPAN LTD\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(112, 'INTERTEAM SA DE CV\r\n', 'INTERTEAM SA DE CV\r\n', 10, 'INTERTEAM SA DE CV\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(113, 'GUANGZHOU LZ CARGO\r\n', 'GUANGZHOU LZ CARGO\r\n', 10, 'GUANGZHOU LZ CARGO\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(114, 'IBEROFORWARDERS\r\n', 'IBEROFORWARDERS\r\n', 10, 'IBEROFORWARDERS\r\n', NULL, NULL, 'empresa@empresa.com', NULL, 1, '2019-11-21 22:15:00', '2019-11-21 22:15:00'),
(115, 'KUMHO TIRE CO., INC.', 'KUMHO TIRE CO., INC.', 4, '', '', NULL, 'KUMHO@GMAIL.COM', '', 1, '2020-01-02 19:24:14', '2020-01-02 19:24:14'),
(116, 'QINGDAO AWESOME INTERNATIONAL TRADE CO.,LTD', 'QINGDAO AWESOME INTERNATIONAL TRADE CO.,LTD', 4, '', '', NULL, 'QINGDAO@GMAIL.COM', '', 1, '2020-01-02 19:25:23', '2020-01-02 19:25:23'),
(117, 'TIRE DIRECT S.A DE C.V', 'TIRE DIRECT S.A DE C.V', 4, 'CARLOS ZAMBRANO', 'BOULEVARD CAMPESTRE 72 COL. CASA BLANCA', NULL, 'CARLOS@TIREDIRECT.COM.MX', 'TDI030516L26', 1, '2020-01-08 10:27:20', '2020-01-08 10:27:20'),
(118, 'STARUP LOGISTICS CO., LTD', 'STARUP LOGISTICS CO., LTD', 10, '', '', NULL, 'QUEENIE@STARUP-LOGISTICS.COM', '', 1, '2020-01-08 10:30:22', '2020-01-08 10:30:22'),
(119, 'HK MEIDIYU TOYS INDUSTRIAL CO., LIMITED', 'HK MEIDIYU TOYS INDUSTRIAL CO., LIMITED', 2, '', '', NULL, 'MEIDIYU@GMAIL.COM', '', 1, '2020-01-17 10:15:04', '2020-01-17 10:15:04'),
(120, 'CEINGIS', 'CEINGIS SA DE CV', 4, 'GREGORIA', '', NULL, 'GREG.ALMANZA@CEINGIS.COM.MX', '', 1, '2020-01-17 10:16:25', '2020-01-17 10:16:25'),
(121, 'WENZHOU CHIEFSTONE COMMERCER & TRADE CO., LTD.', 'WENZHOU CHIEFSTONE COMMERCER & TRADE CO., LTD.', 2, '', '', NULL, 'WENZHOU@GMAIL.COM', '', 1, '2020-01-17 10:29:38', '2020-01-17 10:29:38'),
(122, 'LINCOLN ELECTRIC MANUFACTURA SA DE CV', 'LINCOLN ELECTRIC MANUFACTURA SA DE CV', 4, '', '', NULL, 'LINCOLN@GMAIL.COM', '', 1, '2020-01-17 10:30:49', '2020-01-17 10:30:49'),
(123, 'SHANDONG FUYANG BIO-TECH.CO. LTD. PINGYUAN', 'SHANDONG FUYANG BIO-TECH.CO. LTD. PINGYUAN', 2, '', '', NULL, 'EMAIL@GMAIL.COM', '', 1, '2020-01-17 11:02:04', '2020-01-17 11:02:04'),
(124, 'PROQUIAB', 'PROQUIAB SA DE CV', 4, '', '', NULL, 'COMPRAS@PROQUIAB.COM.MX', '', 1, '2020-01-17 11:05:21', '2020-01-17 11:05:21'),
(125, 'CAIMI LEON', 'CAIMI LEON SA DE CV', 4, 'PAULINA IÑIGUEZ', 'LAMBDA 303 INDUSTRIAL DDELTA, LEON', '52 477 761 3059', 'LOGISTICA@CAIMILEON.COM.MX', 'CLE07-10-08-E35', 1, '2020-01-17 11:51:23', '2020-01-17 11:51:23'),
(126, 'VISA LOGISTICA', 'VISA LOGISTICA', 5, '', '', NULL, 'VISA@GMAIL.COM', '', 1, '2020-01-17 12:48:50', '2020-01-17 12:48:50'),
(127, 'MAXTREK TYRE LIMITED', 'MAXTREK TYRE LIMITED', 2, '', '', NULL, 'MAXTREK@GMAIL.COM', '', 1, '2020-01-17 13:59:07', '2020-01-17 13:59:07'),
(128, 'SHANGHAI LVDING TRADING CO LTD', 'SHANGHAI LVDING TRADING CO.LTD', 2, '', '', NULL, 'SHANGHAI@GMAIL.COM', '', 1, '2020-01-17 15:50:21', '2020-01-17 15:50:21'),
(129, 'NUEVA GENERACION MANUFACTURAS SA DE CV', 'NUEVA GENERACION MANUFACTURAS SA DE CV', 4, '', '', NULL, 'NGM@GMAIL.COM', '', 1, '2020-01-17 15:51:16', '2020-01-17 15:51:16'),
(130, 'DIMICH', 'DIMICH', 4, '', '', NULL, 'dimich@starup.com.mx', '', 1, '2020-01-21 18:38:35', '2020-01-21 18:38:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_entidad_bancos`
--

CREATE TABLE `adm_entidad_bancos` (
  `id` int(11) NOT NULL,
  `id_entidad` int(10) NOT NULL,
  `cuentabancaria` varchar(50) NOT NULL,
  `cinterbancaria` varchar(18) NOT NULL,
  `id_banco` int(10) NOT NULL,
  `id_banco_inter` int(10) DEFAULT NULL,
  `swift` varchar(20) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_incoterm`
--

CREATE TABLE `adm_incoterm` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_incoterm`
--

INSERT INTO `adm_incoterm` (`id`, `nombre`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'FOB', 1, '2019-10-11 15:21:16', NULL),
(2, 'CIF', 1, '2019-10-11 15:21:16', NULL),
(3, 'EXW', 1, '2019-10-11 15:21:16', NULL),
(4, 'DDP', 1, '2019-10-11 15:21:16', NULL),
(5, 'DAP', 1, '2019-10-11 15:21:16', NULL),
(6, 'DAT', 1, '2019-10-11 15:21:16', NULL),
(7, 'CNF', 1, '2019-10-11 15:21:16', NULL),
(8, 'FCA', 1, '2019-10-11 15:21:16', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_liberacion`
--

CREATE TABLE `adm_liberacion` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(40) NOT NULL,
  `tipo` tinytext NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_liberacion`
--

INSERT INTO `adm_liberacion` (`id`, `descripcion`, `tipo`, `activo`) VALUES
(1, 'SWB', 'MBL', 1),
(2, 'Original', 'MBL', 1),
(3, 'Telex', 'HBL', 1),
(4, 'Surrender', 'HBL', 1),
(5, 'Original', 'HBL', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_operaciones`
--

CREATE TABLE `adm_operaciones` (
  `id` int(11) NOT NULL,
  `hbl` varchar(40) DEFAULT NULL,
  `mbl` varchar(25) DEFAULT NULL,
  `file_hbl` varchar(100) DEFAULT NULL,
  `file_mbl` varchar(100) DEFAULT NULL,
  `house_release` varchar(25) DEFAULT NULL,
  `master_release` varchar(25) DEFAULT NULL,
  `ETD` timestamp NULL DEFAULT NULL,
  `ETA` timestamp NULL DEFAULT NULL,
  `vessel` varchar(50) DEFAULT NULL,
  `vogage` varchar(25) DEFAULT NULL,
  `POL` varchar(5) DEFAULT NULL,
  `POD` varchar(5) DEFAULT NULL,
  `final_dest` varchar(80) DEFAULT NULL,
  `shipper` int(10) DEFAULT NULL,
  `consignee` int(10) DEFAULT NULL,
  `agente` int(10) DEFAULT NULL,
  `agente_envio` tinyint(5) NOT NULL,
  `shipping_line` int(10) DEFAULT NULL,
  `INCOTERM` int(10) DEFAULT NULL,
  `commodity` text DEFAULT NULL,
  `EIR` tinyint(2) DEFAULT NULL,
  `file_EIR` varchar(40) DEFAULT NULL,
  `empty_container` timestamp NULL DEFAULT NULL,
  `free_demurrages_days_cust` int(11) DEFAULT 21,
  `free_demurrages_days_sl` int(11) NOT NULL DEFAULT 21,
  `free_storage_days` int(11) DEFAULT NULL,
  `house_release_date` timestamp NULL DEFAULT NULL,
  `master_release_date` timestamp NULL DEFAULT NULL,
  `tipo_operacion` tinyint(4) DEFAULT NULL,
  `operacion_detail` tinyint(4) DEFAULT NULL,
  `operacion` tinyint(1) NOT NULL,
  `activo` tinyint(4) DEFAULT NULL,
  `share` tinyint(1) DEFAULT 0,
  `cy` tinyint(1) NOT NULL,
  `cy_door` tinyint(1) DEFAULT NULL,
  `precio_demora` float(10,2) DEFAULT NULL,
  `PO` varchar(255) DEFAULT NULL,
  `PI` varchar(255) DEFAULT NULL,
  `factura` varchar(255) DEFAULT NULL,
  `sc` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_operaciones`
--

INSERT INTO `adm_operaciones` (`id`, `hbl`, `mbl`, `file_hbl`, `file_mbl`, `house_release`, `master_release`, `ETD`, `ETA`, `vessel`, `vogage`, `POL`, `POD`, `final_dest`, `shipper`, `consignee`, `agente`, `agente_envio`, `shipping_line`, `INCOTERM`, `commodity`, `EIR`, `file_EIR`, `empty_container`, `free_demurrages_days_cust`, `free_demurrages_days_sl`, `free_storage_days`, `house_release_date`, `master_release_date`, `tipo_operacion`, `operacion_detail`, `operacion`, `activo`, `share`, `cy`, `cy_door`, `precio_demora`, `PO`, `PI`, `factura`, `sc`, `created_at`, `updated_at`) VALUES
(1, 'SULGZ0005566', '149906525861', NULL, NULL, '3', '1', '2020-01-10 00:00:00', '2020-02-20 00:00:00', 'CAPE MAHON', '0519-042S', '478', '197', 'TOLUCA', 119, 120, NULL, 118, 70, NULL, 'PLASTIC TOYS', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, NULL, 2, 2, 127.60, NULL, NULL, NULL, '', '2020-01-17 10:18:54', '2020-01-17 10:18:54'),
(2, 'SULGZ0005265', '588517272', NULL, NULL, '3', '1', '2019-11-30 00:00:00', '2019-12-23 00:00:00', 'A.P. MOLLER', '947S', '382', '197', '', 121, 122, NULL, 118, 64, NULL, 'SPARE PARTS FOR WELDING ACCESSORIES', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, NULL, 1, NULL, 170.00, NULL, NULL, '2347', '', '2020-01-17 10:31:59', '2020-01-17 10:31:59'),
(3, 'SULGZ0005154', '6226660790', NULL, NULL, '3', NULL, '2019-11-09 00:00:00', '2019-11-30 00:00:00', 'COSCO YANTIAN', '092E', '381', '198', '', 123, 124, NULL, 118, NULL, 1, 'SODIUM GLUCONATE', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, 0, 1, NULL, 130.00, NULL, NULL, NULL, '', '2020-01-17 11:06:42', '2020-01-17 11:24:35'),
(4, 'SULGZ0005048', '6222213480', NULL, NULL, '5', '1', '2019-10-30 00:00:00', '2019-11-20 00:00:00', 'CMA CGM GANGES', '0MH3VE1', '379', '198', 'MANZANILLO', 121, 125, NULL, 118, NULL, 1, 'ARTIFICIAL LEATHER', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, NULL, 1, NULL, 170.00, NULL, NULL, NULL, '', '2020-01-17 11:53:16', '2020-01-17 11:53:16'),
(5, 'D2020011739PRUEBA123', NULL, NULL, NULL, '4', NULL, '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'QWERTY', 'QWERTY', '381', '480', '', NULL, 117, 126, 118, 64, 3, 'LLANTAS', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 0, 52, 1, NULL, 170.00, NULL, NULL, NULL, '', '2020-01-17 12:57:05', '2020-01-17 13:50:39'),
(6, 'PRUEBA1234', NULL, NULL, NULL, '4', '2', '2019-12-01 00:00:00', '2019-12-02 00:00:00', '789', '789', '382', '198', '', NULL, 117, NULL, 118, NULL, NULL, 'NADA', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 1, 2, 1, NULL, 1, NULL, 170.00, NULL, NULL, NULL, '', '2020-01-17 13:06:09', '2020-01-17 13:06:09'),
(7, 'PRUEBA1', NULL, NULL, NULL, NULL, NULL, '2019-12-02 00:00:00', '2019-12-02 00:00:00', '', 'QWERTY', '382', '356', '', NULL, 117, NULL, 118, 83, 4, 'PRUEBA AVION', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 2, NULL, 2, 1, NULL, 1, NULL, 170.00, NULL, NULL, NULL, '', '2020-01-17 13:10:39', '2020-01-17 13:10:39'),
(8, '123C', NULL, NULL, NULL, NULL, NULL, '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'QWERTY', '', '198', '201', '', NULL, 6, NULL, 118, 4, NULL, 'PRUEBA', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 3, 1, 3, 1, NULL, 2, 3, 170.00, NULL, NULL, NULL, '', '2020-01-17 13:15:06', '2020-01-17 13:15:06'),
(9, 'D2020011758SULGZ0005320', '588739623', NULL, NULL, '3', '1', '2019-12-05 00:00:00', '2020-01-03 00:00:00', 'MAERSK SALINA', '949S', '481', '198', 'MANZANILLO', 127, 117, NULL, 118, 64, 1, 'NEW TIRES', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 0, NULL, 1, NULL, 170.00, NULL, NULL, NULL, '', '2020-01-17 14:08:01', '2020-01-17 15:28:58'),
(10, 'SULGZ0005320', '588739623', NULL, NULL, '3', '1', '2019-12-05 00:00:00', '2020-01-03 00:00:00', 'MAERSK SALINA', '949S', '481', '198', 'MANZANILLO', 127, 117, NULL, 118, 64, 1, 'NEW TIRES', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, NULL, 1, NULL, 170.00, NULL, NULL, NULL, '', '2020-01-17 15:29:03', '2020-01-17 15:29:03'),
(11, 'SULGZ00049274', '584049274', NULL, NULL, '3', '1', '2019-09-24 00:00:00', '2019-10-12 00:00:00', 'MAERSK SAVANNAH', 'V.973S', '381', '197', '', 128, 129, NULL, 118, NULL, NULL, 'ALUMINUK SLUG', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, NULL, 1, NULL, 170.00, NULL, NULL, NULL, '', '2020-01-17 15:54:03', '2020-01-17 15:54:03'),
(12, 'ABCD123', NULL, NULL, NULL, '4', NULL, '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'JBBJ', 'GGHGHG', '8', '10', '', NULL, 4, NULL, 104, NULL, NULL, 'PRUEBA', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, 45, 1, NULL, 0.00, NULL, NULL, NULL, '', '2020-01-20 16:56:20', '2020-01-20 16:56:20'),
(13, 'PRUEBA111', NULL, NULL, NULL, '4', NULL, '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'QWQW', 'QWEQWE', '43', '48', '', NULL, 37, NULL, 118, NULL, NULL, 'ASASDS', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, 45, 1, NULL, 170.00, NULL, NULL, NULL, '', '2020-01-20 17:08:36', '2020-01-20 17:08:36'),
(14, 'D2020012139COM123', NULL, NULL, NULL, '3', NULL, '2020-01-01 06:00:00', '2020-01-02 06:00:00', '1234AAA', '1234AAA', '198', '480', '', NULL, 4, NULL, 105, NULL, NULL, 'ASDADASDSASDAS', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 0, 52, 1, NULL, 150.00, NULL, NULL, NULL, '', '2020-01-21 17:19:38', '2020-01-21 17:56:39'),
(15, 'COMP1', NULL, NULL, NULL, '5', NULL, '2020-01-01 06:00:00', '2020-01-02 06:00:00', '0001', '0001', '8', '8', '', NULL, 5, NULL, 105, NULL, 4, 'ZCZXC', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, 54, 1, NULL, 170.00, NULL, NULL, NULL, '', '2020-01-21 17:51:58', '2020-01-21 17:51:58'),
(16, 'SULGZ0005464', '598763850', NULL, NULL, '3', '1', '2019-12-24 06:00:00', '2020-01-17 06:00:00', 'MAERSK STOCKHOLM', '951S', '382', '198', '', NULL, 130, NULL, 118, NULL, NULL, 'CHAIRS', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, 46, 1, NULL, 170.00, 'JUY-013', NULL, NULL, '', '2020-01-21 18:43:56', '2020-01-21 18:43:56'),
(17, 'WAR12', '', NULL, NULL, '3', NULL, '2020-01-01 06:00:00', '2020-01-02 06:00:00', 'QWE', 'QWE', '20', '8', '', NULL, 3, NULL, 111, NULL, NULL, 'PRUEBA', NULL, NULL, NULL, 21, 21, NULL, NULL, NULL, 1, 2, 1, 1, 0, 1, NULL, 170.00, 'JUY-011', '1', '1', '1r4', '2020-01-21 22:17:52', '2020-01-21 22:44:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_operaciones_contenedor`
--

CREATE TABLE `adm_operaciones_contenedor` (
  `id` int(11) NOT NULL,
  `operacion_id` int(11) NOT NULL,
  `container_number` varchar(40) DEFAULT NULL,
  `container_seal` varchar(15) DEFAULT NULL,
  `mark` varchar(50) DEFAULT NULL,
  `container_type` tinyint(11) DEFAULT NULL,
  `weight` decimal(10,2) DEFAULT NULL,
  `chargable_weight` decimal(10,2) DEFAULT NULL,
  `volume` decimal(10,2) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `empty_container` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_operaciones_contenedor`
--

INSERT INTO `adm_operaciones_contenedor` (`id`, `operacion_id`, `container_number`, `container_seal`, `mark`, `container_type`, `weight`, `chargable_weight`, `volume`, `activo`, `empty_container`, `created_at`, `updated_at`) VALUES
(1, 1, 'EITU1446080', 'EMCEDH9129', '', 4, '6390.00', '0.00', '68.92', 1, NULL, '2020-01-17 10:23:27', '2020-01-17 10:23:27'),
(2, 1, 'EITU1691066', 'EMCEDH7979', '', 4, '5681.50', '0.00', '67.06', 1, NULL, '2020-01-17 10:23:27', '2020-01-17 10:23:27'),
(3, 1, 'EISU9142412', 'EMCEDH7959', '', 4, '6073.00', '0.00', '68.64', 1, NULL, '2020-01-17 10:23:27', '2020-01-17 10:23:27'),
(4, 1, 'DFSU6773188', 'EMCEDH7989', '', 4, '6116.50', '0.00', '68.59', 1, NULL, '2020-01-17 10:23:27', '2020-01-17 10:23:27'),
(5, 1, 'EITU1287748', 'EMCEDH9139', '', 4, '6710.00', '0.00', '66.21', 1, NULL, '2020-01-17 10:23:27', '2020-01-17 10:23:27'),
(6, 1, 'TGBU6609601', 'EMCEDH7969', '', 4, '6000.00', '0.00', '66.51', 1, NULL, '2020-01-17 10:23:27', '2020-01-17 10:23:27'),
(7, 2, 'MSKU7670904', 'ML-CN4134236', '', 1, '9978.00', '0.00', '17.90', 2, '2020-01-02 00:00:00', '2020-01-17 10:32:43', '2020-01-17 10:35:24'),
(8, 3, 'CBHU4113413', 'U667662', '', 3, '25100.00', '0.00', '22.00', 2, '2019-12-28 00:00:00', '2020-01-17 11:09:56', '2020-01-17 11:12:45'),
(9, 3, 'CBHU5845574', 'U667621', '', 3, '25100.00', '0.00', '22.00', 2, '2019-12-28 00:00:00', '2020-01-17 11:09:56', '2020-01-17 11:13:36'),
(10, 3, 'CCLU3941619', 'U667668', '', 3, '25100.00', '0.00', '22.00', 2, '2019-12-28 00:00:00', '2020-01-17 11:09:56', '2020-01-17 11:13:24'),
(11, 3, 'CSNU1365282', 'U667666', '', 3, '25100.00', '0.00', '22.00', 2, '2019-12-28 00:00:00', '2020-01-17 11:09:56', '2020-01-17 11:13:52'),
(12, 4, 'TRHU2590029', '13268733', 'ROLLS', 1, '10400.00', '0.00', '28.00', 2, '2019-12-04 00:00:00', '2020-01-17 11:54:35', '2020-01-17 12:04:27'),
(13, 5, '123456', '2222', '', 4, '123.12', '0.00', '12345.12', 0, NULL, '2020-01-17 12:58:05', '2020-01-17 13:50:39'),
(14, 5, '123YYY', '2222', '', 7, '122.22', '0.00', '3333.22', 0, NULL, '2020-01-17 12:58:05', '2020-01-17 13:50:39'),
(15, 5, 'D20200117062222', '333', '', 5, '1.00', '0.00', '1.00', 0, NULL, '2020-01-17 13:00:55', '2020-01-17 13:50:39'),
(16, 6, '123YYY', '2222', '', 5, '123.22', '0.00', '33.22', 2, '2020-01-15 00:00:00', '2020-01-17 13:06:43', '2020-01-17 13:18:47'),
(17, 7, '', '', '', 0, '1.00', '2.00', '3.00', 2, '2020-01-17 00:00:00', '2020-01-17 13:10:54', '2020-01-17 13:18:11'),
(18, 8, '123YYY', '', '', 0, '12345.00', '0.00', '12.30', 1, NULL, '2020-01-17 13:16:47', '2020-01-17 13:16:47'),
(19, 10, 'MSKU0374449', 'CN9135831', '', 4, '11489.28', '0.00', '68.00', 1, NULL, '2020-01-17 15:30:12', '2020-01-17 15:30:12'),
(20, 10, 'MRKU5747780', 'CN9135827', '', 4, '13329.00', '0.00', '68.00', 1, NULL, '2020-01-17 15:32:30', '2020-01-17 15:32:30'),
(21, 10, '25', '47', '7', 4, '1.00', '0.00', '3.00', 1, NULL, '2020-01-17 15:33:35', '2020-01-17 15:33:35'),
(22, 11, 'MRKU6928680', 'ML-CN9496337', '', 0, '18791.00', '0.00', '22.14', 2, '2019-10-30 00:00:00', '2020-01-17 15:54:59', '2020-01-17 15:58:04'),
(23, 12, 'QWER3', 'QWEQWE', 'QWEQWE', 5, '1112.22', '0.00', '111.22', 1, NULL, '2020-01-20 16:56:42', '2020-01-20 16:56:42'),
(24, 12, 'QWER3', 'QWEQWE', 'QWEQWE', 5, '1112.22', '0.00', '111.22', 1, NULL, '2020-01-20 16:56:42', '2020-01-20 16:56:42'),
(25, 13, '1', '1', '', 4, '1.00', '0.00', '1.00', 1, NULL, '2020-01-20 17:08:47', '2020-01-20 17:08:47'),
(26, 13, 'QWER1234', 'QWEQWEQW', 'PRUEBA', 4, '2.00', '0.00', '2.00', 1, NULL, '2020-01-20 17:10:35', '2020-01-20 17:10:35'),
(27, 14, 'QWE123', 'QWE123', '', 4, '123.00', '0.00', '123.00', 0, NULL, '2020-01-21 17:19:57', '2020-01-21 17:56:40'),
(28, 15, '1', '1', '1', 5, '1.00', '0.00', '1.00', 1, NULL, '2020-01-21 17:52:11', '2020-01-21 17:52:11'),
(29, 17, '1', '1', '1', 5, '1.00', '0.00', '1.00', 1, NULL, '2020-01-21 22:18:14', '2020-01-21 22:18:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_operaciones_usuarios`
--

CREATE TABLE `adm_operaciones_usuarios` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_operacion` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `creator` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_operaciones_usuarios`
--

INSERT INTO `adm_operaciones_usuarios` (`id`, `id_user`, `id_operacion`, `activo`, `type`, `creator`, `created_at`, `updated_at`) VALUES
(1, 51, 1, 1, 1, 0, '2020-01-17 10:18:54', '2020-01-17 10:18:54'),
(2, 51, 2, 1, 1, 0, '2020-01-17 10:31:59', '2020-01-17 10:31:59'),
(3, 48, 3, 1, 1, 0, '2020-01-17 11:06:42', '2020-01-17 11:06:42'),
(4, 53, 4, 1, 1, 0, '2020-01-17 11:53:16', '2020-01-17 11:53:16'),
(5, 54, 5, 1, 1, 0, '2020-01-17 12:57:05', '2020-01-17 12:57:05'),
(6, 52, 5, 1, 0, 0, '2020-01-17 12:57:05', '2020-01-17 12:57:05'),
(7, 54, 6, 1, 1, 0, '2020-01-17 13:06:09', '2020-01-17 13:06:09'),
(8, 54, 7, 1, 1, 0, '2020-01-17 13:10:39', '2020-01-17 13:10:39'),
(9, 54, 8, 1, 1, 0, '2020-01-17 13:15:06', '2020-01-17 13:15:06'),
(10, 52, 9, 1, 1, 0, '2020-01-17 14:08:01', '2020-01-17 14:08:01'),
(11, 52, 10, 1, 1, 0, '2020-01-17 15:29:03', '2020-01-17 15:29:03'),
(12, 49, 11, 1, 1, 0, '2020-01-17 15:54:03', '2020-01-17 15:54:03'),
(13, 54, 12, 1, 1, 0, '2020-01-20 16:56:20', '2020-01-20 16:56:20'),
(14, 45, 12, 1, 0, 0, '2020-01-20 16:56:20', '2020-01-20 16:56:20'),
(15, 54, 13, 1, 1, 0, '2020-01-20 17:08:36', '2020-01-20 17:08:36'),
(16, 45, 13, 1, 0, 0, '2020-01-20 17:08:36', '2020-01-20 17:08:36'),
(17, 54, 14, 1, 1, 0, '2020-01-21 17:19:38', '2020-01-21 17:19:38'),
(18, 52, 14, 1, 0, 0, '2020-01-21 17:19:38', '2020-01-21 17:19:38'),
(19, 46, 15, 1, 1, 0, '2020-01-21 17:51:58', '2020-01-21 17:51:58'),
(20, 54, 15, 1, 0, 0, '2020-01-21 17:51:58', '2020-01-21 17:51:58'),
(21, 54, 16, 1, 1, 0, '2020-01-21 18:43:56', '2020-01-21 18:43:56'),
(22, 46, 16, 1, 0, 0, '2020-01-21 18:43:56', '2020-01-21 18:43:56'),
(23, 46, 17, 1, 1, 0, '2020-01-21 22:17:52', '2020-01-21 22:17:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_paises`
--

CREATE TABLE `adm_paises` (
  `id` int(11) NOT NULL,
  `iso` varchar(5) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_paises`
--

INSERT INTO `adm_paises` (`id`, `iso`, `nombre`) VALUES
(1, 'AF', 'Afganistán'),
(2, 'AX', 'Islas Gland'),
(3, 'AL', 'Albania'),
(4, 'DE', 'Alemania'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antártida'),
(9, 'AG', 'Antigua y Barbuda'),
(10, 'AN', 'Antillas Holandesas'),
(11, 'SA', 'Arabia Saudí'),
(12, 'DZ', 'Argelia'),
(13, 'AR', 'Argentina'),
(14, 'AM', 'Armenia'),
(15, 'AW', 'Aruba'),
(16, 'AU', 'Australia'),
(17, 'AT', 'Austria'),
(18, 'AZ', 'Azerbaiyán'),
(19, 'BS', 'Bahamas'),
(20, 'BH', 'Bahréin'),
(21, 'BD', 'Bangladesh'),
(22, 'BB', 'Barbados'),
(23, 'BY', 'Bielorrusia'),
(24, 'BE', 'Bélgica'),
(25, 'BZ', 'Belice'),
(26, 'BJ', 'Benin'),
(27, 'BM', 'Bermudas'),
(28, 'BT', 'Bhután'),
(29, 'BO', 'Bolivia'),
(30, 'BA', 'Bosnia y Herzegovina'),
(31, 'BW', 'Botsuana'),
(32, 'BV', 'Isla Bouvet'),
(33, 'BR', 'Brasil'),
(34, 'BN', 'Brunéi'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'CV', 'Cabo Verde'),
(39, 'KY', 'Islas Caimán'),
(40, 'KH', 'Camboya'),
(41, 'CM', 'Camerún'),
(42, 'CA', 'Canadá'),
(43, 'CF', 'República Centroafricana'),
(44, 'TD', 'Chad'),
(45, 'CZ', 'República Checa'),
(46, 'CL', 'Chile'),
(47, 'CN', 'China'),
(48, 'CY', 'Chipre'),
(49, 'CX', 'Isla de Navidad'),
(50, 'VA', 'Ciudad del Vaticano'),
(51, 'CC', 'Islas Cocos'),
(52, 'CO', 'Colombia'),
(53, 'KM', 'Comoras'),
(54, 'CD', 'República Democrática del Congo'),
(55, 'CG', 'Congo'),
(56, 'CK', 'Islas Cook'),
(57, 'KP', 'Corea del Norte'),
(58, 'KR', 'Corea del Sur'),
(59, 'CI', 'Costa de Marfil'),
(60, 'CR', 'Costa Rica'),
(61, 'HR', 'Croacia'),
(62, 'CU', 'Cuba'),
(63, 'DK', 'Dinamarca'),
(64, 'DM', 'Dominica'),
(65, 'DO', 'República Dominicana'),
(66, 'EC', 'Ecuador'),
(67, 'EG', 'Egipto'),
(68, 'SV', 'El Salvador'),
(69, 'AE', 'Emiratos Árabes Unidos'),
(70, 'ER', 'Eritrea'),
(71, 'SK', 'Eslovaquia'),
(72, 'SI', 'Eslovenia'),
(73, 'ES', 'España'),
(74, 'UM', 'Islas ultramarinas de Estados Unidos'),
(75, 'US', 'Estados Unidos'),
(76, 'EE', 'Estonia'),
(77, 'ET', 'Etiopía'),
(78, 'FO', 'Islas Feroe'),
(79, 'PH', 'Filipinas'),
(80, 'FI', 'Finlandia'),
(81, 'FJ', 'Fiyi'),
(82, 'FR', 'Francia'),
(83, 'GA', 'Gabón'),
(84, 'GM', 'Gambia'),
(85, 'GE', 'Georgia'),
(86, 'GS', 'Islas Georgias del Sur y Sandwich del Sur'),
(87, 'GH', 'Ghana'),
(88, 'GI', 'Gibraltar'),
(89, 'GD', 'Granada'),
(90, 'GR', 'Grecia'),
(91, 'GL', 'Groenlandia'),
(92, 'GP', 'Guadalupe'),
(93, 'GU', 'Guam'),
(94, 'GT', 'Guatemala'),
(95, 'GF', 'Guayana Francesa'),
(96, 'GN', 'Guinea'),
(97, 'GQ', 'Guinea Ecuatorial'),
(98, 'GW', 'Guinea-Bissau'),
(99, 'GY', 'Guyana'),
(100, 'HT', 'Haití'),
(101, 'HM', 'Islas Heard y McDonald'),
(102, 'HN', 'Honduras'),
(103, 'HK', 'Hong Kong'),
(104, 'HU', 'Hungría'),
(105, 'IN', 'India'),
(106, 'ID', 'Indonesia'),
(107, 'IR', 'Irán'),
(108, 'IQ', 'Iraq'),
(109, 'IE', 'Irlanda'),
(110, 'IS', 'Islandia'),
(111, 'IL', 'Israel'),
(112, 'IT', 'Italia'),
(113, 'JM', 'Jamaica'),
(114, 'JP', 'Japón'),
(115, 'JO', 'Jordania'),
(116, 'KZ', 'Kazajstán'),
(117, 'KE', 'Kenia'),
(118, 'KG', 'Kirguistán'),
(119, 'KI', 'Kiribati'),
(120, 'KW', 'Kuwait'),
(121, 'LA', 'Laos'),
(122, 'LS', 'Lesotho'),
(123, 'LV', 'Letonia'),
(124, 'LB', 'Líbano'),
(125, 'LR', 'Liberia'),
(126, 'LY', 'Libia'),
(127, 'LI', 'Liechtenstein'),
(128, 'LT', 'Lituania'),
(129, 'LU', 'Luxemburgo'),
(130, 'MO', 'Macao'),
(131, 'MK', 'ARY Macedonia'),
(132, 'MG', 'Madagascar'),
(133, 'MY', 'Malasia'),
(134, 'MW', 'Malawi'),
(135, 'MV', 'Maldivas'),
(136, 'ML', 'Malí'),
(137, 'MT', 'Malta'),
(138, 'FK', 'Islas Malvinas'),
(139, 'MP', 'Islas Marianas del Norte'),
(140, 'MA', 'Marruecos'),
(141, 'MH', 'Islas Marshall'),
(142, 'MQ', 'Martinica'),
(143, 'MU', 'Mauricio'),
(144, 'MR', 'Mauritania'),
(145, 'YT', 'Mayotte'),
(146, 'MX', 'México'),
(147, 'FM', 'Micronesia'),
(148, 'MD', 'Moldavia'),
(149, 'MC', 'Mónaco'),
(150, 'MN', 'Mongolia'),
(151, 'MS', 'Montserrat'),
(152, 'MZ', 'Mozambique'),
(153, 'MM', 'Myanmar'),
(154, 'NA', 'Namibia'),
(155, 'NR', 'Nauru'),
(156, 'NP', 'Nepal'),
(157, 'NI', 'Nicaragua'),
(158, 'NE', 'Níger'),
(159, 'NG', 'Nigeria'),
(160, 'NU', 'Niue'),
(161, 'NF', 'Isla Norfolk'),
(162, 'NO', 'Noruega'),
(163, 'NC', 'Nueva Caledonia'),
(164, 'NZ', 'Nueva Zelanda'),
(165, 'OM', 'Omán'),
(166, 'NL', 'Países Bajos'),
(167, 'PK', 'Pakistán'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestina'),
(170, 'PA', 'Panamá'),
(171, 'PG', 'Papúa Nueva Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Perú'),
(174, 'PN', 'Islas Pitcairn'),
(175, 'PF', 'Polinesia Francesa'),
(176, 'PL', 'Polonia'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'GB', 'Reino Unido'),
(181, 'RE', 'Reunión'),
(182, 'RW', 'Ruanda'),
(183, 'RO', 'Rumania'),
(184, 'RU', 'Rusia'),
(185, 'EH', 'Sahara Occidental'),
(186, 'SB', 'Islas Salomón'),
(187, 'WS', 'Samoa'),
(188, 'AS', 'Samoa Americana'),
(189, 'KN', 'San Cristóbal y Nevis'),
(190, 'SM', 'San Marino'),
(191, 'PM', 'San Pedro y Miquelón'),
(192, 'VC', 'San Vicente y las Granadinas'),
(193, 'SH', 'Santa Helena'),
(194, 'LC', 'Santa Lucía'),
(195, 'ST', 'Santo Tomé y Príncipe'),
(196, 'SN', 'Senegal'),
(197, 'CS', 'Serbia y Montenegro'),
(198, 'SC', 'Seychelles'),
(199, 'SL', 'Sierra Leona'),
(200, 'SG', 'Singapur'),
(201, 'SY', 'Siria'),
(202, 'SO', 'Somalia'),
(203, 'LK', 'Sri Lanka'),
(204, 'SZ', 'Suazilandia'),
(205, 'ZA', 'Sudáfrica'),
(206, 'SD', 'Sudán'),
(207, 'SE', 'Suecia'),
(208, 'CH', 'Suiza'),
(209, 'SR', 'Surinam'),
(210, 'SJ', 'Svalbard y Jan Mayen'),
(211, 'TH', 'Tailandia'),
(212, 'TW', 'Taiwán'),
(213, 'TZ', 'Tanzania'),
(214, 'TJ', 'Tayikistán'),
(215, 'IO', 'Territorio Británico del Océano Índico'),
(216, 'TF', 'Territorios Australes Franceses'),
(217, 'TL', 'Timor Oriental'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad y Tobago'),
(222, 'TN', 'Túnez'),
(223, 'TC', 'Islas Turcas y Caicos'),
(224, 'TM', 'Turkmenistán'),
(225, 'TR', 'Turquía'),
(226, 'TV', 'Tuvalu'),
(227, 'UA', 'Ucrania'),
(228, 'UG', 'Uganda'),
(229, 'UY', 'Uruguay'),
(230, 'UZ', 'Uzbekistán'),
(231, 'VU', 'Vanuatu'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Islas Vírgenes Británicas'),
(235, 'VI', 'Islas Vírgenes de los Estados Unidos'),
(236, 'WF', 'Wallis y Futuna'),
(237, 'YE', 'Yemen'),
(238, 'DJ', 'Yibuti'),
(239, 'ZM', 'Zambia'),
(240, 'ZW', 'Zimbabue'),
(242, NULL, 'GRAN BRETAÑA'),
(243, NULL, 'Holanda'),
(244, NULL, 'Zaire');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_puertos`
--

CREATE TABLE `adm_puertos` (
  `id` int(11) NOT NULL,
  `puerto` varchar(50) NOT NULL,
  `pais` int(50) NOT NULL,
  `clave_puerto` varchar(4) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_puertos`
--

INSERT INTO `adm_puertos` (`id`, `puerto`, `pais`, `clave_puerto`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'ANTIGUA', 9, 'UKST', 1, '2019-11-19 18:20:01', NULL),
(2, 'ARUBA', 10, 'NLOA', 1, '2019-11-19 18:20:01', NULL),
(3, 'CURACAO', 10, 'NLWM', 1, '2019-11-19 18:20:01', NULL),
(4, 'WILLEMSTAD', 10, 'NLWS', 1, '2019-11-19 18:20:01', NULL),
(5, 'ALMIRANTE BROWN', 13, 'ARAB', 1, '2019-11-19 18:20:01', NULL),
(6, 'BAHIA BLANCA', 13, 'ARBB', 1, '2019-11-19 18:20:01', NULL),
(7, 'BAHIA SAN BLAS', 13, 'ARSB', 1, '2019-11-19 18:20:01', NULL),
(8, 'BAHIA SAN SEBASTIAN', 13, 'ARBS', 1, '2019-11-19 18:20:01', NULL),
(9, 'BARRANQUERAS', 13, 'ARBR', 1, '2019-11-19 18:20:01', NULL),
(10, 'BUENOS AIRES', 13, 'ARBA', 1, '2019-11-19 18:20:02', NULL),
(11, 'CALETA CORDOBA', 13, 'ARCD', 1, '2019-11-19 18:20:02', NULL),
(12, 'CALETA OLIVIA', 13, 'AROL', 1, '2019-11-19 18:20:02', NULL),
(13, 'CAMPANA', 13, 'ARCM', 1, '2019-11-19 18:20:02', NULL),
(14, 'CARMEN DE PATAGONES', 13, 'ARCD', 1, '2019-11-19 18:20:02', NULL),
(15, 'COLON', 13, 'ARCO', 1, '2019-11-19 18:20:02', NULL),
(16, 'COMODORO RIVADAVIA', 13, 'ARCD', 1, '2019-11-19 18:20:02', NULL),
(17, 'CONCEPTION DEL URUGUAY', 13, 'ARCD', 1, '2019-11-19 18:20:02', NULL),
(18, 'CONCORDIA', 13, 'ARCK', 1, '2019-11-19 18:20:02', NULL),
(19, 'CORRIENTES', 13, 'ARCO', 1, '2019-11-19 18:20:02', NULL),
(20, 'DECEPCION', 13, 'ARPF', 1, '2019-11-19 18:20:02', NULL),
(21, 'DIAMANTE', 13, 'ARDI', 1, '2019-11-19 18:20:02', NULL),
(22, 'EMPEDRADO', 13, 'ARED', 1, '2019-11-19 18:20:02', NULL),
(23, 'ESQUINA', 13, 'ARES', 1, '2019-11-19 18:20:02', NULL),
(24, 'FORMOSA', 13, 'ARPF', 1, '2019-11-19 18:20:02', NULL),
(25, 'GENERAL ALVEAR', 13, 'ARAV', 1, '2019-11-19 18:20:02', NULL),
(26, 'GENERAL BELGRANO', 13, 'ARBE', 1, '2019-11-19 18:20:02', NULL),
(27, 'GENERAL SAN MARTIN', 13, 'ARGS', 1, '2019-11-19 18:20:02', NULL),
(28, 'HERNANDARIAS', 13, 'ARHE', 1, '2019-11-19 18:20:02', NULL),
(29, 'IBICUY', 13, 'ARIB', 1, '2019-11-19 18:20:02', NULL),
(30, 'LA PLATA', 13, 'ARLP', 1, '2019-11-19 18:20:03', NULL),
(31, 'MAR DEL PLATA', 13, 'ARMP', 1, '2019-11-19 18:20:03', NULL),
(32, 'NECOCHEA', 13, 'ARNE', 1, '2019-11-19 18:20:03', NULL),
(33, 'PARANA', 13, 'ARPN', 1, '2019-11-19 18:20:03', NULL),
(34, 'PUERTO BELGRANO', 13, 'ARPB', 1, '2019-11-19 18:20:03', NULL),
(35, 'PUERTO BERMEJO', 13, 'ARPB', 1, '2019-11-19 18:20:03', NULL),
(36, 'PUERTO DESEADO', 13, 'ARPQ', 1, '2019-11-19 18:20:03', NULL),
(37, 'PUERTO GOYA', 13, 'ARXP', 1, '2019-11-19 18:20:03', NULL),
(38, 'PUERTO IBICUY', 13, 'ARPI', 1, '2019-11-19 18:20:03', NULL),
(39, 'PUERTO INGENIERO WHITE', 13, 'ARPI', 1, '2019-11-19 18:20:03', NULL),
(40, 'PUERTO MADRYN', 13, 'ARPJ', 1, '2019-11-19 18:20:03', NULL),
(41, 'PUERTO PILCOMAYO', 13, 'ARPP', 1, '2019-11-19 18:20:03', NULL),
(42, 'PUERTO PIRAMIDE', 13, 'ARPR', 1, '2019-11-19 18:20:03', NULL),
(43, 'PUERTO QUEQUEN', 13, 'ARPQ', 1, '2019-11-19 18:20:03', NULL),
(44, 'PUERTO SAN CARLOS', 13, 'ARXZ', 1, '2019-11-19 18:20:03', NULL),
(45, 'RADA LA PLATA', 13, 'ARRL', 1, '2019-11-19 18:20:04', NULL),
(46, 'RAMALLO', 13, 'ARXP', 1, '2019-11-19 18:20:04', NULL),
(47, 'RECALADA', 13, 'ARRC', 1, '2019-11-19 18:20:04', NULL),
(48, 'RECONQUISTA', 13, 'ARPR', 1, '2019-11-19 18:20:04', NULL),
(49, 'RIO GALLEGOS', 13, 'ARRG', 1, '2019-11-19 18:20:04', NULL),
(50, 'RIO GRANDE', 13, 'ARRG', 1, '2019-11-19 18:20:04', NULL),
(51, 'ROSARIO', 13, 'ARRO', 1, '2019-11-19 18:20:04', NULL),
(52, 'SAN ANTONIO-ALIJE', 13, 'ARAL', 1, '2019-11-19 18:20:04', NULL),
(53, 'SAN ANTONIO', 13, 'ARSL', 1, '2019-11-19 18:20:04', NULL),
(54, 'SAN JULIAN', 13, 'ARSJ', 1, '2019-11-19 18:20:04', NULL),
(55, 'SAN LORENZO', 13, 'ARSJ', 1, '2019-11-19 18:20:04', NULL),
(56, 'SAN MARTIN', 13, 'ARSM', 1, '2019-11-19 18:20:04', NULL),
(57, 'SAN MATIAS-ALIJE', 13, 'ARAS', 1, '2019-11-19 18:20:04', NULL),
(58, 'SAN PEDRO', 13, 'ARXS', 1, '2019-11-19 18:20:04', NULL),
(59, 'SANTA CRUZ', 13, 'ARSX', 1, '2019-11-19 18:20:04', NULL),
(60, 'SANTA FE', 13, 'ARSF', 1, '2019-11-19 18:20:05', NULL),
(61, 'USHUAIA', 13, 'ARUS', 1, '2019-11-19 18:20:05', NULL),
(62, 'VILLA CONSTITUCION', 13, 'ARVC', 1, '2019-11-19 18:20:05', NULL),
(63, 'ZARATE', 13, 'ARZA', 1, '2019-11-19 18:20:05', NULL),
(64, 'BARBADOS', 22, 'BBBW', 1, '2019-11-19 18:20:05', NULL),
(65, 'BELIZE', 25, 'UKBE', 1, '2019-11-19 18:20:05', NULL),
(66, 'ARACAJU', 33, 'BRAR', 1, '2019-11-19 18:20:05', NULL),
(67, 'BAHIA', 33, 'BRBA', 1, '2019-11-19 18:20:05', NULL),
(68, 'BELEM', 33, 'BRPA', 1, '2019-11-19 18:20:05', NULL),
(69, 'BELLO HORIZONTE', 33, 'BRHO', 1, '2019-11-19 18:20:05', NULL),
(70, 'CABEDELLO', 33, 'BRCB', 1, '2019-11-19 18:20:05', NULL),
(71, 'CEARA', 33, 'BRPD', 1, '2019-11-19 18:20:06', NULL),
(72, 'FLORIANOPOLIS', 33, 'BRFL', 1, '2019-11-19 18:20:06', NULL),
(73, 'ILHEUS', 33, 'BRIL', 1, '2019-11-19 18:20:06', NULL),
(74, 'IMBITUBA', 33, 'BRIM', 1, '2019-11-19 18:20:06', NULL),
(75, 'ITAJAI', 33, 'BRIT', 1, '2019-11-19 18:20:06', NULL),
(76, 'MACEIO', 33, 'BRMC', 1, '2019-11-19 18:20:06', NULL),
(77, 'MANAOS', 33, 'BRMX', 1, '2019-11-19 18:20:06', NULL),
(78, 'NATAL', 33, 'BRNA', 1, '2019-11-19 18:20:06', NULL),
(79, 'PARANAGUA', 33, 'BRPG', 1, '2019-11-19 18:20:06', NULL),
(80, 'PORTO ALEGRE', 33, 'BRPJ', 1, '2019-11-19 18:20:06', NULL),
(81, 'RIO DE JANEIRO', 33, 'BRRI', 1, '2019-11-19 18:20:06', NULL),
(82, 'RIO GRANDE', 33, 'BRRG', 1, '2019-11-19 18:20:07', NULL),
(83, 'S.CAETANO DO SUL', 33, 'BRSC', 1, '2019-11-19 18:20:07', NULL),
(84, 'SALVADOR', 33, 'BRSV', 1, '2019-11-19 18:20:07', NULL),
(85, 'SAN PABLO', 33, 'BRSP', 1, '2019-11-19 18:20:07', NULL),
(86, 'SANTA CATARINA', 33, 'BRST', 1, '2019-11-19 18:20:07', NULL),
(87, 'SANTOS', 33, 'BRST', 1, '2019-11-19 18:20:07', NULL),
(88, 'SAO FRANCISCO', 33, 'BRSF', 1, '2019-11-19 18:20:07', NULL),
(89, 'SUAPE', 33, 'BRSU', 1, '2019-11-19 18:20:07', NULL),
(90, 'TUBARAO', 33, 'BRTB', 1, '2019-11-19 18:20:07', NULL),
(91, 'VICTORIA', 33, 'BRVT', 1, '2019-11-19 18:20:07', NULL),
(92, 'ALBERTON', 42, 'CACP', 1, '2019-11-19 18:20:07', NULL),
(93, 'HALIFAX', 42, 'CAHA', 1, '2019-11-19 18:20:07', NULL),
(94, 'JACKSON BAY', 42, 'CAJK', 1, '2019-11-19 18:20:07', NULL),
(95, 'LABRADOR', 42, 'CAWM', 1, '2019-11-19 18:20:07', NULL),
(96, 'MONTREAL', 42, 'CAMO', 1, '2019-11-19 18:20:07', NULL),
(97, 'NEW BRUNSWICK', 42, 'CASZ', 1, '2019-11-19 18:20:07', NULL),
(98, 'NOVA SCOTIA', 42, 'CAPC', 1, '2019-11-19 18:20:08', NULL),
(99, 'ONTARIO', 42, 'CAON', 1, '2019-11-19 18:20:08', NULL),
(100, 'OTTAWA', 42, 'CAOT', 1, '2019-11-19 18:20:08', NULL),
(101, 'QUEBEC', 42, 'CAQB', 1, '2019-11-19 18:20:08', NULL),
(102, 'QUEBEC HARBOUR', 42, 'CAQH', 1, '2019-11-19 18:20:08', NULL),
(103, 'TORONTO', 42, 'CATO', 1, '2019-11-19 18:20:08', NULL),
(104, 'TRENTON', 42, 'CATT', 1, '2019-11-19 18:20:08', NULL),
(105, 'VICTORIA', 42, 'CAVI', 1, '2019-11-19 18:20:08', NULL),
(106, 'WINNIPEG', 42, 'CAWN', 1, '2019-11-19 18:20:08', NULL),
(107, 'YARMOUTH', 42, 'CAYR', 1, '2019-11-19 18:20:08', NULL),
(108, 'BARRANQUILLA', 52, 'COBQ', 1, '2019-11-19 18:20:08', NULL),
(109, 'BOGOTA', 52, 'COBG', 1, '2019-11-19 18:20:08', NULL),
(110, 'BUENAVENTURA', 52, 'COBF', 1, '2019-11-19 18:20:08', NULL),
(111, 'CALI', 52, 'COCA', 1, '2019-11-19 18:20:08', NULL),
(112, 'CARTAGENA', 52, 'COCG', 1, '2019-11-19 18:20:08', NULL),
(113, 'SANTA MARTA', 52, 'COSX', 1, '2019-11-19 18:20:08', NULL),
(114, 'CALDERA', 60, 'CSCD', 1, '2019-11-19 18:20:08', NULL),
(115, 'LIMON', 60, 'CSLI', 1, '2019-11-19 18:20:08', NULL),
(116, 'PORT LIMON', 60, 'CSPF', 1, '2019-11-19 18:20:08', NULL),
(117, 'SAN JOSE', 60, 'CSSJ', 1, '2019-11-19 18:20:09', NULL),
(118, 'BOQUERON', 62, 'CUCM', 1, '2019-11-19 18:20:09', NULL),
(119, 'CIENFUEGOS', 62, 'CUCN', 1, '2019-11-19 18:20:09', NULL),
(120, 'MATANZAS', 62, 'CUMZ', 1, '2019-11-19 18:20:09', NULL),
(121, 'PUERTO MANATI', 62, 'CUXP', 1, '2019-11-19 18:20:09', NULL),
(122, 'SANTA LUCIA', 62, 'CUSF', 1, '2019-11-19 18:20:09', NULL),
(123, 'SANTIAGO DE CUBA', 62, 'CUSI', 1, '2019-11-19 18:20:09', NULL),
(124, 'ANTOFAGASTA', 46, 'CIAF', 1, '2019-11-19 18:20:09', NULL),
(125, 'ARICA', 46, 'CIAR', 1, '2019-11-19 18:20:09', NULL),
(126, 'CONCEPCION', 46, 'CITL', 1, '2019-11-19 18:20:09', NULL),
(127, 'COQUIMBO', 46, 'CICQ', 1, '2019-11-19 18:20:09', NULL),
(128, 'IQUIQUE', 46, 'CIIQ', 1, '2019-11-19 18:20:09', NULL),
(129, 'LIRQUEN', 46, 'CILQ', 1, '2019-11-19 18:20:09', NULL),
(130, 'OSORNO', 46, 'CIOS', 1, '2019-11-19 18:20:09', NULL),
(131, 'PUCON', 46, 'CIPU', 1, '2019-11-19 18:20:09', NULL),
(132, 'PUERTO MONTT', 46, 'CIPX', 1, '2019-11-19 18:20:09', NULL),
(133, 'PUNTA ARENAS', 46, 'CIPA', 1, '2019-11-19 18:20:09', NULL),
(134, 'RIO BUENO', 46, 'CIRB', 1, '2019-11-19 18:20:09', NULL),
(135, 'SAN ANTONIO', 46, 'CISO', 1, '2019-11-19 18:20:09', NULL),
(136, 'SAN?VICENTE', 46, 'CISV', 1, '2019-11-19 18:20:10', NULL),
(137, 'SANTIAGO', 46, 'CISG', 1, '2019-11-19 18:20:10', NULL),
(138, 'TALCAHUANO', 46, 'CITA', 1, '2019-11-19 18:20:10', NULL),
(139, 'TEMUCO', 46, 'CITE', 1, '2019-11-19 18:20:10', NULL),
(140, 'VALDIVIA', 46, 'CIVV', 1, '2019-11-19 18:20:10', NULL),
(141, 'VALPARAISO', 46, 'CIVL', 1, '2019-11-19 18:20:10', NULL),
(142, 'DOMINICA', 64, 'FRRS', 1, '2019-11-19 18:20:10', NULL),
(143, 'CALLO', 66, 'ECPQ', 1, '2019-11-19 18:20:10', NULL),
(144, 'ESMERALDAS', 66, 'ECED', 1, '2019-11-19 18:20:10', NULL),
(145, 'GUAYAQUIL', 66, 'ECGU', 1, '2019-11-19 18:20:10', NULL),
(146, 'MANTA', 66, 'ECMT', 1, '2019-11-19 18:20:10', NULL),
(147, 'PUERTO BOLIVAR', 66, 'ECPB', 1, '2019-11-19 18:20:10', NULL),
(148, 'QUITO', 66, 'ECQU', 1, '2019-11-19 18:20:10', NULL),
(149, 'ACAJUTLA', 68, 'ESAC', 1, '2019-11-19 18:20:11', NULL),
(150, 'ALABAMA', 75, 'USAL', 1, '2019-11-19 18:20:11', NULL),
(151, 'ANCHORAGE', 75, 'USAN', 1, '2019-11-19 18:20:11', NULL),
(152, 'BALTIMORE', 75, 'USBL', 1, '2019-11-19 18:20:11', NULL),
(153, 'BOSTON', 75, 'USBO', 1, '2019-11-19 18:20:11', NULL),
(154, 'BROOKLYN', 75, 'USBB', 1, '2019-11-19 18:20:11', NULL),
(155, 'CLEVELAND', 75, 'USCV', 1, '2019-11-19 18:20:11', NULL),
(156, 'CHARLESTON', 75, 'USCH', 1, '2019-11-19 18:20:11', NULL),
(157, 'CHARLOTTE', 75, 'USRT', 1, '2019-11-19 18:20:11', NULL),
(158, 'CHICAGO', 75, 'USCI', 1, '2019-11-19 18:20:11', NULL),
(159, 'DAVENPORT', 75, 'USDA', 1, '2019-11-19 18:20:11', NULL),
(160, 'ELIZABETHPORT', 75, 'USEZ', 1, '2019-11-19 18:20:11', NULL),
(161, 'FORT LAUDERDALE', 75, 'USFT', 1, '2019-11-19 18:20:12', NULL),
(162, 'FREEPORT', 75, 'USFR', 1, '2019-11-19 18:20:12', NULL),
(163, 'GALVESTON', 75, 'USGV', 1, '2019-11-19 18:20:12', NULL),
(164, 'LOS ANGELES', 75, 'USLO', 1, '2019-11-19 18:20:12', NULL),
(165, 'MIAMI', 75, 'USMI', 1, '2019-11-19 18:20:12', NULL),
(166, 'MISSISSIPPI', 75, 'USMI', 1, '2019-11-19 18:20:12', NULL),
(167, 'NEW JERSEY', 75, 'USNJ', 1, '2019-11-19 18:20:12', NULL),
(168, 'NEW ORLEANS', 75, 'USNW', 1, '2019-11-19 18:20:12', NULL),
(169, 'NEW YORK', 75, 'USNY', 1, '2019-11-19 18:20:12', NULL),
(170, 'NEWARK', 75, 'USPW', 1, '2019-11-19 18:20:12', NULL),
(171, 'NEWPORT', 75, 'USNE', 1, '2019-11-19 18:20:12', NULL),
(172, 'NEWPORT', 75, 'USNU', 1, '2019-11-19 18:20:12', NULL),
(173, 'NORFOLK', 75, 'USNF', 1, '2019-11-19 18:20:12', NULL),
(174, 'OAKLAND', 75, 'USOA', 1, '2019-11-19 18:20:12', NULL),
(175, 'PHILADELPHIA', 75, 'USPH', 1, '2019-11-19 18:20:13', NULL),
(176, 'PORT EVERGLADES', 75, 'USPV', 1, '2019-11-19 18:20:13', NULL),
(177, 'SAN DIEGO', 75, 'USSD', 1, '2019-11-19 18:20:13', NULL),
(178, 'SAN FRANCISCO', 75, 'USSF', 1, '2019-11-19 18:20:13', NULL),
(179, 'SEATTLE', 75, 'USSJ', 1, '2019-11-19 18:20:13', NULL),
(180, 'TAMPA', 75, 'USTM', 1, '2019-11-19 18:20:13', NULL),
(181, 'LIVINGSTON', 94, 'GTLV', 1, '2019-11-19 18:20:13', NULL),
(182, 'PUERTO QUETZAL', 94, 'GTQT', 1, '2019-11-19 18:20:13', NULL),
(183, 'SAN JOSE', 94, 'GTSJ', 1, '2019-11-19 18:20:13', NULL),
(184, 'GEORGE TOWN', 99, 'UKGG', 1, '2019-11-19 18:20:13', NULL),
(185, 'LA CEIBA', 102, 'HOLC', 1, '2019-11-19 18:20:13', NULL),
(186, 'PUERTO CASTILLA', 102, 'HOPQ', 1, '2019-11-19 18:20:13', NULL),
(187, 'PUERTO CORTES', 102, 'HOPY', 1, '2019-11-19 18:20:13', NULL),
(188, 'SAN LORENZO', 102, 'HOSJ', 1, '2019-11-19 18:20:13', NULL),
(189, 'TEGUCIGALPA', 102, 'HOTE', 1, '2019-11-19 18:20:14', NULL),
(190, 'FORT DE FRANCE', 142, 'FRFT', 1, '2019-11-19 18:20:14', NULL),
(191, 'ACAPULCO', 146, 'MXAC', 1, '2019-11-19 18:20:14', NULL),
(192, 'ALTAMIRA', 146, 'MXAE', 1, '2019-11-19 18:20:14', NULL),
(193, 'BENITO JUAREZ', 146, 'MXBJ', 1, '2019-11-19 18:20:14', NULL),
(194, 'CAMPECHE', 146, 'MXCC', 1, '2019-11-19 18:20:14', NULL),
(195, 'COZUMEL', 146, 'MXCZ', 1, '2019-11-19 18:20:14', NULL),
(196, 'GUADALAJARA', 146, 'MXGU', 1, '2019-11-19 18:20:14', NULL),
(197, 'LAZARO CARDENAS', 146, 'MXLC', 1, '2019-11-19 18:20:14', NULL),
(198, 'MANZANILLO', 146, 'MXMZ', 1, '2019-11-19 18:20:14', NULL),
(199, 'MEJICO', 146, 'MXME', 1, '2019-11-19 18:20:14', NULL),
(200, 'TAMPICO', 146, 'MXTP', 1, '2019-11-19 18:20:14', NULL),
(201, 'VERACRUZ', 146, 'MXVR', 1, '2019-11-19 18:20:14', NULL),
(202, 'CORINTO', 157, 'NUCT', 1, '2019-11-19 18:20:15', NULL),
(203, 'PUERTO CABEZAS', 157, 'NUPB', 1, '2019-11-19 18:20:15', NULL),
(204, 'SAN JUAN DEL SUR', 157, 'NUSJ', 1, '2019-11-19 18:20:15', NULL),
(205, 'BALBOA', 170, 'PNBL', 1, '2019-11-19 18:20:15', NULL),
(206, 'COLON', 170, 'PNCX', 1, '2019-11-19 18:20:15', NULL),
(207, 'CRISTOBAL', 170, 'PNCR', 1, '2019-11-19 18:20:15', NULL),
(208, 'PANAMA CITY', 170, 'PNPF', 1, '2019-11-19 18:20:15', NULL),
(209, 'ASUNCION', 172, 'PAAS', 1, '2019-11-19 18:20:15', NULL),
(210, 'LUQUE', 172, 'PALU', 1, '2019-11-19 18:20:15', NULL),
(211, 'SAN LORENZO', 172, 'PASL', 1, '2019-11-19 18:20:15', NULL),
(212, 'CALLAO', 173, 'PECA', 1, '2019-11-19 18:20:15', NULL),
(213, 'ILO', 173, 'PEIL', 1, '2019-11-19 18:20:16', NULL),
(214, 'IQUITOS', 173, 'PEIQ', 1, '2019-11-19 18:20:16', NULL),
(215, 'LIMA', 173, 'PELI', 1, '2019-11-19 18:20:16', NULL),
(216, 'PAITA', 173, 'PEPA', 1, '2019-11-19 18:20:16', NULL),
(217, 'PUERTO PAITA', 173, 'PEXP', 1, '2019-11-19 18:20:16', NULL),
(218, 'TACNA', 173, 'PETA', 1, '2019-11-19 18:20:16', NULL),
(219, 'SAN ? JUAN', 178, 'USSJ', 1, '2019-11-19 18:20:16', NULL),
(220, 'PUERTO PLATA', 65, 'DRXP', 1, '2019-11-19 18:20:16', NULL),
(221, 'PARAMARIBO', 209, 'NSPR', 1, '2019-11-19 18:20:16', NULL),
(222, 'POINT FORTIN', 221, 'TDPT', 1, '2019-11-19 18:20:16', NULL),
(223, 'POINT LISAS', 221, 'TDPN', 1, '2019-11-19 18:20:16', NULL),
(224, 'POINTE A PIERRE', 221, 'TDPP', 1, '2019-11-19 18:20:16', NULL),
(225, 'PORT OF SPAIN', 221, 'TDPO', 1, '2019-11-19 18:20:16', NULL),
(226, 'CANELONES', 229, 'UYCA', 1, '2019-11-19 18:20:16', NULL),
(227, 'CARRASCO', 229, 'UYCA', 1, '2019-11-19 18:20:16', NULL),
(228, 'CARRO LARGO', 229, 'UYCA', 1, '2019-11-19 18:20:16', NULL),
(229, 'COLONIA', 229, 'UYCO', 1, '2019-11-19 18:20:16', NULL),
(230, 'FRAY BENTOS', 229, 'UYFB', 1, '2019-11-19 18:20:16', NULL),
(231, 'MALDONADO', 229, 'UYMD', 1, '2019-11-19 18:20:17', NULL),
(232, 'MONTEVIDEO', 229, 'UYMV', 1, '2019-11-19 18:20:17', NULL),
(233, 'NUEVA PALMIRA', 229, 'UYNV', 1, '2019-11-19 18:20:17', NULL),
(234, 'SALTO', 229, 'UYSL', 1, '2019-11-19 18:20:17', NULL),
(235, 'CARACAS', 232, 'VECC', 1, '2019-11-19 18:20:17', NULL),
(236, 'GUANTA', 232, 'VEGU', 1, '2019-11-19 18:20:17', NULL),
(237, 'LA GUAIRA', 232, 'VELG', 1, '2019-11-19 18:20:17', NULL),
(238, 'MARACAIBO', 232, 'VEMA', 1, '2019-11-19 18:20:17', NULL),
(239, 'PUERTO CABELLO', 232, 'VEPY', 1, '2019-11-19 18:20:17', NULL),
(240, 'PUERTO ORDAZ', 232, 'VEPD', 1, '2019-11-19 18:20:17', NULL),
(241, 'SAN LORENZO', 232, 'VESJ', 1, '2019-11-19 18:20:17', NULL),
(242, 'SIMON BOLIVAR', 232, 'VESB', 1, '2019-11-19 18:20:17', NULL),
(243, 'BREMEN', 4, 'GEBM', 1, '2019-11-19 18:20:17', NULL),
(244, 'BREMERHAVEN', 4, 'GEBH', 1, '2019-11-19 18:20:17', NULL),
(245, 'DUSSELDORF', 4, 'GEDD', 1, '2019-11-19 18:20:17', NULL),
(246, 'FRANKFURT', 4, 'GEFF', 1, '2019-11-19 18:20:18', NULL),
(247, 'HAMBURGO', 4, 'GEHA', 1, '2019-11-19 18:20:18', NULL),
(248, 'ANTWERP', 24, 'BEAN', 1, '2019-11-19 18:20:18', NULL),
(249, 'BRUSSELS', 24, 'BEBX', 1, '2019-11-19 18:20:18', NULL),
(250, 'GENT', 24, 'BEGE', 1, '2019-11-19 18:20:18', NULL),
(251, 'GAND', 24, 'BEGN', 1, '2019-11-19 18:20:18', NULL),
(252, 'LIEJA', 24, 'BELI', 1, '2019-11-19 18:20:18', NULL),
(253, 'ZEEBRUGGE', 24, 'BEZE', 1, '2019-11-19 18:20:18', NULL),
(254, 'SOFIA', 35, 'BUSO', 1, '2019-11-19 18:20:18', NULL),
(255, 'VARNA', 35, 'BUVA', 1, '2019-11-19 18:20:18', NULL),
(256, 'COPENHAGEN', 63, 'DACP', 1, '2019-11-19 18:20:19', NULL),
(257, 'THULE', 63, 'DATH', 1, '2019-11-19 18:20:19', NULL),
(258, 'ALGECIRAS', 73, 'SPAG', 1, '2019-11-19 18:20:19', NULL),
(259, 'BARCELONA', 73, 'SPBR', 1, '2019-11-19 18:20:19', NULL),
(260, 'BILBAO', 73, 'SPBB', 1, '2019-11-19 18:20:19', NULL),
(261, 'CADIZ', 73, 'SPCA', 1, '2019-11-19 18:20:19', NULL),
(262, 'CARTAGENA', 73, 'SPCA', 1, '2019-11-19 18:20:19', NULL),
(263, 'EL FERROL', 73, 'SPEL', 1, '2019-11-19 18:20:19', NULL),
(264, 'FERROL', 73, 'SPFE', 1, '2019-11-19 18:20:19', NULL),
(265, 'GIJON', 73, 'SPGI', 1, '2019-11-19 18:20:19', NULL),
(266, 'IBIZA', 73, 'SPIB', 1, '2019-11-19 18:20:19', NULL),
(267, 'LA CORU?A', 73, 'SPLC', 1, '2019-11-19 18:20:19', NULL),
(268, 'LAS PALMAS', 73, 'SPPE', 1, '2019-11-19 18:20:20', NULL),
(269, 'MADRID', 73, 'SPMA', 1, '2019-11-19 18:20:20', NULL),
(270, 'MALAGA', 73, 'SPMG', 1, '2019-11-19 18:20:20', NULL),
(271, 'SAN SEBASTIAN', 73, 'SPSS', 1, '2019-11-19 18:20:20', NULL),
(272, 'TENERIFE', 73, 'SPTU', 1, '2019-11-19 18:20:20', NULL),
(273, 'VALENCIA', 73, 'SPVL', 1, '2019-11-19 18:20:20', NULL),
(274, 'VIGO', 73, 'SPVG', 1, '2019-11-19 18:20:20', NULL),
(275, 'HELSINGFORS', 80, 'FIHE', 1, '2019-11-19 18:20:21', NULL),
(276, 'KOTKA', 80, 'FIKO', 1, '2019-11-19 18:20:21', NULL),
(277, 'TURKU', 80, 'FITU', 1, '2019-11-19 18:20:21', NULL),
(278, 'BAYONNE', 82, 'FRBY', 1, '2019-11-19 18:20:21', NULL),
(279, 'BIARRITZ', 82, 'FRBR', 1, '2019-11-19 18:20:21', NULL),
(280, 'BOULOGNE-SUR-MER', 82, 'FRBS', 1, '2019-11-19 18:20:21', NULL),
(281, 'BREST', 82, 'FRBE', 1, '2019-11-19 18:20:21', NULL),
(282, 'CAEN', 82, 'FRCN', 1, '2019-11-19 18:20:21', NULL),
(283, 'CALAIS', 82, 'FRCA', 1, '2019-11-19 18:20:21', NULL),
(284, 'CANNES', 82, 'FRCN', 1, '2019-11-19 18:20:22', NULL),
(285, 'CHERBOURG', 82, 'FRCE', 1, '2019-11-19 18:20:22', NULL),
(286, 'FOS SUR MER', 82, 'FRFS', 1, '2019-11-19 18:20:22', NULL),
(287, 'HAVRE', 82, 'FRLE', 1, '2019-11-19 18:20:22', NULL),
(288, 'LILLE', 82, 'FRLI', 1, '2019-11-19 18:20:22', NULL),
(289, 'LYON', 82, 'FRLY', 1, '2019-11-19 18:20:22', NULL),
(290, 'ORLY', 82, 'FROR', 1, '2019-11-19 18:20:22', NULL),
(291, 'ORSAY', 82, 'FROR', 1, '2019-11-19 18:20:22', NULL),
(292, 'PARIS', 82, 'FRPA', 1, '2019-11-19 18:20:22', NULL),
(293, 'TOULOUSE', 82, 'FRTO', 1, '2019-11-19 18:20:22', NULL),
(294, 'BELFAST', 242, 'UKBF', 1, '2019-11-19 18:20:22', NULL),
(295, 'BIRMINGHAN', 242, 'UKBI', 1, '2019-11-19 18:20:22', NULL),
(296, 'BOSTON', 242, 'UKBO', 1, '2019-11-19 18:20:23', NULL),
(297, 'BRIGHTON', 242, 'UKBI', 1, '2019-11-19 18:20:23', NULL),
(298, 'BRISTOL', 242, 'UKBR', 1, '2019-11-19 18:20:23', NULL),
(299, 'CARDIFF', 242, 'UKCD', 1, '2019-11-19 18:20:23', NULL),
(300, 'FELIXSTOWE', 242, 'UKFX', 1, '2019-11-19 18:20:23', NULL),
(301, 'GATWICK', 242, 'UKGA', 1, '2019-11-19 18:20:23', NULL),
(302, 'GLOUCESTER', 242, 'UKGC', 1, '2019-11-19 18:20:23', NULL),
(303, 'LEEDS', 242, 'UKLE', 1, '2019-11-19 18:20:23', NULL),
(304, 'LIVERPOOL', 242, 'UKLV', 1, '2019-11-19 18:20:23', NULL),
(305, 'LONDRES', 242, 'UKLN', 1, '2019-11-19 18:20:23', NULL),
(306, 'MANCHESTER', 242, 'UKMC', 1, '2019-11-19 18:20:24', NULL),
(307, 'PORT GLASGOW', 242, 'UKPG', 1, '2019-11-19 18:20:24', NULL),
(308, 'PORTLAND', 242, 'UKPK', 1, '2019-11-19 18:20:24', NULL),
(309, 'PORTSMOUTH', 242, 'UKPT', 1, '2019-11-19 18:20:24', NULL),
(310, 'SOUTHAMPTON', 242, 'UKSO', 1, '2019-11-19 18:20:24', NULL),
(311, 'TILBURY', 242, 'UKTD', 1, '2019-11-19 18:20:24', NULL),
(312, 'AGRIA', 90, 'GRAR', 1, '2019-11-19 18:20:24', NULL),
(313, 'ATHENS', 90, 'GRPI', 1, '2019-11-19 18:20:24', NULL),
(314, 'SKOPELOS', 90, 'GRSQ', 1, '2019-11-19 18:20:24', NULL),
(315, 'VOLOS', 90, 'GRVO', 1, '2019-11-19 18:20:24', NULL),
(316, 'AMSTERDAM', 243, 'NLAM', 1, '2019-11-19 18:20:24', NULL),
(317, 'ROTTERDAM', 243, 'NLRO', 1, '2019-11-19 18:20:24', NULL),
(318, 'BARI', 112, 'ITBP', 1, '2019-11-19 18:20:24', NULL),
(319, 'BOLONIA', 112, 'ITBO', 1, '2019-11-19 18:20:24', NULL),
(320, 'BRESCIA', 112, 'ITBR', 1, '2019-11-19 18:20:24', NULL),
(321, 'BRINDISI', 112, 'ITBI', 1, '2019-11-19 18:20:25', NULL),
(322, 'CAGLIARI', 112, 'ITCG', 1, '2019-11-19 18:20:25', NULL),
(323, 'CASTELLAMMARE', 112, 'ITCT', 1, '2019-11-19 18:20:25', NULL),
(324, 'CATANIA', 112, 'ITCA', 1, '2019-11-19 18:20:25', NULL),
(325, 'GENOVA', 112, 'ITGE', 1, '2019-11-19 18:20:25', NULL),
(326, 'LA SPEZIA', 112, 'ITLS', 1, '2019-11-19 18:20:25', NULL),
(327, 'LIVORNO', 112, 'ITLE', 1, '2019-11-19 18:20:25', NULL),
(328, 'MESSINA', 112, 'ITME', 1, '2019-11-19 18:20:25', NULL),
(329, 'MILAN', 112, 'ITMI', 1, '2019-11-19 18:20:26', NULL),
(330, 'NAPOLES', 112, 'ITNA', 1, '2019-11-19 18:20:26', NULL),
(331, 'PALERMO', 112, 'ITPA', 1, '2019-11-19 18:20:26', NULL),
(332, 'ROMA', 112, 'ITRO', 1, '2019-11-19 18:20:26', NULL),
(333, 'SORRENTO', 112, 'ITSO', 1, '2019-11-19 18:20:26', NULL),
(334, 'TARANTO', 112, 'ITTR', 1, '2019-11-19 18:20:26', NULL),
(335, 'VADO LIGURE', 112, 'ITVD', 1, '2019-11-19 18:20:26', NULL),
(336, 'LA VALETTA', 137, 'MTVL', 1, '2019-11-19 18:20:26', NULL),
(337, 'NARVIK', 162, 'NONA', 1, '2019-11-19 18:20:26', NULL),
(338, 'OSLO', 162, 'NOSL', 1, '2019-11-19 18:20:26', NULL),
(339, 'GDANSK', 176, 'PLGD', 1, '2019-11-19 18:20:26', NULL),
(340, 'GDYNIA', 176, 'PLGD', 1, '2019-11-19 18:20:26', NULL),
(341, 'SOPOT', 176, 'PLZO', 1, '2019-11-19 18:20:27', NULL),
(342, 'VARSOVIA', 176, 'PLVA', 1, '2019-11-19 18:20:27', NULL),
(343, 'ANGRA (HARBOUR)', 177, 'POAR', 1, '2019-11-19 18:20:27', NULL),
(344, 'AZORES', 177, 'POHO', 1, '2019-11-19 18:20:27', NULL),
(345, 'LISBOA', 177, 'POLI', 1, '2019-11-19 18:20:27', NULL),
(346, 'OPORTO', 177, 'POPJ', 1, '2019-11-19 18:20:27', NULL),
(347, 'SETUBAL', 177, 'POSB', 1, '2019-11-19 18:20:27', NULL),
(348, 'KALININGRADO', 184, 'URKG', 1, '2019-11-19 18:20:27', NULL),
(349, 'KRONSHTADT', 184, 'URKS', 1, '2019-11-19 18:20:27', NULL),
(350, 'MOSCU', 184, 'URMO', 1, '2019-11-19 18:20:27', NULL),
(351, 'ODESSA', 227, 'UROD', 1, '2019-11-19 18:20:27', NULL),
(352, 'SEVASTOPOL', 227, 'URSX', 1, '2019-11-19 18:20:27', NULL),
(353, 'MALMO', 207, 'SWMM', 1, '2019-11-19 18:20:27', NULL),
(354, 'SANDVIK', 207, 'SWSW', 1, '2019-11-19 18:20:27', NULL),
(355, 'STOCKHOLM', 207, 'SWST', 1, '2019-11-19 18:20:27', NULL),
(356, 'ESTAMBUL', 225, 'TUIS', 1, '2019-11-19 18:20:27', NULL),
(357, 'IZMIR', 225, 'TUIZ', 1, '2019-11-19 18:20:27', NULL),
(358, 'MERSIN', 225, 'TUME', 1, '2019-11-19 18:20:27', NULL),
(359, 'ADELAIDE', 16, 'ASPD', 1, '2019-11-19 18:20:27', NULL),
(360, 'ALBANY', 16, 'ASAL', 1, '2019-11-19 18:20:28', NULL),
(361, 'BRISBANE', 16, 'ASBB', 1, '2019-11-19 18:20:28', NULL),
(362, 'FREMANTLE', 16, 'ASFR', 1, '2019-11-19 18:20:28', NULL),
(363, 'JERVIS BAY', 16, 'ASJR', 1, '2019-11-19 18:20:28', NULL),
(364, 'MELBOURNE', 16, 'IIOX', 1, '2019-11-19 18:20:28', NULL),
(365, 'PERTH', 16, 'UKPT', 1, '2019-11-19 18:20:28', NULL),
(366, 'SYDNEY', 16, 'ASSD', 1, '2019-11-19 18:20:28', NULL),
(367, 'SHINHO', 57, 'KNSM', 1, '2019-11-19 18:20:28', NULL),
(368, 'PUSAN', 58, 'KSPU', 1, '2019-11-19 18:20:28', NULL),
(369, 'CHAN CHIANG', 47, 'CHCN', 1, '2019-11-19 18:20:28', NULL),
(370, 'CHANGCHUN', 47, 'CHAN', 1, '2019-11-19 18:20:28', NULL),
(371, 'CHIU CHIANG', 47, 'CHCX', 1, '2019-11-19 18:20:28', NULL),
(372, 'CHIWAN', 47, 'CHWA', 1, '2019-11-19 18:20:28', NULL),
(373, 'CHUNG CHING', 47, 'CHCU', 1, '2019-11-19 18:20:28', NULL),
(374, 'DALIAN', 47, 'CHDL', 1, '2019-11-19 18:20:28', NULL),
(375, 'FUZHOU', 47, 'CHFU', 1, '2019-11-19 18:20:28', NULL),
(376, 'GUANGZHOU', 47, 'CHGU', 1, '2019-11-19 18:20:28', NULL),
(377, 'HONG KONG', 47, 'CHHO', 1, '2019-11-19 18:20:29', NULL),
(378, 'LUNG KOU', 47, 'CHLG', 1, '2019-11-19 18:20:29', NULL),
(379, 'NINGBO', 47, 'CHNG', 1, '2019-11-19 18:20:29', NULL),
(380, 'PEKING', 47, 'CHPK', 1, '2019-11-19 18:20:29', NULL),
(381, 'QINGDAO', 47, 'CHQI', 1, '2019-11-19 18:20:29', NULL),
(382, 'SHANGHAI', 47, 'CHSH', 1, '2019-11-19 18:20:29', NULL),
(383, 'TIANJIN', 47, 'CHTI', 1, '2019-11-19 18:20:29', NULL),
(384, 'WENZHOU', 47, 'CHWZ', 1, '2019-11-19 18:20:29', NULL),
(385, 'XIAMEN', 47, 'CHXM', 1, '2019-11-19 18:20:29', NULL),
(386, 'YANTAI', 47, 'CHYA', 1, '2019-11-19 18:20:29', NULL),
(387, 'MANILA', 79, 'RPMN', 1, '2019-11-19 18:20:29', NULL),
(388, 'BOMBAY', 105, 'INBQ', 1, '2019-11-19 18:20:29', NULL),
(389, 'CALCUTTA', 105, 'INCA', 1, '2019-11-19 18:20:29', NULL),
(390, 'INCHON', 105, 'INCH', 1, '2019-11-19 18:20:29', NULL),
(391, 'MADRAS', 105, 'INMD', 1, '2019-11-19 18:20:29', NULL),
(392, 'MANGALORE', 105, 'INMQ', 1, '2019-11-19 18:20:29', NULL),
(393, 'NEW DELHI', 105, 'INND', 1, '2019-11-19 18:20:29', NULL),
(394, 'SURAT', 105, 'INSR', 1, '2019-11-19 18:20:29', NULL),
(395, 'JAKARTA', 106, 'IDJA', 1, '2019-11-19 18:20:29', NULL),
(396, 'SOERABAJA', 106, 'IDSU', 1, '2019-11-19 18:20:30', NULL),
(397, 'HIROSHIMA', 114, 'JAHR', 1, '2019-11-19 18:20:30', NULL),
(398, 'KAGOSHIMA', 114, 'JAKG', 1, '2019-11-19 18:20:30', NULL),
(399, 'KOBE', 114, 'JAKB', 1, '2019-11-19 18:20:30', NULL),
(400, 'MOJI', 114, 'JAMO', 1, '2019-11-19 18:20:30', NULL),
(401, 'NAGASAKI', 114, 'JANA', 1, '2019-11-19 18:20:30', NULL),
(402, 'NAGOYA', 114, 'JANG', 1, '2019-11-19 18:20:30', NULL),
(403, 'NAHA', 114, 'JANA', 1, '2019-11-19 18:20:30', NULL),
(404, 'OKINAWA', 114, 'JAOK', 1, '2019-11-19 18:20:30', NULL),
(405, 'OSAKA', 114, 'JAOS', 1, '2019-11-19 18:20:30', NULL),
(406, 'TOKYO', 114, 'JATO', 1, '2019-11-19 18:20:30', NULL),
(407, 'YOKOHAMA', 114, 'JAYO', 1, '2019-11-19 18:20:30', NULL),
(408, 'AL FUHAYHIL', 120, 'KUMH', 1, '2019-11-19 18:20:30', NULL),
(409, 'AL KUWAYT', 120, 'KUAL', 1, '2019-11-19 18:20:30', NULL),
(410, 'BRUNEI TOWN', 133, 'MYBN', 1, '2019-11-19 18:20:31', NULL),
(411, 'GEORGE TOWN', 133, 'MYGT', 1, '2019-11-19 18:20:31', NULL),
(412, 'PANGKOR', 133, 'MYPK', 1, '2019-11-19 18:20:31', NULL),
(413, 'AUCKLAND', 164, 'NZAU', 1, '2019-11-19 18:20:31', NULL),
(414, 'CHRISTCHURCH', 164, 'NZPF', 1, '2019-11-19 18:20:31', NULL),
(415, 'WELLINGTON', 164, 'NZWE', 1, '2019-11-19 18:20:31', NULL),
(416, 'SINGAPUR', 200, 'IIOO', 1, '2019-11-19 18:20:31', NULL),
(417, 'AO PHUKET', 211, 'THAO', 1, '2019-11-19 18:20:31', NULL),
(418, 'BANGKOK', 211, 'THBN', 1, '2019-11-19 18:20:31', NULL),
(419, 'BASS HARBOUR', 211, 'THBS', 1, '2019-11-19 18:20:31', NULL),
(420, 'KAOSHSIUNG', 212, 'TWKS', 1, '2019-11-19 18:20:31', NULL),
(421, 'KEELUNG=CHILUNG', 212, 'TWCI', 1, '2019-11-19 18:20:31', NULL),
(422, 'TAI CHUNG', 212, 'TWCH', 1, '2019-11-19 18:20:31', NULL),
(423, 'TAIWAN', 212, 'TWTA', 1, '2019-11-19 18:20:31', NULL),
(424, 'HAIPHONG', 233, 'VNHA', 1, '2019-11-19 18:20:31', NULL),
(425, 'HANOI', 233, 'VNHA', 1, '2019-11-19 18:20:32', NULL),
(426, 'HO CHI MINH (SAIGON)', 233, 'VNHC', 1, '2019-11-19 18:20:32', NULL),
(427, 'FREETOWN', 199, 'SLFT', 1, '2019-11-19 18:20:32', NULL),
(428, 'PEPEL', 199, 'SLPE', 1, '2019-11-19 18:20:32', NULL),
(429, 'LOME', 218, 'TOLO', 1, '2019-11-19 18:20:32', NULL),
(430, 'CASABLANCA', 140, 'MOCS', 1, '2019-11-19 18:20:32', NULL),
(431, 'MELILLA', 140, 'MOMF', 1, '2019-11-19 18:20:32', NULL),
(432, 'RABAT', 140, 'MORA', 1, '2019-11-19 18:20:32', NULL),
(433, 'TANGIER', 140, 'MOTA', 1, '2019-11-19 18:20:32', NULL),
(434, 'ORAN', 12, 'AGOR', 1, '2019-11-19 18:20:32', NULL),
(435, 'TUNIS', 222, 'TSTU', 1, '2019-11-19 18:20:32', NULL),
(436, 'DAKAR', 196, 'SGDA', 1, '2019-11-19 18:20:32', NULL),
(437, 'ST LOUIS', 196, 'SGST', 1, '2019-11-19 18:20:33', NULL),
(438, 'CONAKRY', 96, 'GVCO', 1, '2019-11-19 18:20:33', NULL),
(439, 'DEBREEKA', 96, 'GVDE', 1, '2019-11-19 18:20:33', NULL),
(440, 'DREGER HARBOUR', 96, 'GVDG', 1, '2019-11-19 18:20:33', NULL),
(441, 'SORONG', 96, 'GVSO', 1, '2019-11-19 18:20:33', NULL),
(442, 'BUCHANAN', 125, 'LIBC', 1, '2019-11-19 18:20:33', NULL),
(443, 'MONROVIA', 125, 'LIMV', 1, '2019-11-19 18:20:33', NULL),
(444, 'ROBERT PORT', 125, 'LIRB', 1, '2019-11-19 18:20:33', NULL),
(445, 'ABIDJAN', 59, 'IVAB', 1, '2019-11-19 18:20:33', NULL),
(446, 'RIJEKA', 59, 'IVOX', 1, '2019-11-19 18:20:33', NULL),
(447, 'ACCRA', 87, 'GHAC', 1, '2019-11-19 18:20:33', NULL),
(448, 'CAPE COAST', 87, 'GHCP', 1, '2019-11-19 18:20:33', NULL),
(449, 'FORCADOS', 159, 'NIFO', 1, '2019-11-19 18:20:33', NULL),
(450, 'APAPA', 159, 'NILA', 1, '2019-11-19 18:20:34', NULL),
(451, 'PORT HARCOURT', 159, 'NIPH', 1, '2019-11-19 18:20:34', NULL),
(452, 'DOUALA', 41, 'CMDU', 1, '2019-11-19 18:20:34', NULL),
(453, 'LIBREVILLE', 83, 'GBLB', 1, '2019-11-19 18:20:34', NULL),
(454, 'MATADI', 244, 'CFMA', 1, '2019-11-19 18:20:34', NULL),
(455, 'LOBITO', 6, 'AOLO', 1, '2019-11-19 18:20:34', NULL),
(456, 'LUANDA', 6, 'AOLU', 1, '2019-11-19 18:20:35', NULL),
(457, 'CAPE TOWN', 205, 'SFCT', 1, '2019-11-19 18:20:35', NULL),
(458, 'DURBAN', 205, 'SFDU', 1, '2019-11-19 18:20:35', NULL),
(459, 'EAST LONDON', 205, 'SFEL', 1, '2019-11-19 18:20:35', NULL),
(460, 'JOHANNESBURG', 205, 'SFJO', 1, '2019-11-19 18:20:35', NULL),
(461, 'PORT ELIZABETH', 205, 'SFPF', 1, '2019-11-19 18:20:35', NULL),
(462, 'WALVIS BAY', 205, 'SFWA', 1, '2019-11-19 18:20:35', NULL),
(463, 'DJIBOUTI', 202, 'SODJ', 1, '2019-11-19 18:20:35', NULL),
(464, 'MOGADISCIO', 202, 'SOMO', 1, '2019-11-19 18:20:35', NULL),
(465, 'PORT SUDAN', 206, 'SDPU', 1, '2019-11-19 18:20:35', NULL),
(466, 'SAWAKIN', 206, 'SDSU', 1, '2019-11-19 18:20:35', NULL),
(467, 'PORT SAID', 67, 'EGPQ', 1, '2019-11-19 18:20:36', NULL),
(468, 'RAS GHARIB', 67, 'EGRG', 1, '2019-11-19 18:20:36', NULL),
(469, 'HAMATA', 67, 'EGRG', 1, '2019-11-19 18:20:36', NULL),
(470, 'RAS GIMSAH', 67, 'EGRS', 1, '2019-11-19 18:20:36', NULL),
(471, 'RASHID', 67, 'EGRS', 1, '2019-11-19 18:20:36', NULL),
(472, 'SUEZ CANAL', 67, 'EGSU', 1, '2019-11-19 18:20:36', NULL),
(473, 'WADI FEIRAN', 67, 'EGWA', 1, '2019-11-19 18:20:36', NULL),
(474, 'BANGHAZI', 126, 'LYBN', 1, '2019-11-19 18:20:36', NULL),
(475, 'SIRTE', 126, 'LYSR', 1, '2019-11-19 18:20:37', NULL),
(476, 'TARABULUS (TRIPOLI)', 126, 'LYTR', 1, '2019-11-19 18:20:37', NULL),
(477, 'TOBRUCH', 126, 'LYTU', 1, '2019-11-19 18:20:37', NULL),
(478, 'SHANTOU', 47, NULL, 1, '2020-01-17 10:10:38', '2020-01-17 10:10:38'),
(479, 'mundra - inmun', 105, NULL, 1, '2020-01-17 12:25:38', '2020-01-17 12:25:38'),
(480, 'MANZANILLO - PERU', 173, '', 1, '2020-01-20 22:21:39', '2020-01-20 16:21:39'),
(481, 'SHEKOU', 47, NULL, 1, '2020-01-17 14:04:44', '2020-01-17 14:04:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_user_details`
--

CREATE TABLE `adm_user_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fechaingreso` timestamp NULL DEFAULT NULL,
  `fechasalida` timestamp NULL DEFAULT NULL,
  `idcargo` int(100) DEFAULT NULL,
  `salario` decimal(10,2) DEFAULT NULL,
  `rfc` varchar(255) DEFAULT NULL,
  `curp` varchar(255) DEFAULT NULL,
  `domicilio` text DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `nss` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adm_user_details`
--

INSERT INTO `adm_user_details` (`id`, `user_id`, `fechaingreso`, `fechasalida`, `idcargo`, `salario`, `rfc`, `curp`, `domicilio`, `telefono`, `nss`, `updated_at`, `created_at`) VALUES
(41, 41, '2019-07-01 05:00:00', NULL, 1, '13000.00', 'mi rfc', 'mi curp', 'calle pablo macedo mz 5 lt 19', '5522048817', 'mi nss', '2019-09-24 18:52:54', '2019-09-24 16:47:37'),
(42, 1, '2019-07-01 05:00:00', NULL, 1, '13000.00', 'mi rfc', 'mi curp', 'calle pablo macedo mz 5 lt 19', '5522048817', 'mi nss', '2019-09-24 18:52:54', '2019-09-24 16:47:37'),
(43, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-01 18:59:31', '2019-10-01 18:59:31'),
(44, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-11 18:04:27', '2019-10-11 18:04:27'),
(45, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-19 15:43:20', '2019-11-19 15:43:20'),
(46, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 15:25:26', '2019-11-27 15:25:26'),
(47, 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-03 14:04:28', '2019-12-03 14:04:28'),
(48, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-03 14:05:38', '2019-12-03 14:05:38'),
(49, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-03 14:06:32', '2019-12-03 14:06:32'),
(50, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-03 14:07:23', '2019-12-03 14:07:23'),
(51, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-03 14:09:12', '2019-12-03 14:09:12'),
(52, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-03 14:14:07', '2019-12-03 14:14:07'),
(53, 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-17 12:38:12', '2020-01-17 12:38:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idcategoria` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeriaimagen`
--

CREATE TABLE `galeriaimagen` (
  `idgaleria` int(11) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `idtienda` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_activities`
--

CREATE TABLE `log_activities` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log_activities`
--

INSERT INTO `log_activities` (`id`, `subject`, `url`, `method`, `ip`, `agent`, `user_id`, `updated_at`, `created_at`) VALUES
(1, 'My Testing Add To Log.', 'http://localhost:8000/recibos', 'GET', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-18 19:30:49', '2018-12-18 19:30:49'),
(2, 'My Testing Add To Log.', 'http://localhost:8000/recibos', 'GET', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-17 19:30:49', '2018-12-17 19:30:49'),
(3, 'My Testing Add To Log.', 'http://localhost:8000/recibos', 'GET', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-17 19:30:49', '2018-12-17 19:30:49'),
(4, 'My Testing Add To Log.', 'http://localhost:8000/recibos', 'GET', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-18 19:30:49', '2018-12-18 19:30:49'),
(5, 'Se agrego el usuario: roldan@hotmail.com', 'http://localhost:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 16:02:22', '2018-12-19 16:02:22'),
(6, 'Se actualizo el usuario : roldan@hotmail.com', 'http://localhost:8000/users/eyJpdiI6IkJ4XC9PYkdhNEpuS3crQkxIWHowR0N3PT0iLCJ2YWx1ZSI6Im8wcFIzb1YxQmdRYnpLU09pVVpQT2c9PSIsIm1hYyI6IjA3MzdmZDY2ODAwNWZkNzA1YjU3ZWRhODc5ZDcxMzc0ZGVjYjkzMjU4NGEzNmQyMGI0OTFlMmQyMjY5ZmJlMzQifQ==', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 16:30:32', '2018-12-19 16:30:32'),
(7, 'Se actualizo el usuario : roldan@hotmail.com', 'http://localhost:8000/users/eyJpdiI6IlYrdGwzNlRTTU1aam5cL0VNTzdsb1Z3PT0iLCJ2YWx1ZSI6IjUremUzWFhJZkFjZWhvUDBZOTBBSGc9PSIsIm1hYyI6IjNkM2JkZWY3Yjc3ZTlkMDcxZjI0YWNjODQ2MDc2Y2FiZDY0YjFiZjVmMzAwMmM2ZjJhOGEyN2FiYjI4ZDM0OWUifQ==', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 16:37:23', '2018-12-19 16:37:23'),
(8, 'Se actualizo el usuario : roldan@hotmail.com', 'http://localhost:8000/users/eyJpdiI6IkJYd2tXdVVhVzhhZGpGK1JNR1l5aEE9PSIsInZhbHVlIjoiUjhSaFZTMlpHUzRKWFIzNTdvdDRDUT09IiwibWFjIjoiYzRlZDRkZmE4NTdhZGE2YmY2NTk5MDg3MGM1ZGNiYzEwNzVhYTk2Mzk4MzJjNjY5NTQ4NDhhYmZkOTM0ZDFhZiJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 16:50:30', '2018-12-19 16:50:30'),
(9, 'Se agrego el perfil: Salomon', 'http://localhost:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 18:15:12', '2018-12-19 18:15:12'),
(10, 'Se agrego el perfil: Pedro', 'http://localhost:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 18:15:19', '2018-12-19 18:15:19'),
(11, 'Se actualizo el perfil: Pedro', 'http://localhost:8000/roles/7', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 18:21:04', '2018-12-19 18:21:04'),
(12, 'Se actualizo el perfil: Pedror', 'http://localhost:8000/roles/7', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 18:21:18', '2018-12-19 18:21:18'),
(13, 'Se actualizo el perfil: Pedrorf', 'http://localhost:8000/roles/7', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 18:21:43', '2018-12-19 18:21:43'),
(14, 'Se actualizo el perfil: Pedrorfddd', 'http://localhost:8000/roles/7', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 18:23:14', '2018-12-19 18:23:14'),
(15, 'Se actualizo el perfil: Pedrorfdd', 'http://localhost:8000/roles/7', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-19 18:23:48', '2018-12-19 18:23:48'),
(16, 'Se actualizo el usuario : urbansfl@hotmail.com', 'http://localhost:8000/users/eyJpdiI6ImlzdFlnM1wvOGJISjZSUkREamZSMk93PT0iLCJ2YWx1ZSI6IjZsZWlcL1wvSzBVaFU0Y0RxbUplS0R2dz09IiwibWFjIjoiOWQ3MWY2NzYxNDRhMjdmYzEzZGU3MWFiOTVlYjU5NjdhYzk5ZWE5MmI2MGM1MzNmZThhYjQyZGVlNmFlNzIxMSJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2018-12-20 14:37:53', '2018-12-20 14:37:53'),
(17, 'Se actualizo el usuario : santiago@hotmail.com', 'http://localhost:8000/users/eyJpdiI6ImFtZElUOTE0MjRjYnlSd1IrOElvdlE9PSIsInZhbHVlIjoiZGFjQlhFb0hjTTdnZ2NLMEh3VUxudz09IiwibWFjIjoiMzA5ZDZiMGFjNzljZGVjMTAwYjU1MzUzZmQwZWU5NzdkZmVkMTRhZjQ4ZDJmNmYxNWRiMzk4YTM3NTk3MzI2MiJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-19 13:14:02', '2019-01-19 13:14:02'),
(18, 'Se agrego el usuario: prueba@prueba.com.mx', 'http://localhost:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-19 14:19:12', '2019-01-19 14:19:12'),
(19, 'Se agrego el usuario: carlos@carlos.com.mc', 'http://localhost:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-19 14:55:38', '2019-01-19 14:55:38'),
(20, 'Se agrego el usuario: charly@charly.com.mx', 'http://localhost:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-19 15:22:18', '2019-01-19 15:22:18'),
(21, 'Se agrego el usuario: salomonf.lopez@gmail.com', 'http://localhost:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-22 12:07:04', '2019-01-22 12:07:04'),
(22, 'Se actualizo el usuario : juan@hotmail.com', 'http://localhost:8000/users/eyJpdiI6IkZRMmFYQ2ZsbVo0cldjYU1PKzQreHc9PSIsInZhbHVlIjoiaE0reUpabkxCbDhsUlpISnAwZldVZz09IiwibWFjIjoiNjczMDNhYWFiNTI4ZjMzOTgyNGE4Yzc3NDA4NWRkYjlmZWFmNTkyNTY1N2ZmOTk3YzUyM2M1MzJjZjIzZTg0ZCJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-22 14:21:18', '2019-01-22 14:21:18'),
(23, 'Se agrego el usuario: salomon.flores@sparklabs.com.mx', 'http://localhost:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 15, '2019-01-22 17:51:24', '2019-01-22 17:51:24'),
(24, 'Se actualizo el usuario : salomon.flores@sparklabs.com.mx', 'http://localhost:8000/users/eyJpdiI6ImZVaUFIeSs4bHUzUHBabW5cLzl1eXJBPT0iLCJ2YWx1ZSI6IjNxZ096ZjYwNDZZcXZCOFFvazFjR3c9PSIsIm1hYyI6IjFhODk2NWVlZTU3ZjE4OTIzZDJhOGY2NjA2MjBmMTNlMTQ5M2UzZDc4NzUyMDA3NjlhYTUyZTJhYTBmZGQ0OWMifQ==', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 15, '2019-01-22 18:30:17', '2019-01-22 18:30:17'),
(25, 'Se actualizo el usuario : salomon.flores@sparklabs.com.mxx', 'http://localhost:8000/users/eyJpdiI6IlpydjY4Z054bTlyVWtBaU5oa2dRZXc9PSIsInZhbHVlIjoiVU5KK0h3S1drK2FkQVB0RDM4K0VvUT09IiwibWFjIjoiZWJmNzMxMDc0YjhiMTY3MWEzZmNjM2RjODAzMDNmMjNjMDM2MmJlYzA2YTlkNjg3NGU4YjMyNTQ2NDk3YWNjYyJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 15, '2019-01-22 18:37:01', '2019-01-22 18:37:01'),
(26, 'Se actualizo el usuario : pedros@hotmail.com', 'http://localhost:8000/users/eyJpdiI6ImpYTTl6WXA1RHVsZGZmV0pMcU9NTHc9PSIsInZhbHVlIjoia3Q4WCt6WWFydUxoZSt0QlhOZ1Vrdz09IiwibWFjIjoiZGUxZDIyMmM0N2QxNzU3NGY4ZTk3NjAyNDg5YjU2ODZkY2EwZjJlYWVkNDM3NTRiOTUxNjg1MjRmN2M2MDExOSJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 15, '2019-01-22 18:37:10', '2019-01-22 18:37:10'),
(27, 'Se actualizo el usuario : salomon.flores@sparklabs.com.mx', 'http://localhost:8000/users/eyJpdiI6InhMbFpOMEZCU2xxVnpQczRWRFJ0c1E9PSIsInZhbHVlIjoiaDBSUERROHNnYVNKXC9JeE0ydGpQZ1E9PSIsIm1hYyI6ImMzMzY5M2EyY2NhMWNjYjA2N2M2NzgxYjk5NjUxNzYxYjhkOTRlYTBkMWFiOWIwMjA0NDdmZDZlNDUwN2FhMmYifQ==', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 15, '2019-01-22 18:38:25', '2019-01-22 18:38:25'),
(28, 'Se actualizo el usuario : salomon.flores@sparklabs.com.mx', 'http://localhost:8000/users/eyJpdiI6ImptNUxJeUdHSTZMTW9PNEtWcDNQS1E9PSIsInZhbHVlIjoiVGlnQ3docm1PVFZ6WFwvckxPTkNtXC9nPT0iLCJtYWMiOiIxMTI1NDJmZmU0MjA1NmZkOTMzNzBmYThhMTY5YTJjNWVlZDA4ZTRlNThlNzVlMmQyNTlkYzExMDEwNjVhOWE0In0=', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-22 18:41:33', '2019-01-22 18:41:33'),
(29, 'Se actualizo el usuario : salomonf.lopez@gmail.com', 'http://localhost:8000/users/eyJpdiI6IjZvMU0reWk2THdRUUxPYXFzYVdnMVE9PSIsInZhbHVlIjoiR2N4cEw3RWUrWDJkWXZyOFlvTEIyUT09IiwibWFjIjoiZTUyYjdhZDJhMDdmMWQ4MWMyZmJhZjM0MGE2NWU5NGRjZDJjYmJiYjAzMDFhYmE0MTdjNmY0ZWUxYzEzMzRkMSJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 11:17:44', '2019-01-23 11:17:44'),
(30, 'Se actualizo el usuario : salomonf.lopez@gmail.com', 'http://localhost:8000/users/eyJpdiI6Im1NK0I2Kzk5dkN0NU13Q3JBajVheVE9PSIsInZhbHVlIjoiSnFTNTUrSHcxQVRPbnVLampKOWlmZz09IiwibWFjIjoiNmQ1M2E4ZjUwNmQxMGJmZWZhOTI0NmNjODcwNWYyNjAxN2QyZTkzMzJkZmE0M2U2OWI3ZjA3ZWQ1NTM2ZmM3YyJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 17:34:22', '2019-01-23 17:34:22'),
(31, 'Se actualizo el usuario : salomonf.lopez@gmail.com', 'http://localhost:8000/users/eyJpdiI6IlNyK2t5VnBCcEhKNm1lTWR0cXhTYXc9PSIsInZhbHVlIjoidkVwMTk2eG96M1NmVG8rUnRsaHQyUT09IiwibWFjIjoiNmY2MDEwM2M3YzAxZTA4ZmQxM2QwN2Q0NzAwNWMyYTU3NWRmZjlhYWY3NTYzZDc0NmM5NWNiZTk3MWM1MDRhZiJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 17:39:46', '2019-01-23 17:39:46'),
(32, 'Se actualizo el usuario : salomonf.lopez@gmail.com', 'http://localhost:8000/users/eyJpdiI6ImdzeXZsa0N5SXM3NFBIR2psNXN3eWc9PSIsInZhbHVlIjoiRjhOcloydis0WWJPdzdYS2VlM0dWZz09IiwibWFjIjoiZmIyNmZiZWViN2FiZjkwZTdiZTZjNWRjMmQ4NzRjMmRiNGE5OTY1MWU1Zjg3M2YzMzE1ZmU0ZDYyM2Y1MTkxNSJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 17:40:37', '2019-01-23 17:40:37'),
(33, 'Se actualizo el usuario : salomonf.lopez@gmail.com', 'http://localhost:8000/users/eyJpdiI6IkVkWGNhcUNUYlZmYVwvOVhxdlwvZUlnZz09IiwidmFsdWUiOiIxRWtZZ0RGdWpEK2l4TFwvSjRlQkVIQT09IiwibWFjIjoiMjg3NTAyMjQ1YTNlNzgxMjEwZTdkM2NjOTdjMjEzYWQ5ZTM2OWFmMjZlYmVjNjM4MjFmNTA2ZmM1OGE3NDgxYSJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 17:41:25', '2019-01-23 17:41:25'),
(34, 'Se actualizo el usuario : salomonf.lopez@gmail.com', 'http://localhost:8000/users/eyJpdiI6IlNhY0JaQXpwMERTTnp1SCtkZjFrSnc9PSIsInZhbHVlIjoiSjZtUUhZUXEzQ3NhZnhlYWw3MTBiZz09IiwibWFjIjoiNmRiZTY5OWNmNDllNTQyM2IwMDkxNGMwMDU0NTkyYTFhZWEwMjk2OWFlNWQ2YzkxYmNhNmNiYjBhZGU2NjI4NiJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 17:43:22', '2019-01-23 17:43:22'),
(35, 'Recurso recién creado: werwerwer@hotmail.com', 'http://localhost:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:20:19', '2019-01-23 18:20:19'),
(36, 'Se agrego el perfil: qwed', 'http://localhost:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:20:42', '2019-01-23 18:20:42'),
(37, 'Recurso recién creado: dwere', 'http://localhost:8000/areas', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:21:01', '2019-01-23 18:21:01'),
(38, 'Recurso recién creado: werwerwer', 'http://localhost:8000/costos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:22:01', '2019-01-23 18:22:01'),
(39, 'Recurso recién creado: 234', 'http://localhost:8000/municipios', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:22:14', '2019-01-23 18:22:14'),
(40, 'Recurso recién eliminado: ', 'http://localhost:8000/users/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:40:48', '2019-01-23 18:40:48'),
(41, 'Recurso recién eliminado: Pedro', 'http://localhost:8000/users/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:41:33', '2019-01-23 18:41:33'),
(42, 'Recurso recién eliminado: qwed', 'http://localhost:8000/roles/7', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:41:42', '2019-01-23 18:41:42'),
(43, 'Recurso recién eliminado: dwere', 'http://localhost:8000/areas/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:41:53', '2019-01-23 18:41:53'),
(44, 'Recurso recién eliminado: werwerwer', 'http://localhost:8000/costos/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:42:08', '2019-01-23 18:42:08'),
(45, 'Recurso recién eliminado: okooko', 'http://localhost:8000/empresas/test', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:42:15', '2019-01-23 18:42:15'),
(46, 'Recurso recién creado: asdasdddddd', 'http://localhost:8000/ubicaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:42:44', '2019-01-23 18:42:44'),
(47, 'Recurso recién eliminado: 234', 'http://localhost:8000/municipios/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-23 18:43:00', '2019-01-23 18:43:00'),
(48, 'Recurso recién creado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 13:20:21', '2019-01-25 13:20:21'),
(49, 'Recurso recién creado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 13:33:18', '2019-01-25 13:33:18'),
(50, 'Recurso recién eliminado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos/8', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 14:07:41', '2019-01-25 14:07:41'),
(51, 'Recurso recién eliminado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos/7', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 14:07:43', '2019-01-25 14:07:43'),
(52, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/6', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 14:07:44', '2019-01-25 14:07:44'),
(53, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/5', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 14:07:45', '2019-01-25 14:07:45'),
(54, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/4', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 14:07:49', '2019-01-25 14:07:49'),
(55, 'Recurso recién creado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 14:08:03', '2019-01-25 14:08:03'),
(56, 'Recurso recién creado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 14:27:14', '2019-01-25 14:27:14'),
(57, 'Recurso recién eliminado: 098098', 'http://localhost:8000/recibos/11', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 17:04:34', '2019-01-25 17:04:34'),
(58, 'Recurso recién eliminado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos/9', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 17:04:35', '2019-01-25 17:04:35'),
(59, 'Recurso recién eliminado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos/10', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 17:04:36', '2019-01-25 17:04:36'),
(60, 'Recurso recién creado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 17:04:50', '2019-01-25 17:04:50'),
(61, 'Recurso recién creado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 17:28:35', '2019-01-25 17:28:35'),
(62, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-25 17:38:16', '2019-01-25 17:38:16'),
(63, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/14', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 18:42:45', '2019-01-29 18:42:45'),
(64, 'Recurso recién eliminado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos/13', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 18:42:47', '2019-01-29 18:42:47'),
(65, 'Recurso recién eliminado: DES HIDRAULICOS DE CANCUN SACV', 'http://localhost:8000/recibos/12', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 18:42:48', '2019-01-29 18:42:48'),
(66, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 18:43:16', '2019-01-29 18:43:16'),
(67, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 18:53:07', '2019-01-29 18:53:07'),
(68, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 19:04:20', '2019-01-29 19:04:20'),
(69, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 21:34:32', '2019-01-29 21:34:32'),
(70, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 21:35:46', '2019-01-29 21:35:46'),
(71, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 22:13:35', '2019-01-29 22:13:35'),
(72, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-29 22:32:06', '2019-01-29 22:32:06'),
(73, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/21', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 09:42:04', '2019-01-30 09:42:04'),
(74, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/15', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 09:42:05', '2019-01-30 09:42:05'),
(75, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/16', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 09:42:06', '2019-01-30 09:42:06'),
(76, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/17', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 09:42:07', '2019-01-30 09:42:07'),
(77, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/18', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 09:42:07', '2019-01-30 09:42:07'),
(78, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/19', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 09:42:08', '2019-01-30 09:42:08'),
(79, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/20', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 09:42:09', '2019-01-30 09:42:09'),
(80, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 09:42:22', '2019-01-30 09:42:22'),
(81, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 10:20:44', '2019-01-30 10:20:44'),
(82, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 11:01:24', '2019-01-30 11:01:24'),
(83, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/24', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 11:01:28', '2019-01-30 11:01:28'),
(84, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/23', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 11:01:30', '2019-01-30 11:01:30'),
(85, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/22', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 11:01:30', '2019-01-30 11:01:30'),
(86, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 11:33:15', '2019-01-30 11:33:15'),
(87, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 11:58:47', '2019-01-30 11:58:47'),
(88, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 12:03:22', '2019-01-30 12:03:22'),
(89, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/27', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 12:34:50', '2019-01-30 12:34:50'),
(90, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/25', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 12:34:51', '2019-01-30 12:34:51'),
(91, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/26', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 12:34:52', '2019-01-30 12:34:52'),
(92, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 12:35:05', '2019-01-30 12:35:05'),
(93, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 13:20:10', '2019-01-30 13:20:10'),
(94, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/29', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 13:23:52', '2019-01-30 13:23:52'),
(95, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 13:24:05', '2019-01-30 13:24:05'),
(96, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/28', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 21:50:02', '2019-01-30 21:50:02'),
(97, 'Recurso recién eliminado: AGUAKAN SA DE CVwerwerwer', 'http://localhost:8000/recibos/30', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 21:50:04', '2019-01-30 21:50:04'),
(98, 'Recurso recién eliminado: werwer', 'http://localhost:8000/recibos/31', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 21:50:05', '2019-01-30 21:50:05'),
(99, 'Recurso recién eliminado: Nombre', 'http://localhost:8000/recibos/32', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 21:50:06', '2019-01-30 21:50:06'),
(100, 'Recurso recién eliminado: ñlkñlkñl', 'http://localhost:8000/recibos/33', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 21:50:07', '2019-01-30 21:50:07'),
(101, 'Recurso recién eliminado: 987', 'http://localhost:8000/recibos/34', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 21:50:08', '2019-01-30 21:50:08'),
(102, 'Recurso recién eliminado: Nombre', 'http://localhost:8000/recibos/35', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 21:50:09', '2019-01-30 21:50:09'),
(103, 'Recurso recién eliminado: lklk', 'http://localhost:8000/recibos/36', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-30 21:50:11', '2019-01-30 21:50:11'),
(104, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 17:25:08', '2019-01-31 17:25:08'),
(105, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 18:08:02', '2019-01-31 18:08:02'),
(106, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/41', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 19:36:53', '2019-01-31 19:36:53'),
(107, 'Recurso recién eliminado: ñl', 'http://localhost:8000/recibos/37', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 19:36:54', '2019-01-31 19:36:54'),
(108, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/38', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 19:36:55', '2019-01-31 19:36:55'),
(109, 'Recurso recién eliminado: ñlkñlk', 'http://localhost:8000/recibos/39', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 19:36:57', '2019-01-31 19:36:57'),
(110, 'Recurso recién eliminado: ee', 'http://localhost:8000/recibos/40', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 19:36:59', '2019-01-31 19:36:59'),
(111, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 19:37:11', '2019-01-31 19:37:11'),
(112, 'Recurso recién creado: Tes', 'http://localhost:8000/municipios', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 21:47:50', '2019-01-31 21:47:50'),
(113, 'Recurso recién eliminado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos/42', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 22:08:09', '2019-01-31 22:08:09'),
(114, 'Recurso recién eliminado: poppo', 'http://localhost:8000/recibos/43', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 22:08:10', '2019-01-31 22:08:10'),
(115, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 22:08:24', '2019-01-31 22:08:24'),
(116, 'Recurso recién eliminado: AGUAKAN SA DE CVssss', 'http://localhost:8000/recibos/44', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 23:13:57', '2019-01-31 23:13:57'),
(117, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-01-31 23:14:08', '2019-01-31 23:14:08'),
(118, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-02-01 10:58:12', '2019-02-01 10:58:12'),
(119, 'Recurso recién creado: AGUAKAN SA DE CV', 'http://localhost:8000/recibos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-02-01 10:58:25', '2019-02-01 10:58:25'),
(120, 'Recurso recién eliminado: ñlk', 'http://localhost:8000/recibos/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, '2019-02-01 12:42:49', '2019-02-01 12:42:49'),
(121, 'Descuento Activado: ', 'http://127.0.0.1:8000/clientes/activar/descuento', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36', 1, '2019-02-13 05:32:41', '2019-02-13 05:32:41'),
(122, 'Descuento Activado: ', 'http://127.0.0.1:8000/clientes/activar/descuento', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36', 1, '2019-02-13 05:52:36', '2019-02-13 05:52:36'),
(123, 'Descuento Activado: ', 'http://127.0.0.1:8000/clientes/activar/descuento', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36', 1, '2019-02-13 06:00:05', '2019-02-13 06:00:05'),
(124, 'Descuento Activado: ', 'http://127.0.0.1:8000/clientes/activar/descuento', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36', 1, '2019-02-13 06:03:10', '2019-02-13 06:03:10'),
(125, 'Descuento Activado: ', 'http://127.0.0.1:8000/clientes/activar/descuento', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36', 1, '2019-02-13 06:11:18', '2019-02-13 06:11:18'),
(126, 'Descuento Activado: ', 'http://127.0.0.1:8000/clientes/activar/descuento', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36', 1, '2019-02-13 06:12:11', '2019-02-13 06:12:11'),
(127, 'Recurso recién creado: aleatorio@gmail.com', 'http://127.0.0.1:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 1, '2019-02-20 10:46:00', '2019-02-20 10:46:00'),
(128, 'Se agrego el perfil: consulta', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 1, '2019-02-21 16:32:55', '2019-02-21 16:32:55'),
(129, 'Se agrego el perfil: azulcrema', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 1, '2019-02-21 16:33:15', '2019-02-21 16:33:15'),
(130, 'Se agrego el perfil: ticketm', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 1, '2019-02-21 16:33:42', '2019-02-21 16:33:42'),
(131, 'Se actualizo el perfil: aleatorio', 'http://127.0.0.1:8000/roles/7', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 1, '2019-02-21 16:33:57', '2019-02-21 16:33:57'),
(132, 'Se actualizo el perfil: activos', 'http://127.0.0.1:8000/roles/8', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 1, '2019-02-21 16:34:13', '2019-02-21 16:34:13'),
(133, 'Se actualizo el usuario : salomon.flores@sparklabs.com.mx', 'http://127.0.0.1:8000/users/eyJpdiI6IlgxeVNkZWt4RGR3QjROYmlkQjZIYlE9PSIsInZhbHVlIjoiNHUwSE84TEoyQlhESStsWUR0RXN6QT09IiwibWFjIjoiZDNiOWZiYWQ1NTU5NzI3NjRjZTRkMTA4ZjI1NDA2NTE0MWVhYjYzN2ZmODUxNTNlOWQ1MGI5N2IyN2ZkNDk3MiJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 1, '2019-02-21 16:34:50', '2019-02-21 16:34:50'),
(134, 'Recurso recién creado: alicia.alanis@sparklabs.com.mx', 'http://127.0.0.1:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 1, '2019-02-21 17:12:28', '2019-02-21 17:12:28'),
(135, 'Se agrego el perfil: salesforce', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 1, '2019-03-01 06:03:38', '2019-03-01 06:03:38'),
(136, 'Se actualizo el usuario : salomon.flores@sparklabs.com.mx', 'http://127.0.0.1:8000/users/eyJpdiI6IkdcL0diYkhCWkpFcmc2dkpNV1FuUEZRPT0iLCJ2YWx1ZSI6IlJ6STN2eUhPVjMrSHBhbitQSWdPYmc9PSIsIm1hYyI6IjQ2MTIzZDY1NWI2ZmJiM2Q4MjVlY2E5ODFiNmZlMmY0MGZkZjVkOWI4MTM1YTczZGE5OGMzN2VkOWE1ZThhNzQifQ==', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 1, '2019-03-01 06:09:30', '2019-03-01 06:09:30'),
(137, 'Se agrego el perfil: pcaducados', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 1, '2019-03-08 08:54:23', '2019-03-08 08:54:23'),
(138, 'Se agrego el perfil: estadopago', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 1, '2019-03-08 08:54:44', '2019-03-08 08:54:44'),
(139, 'Se actualizo el usuario : salomon.flores@sparklabs.com.mx', 'http://127.0.0.1:8000/users/eyJpdiI6ImZhVnIrNXFIc1BjUWh6QTRRZEZCUEE9PSIsInZhbHVlIjoiNE5zelpuRFE2TG0yRVB5bmxwWGpMdz09IiwibWFjIjoiZWNhOGU5OGI5MjJmMjBhMWI3YjUzNjcxYzkyY2QyYjZlYTFkYWI5Mjc1ODFhMjY5YTc1OTY3MTA2M2ZkOWQ2MiJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 1, '2019-03-08 08:55:57', '2019-03-08 08:55:57'),
(140, 'Se agrego el perfil: mnto', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 1, '2019-03-09 13:34:55', '2019-03-09 13:34:55'),
(141, 'Se actualizo el usuario : salomon.flores@sparklabs.com.mx', 'http://127.0.0.1:8000/users/eyJpdiI6InFTYTBFQjhiT0hYR29TZUJFR2NCVnc9PSIsInZhbHVlIjoidmNkVFRiRmRyYWVtRTJQSVwvaWdMQUE9PSIsIm1hYyI6IjA4ZjEwZTJlZmFjN2RmZWJlODI4MGZmZWU5MjFkZDQ4NTRjMjZiZWNiZjg3ZDk1MjA5MGJmMmIwYjBkY2VmMDUifQ==', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 1, '2019-03-09 13:44:06', '2019-03-09 13:44:06'),
(142, 'Se agrego el perfil: Tiendas', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1, '2019-05-03 19:11:09', '2019-05-03 19:11:09'),
(143, 'Recurso recién eliminado: Salomon', 'http://127.0.0.1:8000/roles/6', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 19:04:48', '2019-09-20 19:04:48'),
(144, 'Recurso recién eliminado: Owner', 'http://127.0.0.1:8000/roles/2', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 21:06:24', '2019-09-20 21:06:24'),
(145, 'Recurso recién eliminado: Editor', 'http://127.0.0.1:8000/roles/3', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 21:06:33', '2019-09-20 21:06:33'),
(146, 'Recurso recién eliminado: Invitado', 'http://127.0.0.1:8000/roles/4', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 21:06:42', '2019-09-20 21:06:42'),
(147, 'Recurso recién eliminado: Temporal', 'http://127.0.0.1:8000/roles/5', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 21:06:45', '2019-09-20 21:06:45'),
(148, 'Recurso recién eliminado: Tiendas', 'http://127.0.0.1:8000/roles/16', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 21:06:49', '2019-09-20 21:06:49'),
(149, 'Se agrego el perfil: Operador', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 21:07:47', '2019-09-20 21:07:47'),
(150, 'Se agrego el perfil: Finanzas', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 21:08:32', '2019-09-20 21:08:32'),
(151, 'Se agrego el perfil: Ventas', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 21:09:00', '2019-09-20 21:09:00'),
(152, 'Se agrego el perfil: Capturista', 'http://127.0.0.1:8000/roles', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-20 21:09:18', '2019-09-20 21:09:18'),
(153, 'Recurso recién creado: ', 'http://127.0.0.1:8000/cargos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-23 19:00:33', '2019-09-23 19:00:33'),
(154, 'Recurso recién creado: ', 'http://127.0.0.1:8000/cargos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-23 19:00:53', '2019-09-23 19:00:53'),
(155, 'Recurso recién creado: ', 'http://127.0.0.1:8000/cargos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-23 19:01:12', '2019-09-23 19:01:12'),
(156, 'Recurso recién eliminado: Capturista', 'http://127.0.0.1:8000/cargos/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-23 19:14:05', '2019-09-23 19:14:05'),
(157, 'Recurso recién eliminado: Capturista', 'http://127.0.0.1:8000/cargos/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-23 19:14:33', '2019-09-23 19:14:33'),
(158, 'Recurso recién eliminado: alicia alanis', 'http://127.0.0.1:8000/users/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 16:28:42', '2019-09-24 16:28:42'),
(159, 'Recurso recién creado: alisgar9@hotmail.com', 'http://127.0.0.1:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 16:47:38', '2019-09-24 16:47:38'),
(160, 'Se actualizo el usuario : alisgar9@hotmail.com', 'http://127.0.0.1:8000/users/eyJpdiI6ImlzU2xuSnNCT1RSNWZnT1VCK3c1MHc9PSIsInZhbHVlIjoiMW95eHVRczdGemZ3SzI5c0lmMmIzdz09IiwibWFjIjoiODAzZjZmZmFlMDA2NGZiOWE5MWU4NjMyNDk2MmJiMjZlYzg5MTYzYzRhYWU5MTEzOTYzYzI2OGNjZmQxNDIzZCJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 18:21:36', '2019-09-24 18:21:36'),
(161, 'Se actualizo el usuario : alisgar9@hotmail.com', 'http://127.0.0.1:8000/users/eyJpdiI6IktXZ3FCSlNQbllZMm9Sc0JYQ3JjRVE9PSIsInZhbHVlIjoiSlRZZHhOa3JuYUFnSWNzaEt6SW9BUT09IiwibWFjIjoiNjkxOWFkN2Y0M2RlYzIzNjBkMTFmMzdhYzBlZGU5YjQ4ZjFiNjMxMjMxZTJlN2U1ZjIyNzNmYWZkYjI3Y2U5NSJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 18:51:27', '2019-09-24 18:51:27'),
(162, 'Se actualizo el usuario : alisgar9@hotmail.com', 'http://127.0.0.1:8000/users/eyJpdiI6Ikp4SnVadGZTcmxvb3JmYWhPXC9SK3V3PT0iLCJ2YWx1ZSI6IlwvcVlVUXhiaUpoaGNkT3FlMWlreXlBPT0iLCJtYWMiOiJlNWY1OTA2NmI1MzEwMmQ0MzFkYTZjZGFlMmMwOGNhMjgwNjdmNjkzNmYzYTBkZTQ1NGYxZTc3NjdmMTI2YWFhIn0=', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 18:51:54', '2019-09-24 18:51:54'),
(163, 'Se actualizo el usuario : alisgar9@hotmail.com', 'http://127.0.0.1:8000/users/eyJpdiI6IlN3d3gyYTBwVThsMTlxdW9XUnUzVUE9PSIsInZhbHVlIjoiaWl3TE5tWVZscjNRU0FMYXA1OElmZz09IiwibWFjIjoiODQ2ZWQ1MzA4OTc5NWQyNzMwOGZkNTk5NTI0NDlkMGNmNzAzMjg1MzU5OGE3OGFiZDc3NjFlZjdhNWJkNmIwMyJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 18:52:28', '2019-09-24 18:52:28');
INSERT INTO `log_activities` (`id`, `subject`, `url`, `method`, `ip`, `agent`, `user_id`, `updated_at`, `created_at`) VALUES
(164, 'Se actualizo el usuario : alisgar9@hotmail.com', 'http://127.0.0.1:8000/users/eyJpdiI6ImhxOFwvZFdNdE11N0lwcTdaRnVqbGpnPT0iLCJ2YWx1ZSI6Ild5THJPUURZMlg0K0J4OVhQeFFab0E9PSIsIm1hYyI6Ijc3YTlhMjNjOTgzM2ZjMjA1NzUxNTg0NjM1MTFjZmE2YjJlZmRmMGQxOTFiZjMxNjkxMWFhYjJkMWEzOTVhMDYifQ==', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 18:52:43', '2019-09-24 18:52:43'),
(165, 'Se actualizo el usuario : alisgar9@hotmail.com', 'http://127.0.0.1:8000/users/eyJpdiI6IllVd2pNRzZVY3Q0OThoZkFNdmhpQ1E9PSIsInZhbHVlIjoiNTVvQ1RPZ01LRFZWZm5makVzNHZWUT09IiwibWFjIjoiOGYyZTAyMzMyMTdiMTM3MWFmNmRiZjJmN2EwZjE2ZDZhOTc1MTQ2ODNlM2FkNTFlM2MwMmEyNDFlMDY4ZDJlNSJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 18:52:54', '2019-09-24 18:52:54'),
(166, 'Recurso recién eliminado: alicia alanis', 'http://127.0.0.1:8000/users/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 18:53:01', '2019-09-24 18:53:01'),
(167, 'Recurso recién eliminado: alicia alanis', 'http://127.0.0.1:8000/users/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 19:03:05', '2019-09-24 19:03:05'),
(168, 'Se actualizo el usuario : alicia@starup.com.mx', 'http://127.0.0.1:8000/users/eyJpdiI6InBhRmthTE40R0xTUTB0QlRQbUJDYmc9PSIsInZhbHVlIjoicU10cTlOY0c3QStFTFRkWnVsYk8ydz09IiwibWFjIjoiYjljN2UzZTRkMzU4OWIzNzBjZTY4ZWU2MTY3OGIyMzFiMjRkZTliYTAzYWJlODZhNzE5NmFjZWY2ODM1MTVhNyJ9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-24 21:33:07', '2019-09-24 21:33:07'),
(169, 'Recurso recién creado: ', 'http://127.0.0.1:8000/clasificaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 14:48:11', '2019-09-25 14:48:11'),
(170, 'Recurso recién eliminado: Capturista', 'http://127.0.0.1:8000/cargos/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 14:51:48', '2019-09-25 14:51:48'),
(171, 'Recurso recién eliminado: Carrier', 'http://127.0.0.1:8000/clasificaciones/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 15:07:02', '2019-09-25 15:07:02'),
(172, 'Recurso recién creado: ', 'http://127.0.0.1:8000/clasificaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 15:33:43', '2019-09-25 15:33:43'),
(173, 'Recurso recién creado: ', 'http://127.0.0.1:8000/clasificaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 15:37:54', '2019-09-25 15:37:54'),
(174, 'Recurso recién creado: ', 'http://127.0.0.1:8000/clasificaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 15:39:22', '2019-09-25 15:39:22'),
(175, 'Recurso recién creado: ', 'http://127.0.0.1:8000/clasificaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 15:40:39', '2019-09-25 15:40:39'),
(176, 'Recurso recién creado: data 1', 'http://127.0.0.1:8000/entidades', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 18:22:23', '2019-09-25 18:22:23'),
(177, 'Recurso recién creado: prueba', 'http://127.0.0.1:8000/entidades', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 19:04:38', '2019-09-25 19:04:38'),
(178, 'Recurso recién eliminado: ', 'http://127.0.0.1:8000/entidades/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 19:04:47', '2019-09-25 19:04:47'),
(179, 'Se actualizo la entidad : prueba', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 21:43:24', '2019-09-25 21:43:24'),
(180, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 21:47:31', '2019-09-25 21:47:31'),
(181, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 21:51:00', '2019-09-25 21:51:00'),
(182, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 21:55:54', '2019-09-25 21:55:54'),
(183, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 21:59:42', '2019-09-25 21:59:42'),
(184, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:00:40', '2019-09-25 22:00:40'),
(185, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:00:58', '2019-09-25 22:00:58'),
(186, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:15:10', '2019-09-25 22:15:10'),
(187, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:15:22', '2019-09-25 22:15:22'),
(188, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:15:31', '2019-09-25 22:15:31'),
(189, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:15:42', '2019-09-25 22:15:42'),
(190, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:17:36', '2019-09-25 22:17:36'),
(191, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:20:58', '2019-09-25 22:20:58'),
(192, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:21:10', '2019-09-25 22:21:10'),
(193, 'Se actualizo la entidad : 3', 'http://127.0.0.1:8000/entidades/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-25 22:21:22', '2019-09-25 22:21:22'),
(194, 'Recurso recién eliminado: ', 'http://127.0.0.1:8000/entidades/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-26 16:27:13', '2019-09-26 16:27:13'),
(195, 'Recurso recién eliminado: sds', 'http://127.0.0.1:8000/entidadbanco/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-26 16:59:22', '2019-09-26 16:59:22'),
(196, 'Recurso recién creado: 1234567890', 'http://127.0.0.1:8000/entidadbanco', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-26 18:38:17', '2019-09-26 18:38:17'),
(197, 'Recurso recién creado: prueba', 'http://127.0.0.1:8000/entidadbanco', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-26 18:38:52', '2019-09-26 18:38:52'),
(198, 'Recurso recién creado: Bancomer', 'http://127.0.0.1:8000/bancos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-26 19:30:11', '2019-09-26 19:30:11'),
(199, 'Recurso recién creado: BBVA', 'http://127.0.0.1:8000/bancos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-26 19:31:00', '2019-09-26 19:31:00'),
(200, 'Recurso recién eliminado: Bancomer', 'http://127.0.0.1:8000/bancos/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-26 19:35:42', '2019-09-26 19:35:42'),
(201, 'Recurso recién creado: Santander', 'http://127.0.0.1:8000/bancos', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-26 20:38:29', '2019-09-26 20:38:29'),
(202, 'Se actualizo el usuario : alisgar9@hotmail.com', 'http://127.0.0.1:8000/users/eyJpdiI6Imw5b1FcL05PbWdseFFvakhTajNvOE1RPT0iLCJ2YWx1ZSI6IkJ3UXVmWDBcL09aeHJzanJLUitObHp3PT0iLCJtYWMiOiIzZjI4NTlmOTk4NjkxOTVhZDY4ZjVkZTVhOGZkNTJkZTUyYWJjYTAzMmZmMzI0ODVkYjFhZmE3YjcwYzVmN2EzIn0=', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 1, '2019-09-26 21:30:26', '2019-09-26 21:30:26'),
(203, 'Recurso recién creado: gilberto@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.161.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 1, '2019-10-01 14:07:33', '2019-10-01 14:07:33'),
(204, 'Se actualizo el usuario : gil@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6IkMxbXFGeVBRSFdVVnFkNGVLVUh1RkE9PSIsInZhbHVlIjoiZGRLMTU3M2VLZWN6bDRKdFdGR1lwdz09IiwibWFjIjoiMTk4OTVlY2QzMmNlOTk3MWVjYzc0ZjAwOGE1MzY3N2M5YjBhM2IxYzU3YTQxNTM4YjU0Zjc3YWU4MWU5ZmJlOSJ9', 'PUT', '189.146.161.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 1, '2019-10-03 13:21:24', '2019-10-03 13:21:24'),
(205, 'Se actualizo el usuario : gil@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6ImkwM1wvRmNFMDhVKzkrUWt2SnF5NzNRPT0iLCJ2YWx1ZSI6IkJtWHdrTHF0aThGbzJwclZoVUpic0E9PSIsIm1hYyI6ImMxZTk2YTI4NDMzNmIwNTdiZWFiMTRkYWNkMDViZjUzNjkzODJlNWYyMGU5YjUxZDNmZjQzNDRiN2VhNzZjNzEifQ==', 'PUT', '189.146.161.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 1, '2019-10-03 13:22:15', '2019-10-03 13:22:15'),
(206, 'Se actualizo el usuario : gil@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6IjZnRDd3UlkwUGhIcmdCTEtPZjhMaFE9PSIsInZhbHVlIjoiRnhaZGo1aGZDemdGelZTNDRQNXR2Zz09IiwibWFjIjoiZTZmNjFiNjdjZTU3NTE5NjAyMjIyN2ExODRkYmZiZjAwOTIwZDFkMDI5YmRiMzNiNWJhYTY4YmRjNmI4NDY3MyJ9', 'PUT', '189.146.161.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 1, '2019-10-03 13:22:54', '2019-10-03 13:22:54'),
(207, 'Recurso recién creado: Fromax', 'https://starup.com.mx/starupadmin/entidades', 'POST', '189.146.161.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 44, '2019-10-03 13:26:50', '2019-10-03 13:26:50'),
(208, 'Recurso recién creado: ', 'https://starup.com.mx/starupadmin/clasificaciones', 'POST', '189.146.161.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 1, '2019-10-03 16:55:52', '2019-10-03 16:55:52'),
(209, 'Recurso recién creado: ', 'https://starup.com.mx/starupadmin/clasificaciones', 'POST', '189.146.161.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 1, '2019-10-03 16:56:43', '2019-10-03 16:56:43'),
(210, 'Recurso recién creado: ', 'https://starup.com.mx/starupadmin/clasificaciones', 'POST', '189.146.161.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 1, '2019-10-03 16:57:18', '2019-10-03 16:57:18'),
(211, 'Recurso recién creado: Fromax 2', 'https://starup.com.mx/starupadmin/entidades', 'POST', '189.146.161.130', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 1, '2019-10-03 16:59:50', '2019-10-03 16:59:50'),
(212, 'Recurso recién creado: operador@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.168.31', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-10-11 15:43:18', '2019-10-11 15:43:18'),
(213, 'Se agrego el perfil: GnteOpe', 'https://starup.com.mx/starupadmin/roles', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-19 15:42:38', '2019-11-19 15:42:38'),
(214, 'Recurso recién creado: jade@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-19 15:43:21', '2019-11-19 15:43:21'),
(215, 'Recurso recién creado: ASDSDA2323', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 12:02:24', '2019-11-20 12:02:24'),
(216, 'Recurso recién creado: 123', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 12:10:13', '2019-11-20 12:10:13'),
(217, 'Recurso recién creado: PRUEBA', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 12:25:09', '2019-11-20 12:25:09'),
(218, 'Recurso recién creado: 123', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 13:28:34', '2019-11-20 13:28:34'),
(219, 'Recurso recién creado: PRUEBA2', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 13:36:32', '2019-11-20 13:36:32'),
(220, 'Updated Operation: PRUEBA2', 'https://starup.com.mx/starupadmin/operaciones/2', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 13:45:34', '2019-11-20 13:45:34'),
(221, 'Se actualizo el perfil: Operador', 'https://starup.com.mx/starupadmin/roles/17', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-21 11:48:55', '2019-11-21 11:48:55'),
(222, 'Se actualizo el perfil: Operador', 'https://starup.com.mx/starupadmin/roles/17', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-21 11:49:17', '2019-11-21 11:49:17'),
(223, 'Recurso recién creado: ABCD12', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 45, '2019-11-21 11:51:39', '2019-11-21 11:51:39'),
(224, 'Recurso recién creado: SULG1111', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-11-21 12:53:28', '2019-11-21 12:53:28'),
(225, 'Updated Operation: SULG1111', 'https://starup.com.mx/starupadmin/operaciones/2', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-11-21 12:57:49', '2019-11-21 12:57:49'),
(226, 'Recurso recién eliminado: D2019112135ABCD12', 'https://starup.com.mx/starupadmin/operaciones/1', 'DELETE', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 45, '2019-11-21 13:01:35', '2019-11-21 13:01:35'),
(227, 'Recurso recién creado: ABCD123', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 45, '2019-11-22 16:11:46', '2019-11-22 16:11:46'),
(228, 'Recurso recién creado: AIR123', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-25 10:32:08', '2019-11-25 10:32:08'),
(229, 'Recurso recién creado: SULG11112', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-11-27 15:23:35', '2019-11-27 15:23:35'),
(230, 'Recurso recién creado: operador2@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-27 15:25:26', '2019-11-27 15:25:26'),
(231, 'Updated Operation: SULG11112', 'https://starup.com.mx/starupadmin/operaciones/3', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-11-27 15:28:28', '2019-11-27 15:28:28'),
(232, 'Detail Update: MSKU12345', 'https://starup.com.mx/starupadmin/operation/details/update', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 45, '2019-11-27 15:40:21', '2019-11-27 15:40:21'),
(233, 'Updated Operation: AIR123', 'https://starup.com.mx/starupadmin/operaciones/2', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-28 13:27:34', '2019-11-28 13:27:34'),
(234, 'Recurso recién creado: LAN123', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 45, '2019-11-28 13:34:23', '2019-11-28 13:34:23'),
(235, 'Recurso recién creado: SULGZ0004975', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', 46, '2019-11-28 15:30:27', '2019-11-28 15:30:27'),
(236, 'Se actualizo el usuario : jaqueline@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6IkdpbTVRQ2lGM0MrN1BEUkZKSnNzb2c9PSIsInZhbHVlIjoialdkSTJtMTJJbktXalRYYXZmdk1OQT09IiwibWFjIjoiZWI3ZDgxN2M3N2RjZjYzZDJiODAxNTEyMWJiMGY3ZmNkMzgxMmViNmZhN2ViZDJkMzExNjQ0MmY4MjZlMGMzNSJ9', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, '2019-12-03 14:03:29', '2019-12-03 14:03:29'),
(237, 'Recurso recién creado: ricardo@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, '2019-12-03 14:04:28', '2019-12-03 14:04:28'),
(238, 'Recurso recién creado: zaydee@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, '2019-12-03 14:05:38', '2019-12-03 14:05:38'),
(239, 'Recurso recién creado: yatzil@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, '2019-12-03 14:06:32', '2019-12-03 14:06:32'),
(240, 'Recurso recién creado: christian@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, '2019-12-03 14:07:23', '2019-12-03 14:07:23'),
(241, 'Recurso recién creado: estefania@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, '2019-12-03 14:09:12', '2019-12-03 14:09:12'),
(242, 'Recurso recién creado: alejandra.m@starup.com.mx', 'https://starup.com.mx/starupadmin/users', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, '2019-12-03 14:14:07', '2019-12-03 14:14:07'),
(243, 'Recurso recién creado: AR0001', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-03 16:10:09', '2019-12-03 16:10:09'),
(244, 'Updated Operation: AR0001', 'https://starup.com.mx/starupadmin/operaciones/1', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-03 16:13:00', '2019-12-03 16:13:00'),
(245, 'Recurso recién creado: ACOPOL S.A. DE C.V.', 'https://starup.com.mx/starupadmin/entidades', 'POST', '201.103.252.245', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', 47, '2019-12-04 11:56:07', '2019-12-04 11:56:07'),
(246, 'Recurso recién creado: STAR UP CHINA', 'https://starup.com.mx/starupadmin/entidades', 'POST', '201.103.252.245', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', 47, '2019-12-04 11:59:17', '2019-12-04 11:59:17'),
(247, 'Recurso recién creado: SULGZ0004964', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '201.103.252.245', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', 47, '2019-12-04 12:01:51', '2019-12-04 12:01:51'),
(248, 'Recurso recién creado: SULGZ0004958', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '201.103.252.245', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0', 47, '2019-12-04 12:07:36', '2019-12-04 12:07:36'),
(249, 'Updated Operation: SULGZ0004964', 'https://starup.com.mx/starupadmin/operaciones/2', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 47, '2019-12-05 12:08:18', '2019-12-05 12:08:18'),
(250, 'Updated Operation: SULGZ0004964', 'https://starup.com.mx/starupadmin/operaciones/2', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 47, '2019-12-05 12:08:32', '2019-12-05 12:08:32'),
(251, 'Updated Operation: SULGZ0004958', 'https://starup.com.mx/starupadmin/operaciones/3', 'PUT', '189.146.146.220', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 47, '2019-12-05 12:08:58', '2019-12-05 12:08:58'),
(344, 'Recurso recién creado: ', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 20:08:45', '2019-11-04 20:08:45'),
(345, 'Recurso recién eliminado: ', 'http://127.0.0.1:8000/operationdetails/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 20:08:58', '2019-11-04 20:08:58'),
(346, 'Recurso recién creado: ', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 20:11:05', '2019-11-04 20:11:05'),
(347, 'Recurso recién creado: ', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:06:48', '2019-11-04 21:06:48'),
(348, 'Recurso recién creado: ', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:17:17', '2019-11-04 21:17:17'),
(349, 'Recurso recién creado: ', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:24:45', '2019-11-04 21:24:45'),
(350, 'Recurso recién creado: ', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:29:05', '2019-11-04 21:29:05'),
(351, 'Recurso recién creado: 1ASW12', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:30:00', '2019-11-04 21:30:00'),
(352, 'Recurso recién eliminado: 1ASW12', 'http://127.0.0.1:8000/operationdetails/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:47:48', '2019-11-04 21:47:48'),
(353, 'Recurso recién eliminado: ', 'http://127.0.0.1:8000/operationdetails/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:47:54', '2019-11-04 21:47:54'),
(354, 'Recurso recién eliminado: ', 'http://127.0.0.1:8000/operationdetails/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:49:32', '2019-11-04 21:49:32'),
(355, 'Recurso recién eliminado: ', 'http://127.0.0.1:8000/operationdetails/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:49:41', '2019-11-04 21:49:41'),
(356, 'Recurso recién eliminado: ', 'http://127.0.0.1:8000/operationdetails/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:50:51', '2019-11-04 21:50:51'),
(357, 'Recurso recién eliminado: ', 'http://127.0.0.1:8000/operationdetails/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:51:01', '2019-11-04 21:51:01'),
(358, 'Recurso recién creado: 1ASW12', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:51:16', '2019-11-04 21:51:16'),
(359, 'Recurso recién eliminado: 1ASW12', 'http://127.0.0.1:8000/operationdetails/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 21:51:25', '2019-11-04 21:51:25'),
(360, 'Recurso recién creado: Q1ASW12', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 22:47:42', '2019-11-04 22:47:42'),
(361, 'Recurso recién creado: 2345678', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 22:48:16', '2019-11-04 22:48:16'),
(362, 'Detail Update: 2', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 23:06:31', '2019-11-04 23:06:31'),
(363, 'Detail Update: 2', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 23:06:45', '2019-11-04 23:06:45'),
(364, 'Detail Update: 2', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 23:34:34', '2019-11-04 23:34:34'),
(365, 'Detail Update: 23eer', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-04 23:36:24', '2019-11-04 23:36:24'),
(366, 'Detail Update: 1ABCD12', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 15:46:42', '2019-11-05 15:46:42'),
(367, 'Updated Operation: 123AQW', 'http://127.0.0.1:8000/operaciones/9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 16:48:18', '2019-11-05 16:48:18'),
(368, 'Updated Operation: 123AIR', 'http://127.0.0.1:8000/operaciones/10', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 16:51:19', '2019-11-05 16:51:19'),
(369, 'Updated Operation: 123AQW', 'http://127.0.0.1:8000/operaciones/9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 17:06:59', '2019-11-05 17:06:59'),
(370, 'Updated Operation: 123AQW', 'http://127.0.0.1:8000/operaciones/9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 17:09:46', '2019-11-05 17:09:46'),
(371, 'Updated Operation: PB1', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 17:43:32', '2019-11-05 17:43:32'),
(372, 'Updated Operation: PB1', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 17:49:31', '2019-11-05 17:49:31'),
(373, 'Updated Operation: PB1', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 17:54:30', '2019-11-05 17:54:30'),
(374, 'Recurso recién creado: AIR123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 18:01:09', '2019-11-05 18:01:09'),
(375, 'Recurso recién eliminado: D2019110514AIR123', 'http://127.0.0.1:8000/operaciones/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 18:05:14', '2019-11-05 18:05:14'),
(376, 'Recurso recién creado: 12340AIR', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 18:29:19', '2019-11-05 18:29:19'),
(377, 'Recurso recién eliminado: D2019110502123AIR', 'http://127.0.0.1:8000/operaciones/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 18:31:02', '2019-11-05 18:31:02'),
(378, 'Updated Operation: 123AQW', 'http://127.0.0.1:8000/operaciones/9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 18:31:26', '2019-11-05 18:31:26'),
(379, 'Detail Update: ', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 18:38:12', '2019-11-05 18:38:12'),
(380, 'Updated Operation: 12340AIR', 'http://127.0.0.1:8000/operaciones/13', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 18:50:10', '2019-11-05 18:50:10'),
(381, 'Updated Operation: 12340AIR', 'http://127.0.0.1:8000/operaciones/13', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 18:50:27', '2019-11-05 18:50:27'),
(382, 'Detail Update: 1ABCD12', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 19:18:15', '2019-11-05 19:18:15'),
(383, 'Updated Operation: PB1', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 19:20:55', '2019-11-05 19:20:55'),
(384, 'Updated Operation: PB1', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 19:21:19', '2019-11-05 19:21:19'),
(385, 'Recurso recién creado: 23EER', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 19:29:49', '2019-11-05 19:29:49'),
(386, 'Recurso recién creado: 3455', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 19:59:22', '2019-11-05 19:59:22'),
(387, 'Recurso recién creado: SULGZ0036126', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 20:07:40', '2019-11-05 20:07:40'),
(388, 'Updated Operation: 12340AIR', 'http://127.0.0.1:8000/operaciones/13', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 20:19:03', '2019-11-05 20:19:03'),
(389, 'Updated Operation: 12340AIR', 'http://127.0.0.1:8000/operaciones/13', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 20:20:25', '2019-11-05 20:20:25'),
(390, 'Updated Operation: 12340AIR', 'http://127.0.0.1:8000/operaciones/13', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 20:21:06', '2019-11-05 20:21:06'),
(391, 'Updated Operation: 12340AIR', 'http://127.0.0.1:8000/operaciones/13', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 20:21:27', '2019-11-05 20:21:27'),
(392, 'Updated Operation: 12340AIR', 'http://127.0.0.1:8000/operaciones/13', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-05 20:21:47', '2019-11-05 20:21:47'),
(393, 'Updated Operation: SULGZ0036126', 'http://127.0.0.1:8000/operaciones/14', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-06 16:55:46', '2019-11-06 16:55:46'),
(394, 'Updated Operation: SULGZ0036126', 'http://127.0.0.1:8000/operaciones/14', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-06 16:55:57', '2019-11-06 16:55:57'),
(395, 'Updated Operation: SULGZ0036126', 'http://127.0.0.1:8000/operaciones/14', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-06 17:13:15', '2019-11-06 17:13:15'),
(396, 'Updated Operation: SULGZ0036126', 'http://127.0.0.1:8000/operaciones/14', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-06 17:19:41', '2019-11-06 17:19:41'),
(397, 'Updated Operation: SULGZ0036126', 'http://127.0.0.1:8000/operaciones/14', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-06 17:24:11', '2019-11-06 17:24:11'),
(398, 'Updated Operation: SULGZ0036126', 'http://127.0.0.1:8000/operaciones/14', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-06 17:28:56', '2019-11-06 17:28:56'),
(399, 'Updated Operation: SULGZ0036126', 'http://127.0.0.1:8000/operaciones/14', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-06 18:03:40', '2019-11-06 18:03:40'),
(400, 'Updated Operation: 123AQW', 'http://127.0.0.1:8000/operaciones/9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-06 19:45:31', '2019-11-06 19:45:31'),
(401, 'Updated Operation: SULGZ0036126', 'http://127.0.0.1:8000/operaciones/14', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-06 23:33:31', '2019-11-06 23:33:31'),
(402, 'Updated Operation: PB1', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-07 16:13:15', '2019-11-07 16:13:15'),
(403, 'Recurso recién eliminado: D20191108512', 'http://127.0.0.1:8000/operationdetails/0', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-08 18:16:52', '2019-11-08 18:16:52'),
(404, 'Updated Operation: 123AQW', 'http://127.0.0.1:8000/operaciones/9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-08 18:24:58', '2019-11-08 18:24:58'),
(405, 'Updated Operation: OCP', 'http://127.0.0.1:8000/operaciones/11', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-08 18:25:48', '2019-11-08 18:25:48'),
(406, 'Updated Operation: 12340AIR', 'http://127.0.0.1:8000/operaciones/13', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-08 18:27:30', '2019-11-08 18:27:30'),
(407, 'Detail Update: 11234er1', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-08 19:11:24', '2019-11-08 19:11:24'),
(408, 'Detail Update: abcd1234', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-08 19:22:25', '2019-11-08 19:22:25'),
(409, 'Detail Update: ABCD1234', 'http://127.0.0.1:8000/operation/details/update', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 1, '2019-11-08 19:23:57', '2019-11-08 19:23:57'),
(410, 'Recurso recién creado: AIRQWER123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36', 1, '2019-11-11 17:07:53', '2019-11-11 17:07:53'),
(411, 'Recurso recién creado: 123INL1234', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36', 1, '2019-11-11 17:21:06', '2019-11-11 17:21:06'),
(412, 'Recurso recién creado: 123OCE123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36', 1, '2019-11-11 18:58:57', '2019-11-11 18:58:57'),
(413, 'Recurso recién creado: 123ABC12', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36', 1, '2019-11-11 23:02:13', '2019-11-11 23:02:13'),
(414, 'Updated Operation: 123AQW', 'http://127.0.0.1:8000/operaciones/9', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-12 19:12:24', '2019-11-12 19:12:24'),
(415, 'Recurso recién creado: ASASDASD', 'http://127.0.0.1:8000/entidades', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-12 21:30:22', '2019-11-12 21:30:22'),
(416, 'Recurso recién creado: DFSFSDFSDF', 'http://127.0.0.1:8000/entidades', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-12 21:41:46', '2019-11-12 21:41:46'),
(417, 'Recurso recién creado: AADASD', 'http://127.0.0.1:8000/entidades', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-12 21:51:43', '2019-11-12 21:51:43'),
(418, 'Recurso recién creado: AIRFSFSFSDFSD123123', 'http://127.0.0.1:8000/entidades', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-12 22:02:56', '2019-11-12 22:02:56'),
(419, 'Se actualizo la entidad : 19', 'http://127.0.0.1:8000/entidades/19', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-12 22:04:18', '2019-11-12 22:04:18'),
(420, 'Recurso recién creado: KKKKKK111', 'http://127.0.0.1:8000/operation/details/store', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 17:38:57', '2019-11-13 17:38:57'),
(421, 'Recurso recién creado: QWERTY123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 18:03:31', '2019-11-13 18:03:31'),
(422, 'Recurso recién creado: WEQWE890', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 18:05:49', '2019-11-13 18:05:49'),
(423, 'Recurso recién creado: AIR 123456', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 18:18:21', '2019-11-13 18:18:21'),
(424, 'Recurso recién creado: 123WAR', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 18:32:39', '2019-11-13 18:32:39'),
(425, 'Updated Operation: AIR 123456', 'http://127.0.0.1:8000/operaciones/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 18:35:35', '2019-11-13 18:35:35'),
(426, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 19:33:16', '2019-11-13 19:33:16'),
(427, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 19:39:17', '2019-11-13 19:39:17'),
(428, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 19:46:55', '2019-11-13 19:46:55'),
(429, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 20:02:36', '2019-11-13 20:02:36'),
(430, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 21:50:44', '2019-11-13 21:50:44'),
(431, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:20:28', '2019-11-13 22:20:28'),
(432, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:21:34', '2019-11-13 22:21:34'),
(433, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:30:10', '2019-11-13 22:30:10');
INSERT INTO `log_activities` (`id`, `subject`, `url`, `method`, `ip`, `agent`, `user_id`, `updated_at`, `created_at`) VALUES
(434, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:32:44', '2019-11-13 22:32:44'),
(435, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:38:11', '2019-11-13 22:38:11'),
(436, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:39:42', '2019-11-13 22:39:42'),
(437, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:40:35', '2019-11-13 22:40:35'),
(438, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:41:52', '2019-11-13 22:41:52'),
(439, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:44:46', '2019-11-13 22:44:46'),
(440, 'Updated Operation: QWERTY123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:45:35', '2019-11-13 22:45:35'),
(441, 'Recurso recién creado: 123QWER', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 22:55:47', '2019-11-13 22:55:47'),
(442, 'Recurso recién creado: 1234', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 23:01:51', '2019-11-13 23:01:51'),
(443, 'Recurso recién creado: SFSFSDF', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 23:05:54', '2019-11-13 23:05:54'),
(444, 'Recurso recién creado: QWDQWD', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 23:12:15', '2019-11-13 23:12:15'),
(445, 'Recurso recién creado: 12345OC', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 23:14:54', '2019-11-13 23:14:54'),
(446, 'Recurso recién creado: ASDASDA', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 23:18:53', '2019-11-13 23:18:53'),
(447, 'Updated Operation: ASDASDA', 'http://127.0.0.1:8000/operaciones/4', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-13 23:27:27', '2019-11-13 23:27:27'),
(448, 'Recurso recién creado: LAND12340', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 15:48:27', '2019-11-14 15:48:27'),
(449, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 15:56:24', '2019-11-14 15:56:24'),
(450, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 16:30:18', '2019-11-14 16:30:18'),
(451, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 16:35:48', '2019-11-14 16:35:48'),
(452, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 16:35:59', '2019-11-14 16:35:59'),
(453, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 16:40:01', '2019-11-14 16:40:01'),
(454, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 16:46:44', '2019-11-14 16:46:44'),
(455, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 16:48:10', '2019-11-14 16:48:10'),
(456, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 17:00:05', '2019-11-14 17:00:05'),
(457, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 17:01:09', '2019-11-14 17:01:09'),
(458, 'Updated Operation: QWDQWD', 'http://127.0.0.1:8000/operaciones/2', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 17:03:27', '2019-11-14 17:03:27'),
(459, 'Updated Operation: QWDQWD', 'http://127.0.0.1:8000/operaciones/2', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 17:03:32', '2019-11-14 17:03:32'),
(460, 'Updated Operation: QWDQWD', 'http://127.0.0.1:8000/operaciones/2', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 17:06:18', '2019-11-14 17:06:18'),
(461, 'Updated Operation: QWDQWD', 'http://127.0.0.1:8000/operaciones/2', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 17:07:45', '2019-11-14 17:07:45'),
(462, 'Updated Operation: LAND12340', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 17:13:44', '2019-11-14 17:13:44'),
(463, 'Updated Files: LAND12340', 'http://127.0.0.1:8000/operaciones-files', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 22:25:31', '2019-11-14 22:25:31'),
(464, 'Updated Files: LAND12340', 'http://127.0.0.1:8000/operaciones-files', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 22:54:54', '2019-11-14 22:54:54'),
(465, 'Updated Files: LAND12340', 'http://127.0.0.1:8000/operaciones-files', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 23:00:51', '2019-11-14 23:00:51'),
(466, 'Updated Files: LAND12340', 'http://127.0.0.1:8000/operaciones-files', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 23:02:48', '2019-11-14 23:02:48'),
(467, 'Updated Files: LAND12340', 'http://127.0.0.1:8000/operaciones-files', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-14 23:04:07', '2019-11-14 23:04:07'),
(468, 'Updated Files: LAND12340', 'http://127.0.0.1:8000/operaciones-files', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-15 17:17:52', '2019-11-15 17:17:52'),
(469, 'Updated Operation: SFSFSDF', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-15 19:44:09', '2019-11-15 19:44:09'),
(470, 'Updated Operation: 12345OC', 'http://127.0.0.1:8000/operaciones/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-15 19:44:37', '2019-11-15 19:44:37'),
(471, 'Recurso recién creado: jade@starup.com.mx', 'http://127.0.0.1:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-19 19:02:50', '2019-11-19 19:02:50'),
(472, 'Recurso recién creado: 1234ASAS', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-19 22:45:11', '2019-11-19 22:45:11'),
(473, 'Recurso recién creado: XXXXXXXX', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-19 23:16:08', '2019-11-19 23:16:08'),
(474, 'Updated Operation: XXXXXXXX', 'http://127.0.0.1:8000/operaciones/7', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 17:15:35', '2019-11-20 17:15:35'),
(475, 'Recurso recién creado: 123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 20:02:32', '2019-11-20 20:02:32'),
(476, 'Recurso recién creado: 123456QA', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 20:06:37', '2019-11-20 20:06:37'),
(477, 'Updated Operation: 123456QA', 'http://127.0.0.1:8000/operaciones/2', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-20 20:07:59', '2019-11-20 20:07:59'),
(478, 'Recurso recién creado: WWER', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 45, '2019-11-20 21:38:44', '2019-11-20 21:38:44'),
(479, 'Recurso recién creado: IN001', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 45, '2019-11-20 22:33:12', '2019-11-20 22:33:12'),
(480, 'Recurso recién creado: CDE123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-21 22:54:37', '2019-11-21 22:54:37'),
(481, 'Updated Operation: CDE123', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-21 23:07:03', '2019-11-21 23:07:03'),
(482, 'Updated Operation: CDE123', 'http://127.0.0.1:8000/operaciones/5', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-21 23:10:49', '2019-11-21 23:10:49'),
(483, 'Recurso recién eliminado: D2019112254123', 'http://127.0.0.1:8000/operaciones/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-22 16:20:54', '2019-11-22 16:20:54'),
(484, 'Recurso recién creado: PRUEBA123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-22 16:27:29', '2019-11-22 16:27:29'),
(485, 'Recurso recién creado: 12345ABC', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-22 16:46:31', '2019-11-22 16:46:31'),
(486, 'Recurso recién creado: LINE0001', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 45, '2019-11-22 19:14:24', '2019-11-22 19:14:24'),
(487, 'Recurso recién eliminado: D201911225512345ABC', 'http://127.0.0.1:8000/operaciones/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-22 20:12:56', '2019-11-22 20:12:56'),
(488, 'Recurso recién eliminado: D201911222212345ABC', 'http://127.0.0.1:8000/operaciones/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-22 21:50:22', '2019-11-22 21:50:22'),
(489, 'Updated Files: LINE0001', 'http://127.0.0.1:8000/operaciones-files', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 45, '2019-11-27 21:39:59', '2019-11-27 21:39:59'),
(490, 'Updated Files: LINE0001', 'http://127.0.0.1:8000/operaciones-files', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 45, '2019-11-27 21:40:00', '2019-11-27 21:40:00'),
(491, 'Updated Files: LINE0001', 'http://127.0.0.1:8000/operaciones-files', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 45, '2019-11-27 21:40:32', '2019-11-27 21:40:32'),
(492, 'Recurso recién creado: operador2@gmail.com', 'http://127.0.0.1:8000/users', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 1, '2019-11-28 17:00:22', '2019-11-28 17:00:22'),
(493, 'Updated Operation: PRUEBA123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-28 17:05:02', '2019-11-28 17:05:02'),
(494, 'Updated Operation: PRUEBA123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-28 17:13:23', '2019-11-28 17:13:23'),
(495, 'Updated Operation: PRUEBA123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-28 17:26:44', '2019-11-28 17:26:44'),
(496, 'Updated Operation: PRUEBA123', 'http://127.0.0.1:8000/operaciones/1', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 46, '2019-11-28 17:38:29', '2019-11-28 17:38:29'),
(497, 'Recurso recién creado: ABCD123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 47, '2019-11-28 19:15:31', '2019-11-28 19:15:31'),
(498, 'Recurso recién creado: LAN123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 47, '2019-11-28 19:32:18', '2019-11-28 19:32:18'),
(499, 'Recurso recién creado: IL12341', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-02 17:29:28', '2019-12-02 17:29:28'),
(500, 'Recurso recién creado: OC0001', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-02 22:50:40', '2019-12-02 22:50:40'),
(501, 'Recurso recién creado: AR0001', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-03 18:49:11', '2019-12-03 18:49:11'),
(502, 'Updated Operation: AR0001', 'http://127.0.0.1:8000/operaciones/8', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-03 19:09:12', '2019-12-03 19:09:12'),
(503, 'Updated Operation: AR0001', 'http://127.0.0.1:8000/operaciones/8', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-03 19:10:23', '2019-12-03 19:10:23'),
(504, 'Recurso recién creado: 123QWER', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-05 18:56:15', '2019-12-05 18:56:15'),
(505, 'Updated Return empty container: eyJpdiI6IjdxcGtlTjM3WXRUek84eTNKXC9Xc2pRPT0iLCJ2YWx1ZSI6Ikx3Mjl3MDhrYnFMWmJSNUV0MTVKc3c9PSIsIm1hYyI6Ijc0OTJhOGQ2M2Q2MjM5MDdhNDkxZWM1NWY2MGIzMzNlODk3NjgwMWY2MTY1NWRhMDQwNWI4M2M1N2YzZDU3Y2MifQ== of HBL: 123QWER', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-12 18:34:24', '2019-12-12 18:34:24'),
(506, 'Updated Return empty container:  of HBL: 123QWER', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-12 18:41:22', '2019-12-12 18:41:22'),
(507, 'Updated Return empty container:  of HBL: 123QWER', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-12 18:42:44', '2019-12-12 18:42:44'),
(508, 'Updated Return empty container:  of HBL: 123QWER', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-12 18:43:56', '2019-12-12 18:43:56'),
(509, 'Updated Return empty container:  of HBL: 123QWER', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-12 18:57:12', '2019-12-12 18:57:12'),
(510, 'Recurso recién creado: SULG20010', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 46, '2019-12-12 19:49:04', '2019-12-12 19:49:04'),
(511, 'Updated Return empty container: QWER1 of HBL: SULG20010', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-13 17:36:08', '2019-12-13 17:36:08'),
(512, 'Updated Operation: SULG20010', 'http://127.0.0.1:8000/operaciones/2', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-13 18:42:47', '2019-12-13 18:42:47'),
(513, 'Updated Operation: SULG20010', 'http://127.0.0.1:8000/operaciones/2', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-13 18:48:33', '2019-12-13 18:48:33'),
(514, 'Recurso recién eliminado: D2019121319SULG20010', 'http://127.0.0.1:8000/operaciones/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-13 18:49:19', '2019-12-13 18:49:19'),
(515, 'Recurso recién eliminado: D2019121337123QWER', 'http://127.0.0.1:8000/operaciones/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-13 18:54:37', '2019-12-13 18:54:37'),
(516, 'Recurso recién creado: SULG20010', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-13 19:42:10', '2019-12-13 19:42:10'),
(517, 'Updated Return empty container: 1 of HBL: SULG20010', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-13 19:50:10', '2019-12-13 19:50:10'),
(518, 'Updated Return empty container: 2 of HBL: SULG20010', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-13 19:50:43', '2019-12-13 19:50:43'),
(519, 'Updated Return empty container: 3 of HBL: SULG20010', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-13 19:51:00', '2019-12-13 19:51:00'),
(520, 'Recurso recién creado: SULG20011', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-19 22:02:43', '2019-12-19 22:02:43'),
(521, 'Updated Return empty container: 1 of HBL: SULG20011', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-19 22:11:23', '2019-12-19 22:11:23'),
(522, 'Updated Return empty container: 2 of HBL: SULG20011', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-19 22:11:40', '2019-12-19 22:11:40'),
(523, 'Updated Return empty container: 3 of HBL: SULG20011', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 46, '2019-12-19 22:12:05', '2019-12-19 22:12:05'),
(524, 'Recurso recién creado: 1234QWE', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2019-12-26 21:46:21', '2019-12-26 21:46:21'),
(525, 'Updated Return empty container: QWE123 of HBL: 1234QWE', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2019-12-26 21:56:05', '2019-12-26 21:56:05'),
(526, 'Recurso recién creado: KUMHO TIRE CO., INC.', 'http://127.0.0.1:8000/entidades', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-02 19:24:14', '2020-01-02 19:24:14'),
(527, 'Recurso recién creado: QINGDAO AWESOME INTERNATIONAL TRADE CO.,LTD', 'http://127.0.0.1:8000/entidades', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-02 19:25:23', '2020-01-02 19:25:23'),
(528, 'Recurso recién creado: ASLS99677328', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-02 19:31:36', '2020-01-02 19:31:36'),
(529, 'Updated Operation: ASLS99677328', 'http://127.0.0.1:8000/operaciones/6', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-02 19:35:37', '2020-01-02 19:35:37'),
(530, 'Updated Operation: ASLS99677328', 'http://127.0.0.1:8000/operaciones/6', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-02 19:48:46', '2020-01-02 19:48:46'),
(531, 'Updated Operation: ASLS99677328', 'http://127.0.0.1:8000/operaciones/6', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-02 19:49:11', '2020-01-02 19:49:11'),
(532, 'Updated Return empty container: ABCD123 of HBL: 1234QWE', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-03 17:24:34', '2020-01-03 17:24:34'),
(533, 'Updated Operation: SULG20010', 'http://127.0.0.1:8000/operaciones/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-03 18:49:14', '2020-01-03 18:49:14'),
(534, 'Updated Operation: SULG20010', 'http://127.0.0.1:8000/operaciones/3', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-03 18:50:04', '2020-01-03 18:50:04'),
(535, 'Recurso recién creado: SULGZ0005448', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 45, '2020-01-03 19:30:55', '2020-01-03 19:30:55'),
(536, 'Recurso recién creado: PBA001', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 45, '2020-01-03 19:44:14', '2020-01-03 19:44:14'),
(537, 'Updated Operation: PBA001', 'http://127.0.0.1:8000/operaciones/8', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 45, '2020-01-03 19:54:34', '2020-01-03 19:54:34'),
(538, 'Updated Return empty container: 123QWER of HBL: PBA001', 'http://127.0.0.1:8000/operation/details/emptyContainer', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 45, '2020-01-03 21:00:19', '2020-01-03 21:00:19'),
(539, 'Recurso recién creado: TIRE DIRECT S.A DE C.V', 'https://starup.com.mx/starupadmin/entidades', 'POST', '189.146.92.56', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 50, '2020-01-08 10:27:20', '2020-01-08 10:27:20'),
(540, 'Recurso recién creado: STARUP LOGISTICS CO., LTD', 'https://starup.com.mx/starupadmin/entidades', 'POST', '189.146.92.56', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 50, '2020-01-08 10:30:22', '2020-01-08 10:30:22'),
(541, 'Recurso recién creado: AIMTS1911016', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.92.56', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 50, '2020-01-08 10:45:07', '2020-01-08 10:45:07'),
(542, 'Updated Return empty container: MRKU4044551 of HBL: AIMTS1911016', 'https://starup.com.mx/starupadmin/operation/details/emptyContainer', 'POST', '189.146.92.56', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 50, '2020-01-08 10:46:11', '2020-01-08 10:46:11'),
(543, 'Updated Return empty container: MRKU4044551 of HBL: AIMTS1911016', 'https://starup.com.mx/starupadmin/operation/details/emptyContainer', 'POST', '189.146.92.56', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-08 10:51:58', '2020-01-08 10:51:58'),
(544, 'Updated Return empty container: MRKU4044551 of HBL: AIMTS1911016', 'https://starup.com.mx/starupadmin/operation/details/emptyContainer', 'POST', '189.146.92.56', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 46, '2020-01-08 10:54:45', '2020-01-08 10:54:45'),
(545, 'Recurso recién creado: SHANTOU', 'http://www.starup.com.mx/starupadmin/puertos', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 51, '2020-01-17 10:10:38', '2020-01-17 10:10:38'),
(546, 'Recurso recién creado: HK MEIDIYU TOYS INDUSTRIAL CO., LIMITED', 'http://www.starup.com.mx/starupadmin/entidades', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 51, '2020-01-17 10:15:04', '2020-01-17 10:15:04'),
(547, 'Recurso recién creado: CEINGIS', 'http://www.starup.com.mx/starupadmin/entidades', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 51, '2020-01-17 10:16:25', '2020-01-17 10:16:25'),
(548, 'Recurso recién creado: SULGZ0005566', 'http://www.starup.com.mx/starupadmin/operaciones', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 51, '2020-01-17 10:18:54', '2020-01-17 10:18:54'),
(549, 'Recurso recién creado: WENZHOU CHIEFSTONE COMMERCER & TRADE CO., LTD.', 'http://www.starup.com.mx/starupadmin/entidades', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 51, '2020-01-17 10:29:38', '2020-01-17 10:29:38'),
(550, 'Recurso recién creado: LINCOLN ELECTRIC MANUFACTURA SA DE CV', 'http://www.starup.com.mx/starupadmin/entidades', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 51, '2020-01-17 10:30:49', '2020-01-17 10:30:49'),
(551, 'Recurso recién creado: SULGZ0005265', 'http://www.starup.com.mx/starupadmin/operaciones', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 51, '2020-01-17 10:31:59', '2020-01-17 10:31:59'),
(552, 'Updated Return empty container: MSKU7670904 of HBL: SULGZ0005265', 'http://www.starup.com.mx/starupadmin/operation/container/emptyContainer', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 51, '2020-01-17 10:35:24', '2020-01-17 10:35:24'),
(553, 'Recurso recién creado: SHANDONG FUYANG BIO-TECH.CO. LTD. PINGYUAN', 'http://starup.com.mx/starupadmin/entidades', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:02:04', '2020-01-17 11:02:04'),
(554, 'Recurso recién creado: PROQUIAB', 'http://starup.com.mx/starupadmin/entidades', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:05:21', '2020-01-17 11:05:21'),
(555, 'Recurso recién creado: SULGZ0005154', 'http://starup.com.mx/starupadmin/operaciones', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:06:42', '2020-01-17 11:06:42'),
(556, 'Updated Return empty container: CBHU4113413 of HBL: SULGZ0005154', 'http://starup.com.mx/starupadmin/operation/container/emptyContainer', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:12:45', '2020-01-17 11:12:45'),
(557, 'Updated Return empty container: CCLU3941619 of HBL: SULGZ0005154', 'http://starup.com.mx/starupadmin/operation/container/emptyContainer', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:13:24', '2020-01-17 11:13:24'),
(558, 'Updated Return empty container: CBHU5845574 of HBL: SULGZ0005154', 'http://starup.com.mx/starupadmin/operation/container/emptyContainer', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:13:36', '2020-01-17 11:13:36'),
(559, 'Updated Return empty container: CSNU1365282 of HBL: SULGZ0005154', 'http://starup.com.mx/starupadmin/operation/container/emptyContainer', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:13:52', '2020-01-17 11:13:52'),
(560, 'Updated Operation: SULGZ0005154', 'http://starup.com.mx/starupadmin/operaciones/3', 'PUT', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:19:59', '2020-01-17 11:19:59'),
(561, 'Updated Operation: SULGZ0005154', 'http://starup.com.mx/starupadmin/operaciones/3', 'PUT', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:21:34', '2020-01-17 11:21:34'),
(562, 'Updated Operation: SULGZ0005154', 'http://starup.com.mx/starupadmin/operaciones/3', 'PUT', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:24:19', '2020-01-17 11:24:19'),
(563, 'Updated Operation: SULGZ0005154', 'http://starup.com.mx/starupadmin/operaciones/3', 'PUT', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 48, '2020-01-17 11:24:35', '2020-01-17 11:24:35'),
(564, 'Se actualizo el usuario : monica@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6IkFFcHcyVEtlUmwxQ240UzdBWFVOaXc9PSIsInZhbHVlIjoiWnVSb2ExSm1ic3hMNHE0a1lmQ1NBdz09IiwibWFjIjoiNDY4OWYwNDU0NTE0MmQ5NzJmZTdiMDRmMjI0NTFkNGVhOTZmNTFhMzc0MmU0NTI5NmMyNTgwNDNlZDA0ZDI4NCJ9', 'PUT', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-17 11:39:31', '2020-01-17 11:39:31'),
(565, 'Recurso recién creado: CAIMI LEON', 'https://starup.com.mx/starupadmin/entidades', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 53, '2020-01-17 11:51:23', '2020-01-17 11:51:23'),
(566, 'Recurso recién creado: SULGZ0005048', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 53, '2020-01-17 11:53:17', '2020-01-17 11:53:17'),
(567, 'Updated Return empty container: TRHU2590029 of HBL: SULGZ0005048', 'https://starup.com.mx/starupadmin/operation/container/emptyContainer', 'POST', '187.170.129.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 53, '2020-01-17 12:04:27', '2020-01-17 12:04:27'),
(568, 'Recurso recién creado: mundra - inmun', 'https://starup.com.mx/starupadmin/puertos', 'POST', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 53, '2020-01-17 12:25:38', '2020-01-17 12:25:38'),
(569, 'Recurso recién creado: oscar@starup.com.mx', 'http://starup.com.mx/starupadmin/users', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1, '2020-01-17 12:38:12', '2020-01-17 12:38:12'),
(570, 'Recurso recién creado: Manzanillo - peru', 'http://starup.com.mx/starupadmin/puertos', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 12:46:02', '2020-01-17 12:46:02'),
(571, 'Recurso recién creado: VISA LOGISTICA', 'http://starup.com.mx/starupadmin/entidades', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 12:48:50', '2020-01-17 12:48:50'),
(572, 'Recurso recién creado: PRUEBA123', 'http://starup.com.mx/starupadmin/operaciones', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 12:57:05', '2020-01-17 12:57:05'),
(573, 'Recurso recién creado: 2222', 'http://starup.com.mx/starupadmin/operation/container/store', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 13:00:55', '2020-01-17 13:00:55'),
(574, 'Recurso recién eliminado: D20200117062222', 'http://starup.com.mx/starupadmin/operation/container/delete', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 13:01:06', '2020-01-17 13:01:06'),
(575, 'Recurso recién creado: PRUEBA1234', 'http://starup.com.mx/starupadmin/operaciones', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 13:06:09', '2020-01-17 13:06:09'),
(576, 'Recurso recién creado: PRUEBA1', 'http://starup.com.mx/starupadmin/operaciones', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 13:10:39', '2020-01-17 13:10:39'),
(577, 'Recurso recién creado: 123C', 'http://starup.com.mx/starupadmin/operaciones', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 13:15:06', '2020-01-17 13:15:06'),
(578, 'Updated Return empty container:  of HBL: PRUEBA1', 'http://starup.com.mx/starupadmin/operation/container/emptyContainer', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 13:18:11', '2020-01-17 13:18:11'),
(579, 'Updated Return empty container: 123YYY of HBL: PRUEBA1234', 'http://starup.com.mx/starupadmin/operation/container/emptyContainer', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 54, '2020-01-17 13:18:47', '2020-01-17 13:18:47'),
(580, 'Se actualizo el usuario : STEPHANiA@starup.com.mx', 'http://starup.com.mx/starupadmin/users/eyJpdiI6IlRnaWNCRXc0SEFsUjRxdUJvU0wrbnc9PSIsInZhbHVlIjoieXhqWFNyVGdHZWhNam9nVSt5UklkQT09IiwibWFjIjoiZmFiY2M1M2Y3NzYwYThkNmRkYzljZTY0OGYyMTNiMjZlODQyZWEyMmNiOTlkNDIxYjU5M2ExNjYxNWIyMzQ5NiJ9', 'PUT', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 1, '2020-01-17 13:48:30', '2020-01-17 13:48:30'),
(581, 'Recurso recién eliminado: D2020011739PRUEBA123', 'http://starup.com.mx/starupadmin/operaciones/1', 'DELETE', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 52, '2020-01-17 13:50:39', '2020-01-17 13:50:39'),
(582, 'Recurso recién creado: MAXTREK TYRE LIMITED', 'http://starup.com.mx/starupadmin/entidades', 'POST', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 52, '2020-01-17 13:59:07', '2020-01-17 13:59:07'),
(583, 'Recurso recién creado: SHEKOU', 'http://starup.com.mx/starupadmin/puertos', 'POST', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 52, '2020-01-17 14:04:44', '2020-01-17 14:04:44'),
(584, 'Recurso recién creado: SULGZ0005320', 'http://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 52, '2020-01-17 14:08:01', '2020-01-17 14:08:01'),
(585, 'Recurso recién eliminado: D2020011758SULGZ0005320', 'http://starup.com.mx/starupadmin/operaciones/1', 'DELETE', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 52, '2020-01-17 15:28:58', '2020-01-17 15:28:58'),
(586, 'Recurso recién creado: SULGZ0005320', 'http://starup.com.mx/starupadmin/operaciones', 'POST', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 52, '2020-01-17 15:29:03', '2020-01-17 15:29:03'),
(587, 'Recurso recién creado: MRKU5747780', 'http://starup.com.mx/starupadmin/operation/container/store', 'POST', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 52, '2020-01-17 15:32:30', '2020-01-17 15:32:30'),
(588, 'Recurso recién creado: 25', 'http://starup.com.mx/starupadmin/operation/container/store', 'POST', '189.146.99.143', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 52, '2020-01-17 15:33:35', '2020-01-17 15:33:35'),
(589, 'Recurso recién creado: SHANGHAI LVDING TRADING CO LTD', 'https://starup.com.mx/starupadmin/entidades', 'POST', '187.189.123.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 49, '2020-01-17 15:50:21', '2020-01-17 15:50:21'),
(590, 'Recurso recién creado: NUEVA GENERACION MANUFACTURAS SA DE CV', 'https://starup.com.mx/starupadmin/entidades', 'POST', '187.189.123.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 49, '2020-01-17 15:51:16', '2020-01-17 15:51:16'),
(591, 'Recurso recién creado: SULGZ00049274', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '187.189.123.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 49, '2020-01-17 15:54:03', '2020-01-17 15:54:03'),
(592, 'Updated Return empty container: MRKU6928680 of HBL: SULGZ00049274', 'https://starup.com.mx/starupadmin/operation/container/emptyContainer', 'POST', '187.189.123.27', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 49, '2020-01-17 15:58:04', '2020-01-17 15:58:04'),
(593, 'Se actualizo el usuario : STEPHANiA@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6IjFyTVwvZnI3MnMwb2Y4dmlUWnBuVWtnPT0iLCJ2YWx1ZSI6ImF3NVwvalpQSnk2VmIzaGh4OVVYb2pRPT0iLCJtYWMiOiI5YTE4ZDhlOWExOWRjOGQwYTE1N2JkNWRhYjlhYjg3YTkxYzdlMjZlODI1YTI2OGQ5Y2EwNmU0Y2ZiMGVhNzlhIn0=', 'PUT', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-20 16:03:15', '2020-01-20 16:03:15'),
(594, 'Se actualizo el usuario : STEPHANiA@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6Ilg2S3JVajU1NG9qR2szTkRTUFNEVFE9PSIsInZhbHVlIjoiUHNmbDlcLytkWld2U0p5MzRwRHhkRnc9PSIsIm1hYyI6IjZmNzIzNDg4ZDc0NzllZGE2NDdjNGNlMmY5YjQ2Nzk2MDU4ZDA2ZGM3YmE0OTY5N2EyOTQwN2Q1MTgyYTRlZmQifQ==', 'PUT', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-20 16:04:07', '2020-01-20 16:04:07'),
(595, 'Recurso recién creado: OTRO', 'https://starup.com.mx/starupadmin/contenedores/8', 'PUT', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-20 16:20:17', '2020-01-20 16:20:17'),
(596, 'Recurso actualizado: MANZANILLO - PERU', 'https://starup.com.mx/starupadmin/puertos/480', 'PUT', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-20 16:21:39', '2020-01-20 16:21:39'),
(597, 'Se actualizo el usuario : STEPHANiA@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6InRLZGdOMXB4dks5Z3ZWSEUwRG4xelE9PSIsInZhbHVlIjoiWjdvUm90YWVSZHh4MXhFaEIrbG5CQT09IiwibWFjIjoiOTBmN2JmM2E2ZTFiMGM2OWY2ODE3MDA1NGI3MjY4NzNkNGQwNzI3YzY1MzBkNWYwZjI5MzFhZTFjNWMyMmQ3MCJ9', 'PUT', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-20 16:22:37', '2020-01-20 16:22:37'),
(598, 'Se actualizo el usuario : STEPHANiA@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6ImVST2lwUGR1T0t4SUpTaVwvaUtYZ3hRPT0iLCJ2YWx1ZSI6ImtjRkRRZ2ljdTlNTFg3Q0hucSt1VWc9PSIsIm1hYyI6ImFiNmFiZDQwYTAwODRkNTY4MjBhZWI0YTRiOGZlZmNkMTIxNTVjMWEwMGNjY2EyMjVmNTAxZGNmZjg1Y2E3ODEifQ==', 'PUT', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-20 16:24:12', '2020-01-20 16:24:12'),
(599, 'Se actualizo el usuario : GIL@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6IkVlMUFkS2NnTXVBN2VNZVlKeFpNbVE9PSIsInZhbHVlIjoia2FwUzB3OGJNXC91T0F6TjVxejBwVkE9PSIsIm1hYyI6IjkwZjBiNDM3MjAxNDdiNWNjMzc3YzQxYTUyZGNhYWNmYjVhMzQzZTg2YTNkNmU0MjVhZTZlMWRiZWRjZjhjYjAifQ==', 'PUT', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-20 16:25:49', '2020-01-20 16:25:49'),
(600, 'Se actualizo el usuario : GIL@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6IkVlMUFkS2NnTXVBN2VNZVlKeFpNbVE9PSIsInZhbHVlIjoia2FwUzB3OGJNXC91T0F6TjVxejBwVkE9PSIsIm1hYyI6IjkwZjBiNDM3MjAxNDdiNWNjMzc3YzQxYTUyZGNhYWNmYjVhMzQzZTg2YTNkNmU0MjVhZTZlMWRiZWRjZjhjYjAifQ==', 'PUT', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-20 16:25:50', '2020-01-20 16:25:50'),
(601, 'Se actualizo el usuario : oscar@starup.com.mx', 'https://starup.com.mx/starupadmin/users/eyJpdiI6IjdhdkJ1Q20wWXdBanNyWGZGRWFhZ3c9PSIsInZhbHVlIjoiN1BWVUpUVldIV0NJSlViekU3SXpKUT09IiwibWFjIjoiMWQwZjY5NGE1OWQxZTA0Y2RhNjczYWNiYWEyYTg2ZDg4NWVlYjU5MjAzNDMyODZiNWFhNThmYzc3ZmEwYWRiNiJ9', 'PUT', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 1, '2020-01-20 16:44:58', '2020-01-20 16:44:58'),
(602, 'Recurso recién creado: ABCD123', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 54, '2020-01-20 16:56:20', '2020-01-20 16:56:20'),
(603, 'Recurso recién creado: PRUEBA111', 'https://starup.com.mx/starupadmin/operaciones', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 54, '2020-01-20 17:08:36', '2020-01-20 17:08:36'),
(604, 'Recurso recién creado: QWER1234', 'https://starup.com.mx/starupadmin/operation/container/store', 'POST', '201.103.141.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 45, '2020-01-20 17:10:35', '2020-01-20 17:10:35'),
(605, 'Recurso recién creado: COM123', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 54, '2020-01-21 17:19:38', '2020-01-21 17:19:38'),
(606, 'Recurso recién creado: COMP1', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 46, '2020-01-21 17:51:58', '2020-01-21 17:51:58');
INSERT INTO `log_activities` (`id`, `subject`, `url`, `method`, `ip`, `agent`, `user_id`, `updated_at`, `created_at`) VALUES
(607, 'Recurso recién eliminado: D2020012139COM123', 'http://127.0.0.1:8000/operaciones/1', 'DELETE', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 54, '2020-01-21 17:56:40', '2020-01-21 17:56:40'),
(608, 'Recurso recién creado: DIMICH', 'http://127.0.0.1:8000/entidades', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 54, '2020-01-21 18:38:35', '2020-01-21 18:38:35'),
(609, 'Recurso recién creado: SULGZ0005464', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 54, '2020-01-21 18:43:56', '2020-01-21 18:43:56'),
(610, 'Recurso recién creado: WAR12', 'http://127.0.0.1:8000/operaciones', 'POST', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 46, '2020-01-21 22:17:52', '2020-01-21 22:17:52'),
(611, 'Updated Operation: WAR12', 'http://127.0.0.1:8000/operaciones/17', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 46, '2020-01-21 22:29:48', '2020-01-21 22:29:48'),
(612, 'Updated Operation: WAR12', 'http://127.0.0.1:8000/operaciones/17', 'PUT', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 46, '2020-01-21 22:44:41', '2020-01-21 22:44:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(1, 'App\\User', 3),
(1, 'App\\User', 10),
(1, 'App\\User', 15),
(1, 'App\\User', 17),
(1, 'App\\User', 18),
(1, 'App\\User', 20),
(1, 'App\\User', 31),
(17, 'App\\User', 1),
(17, 'App\\User', 45),
(17, 'App\\User', 47),
(17, 'App\\User', 48),
(17, 'App\\User', 49),
(17, 'App\\User', 50),
(17, 'App\\User', 51),
(17, 'App\\User', 52),
(17, 'App\\User', 53),
(17, 'App\\User', 54),
(18, 'App\\User', 1),
(18, 'App\\User', 41),
(18, 'App\\User', 44),
(19, 'App\\User', 1),
(20, 'App\\User', 1),
(21, 'App\\User', 46);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('4880cc94ca784c689d8ae112e0c682c9e56e1d1757247832ac001594ef5d4729ca43e72896ceb141', 39, 1, 'MyApp', '[]', 0, '2019-05-05 00:48:40', '2019-05-05 00:48:40', '2020-05-04 14:48:40'),
('cf89e955d59d635c22fb6cb4d60208351e0abdc83875c6284a545bba0bf9d40fdc15ae2a40c192e3', 37, 1, 'MyApp', '[]', 0, '2019-05-03 01:48:26', '2019-05-03 01:48:26', '2020-05-02 15:48:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'Lzk9vFQ6vGf08Av3uFnoAx853HCaim527LYRSvOi', 'http://localhost', 1, 0, 0, '2019-05-02 23:48:07', '2019-05-02 23:48:07'),
(2, NULL, 'Laravel Password Grant Client', 'pr9gCVeqnsDU6sG89V4A1SmEpgn2ZUEhOz7WaDDa', 'http://localhost', 0, 1, 0, '2019-05-02 23:48:07', '2019-05-02 23:48:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-05-02 23:48:07', '2019-05-02 23:48:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('urbansfl@hotmail.com', '$2y$10$lxY.kvNK2cnFIBBe5PRRF.Whg4/UBoZc7904EvtCeLHgOu8yt5pLe', '2018-12-20 15:22:08'),
('santiago@hotmail.com', '$2y$10$ji3cm12Q9Fsyi65hVrDOUuD59yvDmlj1PhHsWjba8PfjE0lorKsOq', '2018-12-22 14:11:14'),
('alisgar9@gmail.com', '$2y$10$MiraERgULR7Q2Fca0UAVMOspwUSGaV3lC3jcSKrwyRsq1k.W4BLGe', '2019-09-27 16:09:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Crear', 'web', '2018-09-11 23:12:40', '2018-09-13 11:23:55'),
(2, 'Editar', 'web', '2018-09-11 23:12:54', '2018-09-13 11:24:05'),
(3, 'Eliminar', 'web', '2018-09-11 23:13:07', '2018-09-13 11:24:25'),
(4, 'Administer roles & permissions', 'web', '2018-09-11 23:13:24', '2018-09-11 23:13:24'),
(15, 'Ver', 'web', '2018-09-13 20:59:44', '2018-09-13 20:59:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2018-09-11 23:15:02', '2018-09-11 23:15:02'),
(17, 'Operador', 'web', '2019-09-20 21:07:47', '2019-09-20 21:07:47'),
(18, 'Finanzas', 'web', '2019-09-20 21:08:31', '2019-09-20 21:08:31'),
(19, 'Ventas', 'web', '2019-09-20 21:09:00', '2019-09-20 21:09:00'),
(20, 'Capturista', 'web', '2019-09-20 21:09:17', '2019-09-20 21:09:17'),
(21, 'GnteOpe', 'web', '2019-11-19 15:42:38', '2019-11-19 15:42:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(2, 1),
(2, 17),
(2, 18),
(2, 19),
(2, 21),
(3, 1),
(3, 17),
(3, 18),
(3, 21),
(4, 1),
(15, 1),
(15, 17),
(15, 18),
(15, 19),
(15, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin` tinyint(1) DEFAULT 0,
  `token_login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `admin`, `token_login`, `approved_at`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'Admin', 'alicia@starup.com.mx', NULL, '$2y$10$FLxD2m.ZVsJsE7BdiPn1LewkFWRqMgoEY7aADiP4du2EzSBI02YLq', 'utFdDXjnBne0U2PZrUZImXuJQr1MbMpucHfIoOaCLGRAHRbuB6Tks5hEEeJr', 1, '', '2019-01-22 00:00:00', 0, '2018-09-20 22:22:38', '2019-10-02 11:37:34'),
(44, 'Gilberto Meza', 'gil', 'GIL@starup.com.mx', NULL, '$2y$10$FZNU5Hz/6.AxfTKEYoUiG.HUuzVtPuQpKmP5o7OFDcrKC4JHdn5U6', 'LMGikFx9nVDOEO5Au5zDy2OwxXBVeSrBUlr9J38yTCCPiSQVYmLdjCE9PcVT', 0, NULL, '2019-10-01 14:09:19', 1, '2019-10-01 14:07:33', '2020-01-20 16:25:49'),
(45, 'operador', 'operador', 'operador@starup.com.mx', NULL, '$2y$10$aIcOJvlGSb//Q.SU8OVPQOTBFLNG6DaQQwrBc8QuwMRLY4qtfDwum', 'VD9ITI9QKTK9kQpCmmWC0PHBVg9waOyy42kXq9LA7e3zGkCaiQQkCMI3Oo0L', 0, NULL, '2019-10-11 15:49:07', 1, '2019-10-11 15:43:18', '2019-10-11 15:49:07'),
(46, 'JADE ATZIN CARBAJAL GUTIERREZ', 'jade', 'jade@starup.com.mx', NULL, '$2y$10$.DoL9d8ED/MyEsK51mG61eRvT1MmZhDOFlVrAKyH2C5T85EFNLehq', 'CEMZDY55JcvLfUYcpUT1s0YPMYN3LudRUE8s6IOLr1PLoUR7yQzSgYL9eQs8', 2, NULL, '2019-11-20 11:57:52', 1, '2019-11-19 15:43:20', '2019-11-20 11:57:52'),
(47, 'JAQUELINE DUARTE MADRIGAL', 'operador2', 'jaqueline@starup.com.mx', NULL, '$2y$10$yxau97dc2dhYfL.TepJBweEGjLawBi/kahow9F1BEBWZI/Q1Sy6wO', 'qZqIVZznzlCjDMWFNEBV3BZbbTHmBu5HBbrTQnqAOwkK9rhJthRHsJMme46j', 0, NULL, '2019-11-27 15:25:33', 1, '2019-11-27 15:25:26', '2019-12-03 14:03:29'),
(48, 'RICARDO ISAAC MARTÍNEZ BRAVO', 'ricardo', 'ricardo@starup.com.mx', NULL, '$2y$10$RragWhVgIzF6K0CQGFSzsufA9E3Y4QK7Wli7A42lL.pOZ39/Xt2tS', NULL, 0, NULL, '2019-12-03 14:14:30', 1, '2019-12-03 14:04:28', '2019-12-03 14:14:30'),
(49, 'ZAIDEE SALGADO PALOMO', 'ZAIDEE', 'zaydee@starup.com.mx', NULL, '$2y$10$LRR8G6.SD0z8l1FxtLDguuQ8t2egGXGftL6iJrHEjl79ElTEKl6wy', NULL, 0, NULL, '2019-12-03 14:14:34', 1, '2019-12-03 14:05:38', '2019-12-03 14:14:34'),
(50, 'YIYARI YATZIL ZUÑIGA JIMENEZ', 'YIYARI', 'yatzil@starup.com.mx', NULL, '$2y$10$n8ymFQXGRYaZOMpdIW4/derPleDyQCKER4LPCr8kGWG73e.1SjcHO', 'zQ9swESXcODcCxLpMR5P5rKZd1IGCUX3WNMzc51RDKt8Cp8gYqqt2z79hAxT', 0, NULL, '2019-12-03 14:14:37', 1, '2019-12-03 14:06:32', '2019-12-03 14:14:37'),
(51, 'CHRISTIAN MEDA RODRÍGUEZ', 'CHRISTIAN', 'christian@starup.com.mx', NULL, '$2y$10$ItPWa5aLm8.Z0iciBWBsJOw32oMkvcMcFFsoOtydYe2HgQt4ZZbYC', 'JlAmS2YXIaGdbjDkccLhrhWT27TRswEWVY0qdOj4jq16tv4icyHbfhEwczK7', 0, NULL, '2019-12-03 14:14:40', 1, '2019-12-03 14:07:23', '2019-12-03 14:14:40'),
(52, 'STEPHANÍA TORRES SÁNCHEZ', 'STEPHANÍA', 'stephania@starup.com.mx', NULL, '$2y$10$GQoW5ks.Bjmfp0vcDAMbA.Z1V8bm.Aju1NPvnv.nOy2KA0L4qqVji', '3Zc5fmNvHHnNdMf5ILByeg7sajZKU5MnP9zTxV3RoK9aiTDAobtySClxpxWr', 0, NULL, '2019-12-03 14:14:44', 1, '2019-12-03 14:09:12', '2020-01-17 13:48:30'),
(53, 'MÒNICA ALEJANDRA HERNÀNDEZ TORRES', 'monica', 'monica@starup.com.mx', NULL, '$2y$10$dbEmPB/.dRLRJtEu8HIYuOS.KBQxa2AM4o7PHsy2EhkitROcefB36', NULL, 0, NULL, '2019-12-03 14:14:50', 1, '2019-12-03 14:14:07', '2020-01-17 11:39:31'),
(54, 'Oscar Daniel Ramon Velez', 'oscar@starup.com.mx', 'oscar@starup.com.mx', NULL, '$2y$10$EBPEEvR9FrT18Drsb7yace7AvYgUn7bFw02HHrFlNrQQLD6K2EPLS', 'yD40jmUnRS6gMxHr8m2zjgX41AaYc26V5TcqZSRDnPwHBAkOerLJ7rNUAJpk', 0, NULL, '2020-01-17 12:40:32', 1, '2020-01-17 12:38:12', '2020-01-20 16:44:58');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adm_bancos`
--
ALTER TABLE `adm_bancos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_cargo`
--
ALTER TABLE `adm_cargo`
  ADD PRIMARY KEY (`idcargo`);

--
-- Indices de la tabla `adm_clasificacion`
--
ALTER TABLE `adm_clasificacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_contenedor`
--
ALTER TABLE `adm_contenedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_entidad`
--
ALTER TABLE `adm_entidad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_entidad_bancos`
--
ALTER TABLE `adm_entidad_bancos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_incoterm`
--
ALTER TABLE `adm_incoterm`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_liberacion`
--
ALTER TABLE `adm_liberacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_operaciones`
--
ALTER TABLE `adm_operaciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hbl` (`hbl`);

--
-- Indices de la tabla `adm_operaciones_contenedor`
--
ALTER TABLE `adm_operaciones_contenedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_operaciones_usuarios`
--
ALTER TABLE `adm_operaciones_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_paises`
--
ALTER TABLE `adm_paises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_puertos`
--
ALTER TABLE `adm_puertos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_user_details`
--
ALTER TABLE `adm_user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `log_activities`
--
ALTER TABLE `log_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adm_bancos`
--
ALTER TABLE `adm_bancos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `adm_cargo`
--
ALTER TABLE `adm_cargo`
  MODIFY `idcargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `adm_clasificacion`
--
ALTER TABLE `adm_clasificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `adm_contenedor`
--
ALTER TABLE `adm_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `adm_entidad`
--
ALTER TABLE `adm_entidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT de la tabla `adm_entidad_bancos`
--
ALTER TABLE `adm_entidad_bancos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `adm_incoterm`
--
ALTER TABLE `adm_incoterm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `adm_liberacion`
--
ALTER TABLE `adm_liberacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `adm_operaciones`
--
ALTER TABLE `adm_operaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `adm_operaciones_contenedor`
--
ALTER TABLE `adm_operaciones_contenedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `adm_operaciones_usuarios`
--
ALTER TABLE `adm_operaciones_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `adm_paises`
--
ALTER TABLE `adm_paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT de la tabla `adm_puertos`
--
ALTER TABLE `adm_puertos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=482;

--
-- AUTO_INCREMENT de la tabla `adm_user_details`
--
ALTER TABLE `adm_user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `log_activities`
--
ALTER TABLE `log_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=613;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
