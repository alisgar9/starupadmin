function add(){

     $('input[name="cy"]').click(function(){
            var inputValue = $(this).attr("value");
            if(inputValue == 2){
                $("#div-cydoor" ).append('<div class="col-md-12">{{ Form::label("cdt", "CY Door Type *") }}</div><div class="form-group"><div class="col-md-6">{!! Form::select("cy_door", array("" => "Choose one...","1" => "All Truck", "2" => "Train - Truck", "3" => "Train + Ramp"), old("cy_door"), ["class" => "form-control", "id" => "cdt"]); !!}</div></div>');
            }else{ $("#div-cydoor" ).empty(); }
        });
        

        $('input[name="tipo_operacion"]').click(function(){
            var inputValue = $(this).attr("value");
            if(inputValue == 1){
                $('#details-text').empty();
                $('#view-vessel-div').show(); 
                $('#details-text').append("<i class='fa fa-archive' aria-hidden='true'></i> Container Details");
                $('#hbl-text').text("House (HBL) *"); 
                $('#mbl-text').text("MBL (MBL)");
                $('#shl-text').text("shipping line"); 
                $('#vogage-text').text("Voyage *");
                $('#vessel-text').text("Vessel *");
                $('#shl-text').text("Shipping Line");
                $('#dt-ct-ct').empty(); $('#dt-ct-ct').append("<b># Ctnr *</b>");
                $('#dt-ct-sl').empty(); $('#dt-ct-sl').append("<b>Ctnr Seal*</b>");
                $('#dt-ct-mk').empty(); $('#dt-ct-mk').append("<b>Marks</b>");
                $('#dt-ct-tc').empty(); $('#dt-ct-tc').append("<b>Type Ctnr</b>");
                $('#voyage').show(); 
                $('#HRD-div').show(); 
                $('#MRD-div').show();
                $('#FDDC-div').show(); 
                $('#FDDSL-div').show();
                $('#free_demurrages_days_cust').val('21'); 
                $('#free_demurrages_days_sl').val('21');
                $('#view-HRM').show(); 
                $('#view-MRM').show(); 
                $("#div-terrestre").empty();
                $("#div-maritima").empty(); 
                $('#div-maritima').show();
                $('#dt-ct-ct').show(); 
                $('#dt-ct-sl').show();  
                $('#dt-ct-mk').show();
                $('#dt-ct-tc').show(); 
                $('#dt-ct-ct-input').show(); 
                $('#dt-ct-sl-input').show();
                $('#dt-ct-mk-input').show(); 
                $('#dt-ct-tc-input').show();
                $('#dt-ct-cwe').hide(); 
                $('#dt-ct-cwe-input').hide();
                $('#POL-text').text("Select POL *");
                $('#POD-text').text("Select POD *");

                $("#div-maritima" ).append('<div class="col-md-12"><br>{{ Form::label("hbl", "Operation Profile *") }}</div><div class="form-group"><div class="col-md-6"><input type="radio" name="operacion_detail" {{old('operacion_detail')==2 ? 'checked='.'"'.'checked'.'"' : '' }}  value="2" id="fcl" class="form-radio"> <label class="text-small" for="fcl">FCL </label></div><div class="col-md-6"><input type="radio"  name="operacion_detail" {{old('operacion_detail')==1 ? 'checked='.'"'.'checked'.'"' : '' }} value="1" id="LCL" class="form-radio"> <label for="LCL" class="text-small">LCL </label></div></div>');
               
                $('input[name="operacion_detail"]').click(function(){
                    var tipo_detail = $(this).attr("value");
                    if(tipo_detail == 2)
                    {
                        $("#botonadd").show();
                    }
                    else{
                        $("#botonadd").hide();  
                    }
                });
            }
            else if(inputValue == 2){
                $('#dt-ct-ac').hide(); 
                $("#botonadd").hide(); 
                $('#view-vessel-div').hide(); 
                $("#div-terrestre").empty(); 
                $("#div-maritima").empty(); 
                $('#hbl-text').text("House (HWB) *");
                $('#mbl-text').text("MBL (MWB)"); 
                $('#shl-text').text("Air Line");
                $('#voyage').show();
                $('#vogage-text').text("Flight *"); 
                $('#free_demurrages_days_cust').val('3');
                $('#free_demurrages_days_sl').val('3'); 
                $('#view-HRM').hide();
                $('#view-MRM').hide(); 
                $('#details-text').empty();
                $('#details-text').append("<i class='fa fa-archive' aria-hidden='true'></i> Cargo Details");
                $('#dt-ct-ct').hide(); 
                $('#dt-ct-sl').hide(); 
                $('#dt-ct-mk').hide();
                $('#dt-ct-tc').hide(); 
                $('#dt-ct-ct-input').hide(); 
                $('#dt-ct-sl-input').hide();
                $('#dt-ct-mk-input').hide(); 
                $('#dt-ct-tc-input').hide(); 
                $('#dt-ct-cwe').show();
                $('#dt-ct-cwe-input').show();            
            }  
            else if(inputValue == 3){
                $('#dt-ct-cwe').hide(); 
                $('#dt-ct-cwe-input').hide(); 
                $('#vessel-text').text("Unit No. *");
                $('#shl-text').text("In land Carrier"); 
                $('#details-text').empty();
                $('#details-text').append("<i class='fa fa-archive' aria-hidden='true'></i> Units Details");
                $('#voyage').hide(); 
                $('#HRD-div').hide(); 
                $('#MRD-div').hide(); 
                $('#FDDC-div').hide();
                $('#FDDSL-div').hide(); 
                $("#botonadd").show(); 
                $("#div-maritima").empty();
                $("#div-terrestre").empty(); 
                $('#div-terrestre').show(); 
                $('#dt-ct-ct').empty();
                $('#dt-ct-ct').show(); 
                $('#dt-ct-ct').append("<b># Box *</b>"); 
                $('#dt-ct-ct-input').show();
                $('#dt-ct-sl-input').hide(); 
                $('#dt-ct-mk-input').hide(); 
                $('#dt-ct-tc-input').hide();
                $('#dt-ct-sl').hide(); 
                $('#dt-ct-mk').hide(); 
                $('#dt-ct-tc').hide();
                $('#view-vessel-div').show();
                $('#view-HRM').hide();
                $('#view-MRM').hide();
                $('#POL-text').text("Select Origin *");
                $('#POD-text').text("Select Destination *");
                
                
                $("#div-terrestre").append('<div class="col-md-12"><br>{{ Form::label("hbl", "Operation Profile *")}}</div><div class="form-group"><div class="col-md-6"><input type="radio"  required name="operacion_detail" value="1" {{old('operacion_detail')==1 ? 'checked='.'"'.'checked'.'"' : '' }} id="Nacional" class="form-radio"> <label for="Nacional" class="text-small">Nacional</label></div><div class="col-md-6"><input type="radio" name="operacion_detail" value="2" {{old('operacion_detail')==2 ? 'checked='.'"'.'checked'.'"' : '' }} id="Internacional" class="form-radio"> <label for="Internacional" class="text-small">Internacional </label></div></div>');
            }
            else{
                $("#div-terrestre").empty(); $("#div-maritima").empty(); 
                $("#botonadd" ).hide();
            }
        
        });

        

        /** Autocomplete **/

    $('#basics').ready(function(){
        $('input[name="tipo_operacion"]').click(function(){
            var inputValue = $(this).attr("value");
            puertoSearch("basics", "POL", inputValue);
        });
    });

    $('#podsearch').ready(function(){
        $('input[name="tipo_operacion"]').click(function(){
            var inputValue = $(this).attr("value");
            puertoSearch("podsearch", "POD", inputValue);
        });
    });

    $('#shipper_desc').ready(function(){
        entidadSearch("shipper", "shipper_desc");
    });

    $('#consignee_desc').ready(function(){
        entidadSearch("consignee", "consignee_desc");
    });

    $('#agente_desc').ready(function(){
        entidadSearch("agente", "agente_desc");
    });

    $('#agente_envio_desc').ready(function(){
        entidadSearch("agente_envio", "agente_envio_desc");
    });
    

    $('#shipping_line_desc').ready(function(){

        $('input[name="tipo_operacion"]').click(function(){
            var inputValue = $(this).attr("value");
            var valor = '';
            if(inputValue == 1){
                valor = 'shipping_line';

            }else if(inputValue == 2){
                valor = 'air_line';

            }else if(inputValue == 3){
                valor = 'carrier_line';
            }else{
                valor = 'shipping_line';
            }   

            shlentidadSearch(valor, "shipping_line_desc");   

        });
        
    });


    function puertoSearch(etiqueta, valor, type){
            $("#"+etiqueta).easyAutocomplete({
                url: function(search) {
                    return "{{route('autocomplete.fetch')}}?search=" + search + "&type=" + type;
                },

                getValue: "puerto",
                list: {
                        onSelectItemEvent: function() {
                            var puerto = $("#" + etiqueta).getSelectedItemData().id;
                            $("#"+valor).val(puerto);
                        
                        },
                        match: {
                            enabled: true
                        }
                }
            });
    }

    function entidadSearch(tipo,etiqueta){
        $("#"+etiqueta).easyAutocomplete({
            url: function(search) {
                return "{{route('autocomplete.entidad')}}?tipo=" + tipo +"&search=" + search;
            },
        
            getValue: "nombre",
            list: {
                    onSelectItemEvent: function() {
                        var dataID = $("#"+etiqueta).getSelectedItemData().id;
                        $("#"+tipo).val(dataID);
                    },
            match: {
                     enabled: true
                    }
            }
        });
    }

    function shlentidadSearch(tipo,etiqueta){

        $("#"+etiqueta).easyAutocomplete({
            url: function(search) {
                return "{{route('autocomplete.entidad')}}?tipo=" + tipo +"&search=" + search;
            },
        
            getValue: "nombre",
            list: {
                    onSelectItemEvent: function() {
                        var dataID = $("#"+etiqueta).getSelectedItemData().id;
                        $("#shipping_line").val(dataID);
                    },
            match: {
                     enabled: true
                    }
            }
        });
    }
    
}