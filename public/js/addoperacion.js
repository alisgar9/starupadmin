$(document).ready(function(){

    $('#basics').change(function() {
        $("#POL").val("");    
    });

    autocompletar();

    function autocompletar(){

        $("#basics").easyAutocomplete({
     

            url: function(search) {
                return "{{route('autocomplete.fetch')}}?search=" + search;
            },
        
            getValue: "puerto",
            list: {
                    onSelectItemEvent: function() {
                        var puerto = $("#basics").getSelectedItemData().id;
                        $("#POL").val(puerto);
                    
                    }
            }
        
        });

    }

    
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        if(inputValue == 1){
            $("#div-terrestre").empty();
            $('#div-maritima').show();
            $("#div-maritima" ).append('<div class="col-md-12">{{ Form::label("hbl", "Perfil  de Operación") }}</div><div class="form-group"><div class="col-md-6"><input type="radio" name="demo" value="1" id="maritima" class="form-radio"> <label for="maritima">LCL (40 HC)</label></div><div class="col-md-6"><input type="radio" name="demo" value="2" id="aerea" class="form-radio"> <label for="aerea">FCL (Contenedor) </label></div></div>');
        }    
        else if(inputValue == 3){
            $("#div-maritima").empty();
            $('#div-terrestre').show();
            $("#div-maritima" ).append('<div class="col-md-12">{{ Form::label("hbl", "Perfil  de Operación")}}</div><div class="form-group"><div class="col-md-6"><input type="radio" name="demo" value="1" id="maritima" class="form-radio"> <label for="maritima">Nacional</label></div><div class="col-md-6"><input type="radio" name="demo" value="2" id="aerea" class="form-radio"> <label for="aerea">Internacional </label></div></div>');
        }
        else{
            $("#div-terrestre").empty();
            $("#div-maritima").empty();
        }
       
    });

    
    



});