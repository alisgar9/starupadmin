<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home')->middleware('auth');
Route::middleware(['approved'])->group(function () {

    Route::get('/home', 'HomeController@home')->name('home');

    
});

Route::group(['middleware' => 'auth'], function () {
   
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});

Auth::routes();

Route::get('logActivity', 'HomeController@logActivity');
Route::get('get-data-eventos-datatables', ['as'=>'get.eventos','uses'=>'HomeController@getData']);

/* Usuarios */
Route::resource('users', 'UserController');
Route::get('get-data-usuarios-datatables', ['as'=>'get.usuarios','uses'=>'UserController@getData']);

/* Bancos */
Route::resource('bancos', 'BancosController');
Route::get('get-data-bancos-datatables', ['as'=>'get.bancos','uses'=>'BancosController@getData']);

/* Entidades */
Route::resource('entidades', 'EntidadesController');
Route::post('entidades/update','EntidadesController@update')->name('entidades/update');
Route::get('get-data-entidades-datatables', ['as'=>'get.entidades','uses'=>'EntidadesController@getData']);

/* Entidades Banco*/
Route::resource('entidadbanco', 'EntidadesBancoController');
Route::get('get-data-entidades-banco-datatables', ['as'=>'get.entidades.banco','uses'=>'EntidadesController@getDataBanco']);
Route::get('get-data-ent-banco-datatables', ['as'=>'get.ent.banco','uses'=>'EntidadesBancoController@getDataBancoView']);

/* clasificacion admin */
Route::resource('clasificaciones', 'ClasificacionController');
Route::get('get-data-clasificaciones-datatables', ['as'=>'get.clasificaciones','uses'=>'ClasificacionController@getData']);

/* roles */
Route::resource('roles', 'RoleController');

/* permisos */
Route::resource('permissions', 'PermissionController');

/* contenedores operaciones */
Route::resource('contenedores', 'ContenedorController');
Route::get('get-data-contenedores-datatables', ['as'=>'get.contenedores','uses'=>'ContenedorController@getData']);

/* puertos operaciones */
Route::resource('puertos', 'PuertoController');
Route::get('get-data-puertos-datatables', ['as'=>'get.puertos','uses'=>'PuertoController@getData']);

/* catalogo aeropuertos de  operaciones */
Route::resource('airports', 'AeropuertoController');
Route::get('get-data-airports-datatables', ['as'=>'get.airports','uses'=>'AeropuertoController@getData']);

/* catalogo estados de  operaciones */
Route::resource('states', 'EstadoController');
Route::get('get-data-states-datatables', ['as'=>'get.states','uses'=>'EstadoController@getData']);

/* operaciones -- operaciones */
Route::resource('operaciones', 'OperacionController');
Route::get('get-data-operaciones-datatables', ['as'=>'get.operaciones','uses'=>'OperacionController@getData']);
Route::post('operaciones/edit/hbl', 'OperacionController@updateHBL')->name('operaciones/edit/hbl');
/*Agregar detalle de contenedor*/
Route::get('operaciones/dcontainer/{idoperation}/{typeoperation}/{operaciondetail}','OperacionController@createdetails')->name('operaciones.dcontainer');
Route::post('operaciones/addDetails', 'OperacionController@addDetails')->name('operaciones/addDetails');

Route::get('get-data-columns', ['as'=>'get.columns','uses'=>'OperacionController@getColumns']);
Route::post('operaciones-puerto','OperacionController@puertoSearch');
Route::get('autocomplete/fetch', 'OperacionController@puertoSearch')->name('autocomplete.fetch');
Route::get('autocomplete/entidad', 'OperacionController@entidadSearch')->name('autocomplete.entidad');
Route::post('operaciones-files','OperacionController@files');
Route::get('operaciones/view/{file}','OperacionController@viewFile')->name('view.file');
Route::post('operaciones/update/telex', 'OperacionController@telex')->name('operaciones/update/telex');
Route::post('operaciones/restore', 'OperacionController@restorePreOperation')->name('operaciones/restore');


/** operacion detalle contenedor  **/
Route::post('operation/container/store','OperationDetails@store')->name('container.store');
Route::post('operation/container/update','OperationDetails@update')->name('container.update');
Route::post('operation/container/delete','OperationDetails@destroy')->name('container.delete');
Route::get('get-data-container-details', ['as'=>'get.container.details','uses'=>'OperationDetails@getData']);
/** Busqueda por contenedor **/
Route::get('operacion/contenedor/filter','OperationDetails@search')->name('operationdetail.filter');
Route::get('get-containers', ['as'=>'get.containers','uses'=>'OperacionController@getContainer']);
Route::get('get-data-container-filter', ['as'=>'get.container.filter','uses'=>'OperationDetails@getDataDetails']);
/** Retornar vacio contenedor **/
Route::post('operation/container/emptyContainer','OperationDetails@emptyContainer')->name('container.emptyContainer');

Route::post('operation/details/emptyContainer','OperacionDetalleController@emptyContainer')->name('operationdetail.emptyContainer');
Route::get('get-data-operacion-filter', ['as'=>'get.operacion.filter','uses'=>'OperacionDetalleController@getDataDetails']);

/* Demurrage  Reports */

Route::get('report/demurrage', ['as'=>'get.demurrage','uses'=>'ReportDemurrageController@Demurrage']);
Route::post('generate/demurrage', 'ReportDemurrageController@getData')->name('generate/demurrage');
Route::get('demurrage/download/{custom}/{dateStart}/{dateFinish}', 'ReportDemurrageController@generateReportxls')->name('demurrage/download');
/* Demurrage  General */
Route::get('report/general', ['as'=>'get.general','uses'=>'ReportGeneralController@Demurrage']);
Route::post('generate/general', 'ReportGeneralController@getData')->name('generate/general');
Route::get('general/download/{custom}/{dateStart}/{dateFinish}', 'ReportGeneralController@exportGenerate')->name('demurrage/download');


Route::get('report/download', 'ReportDemurrageController@downloadExcel');
/* TTR Report */
Route::get('report/ttr', ['as'=>'get.general','uses'=>'ReportTTRController@Demurrage']);
Route::post('generate/ttr', 'ReportTTRController@getData')->name('generate/general');
Route::get('ttr/download/{custom}/{dateStart}/{dateFinish}', 'ReportTTRController@exportGenerate')->name('demurrage/download');

/* Containers Report */
Route::get('report/containers', ['as'=>'get.containersR','uses'=>'ReportContainersController@index']);


Route::get('containers/download/{custom}/{dateStart}/{dateFinish}', 'ReportContainersController@generateReportxls')->name('demurrage/download');

/*
/* cargos admin */
Route::resource('cargos', 'CargoController');
Route::get('get-data-cargos-datatables', ['as'=>'get.cargos','uses'=>'CargoController@getData']);

Route::get('/users/{user_id}/approve', 'UserController@approve')->name('admin.users.approve');

Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});

Route::get('approval', 'HomeController@approval', function () {
    return abort(404);
});

Route::get('mail/operation', 'EmailsController@vista');
//Route::get('mail/send', 'EmailsController@build');
Route::get('mail/send', 'OperacionController@sendMailCreate');
Route::get('excel', 'ReportGeneralController@excel');
Route::get('/export-users', 'ReportGeneralController@importOperation');
Route::get('/export', 'ReportGeneralController@exportUserData');

/* Portal Acceso a Clientes */
Route::resource('customer', 'CustomerController');
Route::get('get-data-customer-datatables', ['as'=>'get.customer','uses'=>'CustomerController@getData']);
Route::post('customer/delete', 'CustomerController@delete')->name('customer/delete');
Route::post('customer/create', 'CustomerController@add')->name('customer/create');


