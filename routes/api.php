<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1','middleware' => 'auth:api'], function () {
    //    Route::resource('task', 'TasksController');

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_api_routes
});

Route::resource('operations', 'APIController');
Route::get('operation/{idoperation}','APIController@showOperation')->name('operation');

Route::get('conteiners/{consignee}/{status}','APIController@showContainers')->name('conteiners');

Route::get('datos/{consignee}/{status}','APIController@datos')->name('datos');
/** 
 * Reporte General
*/
Route::get('report/general/{etas}/{etast}/{consignee}','APIController@reportGeneral')->name('report/general');
Route::post('report/download','APIController@reportGeneralDownload')->name('report/download');

/** 
 * Reporte Demoras
*/
Route::get('report/demurrage/{etas}/{etast}/{consignee}','APIController@getDataDemurrage')->name('report/demurrage');
Route::post('demurrage/download','APIController@reportDemurrageDownload')->name('demurrage/download');

/** 
 * Reporte TT
*/
Route::get('report/tt/{etas}/{etast}/{consignee}','APIController@getDataTT')->name('report/tt');
Route::post('tt/download','APIController@exportGenerateTT')->name('tt/download');

/** 
 * Reporte Containers
*/
Route::get('report/containers/{etas}/{etast}/{consignee}','APIController@getDataContainers')->name('report/containers');
Route::post('containers/download','APIController@generateReportContainersxls')->name('containers/download');

/** 
 * Cards en home
*/
Route::get('cards/{consignee}/{tipo}','APIController@cards')->name('cards/tipo');
