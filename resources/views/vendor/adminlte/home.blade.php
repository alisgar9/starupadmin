@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Dashboard<small>Tablero</small></h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $countUsers }}</h3>

          <p>Usuarios</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        @hasrole('Admin')
        <a href="{{ url('users') }}" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
        @endhasrole
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $countMunicipios }}</h3>

          <p>Municipios</p>
        </div>
        <div class="icon">
          <i class="fa fa-map-marker"></i>
        </div>
        @hasrole('Admin')
        <a href="{{ url('municipios') }}" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
        @endhasrole
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $countAreas }}</h3>

          <p>Áreas</p>
        </div>
        <div class="icon">
          <i class="fa fa-map"></i>
        </div>
        @hasrole('Admin')
        <a href="{{ url('areas') }}" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
        @endhasrole
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $countRecibos }}</h3>

          <p>Recibos</p>
        </div>
        <div class="icon">
          <i class="fa fa-file-text"></i>
        </div>
        @hasrole('Admin')
        <a href="{{ url('recibos') }}" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
        @endhasrole
      </div>
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->

  <!-- .row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Análisis de los Meses Facturados</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>

          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-8">


              <div class="chart">
                <!-- Sales Chart Canvas -->

                <div class="panel-body">
                  {!! $chartBar->html() !!}
                </div>
              </div>
              <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Uso de Almacenamiento por Municipio</strong>
              </p>
              <div class="direct-chat-messages">
                <!-- Message -->
                @foreach ($municipioMeses as $itemMunicipio)
                <div class="progress-group">
                  <span class="progress-text">{{ $itemMunicipio->municipio }}</span>
                  <span class="progress-number"><b>{{ $itemMunicipio->count }}</b>/500</span>
                  <div class="progress progress-sm active">
                    <div class="progress-bar progress-bar-green progress-bar-striped" style="width: {{ ($itemMunicipio->count * 100) / 500 }}%"></div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- ./box-body -->

        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->


  <!-- .row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Análisis de los Meses Facturados</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>

          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">


              <div class="chart">
                <!-- Sales Chart Canvas -->

                <div class="panel-body">
                  {!! $chartPline->html() !!}
                </div>
              </div>
              <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->

          </div>
          <!-- /.row -->
        </div>
        <!-- ./box-body -->

        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->


</section>
{!! Charts::scripts() !!}
{!! $chartBar->script() !!}
{!! $chartPline->script() !!}
@endsection