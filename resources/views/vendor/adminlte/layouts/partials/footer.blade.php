<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
       
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date("Y"); ?></strong> <a href="https://starup.com.mx/"> <img src="{{URL::asset('/img/footer-logo.png')}}" class="user-image" alt="User Image"/></a>
</footer>
