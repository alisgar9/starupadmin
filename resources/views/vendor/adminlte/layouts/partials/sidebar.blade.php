<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{URL::asset('/img/avatar-user.png')}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> on-line</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Main</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ Request::path() == 'home' ? 'active' : '' }}"><a href="{{ url('home') }}"><i class='fa fa-tachometer'></i> <span>Dashboard</span></a></li>
            @hasrole('Admin')
            <li class="{{ Request::path() == 'logActivity' ? 'active' : '' }}"><a href="{{ url('logActivity') }}"><i class='fa fa-database'></i> <span>Bitácora</span></a></li>
            @endhasrole
            
            <!-- Only Admin -->
            @hasrole('Admin')
            <li class="header">ADMINISTRACIÓN DE USUARIOS</li>
            <li class="{{ strpos(Request::path(), 'users') === 0 ? 'active' : '' }}"><a href="{{ url('users') }}"><i class='fa fa-users'></i> <span>Usuarios</span></a></li>
            <li class="{{ Request::path() == 'cargos' ? 'active' : '' }}"><a href="{{ url('cargos') }}"><i class='fa fa-user'></i> <span>Cargos</span></a></li>
            <li class="{{ strpos(Request::path(), 'permissions') === 0 ? 'active' : '' }}"><a href="{{ url('permissions') }}"><i class='fa fa-key'></i> <span>Permisos</span></a></li>
            <li class="{{ Request::path() == 'roles' ? 'active' : '' }}"><a href="{{ url('roles') }}"><i class='fa fa-lock'></i> <span>Perfiles</span></a></li>
            @endhasrole
            <!-- /.Only Admin -->

            
            @hasrole('Finanzas')
            <li class="header">ADMINISTRACIÓN DE ENTIDADES</li>
            <li class="{{ strpos(Request::path(), 'entidades') === 0 ? 'active' : '' }}"><a href="{{ url('entidades') }}"><i class='fa fa-address-book'></i> <span>Entidades</span></a></li>
            <li class="{{ Request::path() == 'clasificaciones' ? 'active' : '' }}"><a href="{{ url('clasificaciones') }}"><i class='fa fa-list'></i> <span>Clasificación</span></a></li>
            <li class="{{ Request::path() == 'bancos' ? 'active' : '' }}"><a href="{{ url('bancos') }}"><i class='fa fa-university'></i> <span>Bancos</span></a></li>
            @endhasrole
            @hasrole('Operador')
            <li class="header">OPERATIVE ADMINISTRATION</li>
            <li class="{{ strpos(Request::path(), 'operaciones') === 0 ? 'active' : '' }}"><a href="{{ url('operaciones') }}"><i class='fa fa-exchange'></i> <span>Operations</span></a></li>
            <li class="{{ strpos(Request::path(), 'operacionesdetails') === 0 ? 'active' : '' }}"><a href="{{ URL::to('operacion/contenedor/filter') }}"><i class='fa fa-search'></i> <span>Filter by Container</span></a></li>
            <li class="{{ strpos(Request::path(), 'entidades') === 0 ? 'active' : '' }}"><a href="{{ url('entidades') }}"><i class='fa fa-address-book'></i> <span>Entities</span></a></li>
            <li class="{{ Request::path() == 'contenedores' ? 'active' : '' }}"><a href="{{ url('contenedores') }}"><i class='fa fa-archive'></i> <span>Containers</span></a></li>
            <li class="{{ Request::path() == 'puertos' ? 'active' : '' }}"><a href="{{ url('puertos') }}"><i class='fa fa-ship'></i> <span>Ports</span></a></li>
            <li class="{{ Request::path() == 'airports' ? 'active' : '' }}"><a href="{{ url('airports') }}"><i class='fa fa-plane'></i> <span>Airports</span></a></li>
            <li class="{{ Request::path() == 'states' ? 'active' : '' }}"><a href="{{ url('states') }}"><i class='fa fa-truck'></i> <span>Land</span></a></li>
            @endhasrole

            @hasrole('GnteOpe')
            <li class="header">OPERATIVE ADMINISTRATION</li>
            <li class="{{ strpos(Request::path(), 'operaciones') === 0 ? 'active' : '' }}"><a href="{{ url('operaciones') }}"><i class='fa fa-exchange'></i> <span>Operations</span></a></li>
            <li class="{{ strpos(Request::path(), 'operacionesdetails') === 0 ? 'active' : '' }}"><a href="{{ URL::to('operacion/contenedor/filter') }}"><i class='fa fa-search'></i> <span>Filter by Container</span></a></li>
            <li class="{{ strpos(Request::path(), 'entidades') === 0 ? 'active' : '' }}"><a href="{{ url('entidades') }}"><i class='fa fa-address-book'></i> <span>Entities</span></a></li>
            <li class="{{ Request::path() == 'contenedores' ? 'active' : '' }}"><a href="{{ url('contenedores') }}"><i class='fa fa-archive'></i> <span>Containers</span></a></li>
            <li class="{{ Request::path() == 'puertos' ? 'active' : '' }}"><a href="{{ url('puertos') }}"><i class='fa fa-ship'></i> <span>Ports</span></a></li>
            <li class="{{ Request::path() == 'airports' ? 'active' : '' }}"><a href="{{ url('airports') }}"><i class='fa fa-plane'></i> <span>Airports</span></a></li>
            <li class="{{ Request::path() == 'states' ? 'active' : '' }}"><a href="{{ url('states') }}"><i class='fa fa-truck'></i> <span>Land</span></a></li>
            @endhasrole

            <li class="header"><i class="fa fa-pie-chart"></i>
                <span>Reports</span></li>

                @hasrole('Operador')
                <li class="{{ strpos(Request::path(), 'report/containers') === 0 ? 'active' : '' }}"><a href="{{ url('report/containers') }}"><i class="fa fa-th" aria-hidden="true"></i> <span>Containers Report</span></a></li>
                <li class="{{ strpos(Request::path(), 'report/demurrage') === 0 ? 'active' : '' }}"><a href="{{ url('report/demurrage') }}"><i class="fa fa-hand-o-up" aria-hidden="true"></i> <span>Demurrage Report</span></a></li>
                <li class="{{ strpos(Request::path(), 'report/general') === 0 ? 'active' : '' }}"><a href="{{ url('report/general') }}"><i class="fa fa-area-chart" aria-hidden="true"></i> <span>General Report</span></a></li>
                <li class="{{ strpos(Request::path(), 'report/ttr') === 0 ? 'active' : '' }}"><a href="{{ url('report/ttr') }}"><i class="fa fa-exchange" aria-hidden="true"></i><span>Transit Time Report</span></a></li>
                @endhasrole

                @hasrole('GnteOpe')
                <li class="{{ strpos(Request::path(), 'report/containers') === 0 ? 'active' : '' }}"><a href="{{ url('report/containers') }}"><i class="fa fa-th" aria-hidden="true"></i> <span>Containers Report</span></a></li>
                <li class="{{ strpos(Request::path(), 'report/demurrage') === 0 ? 'active' : '' }}"><a href="{{ url('report/demurrage') }}"><i class="fa fa-hand-o-up" aria-hidden="true"></i> <span>Demurrage Report</span></a></li>
                <li class="{{ strpos(Request::path(), 'report/general') === 0 ? 'active' : '' }}"><a href="{{ url('report/general') }}"><i class="fa fa-area-chart" aria-hidden="true"></i> <span>General Report</span></a></li>
                <li class="{{ strpos(Request::path(), 'report/ttr') === 0 ? 'active' : '' }}"><a href="{{ url('report/ttr') }}"><i class="fa fa-exchange" aria-hidden="true"></i> <span>Transit Time Report</span></a></li>
                @endhasrole
            
            <li class="header"><i class="fa fa-users"></i>
                <span>Acceso Portal a Clientes</span></li>
                <li class="{{ strpos(Request::path(), 'customer') === 0 ? 'active' : '' }}"><a href="{{ url('customer') }}"><i class="fa fa-lock" aria-hidden="true"></i> <span>Acceso a Clientes</span></a></li>
            <li>  
                <a href="http://127.0.0.1:8000/logout" onclick="event.preventDefault();document.getElementById('logout-form').submit();" > <i class='fa fa-sign-out'></i> Logout
                </a>
            </li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
