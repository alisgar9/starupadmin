@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Clientes<small>Azulcrema</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Clientes Azulcrema</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">



  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-ame"><i class="fa fa-users"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Clientes Azulcrema</span>
          <span class="info-box-number">{{$americanistas}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-ame"><i class="fa fa-download" aria-hidden="true"></i></span>
        <div class="info-box-content">
          <a href="{{ route('get.download') }}">
            <span class="info-box-text">Árchivo</span>
            <span class="info-box-number">Descargar</span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Especificaciones de lectura para el documento de Clientes Azulcrema.</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-md-3">
            <div class="card border border-primary">
              <div class="card-header">
                <strong class="card-title text text-center">Valor</strong>
              </div>
              <div class="card-body text-center">
                
                <h4><i class="fa fa-check" aria-hidden="true"></i> 1</h4>
                <h4><i class="fa fa-check" aria-hidden="true"></i> 2</h4>
             

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card border border-primary">
              <div class="card-header">
                <strong class="card-title text text-center">Status</strong>
              </div>
              <div class="card-body">
                <ul>
               
                  <h4>ACTIVO</h4>
                  <h4>CAMBIO DE CONTRASEÑA</h4>
              
                </ul>
              </div>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->

  </div>

</section>

<script>
  $(function () {
    $('#americanista-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.americanistas') }}",
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'numeroAmericanista', name: 'numeroAmericanista', title: 'Número Americanista' },
        { data: 'nombre', name: 'nombre', title: 'Nombre' },
        { data: 'paterno', name: 'paterno', title: 'Apellido Paterno' },
        { data: 'materno', name: 'materno', title: 'Apellido Materno' },
        { data: 'correo', name: 'correo', title: 'Correo Electrónico' },
        { data: 'telefono', name: 'telefono', title: 'Teléfono' },
        {
          data: 'activo', name: 'activo', title: 'Estado', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Habilitada';
            }else if (data == 2) {
              return '<div class="iradio_flat-yellow checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Habilitada';
            }
            else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Eliminada';
            }
          }
        }
      ]
    });
  });
</script>
</section>
@endsection