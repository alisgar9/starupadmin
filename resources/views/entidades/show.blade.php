@extends('adminlte::layouts.app')
<link href="{{ URL::asset('/css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Vista de Entidad<small>Control de Entidades</small></h1>
     <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ url('entidades') }}">Entidades</a></li>
        <li class="active">Vista</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Vista de Entidad</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::model($entidades, array('route' => array('entidades.update', $entidades->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with permission data --}}
 <div class="box-body">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('nombre', 'Nombre(Alias) *') }}
            {!! Form::text('nombre', null, [
                    'class'                         => 'form-control',
                    'readonly'                      => 'readonly',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('razonsocial', 'Razón Social *') }}
            {!! Form::text('razonsocial', null, [
                    'class'                         => 'form-control',
                    'readonly'                      => 'readonly',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('nombrecontacto', 'Nombre de Contacto *') }}
            {!! Form::text('nombrecontacto', null, [
                    'class'                         => 'form-control',
                    'readonly'                      => 'readonly',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('email', 'Correo Electrónico *') }}
            {{ Form::email('email', null, array('class' => 'form-control','readonly' => 'readonly')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('email', 'Correos Electrónicos para Con Copia') }}
            {!! Form::text('email_cc',json_decode($entidades->email_cc), [
                                'data-role' => 'tagsinput',
                                'class' => 'form-control',
                                'readonly' => 'readonly'
                                ]) !!}
            
        </div>

    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('direccion', 'Dirección *') }}
            {!! Form::text('direccion', null, [
                    'class'                         => 'form-control',
                    'readonly' => 'readonly'
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('telefono', 'Teléfono *') }}
            {!! Form::text('telefono', null, [
                    'class'                         => 'form-control',
                    'readonly'                      => 'readonly',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('taxid', 'Tax ID *') }}
            {!! Form::text('taxid', null, [
                    'class'                         => 'form-control',
                    'readonly'                      => 'readonly',
                    ]) !!}
        </div>
    </div>

    <div class="col-md-6">
                    <div class="form-group">

                        {{ Form::label('id_clasificacion', 'Seleccione tipos de Clasificación *') }}<br>
                        {{ Form::select('id_clasificacion[]',$clasificaciones,$clasificacion,[
                                'multiple' =>'multiple','requerid' => 'requerid', 'readonly' =>'readonly', 'id' =>'demo'],array('name'=>'id_clasificacion[]'))}}

                        
                    </div>        
    </div>

   
    

    
    
      </div>

 <div class="box-footer">

    </div>

    {{ Form::close() }}

            
          </div>
          <!-- /.box -->

        

        


        </div>
        <!--/.col (left) -->
       
      </div>
      <!-- /.row -->


      <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
      <div class="col-md-6">
       <div class="box-header with-border">
          <h3 class="box-title">Lista Detalles Bancarios</h3>
        </div></div>
      <div class="col-md-6 text-right">
      <br/>
      </div>
        
        <!-- /.box-header -->
        <div class="box-body">
          <table id="usuarios-table" class="table table-striped table-bordered dt-responsive nowrap"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      
    </div>
    <!-- /.col -->
  </div>



  <div class="row no-print">
    <div class="col-xs-12">
      <a href="#" class="btn btn-info" onClick="window.print()"><i class="fa fa-print"></i> Imprimir</a>
    </div>
  </div>

</section>
<!-- /.content -->

@endsection
@yield('content')
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-angular.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    $('#demo').multiselect();
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('idemp');
      var idEnt = $(e.relatedTarget).data('ident');
      modal.find('.modal-body #id_emp').val(idEmpresa);
      modal.find('.modal-body #id_entidad').val(idEnt);
    });
  });
  $(function () {
    $('#usuarios-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.ent.banco') }}?showDeleted="+{{$entidades->id}},
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'id', name: 'id', title: '#' },
        { data: 'cuentabancaria', name: 'cuentabancaria', title: 'Cuenta Bancaria' },
        { data: 'cinterbancaria', name: 'cinterbancaria', title: 'Clabe Interbancaria' },
        { data: 'banco', name: 'banco', title: 'Banco' },
        { data: 'bancointer', name: 'bancointer', title: 'Banco intermediario' },
        { data: 'swift', name: 'swift', title: 'Swift' },
        { 
          data: 'activo', name: 'status', title: 'Estado', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<small class="label pull-center bg-green">Activo</small>';
            } else {
              return '<small class="label pull-center bg-red">Inactivo</small>';
            }
          }
        },
      ]
    });
  });
</script>

@stop
@yield('scripts')