@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Entidades<small>Control de Entidades</small></h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Entidades</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

  @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Éxito!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Entidades Totales</span>
          <span class="info-box-number">
            <h3>{{ count($users) }}</h3>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-user-plus"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Nuevas Entidades Este Mes</span>
          <span class="info-box-number">
            <h3>{{ count($usersThisMonth) }}</h3>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-user-times"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Entidades Eliminadas</span>
          <span class="info-box-number">
            <h3>{{ count($delUsers) }}</h3>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
  </div>
  <!-- /.row -->

  <!-- .row -->
  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Historial de Entidades Registradas</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="chart">
                <!-- Sales Chart Canvas -->
                <div class="panel-body">
                  {!! $chartPline->html() !!}
                </div>
              </div>
              <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- ./box-body -->
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->

    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Últimas Entidades Registradas</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <ul class="products-list product-list-in-box">
            @foreach ($lastUsers as $user)
            <li class="item">
              <div class="product-img">
                <img class="direct-chat-img" src="{{ Gravatar::get($user->email) }}" alt="message user image">
              </div>
              <div class="product-info">
                <a href="javascript:void(0)" class="product-title">{{ $user->nombre }}
                  <span class="pull-center badge bg-primary pull-right"><i class="fa fa-clock-o"></i> {{ $user->created_at }}</span></a>
                <span class="product-description">
                  {{ $user->email }}
                </span>
              </div>
            </li>
            @endforeach
            <!-- /.item -->
          </ul>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Lista de Entidades</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="usuarios-table" class="table table-striped table-bordered dt-responsive nowrap"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <a href="{{ route('entidades.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Agregar Entidad</a>
    </div>
    <!-- /.col -->
  </div>

  <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Eliminar Entidad</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea eliminar la Entidad: <b><span id="fav-title"></span></b>?
          <hr>
          <form action="{{ route('entidades.destroy','0') }}" id="delForm" method="post">
            {{method_field('delete')}} {{csrf_field()}}
            <input name="id_entidad" id="id_emp" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->
</section>

<script type="text/javascript">
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var idEmpresa = $(e.relatedTarget).data('idemp');
      modal.find('.modal-body #id_emp').val(idEmpresa);
    });
  });
  $(function () {
    $('#usuarios-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.entidades') }}",
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'id', name: 'id', title: '#' },
        { data: 'nombre', name: 'nombre', title: 'Nombre' },
        { data: 'email', name: 'email', title: 'Correo' },
        { data: 'created_at', name: 'created_at', title: 'Fecha / Hora Alta' },
        { 
          data: 'status', name: 'status', title: 'Estado', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<small class="label pull-center bg-green">Activo</small>';
            } else {
              return '<small class="label pull-center bg-red">Inactivo</small>';
            }
          }
        },
        { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
      ]
    });
  });
</script>
{!! Charts::scripts() !!}
{!! $chartPline->script() !!}
@endsection