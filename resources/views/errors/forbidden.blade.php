@extends('adminlte::layouts.errors')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.pagenotfound') }}
@endsection

@section('main-content')

    <div class="error-page">
        <h2 class="headline text-yellow"> 400</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Prohibido! Solicitud incorrecta.</h3>
            <p>Esta acción no está permitida.</p>
            <form class='search-form'>
                <div class='input-group'>
                    <div class="input-group-btn">
                        <a href="{{ url('home') }}" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                        Inicio </a>
                    </div>
                </div><!-- /.input-group -->
            </form>
        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
@endsection