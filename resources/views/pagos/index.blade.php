@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Aclaración de<small> Pagos</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Aclaración de Pagos</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Introduce el correo electrónico a validar</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body text-center">
         
         @if(session()->has('message'))
            <div class="alert alert-success">{{ session()->get('message') }} </div>
         @endif 
            <form id="filtrar">
               <div class="row">
                  <div class="col-md-12">
                     <div class="input-group input-group-sm">
                        <span class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i> Correo Electrónico:</span>
                        <input type="email" class="form-control" placeholder="email" name="email" id="email" autocomplete="off" required="required" >
                        <span class="input-group-btn">
                        <button type="button" id="btnFiltro" class="btn btn-info btn-flat"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                        </span>
                     </div>
                     <!-- /input-group -->
                  </div>
                  <!-- /.col-lg-6 -->
               </div>
            </form>

        </div>
      </div>
    <!-- /.col -->
    </div>

      <div class="row" id="resultado">
      
      </div>    


    </div>

  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Detalles</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
            <div class="row">
                <div class="col-md-12">
                <label>Código Azulcrema.</label> 
                </div>
                <div class="col-md-12">
                <input name="id_americanista" id="id_emp" type="text" readonly class="form-control font-weight-bold" />
                </div>
            </div>
            <div class="row">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade in" id="modal-danger" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">* Campo Requerido</h4>
              </div>
              <div class="modal-body">
                <h4 class="text-center">Se requiere el correo electrónico a consultar.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Intentar de Nuevo</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
  </section>
<script type="text/javascript">
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var id_americanista = $(e.relatedTarget).data('idemp');
      modal.find('.modal-body #id_emp').val(id_americanista);
    });
  });
  $(document).ready( function() {
    $("#resultado").hide();  
    
    
    $("button[id=btnFiltro]").on('click',function () {
      $("#resultado").empty();
        var email = $("#email").val();
        if(email.length > 0){ 
              $("#resultado").show();
              $("#resultado").append('<div class="col-md-12"><div class="box box-primary"><div class="box-header with-border"><h3 class="box-title"></h3></div><div class="box-body text-center"><table id="americanista-table" class="table table-striped table-bordered dt-responsive"></table></div></div></div>');
              $('#americanista-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('pagos/buscar') }}",
                    type: "POST",
                    data: function (d) {
                          d.email = email;
                          d._token = "{{ csrf_token() }}";
                    },
                },
                language: {
                  "search": "Buscar",
                  "lengthMenu": "Mostar _MENU_ registros por página",
                  "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
                  "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
                  "infoEmpty": "Registros no encontrados",
                  "infoFiltered": "(Filtrado en _MAX_ registros totales)",
                  "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente",
                  },
                },
                columns: [
                  { data: 'nombre', name: 'nombre', title: 'Nombre' },
                  { data: 'a_paterno', name: 'a_paterno', title: 'Apellido Paterno' },
                  { data: 'a_materno', name: 'a_materno', title: 'Apellido Materno' },
                  { data: 'email', name: 'email', title: 'Correo Electrónico' },
                  { data: 'telefono', name: 'telefono', title: 'Teléfono' },
                  { data: 'fecha', name: 'fecha', title: 'Fecha',"render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return date.getFullYear() + "-" + (month.length > 1 ? month :  month) + "-" + date.getDate()  ;
                      } 
                  },                  
                  {
                    data: 'status', name: 'status', title: 'Estado', "render": function (data, type, full, meta) {
                      if (data == 1) {
                        return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Activo';
                      } else {
                        return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Inactivo';
                      }
                    }
                  },
                  { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
                ]
              });

          }else{
            $('#modal-danger').modal('show');
          }

    });    
});

</script>
@endsection