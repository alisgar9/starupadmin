@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Editar Centro de Costos<small>Catálogo de centros</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ url('costos') }}">Centros de Costo</a></li>
        <li class="active">Editar</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Editar Centro de Costos <strong>{{$costo->centro}}</strong></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{ Form::model($costo, array('route' => array('costos.update', encrypt($costo->id)), 'method' =>
                'PUT')) }}{{-- Form model binding to automatically populate our fields with permission data --}}
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('nombre', 'Nombre') }}
                        {!! Form::text('centro', null, [
                        'class' => 'form-control',
                        'required' => 'required',
                        ]) !!}
                    </div>
                </div>
                <div class="box-footer">
                    {{ Form::button('<i class="fa fa-refresh" aria-hidden="true"></i> Actualizar', ['class' => 'btn
                    btn-primary', 'type' => 'submit']) }}
                </div>
                {{ Form::close() }}
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection