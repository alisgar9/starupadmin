@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Agregar Usuario<small>Control de usuarios</small></h1>
     <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ url('users') }}">Usuarios</a></li>
        <li class="active">Agregar</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    </div>
    @endif


      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Nuevo Usuario</h3>
              <p>Los campos marcados con <b>*</b> son requeridos</p>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(array('url' => 'users')) }}
 <div class="box-body">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('name', 'Nombre Completo *') }}
            {!! Form::text('name', null, [
                    'class'                         => 'form-control',
                    'required'                      => 'required',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('username', 'Nombre de usuario *') }}
            {!! Form::text('username', null, [
                    'class'                         => 'form-control',
                    'required'                      => 'required',
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('email', 'Correo Electrónico *') }}
            {{ Form::email('email', '', array('class' => 'form-control','required' => 'required')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class='form-group'>
        {{ Form::label('password', 'Tipo de Usuario *') }}<br>
            @foreach ($roles as $role)
                {{ Form::checkbox('roles[]',  $role->id ) }}
                {{ Form::label($role->name, ucfirst($role->name)) }}<br>

            @endforeach
        </div>
        
    </div> 
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('password', 'Contraseña*') }}<br>
            {{ Form::password('password', array('class' => 'form-control')) }}

        </div>
    </div> 
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('password', 'Confirmar contraseña*') }}<br>
            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

        </div>
    </div> 
    <div class="col-md-12">
              <h3 class="box-title">Detalles Administrativos</h3>
              <hr class="hr-primary" />
    </div> 
    <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('fechaingreso', 'Fecha Ingreso') }}
                        {!! Form::date('fechaingreso', null, [
                        'class' => 'form-control',]) !!}
                    </div>
    </div> 
    <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('fechasalida', 'Fecha Salida') }}
                        {!! Form::date('fechasalida', null, [
                        'class' => 'form-control',]) !!}
                    </div>
    </div> 
    <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('idcargo', 'Tipo de Cargo') }}
                        {{ Form::select('idcargo',$cargos,null,[
                        'class' => 'form-control','placeholder' => 'seleccione uno...'],array('name'=>'cargos[]'))}}
                        
                    </div>        
    </div> 
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('salario', 'Salario') }}
            {!! Form::number('salario', null, [
                    'class' => 'form-control'
                    ]) !!}
        </div>
    </div> 
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('rfc', 'RFC') }}
            {!! Form::text('rfc', null, [
                    'class' => 'form-control'
                    ]) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('curp', 'CURP') }}
            {!! Form::text('curp', null, [
                    'class' => 'form-control'
                    ]) !!}
        </div>
    </div>  
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('domicilio', 'Domicilio') }}
            {!! Form::text('domicilio', null, [
                    'class' => 'form-control'
                    ]) !!}
        </div>
    </div> 
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('telefono', 'Teléfono') }}
            {!! Form::text('telefono', null, [
                    'class' => 'form-control'
                    ]) !!}
        </div>
    </div> 
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('nss', 'NSS') }}
            {!! Form::text('nss', null, [
                    'class' => 'form-control'
                    ]) !!}
        </div>
    </div> 
    
    

    

    

    

    

    
      </div>

 <div class="box-footer">
    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Guardar', ['class' => 'btn btn-primary', 'type' => 'submit']) }}

    </div>

    {{ Form::close() }}

            
          </div>
          <!-- /.box -->

        

        


        </div>
        <!--/.col (left) -->
       
      </div>
      <!-- /.row -->
</section>
<!-- /.content -->
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>

@endsection