@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>States <small>States Catalog</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ url('states') }}">States</a></li>
    <li class="active">Detail</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="fa fa-globe"></i> {{ $estado->nombre }}
        <small class="pull-right">Date: {{ \Carbon\Carbon::parse($user->from_date)->format('d/m/Y')}}</small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      State Name:
      <address>
        <strong>{{ $estado->nombre }}</strong><br>
      </address>
    </div>
    <div class="col-sm-4 invoice-col">
      Country:
      <address>
        <strong>{{ $estado->Pais['nombre'] }}</strong><br>
      </address>
    </div>
    <div class="col-sm-4 invoice-col">
      State key:
      <address>
        <strong>{{ $estado->clave }}</strong><br>
      </address>
    </div>

    
    <div class="col-sm-4 invoice-col">
      <b>key #{{ $estado->id }}</b><br>
      <br>
      <b>Creation Date:</b> {{ $estado->created_at }}<br>
      <b>Update Date:</b> {{ $estado->updated_at }}<br>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  
  </br>
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="#" class="btn btn-info" onClick="window.print()"><i class="fa fa-print"></i> Imprimir</a>
    </div>
  </div>
</section>

<!-- Main content -->
<section class="content">




</section>
@endsection
