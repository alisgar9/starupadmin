@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Recibos<small>Administrador de recibos</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Recibos</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->


<!-- Main content -->
<section class="content">

  @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Éxito!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  @if(session()->has('flash_message_error'))
  <div class="alert alert-error alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-exclamation"></i> Ocurrió un Problema!</h4>
    {{ session()->get('flash_message_error') }}
  </div>
  @endif

  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-file-text"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Total de Recibos</span>
          <span class="info-box-number">
            <h3>{{ count($recibos) }}</h3>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-upload"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Recibos Cargados</span>
          <span class="info-box-number">
            <h3>{{ count($recibosCargados) }}</h3>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-pencil"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Recibos Capturados</span>
          <span class="info-box-number">
            <h3>{{ count($recibosCapturados) }}</h3>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->

  <!-- row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Lista de Recibos</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table id="recibos-table" class="table table-striped table-bordered dt-responsive"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      @can('Crear')
      <a href="{{ URL::to('recibos/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Agregar Recibo</a>
      <a href="{{ URL::to('recibos/capture') }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Capturar Recibo</a>
      @endcan
    </div>
    <!-- /.col -->
  </div>

  <!-- Delete Model -->
  <div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Eliminar Recibo</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea eliminar el recibo: <b><span id="fav-title"></span></b>?
          <hr>
          <form action="{{ route('recibos.destroy','0') }}" id="delForm" method="post">
            {{method_field('delete')}} {{csrf_field()}}
            <input name="id_recibo" id="id_emp" type="hidden" />
            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Delete Model -->

  <script>
    $(document).ready(function () {
      // se activa cuando el modal está a punto de ser mostrado.
      $('#confirmDeleteComment').on('show.bs.modal', function (e) {
        var modal = $(this);
  
        // obtener el atributo idEmpresa del elemento pulsado.
        $("#fav-title").html($(e.relatedTarget).data('title'));
        var idEmpresa = $(e.relatedTarget).data('idemp');
        modal.find('.modal-body #id_emp').val(idEmpresa);
      });
    });
  
    $(function () {
      $('#recibos-table').DataTable({
        processing: true,
        serverSide: true,
        type: 'GET',
        ajax: "{{ route('get.recibos') }}",
        language: {
          "search": "Buscar",
          "lengthMenu": "Mostar _MENU_ registros por página",
          "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
          "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
          "infoEmpty": "Registros no encontrados",
          "infoFiltered": "(Filtrado en _MAX_ registros totales)",
          "paginate": {
            "previous": "Anterior",
            "next": "Siguiente",
          },
        },
        columns: [
          { data: 'id', name: 'id', title: '#' },
          { data: 'nombre', name: 'nombre', title: 'Nombre' },
          { data: 'rfc', name: 'rfc', title: 'RFC' },
          { data: 'rmu', name: 'rmu', title: 'RMU' },
          { data: 'no_medidor', name: 'no_medidor', title: 'No. de Medidor' },
          { data: 'tarifa', name: 'tarifa', title: 'Tarifa' },
          { data: 'mes_facturado', name: 'mes_facturado', title: 'Mes Facturado' },
          { data: 'total',  render: $.fn.dataTable.render.number(',', '.', 2, '$'), name: 'total', title: 'Total' },
          { data: 'municipio', name: 'municipio', title: 'Municipio' },
          { data: 'created_at', name: 'created_at', title: 'Creado' },
          { data: 'updated_at', name: 'updated_at', title: 'Actualizado' },
          { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
        ]
      });
    });
  </script>
</section>
@endsection