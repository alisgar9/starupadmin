@extends('adminlte::layouts.app')
@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>Recibos<small>Administrador de recibos</small></h1>
      <ol class="breadcrumb">
            <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="{{ url('recibos') }}">Recibos</a></li>
            <li class="active">Agregar</li>
      </ol>
</section>
<!-- /.Content Header (Page header) -->
<!-- Main content -->
<section class="content">
      @if ($errors->any())
      <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
            <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
            </ul>
      </div>
      @endif
      <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                  <!-- general form elements -->
                  <div class="box box-primary">
                        <div class="box-header with-border">
                              <h3 class="box-title">Nuevo Recibo</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {{ Form::open(array('url' => 'recibos/savecapture','files' => true)) }}
                        <div class="box-body">
                              <div class="row">
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {{ Form::label('nombre', 'Nombre') }}
                                                {!! Form::text('nombre', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                          <div class="form-group">
                                                {{ Form::label('nompob', 'Población') }}
                                                {!! Form::text('nompob', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                          <div class="form-group">
                                                {{ Form::label('rmu', 'No. de servicio (RMU)') }}
                                                {!! Form::text('rmu', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {{ Form::label('direccion', 'Dirección') }}
                                                {!! Form::text('direccion', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                          <div class="form-group">
                                                {{ Form::label('nomest', 'Estado') }}
                                                {!! Form::text('nomest', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                          <div class="form-group">
                                                {{ Form::label('rpu', 'RPU') }}
                                                {!! Form::text('rpu', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {{ Form::label('calle', 'Calle') }}
                                                {!! Form::text('calle', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <div class="form-group">
                                                {{ Form::label('codigoPostal', 'C.P.') }}
                                                {!! Form::number('codigoPostal', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                          <!-- /.form-group -->
                                          <div class="form-group">
                                                {{ Form::label('no_medidor', 'No. de medidor') }}
                                                {!! Form::text('no_medidor', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {{ Form::label('colonia', 'Colonia') }}
                                                {!! Form::text('colonia', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                          <div class="form-group">
                                                {{ Form::label('rfc', 'RFC') }}
                                                {!! Form::text('rfc', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                          <div class="form-group">
                                                {{ Form::label('no_cuenta', 'No. de cuenta') }}
                                                {!! Form::text('no_cuenta', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                              </div>
                              <div class="row">
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {!! Form::Label('item', 'Tarifa') !!}
                                                {!! Form::select('id_tarifa', $tarifas, null,['class'=>'form-control', 'placeholder'=>'Elige una Tarifa', 'required']); !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {{ Form::label('multiplo', 'Múltiplo') }}
                                                {!! Form::number('multiplo', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {{ Form::label('demanda_contratada', 'Demanda Contratada kW') }}
                                                {!! Form::number('demanda_contratada', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {!! Form::Label('carga_conectada', 'Carga Conectada kW') !!}
                                                {!! Form::number('carga_conectada', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {!! Form::Label('item', 'Municipio') !!}
                                                {!! Form::select('id_municipio', $municipios, null,['class'=>'form-control', 'placeholder'=>'Elige un Municipio', 'required']); !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {!! Form::Label('item', 'Área') !!}
                                                {!! Form::select('id_area', $areas, null,['class'=>'form-control', 'placeholder'=>'Eliga una Área', 'required']); !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {{ Form::label('feclimite', 'Límite de Pago') }}
                                                {!! Form::date('feclimite', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {!! Form::Label('fecorte', 'Corte a partir') !!}
                                                {!! Form::date('fecorte', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {{ Form::label('totalPagar', 'Total A Pagar') }}
                                                {!! Form::number('totalPagar', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {!! Form::Label('totalLetra', 'Importe con Letra') !!}
                                                {!! Form::text('totalLetra', null, [
                                                'class' => 'form-control',
                                                'required' => 'required',]) !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {!! Form::Label('item', 'Centro de Costos') !!}
                                                {!! Form::select('id_costo', $costos, null,['class'=>'form-control', 'placeholder'=>'Eliga un Centro de Costos', 'required']); !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {!! Form::Label('item', 'Ubicaciones') !!}
                                                {!! Form::select('id_ubicacion', $ubicaciones, null,['class'=>'form-control', 'placeholder'=>'Eliga una Ubicación', 'required']); !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                              </div>
                              <div class="row">
                                    <div class="col-md-3">
                                          <div class="form-group">
                                                {!! Form::Label('item', 'Empresas') !!}
                                                {!! Form::select('id_empresa', $empresas, null,['class'=>'form-control', 'placeholder'=>'Eliga una Empresa', 'required']); !!}
                                          </div>
                                          <!-- /.form-group -->
                                    </div>
                              </div>
                              <div class="box-header with-border">
                                    <h3 class="box-title"><a href="#"><strong><i class="fa fa-pencil margin-r-5"></i>
                                                      Datos Variables de la Factura (Energia).</strong></a></h3>
                              </div>

                              <div class="box-body">
                                    <div class="row">
                                          <div class="col-md-3">
                                                <div class="form-group">
                                                      {{ Form::label('mes_facturado', 'Mes facturado') }}
                                                      {!! Form::date('mes_facturado', null, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',]) !!}
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                      {{ Form::label('kwh_energia_intermedia', 'KWH Energía intermedia') }}
                                                      {!! Form::number('kwh_energia_intermedia', 0, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',
                                                      'onchange' => 'calcularFP()']) !!}
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                      {{ Form::label('kw_demanda_punta', 'Kw Demanda Punta') }}
                                                      {!! Form::number('kw_demanda_punta', 0, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',
                                                      'onchange' => 'calcularFC()']) !!}
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                      {{ Form::label('fc', 'F.C.') }}
                                                      {!! Form::number('fc', 0, [
                                                      'class' => 'form-control',
                                                      'readonly',
                                                      'required' => 'required',]) !!}
                                                </div>
                                                <!-- /.form-group -->
                                          </div>
                                          <!-- /.col -->
                                          <div class="col-md-3">
                                                <div class="form-group">
                                                      {{ Form::label('fecdesde', 'Fecha desde') }}
                                                      {!! Form::date('fecdesde', null, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',
                                                      'onchange' => 'calcularFC()']) !!}
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                      {{ Form::label('kwh_energia_punta', 'KWH Energía Punta') }}
                                                      {!! Form::number('kwh_energia_punta', 0, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',
                                                      'onchange' => 'calcularFP()']) !!}
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                      {{ Form::label('kw_max_movil', 'kW Max Año Movil') }}
                                                      {!! Form::number('kw_max_movil', 0, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',]) !!}
                                                </div>
                                                <!-- /.form-group -->
                                          </div>
                                          <!-- /.col -->
                                          <div class="col-md-3">
                                                <div class="form-group">
                                                      {{ Form::label('fechasta', 'Fecha hasta') }}
                                                      {!! Form::date('fechasta', null, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',
                                                      'onchange' => 'calcularFC()']) !!}
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                      {{ Form::label('kw_demanda_base', 'KW Demanda Base') }}
                                                      {!! Form::number('kw_demanda_base', 0, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',
                                                      'onchange' => 'calcularFC()']) !!}
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                      {{ Form::label('kvarh_total', 'KVARH') }}
                                                      {!! Form::number('kvarh_total', 0, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',
                                                      'onchange' => 'calcularFP()']) !!}
                                                </div>
                                                <!-- /.form-group -->
                                          </div>
                                          <!-- /.col -->
                                          <div class="col-md-3">
                                                <div class="form-group">
                                                      {{ Form::label('kwh_energia_base', 'KWH Energía base') }}
                                                      {!! Form::number('kwh_energia_base', 0, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',
                                                      'onchange' => 'calcularFP()']) !!}
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                      {{ Form::label('kw_demanda_intermedia', 'KW Demanda Intermedia') }}
                                                      {!! Form::number('kw_demanda_intermedia', 0, [
                                                      'class' => 'form-control',
                                                      'required' => 'required',
                                                      'onchange' => 'calcularFC()']) !!}
                                                </div>
                                                <!-- /.form-group -->
                                                <div class="form-group">
                                                      {{ Form::label('fp', 'F.P.') }}
                                                      {!! Form::number('fp', 0, [
                                                      'class' => 'form-control',
                                                      'readonly',
                                                      'required' => 'required',]) !!}
                                                </div>
                                                <!-- /.form-group -->
                                          </div>
                                          <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                    <div class="box-header with-border">
                                          <h3 class="box-title"><a href="#"><strong><i class="fa fa-pencil margin-r-5"></i>
                                                            Datos Variables de la Factura (Importe).</strong></a></h3>
                                    </div>
                                    <div class="box-body">
                                          <div class="row">
                                                <div class="col-md-2">
                                                      <div class="form-group">
                                                            {{ Form::label('suministro', '$ Suministro') }}
                                                            {!! Form::number('suministro', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('generacionP', '$ Generación P') }}
                                                            {!! Form::number('generacionP', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('fpcargo', '$ FP (cargo)') }}
                                                            {!! Form::number('fpcargo', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('totalkwh', 'Total $/KWH') }}
                                                            {!! Form::number('totalkwh', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-md-2">
                                                      <div class="form-group">
                                                            {{ Form::label('distribucion', '$ Distribución') }}
                                                            {!! Form::number('distribucion', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('capacidad', '$ Capacidad') }}
                                                            {!! Form::number('capacidad', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('subtotalsiniva', 'Subtotal (sin/IVA)') }}
                                                            {!! Form::number('subtotalsiniva', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-md-2">
                                                      <div class="form-group">
                                                            {{ Form::label('transmision', '$ Transmisión') }}
                                                            {!! Form::number('transmision', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('scnmem', '$ SCnMEM') }}
                                                            {!! Form::number('scnmem', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('iva', '$ IVA') }}
                                                            {!! Form::number('iva', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-md-2">
                                                      <div class="form-group">
                                                            {{ Form::label('cenace', '$ CENACE') }}
                                                            {!! Form::number('cenace', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('energia', '$ Energía') }}
                                                            {!! Form::number('energia', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('dap', '$ DAP') }}
                                                            {!! Form::number('dap', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-md-2">
                                                      <div class="form-group">
                                                            {{ Form::label('generacionB', '$ Generación B') }}
                                                            {!! Form::number('generacionB', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('medbt', '$ Med BT') }}
                                                            {!! Form::number('medbt', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('total', 'Total') }}
                                                            {!! Form::number('total', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-md-2">
                                                      <div class="form-group">
                                                            {{ Form::label('generacionI', '$ Generación I') }}
                                                            {!! Form::number('generacionI', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('fpbonificacion', '$ FP (bonificación)') }}
                                                            {!! Form::number('fpbonificacion', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                      <div class="form-group">
                                                            {{ Form::label('totalkw', 'Total $/KW') }}
                                                            {!! Form::number('totalkw', 0, [
                                                            'class' => 'form-control',
                                                            'step'=>'any',
                                                            'required' => 'required',]) !!}
                                                      </div>
                                                      <!-- /.form-group -->
                                                </div>
                                                <!-- /.col -->
                                          </div>
                                    </div>
                              </div>
                              <div class="box-footer">
                                    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Guardar', ['class'
                                    => 'btn btn-primary', 'type' => 'submit']) }}
                              </div>
                              {{ Form::close() }}
                        </div>
                        <!-- /.box -->
                  </div>
                  <!--/.col (left) -->
            </div>
            <!-- /.row -->
</section>
<!-- /.content -->

<script>
      // Función para calcular FP. 
      function calcularFP() {
            var kvarh = parseInt(document.getElementById('kvarh_total').value); // KVARH variable
            
            // KWH Total = KWH Energía base + KWH energía intermedia + KWH Energía punta
            var kwhTotal = parseInt(document.getElementById('kwh_energia_base').value) + parseInt(document.getElementById('kwh_energia_intermedia').value) + parseInt(document.getElementById('kwh_energia_punta').value);
            
            // Calculo -> FP % = cos(arc tan(KVARH variable/Consumo total KWH))
            var fp = Math.cos(Math.atan(kvarh/kwhTotal));

            // Retornamos resultado en input F.P.
            document.getElementById('fp').value = parseFloat(fp*100).toFixed(2);
      }

      // Función para calcular FC.
      function calcularFC() {
            
            // KWH Total = KWH Energía base + KWH energía intermedia + KWH Energía punta.
            var kwhTotal = parseInt(document.getElementById('kwh_energia_base').value) + parseInt(document.getElementById('kwh_energia_intermedia').value) + parseInt(document.getElementById('kwh_energia_punta').value);

            // KW Dem Max.
            var kwDemMax = Math.max(parseInt(document.getElementById('kw_demanda_base').value), parseInt(document.getElementById('kw_demanda_intermedia').value), parseInt(document.getElementById('kw_demanda_punta').value));
         
            // No. días períod.
            var diffDays = Math.abs(Date.parse(document.getElementById('fechasta').value) - Date.parse(document.getElementById('fecdesde').value));
            var noDiasPeriodo = Math.ceil(diffDays / (1000 * 3600 * 24));
            
            // % FC=KWH Total/(KW Dem Max*No. días período*24).
            var fc = kwhTotal / (kwDemMax * noDiasPeriodo * 24);

            // Retornamos resultado en input F.P.
            document.getElementById('fc').value = parseFloat(fc*100).toFixed(2);
      }
</script>
@endsection