@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Recibo<small>Detalle de recibo</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ url('recibos') }}">Recibos</a></li>
    <li class="active">Detalle</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="fa fa-file-text"></i> {{ $recibo->nombre }}
        <small class="pull-right">Fecha: {{ \Carbon\Carbon::parse($user->from_date)->format('d/m/Y')}}</small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      Receptor
      <address>
        <strong>{{ $recibo->nombre }}</strong><br>
        {{ $recibo->direccion }}<br>
        {{ $recibo->calle1 }}<br>
        {{ $recibo->colonia }}<br>
        {{ $recibo->nompob }}, {{ $recibo->nomest }}<br>
        <strong>C.P. </strong>{{ $recibo->cp }}<br>
        <b>RFC: </b>{{ $recibo->rfc }}<br>
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <strong>NO. DE SERVICIO (RMU)</strong>
      <address>
        {{ $recibo->rmu }}<br>
        <strong>RPU: </strong>{{ $recibo->rpu }}<br>
        <strong>TARIFA: </strong>{{ $recibo->tarifa }}<br>
        <strong>NO. MEDIDOR: </strong>{{ $recibo->no_medidor }}<br>
        <strong>MULTIPLICADOR: </strong>{{ $recibo->multiplo }}<br>
        <strong>CARGA CONECTADA kW: </strong>{{ $recibo->carga_conectada }}<br>
        <strong>DEMANDA CONTRATADA kW: </strong>{{ $recibo->demanda_contratada }}
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <p class="lead"><strong>TOTAL A PAGAR: $ {{ number_format($recibo->total, 2, '.', ',')}}</strong><br>{{$recibo->monto_apagar_enletras}}</p>
      <b>PERIODO FACTURADO: </b>{{ $recibo->periodo_facturado }}<br>
      <br>
      <b>LÍMITE DE PAGO: </b>{{ Carbon\Carbon::parse($recibo->feclimite)->format('d M Y') }}<br>
      <b>CORTE A PARTIR: </b>{{ Carbon\Carbon::parse($recibo->fecorte)->format('d M Y') }}<br>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <p class="lead"></p>

  <div class="box box-success collapsed-box">
    <div class="box-header with-border">
      <h3 class="box-title">Datos variables de la factura (energia)</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
        </button>
      </div>
      <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: none;">

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr bgcolor="#1dd1a1">
                <th style="text-align: center;">Mes facturado</th>
                <th style="text-align: center;">Fecha Desde</th>
                <th style="text-align: center;">Fecha Hasta</th>
                <th>KWH Energía Base</th>
                <th>KWH Energía Intermedia</th>
                <th>KWH Energía Punta</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="text-align: center;">{{ $recibo->mes_facturado }}</td>
                <td style="text-align: center;">{{ \Carbon\Carbon::parse($recibo->fecdesde)->format('d M Y')}}</td>
                <td style="text-align: center;">{{ \Carbon\Carbon::parse($recibo->fechasta)->format('d M Y')}}</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
            <thead>
              <tr bgcolor="#1dd1a1">
                <th style="text-align: center;">Mes</th>
                <th>Días de mes</th>
                <th style="text-align: center;">Consumo prom. diario</th>
                <th style="text-align: center;">Energía kWh</th>
                <th>Precios $kWh</th>
                <th>Importe (MXN)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>kWh base</td>
                <td></td>
                <td align="center">{{ isset($cfdiAddenda['DEMANDA3P']) ? number_format($cfdiAddenda['DEMANDA3P'], 0,
                  '.', ',') : 'Sin Valor'}}</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>kWh intermedia</td>
                <td></td>
                <td align="center">{{ isset($cfdiAddenda['DEMANDA2P']) ? number_format($cfdiAddenda['DEMANDA2P'], 0,
                  '.', ',') : 'Sin Valor'}}</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>kWh punta</td>
                <td></td>
                <td align="center">{{ isset($cfdiAddenda['DEMANDA1P']) ? number_format($cfdiAddenda['DEMANDA1P'], 0,
                  '.', ',') : 'Sin Valor'}}</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
            <thead>
              <tr bgcolor="#1dd1a1">
                <th style="text-align: center;">Mes</th>
                <th>Factor de proporción</th>
                <th>Demanda máxima $/kW</th>
                <th style="text-align: center;">Precios $/kW</th>
                <th>Importe (MXN)</th>
                <th>Factor de potencia</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>kWMaxAñoMovil</td>
                <td></td>
                <td align="center">{{ isset($cfdiAddenda['DEMANDA3P']) ? number_format($cfdiAddenda['DEMANDA3P'], 0,
                  '.', ',') : 'Sin Valor'}}</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>kVArh</td>
                <td></td>
                <td align="center">{{ isset($cfdiAddenda['KVARH']) ? number_format($cfdiAddenda['KVARH'], 0, '.', ',')
                  : 'Sin Valor'}}</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>Factor de potencia %</td>
                <td></td>
                <td align="center">{{ isset($cfdiAddenda['FacPot']) ? $cfdiAddenda['FacPot'] : 'Sin Valor'}}</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </div>
    <!-- /.box-body -->
  </div>

  <!-- start box2 -->
  <div class="box box-success collapsed-box">
    <div class="box-header with-border">
      <h3 class="box-title">Datos variables de la factura (importe)</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
        </button>
      </div>
      <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: none;">

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-8 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr bgcolor="#1dd1a1">
                <th style="text-align: center;">Concepto</th>
                <th style="text-align: center;">$</th>
                <th style="text-align: center;">$/kW</th>
                <th style="text-align: center;">$/kWh</th>
                <th style="text-align: center;">Importe (MXN)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Suministro</td>
                <td align="center">{{ $datosVariables['suministro']['impte'] }}</td>
                <td align="right">{{ $datosVariables['suministro']['kw'] }}</td>
                <td align="right">{{ $datosVariables['suministro']['kwh'] }}</td>
                <td align="right">{{ $datosVariables['suministro']['tot'] }}</td>
              </tr>
              <tr>
                <td>Distribución</td>
                <td align="center">{{ $datosVariables['distribucion']['impte'] }}</td>
                <td align="right">{{ $datosVariables['distribucion']['kw'] }}</td>
                <td align="right">{{ $datosVariables['distribucion']['kwh'] }}</td>
                <td align="right">{{ $datosVariables['distribucion']['tot'] }}</td>
              </tr>
              <tr>
                <td>Transmisión</td>
                <td align="center">{{ $datosVariables['transmision']['impte'] }}</td>
                <td align="right">{{ $datosVariables['transmision']['kw'] }}</td>
                <td align="right">{{ $datosVariables['transmision']['kwh'] }}</td>
                <td align="right">{{ $datosVariables['transmision']['tot'] }}</td>
              </tr>
              <tr>
                <td>CENACE</td>
                <td align="center">{{ $datosVariables['cenace']['impte'] }}</td>
                <td align="right">{{ $datosVariables['cenace']['kw'] }}</td>
                <td align="right">{{ $datosVariables['cenace']['kwh'] }}</td>
                <td align="right">{{ $datosVariables['cenace']['tot'] }}</td>
              </tr>
              <tr>
                <td>Generación B</td>
                <td align="center">{{ isset($datosVariables['generacionB']['impte']) ? $datosVariables['generacionB']['impte'] : 0 }}</td>
                <td align="right">{{ isset($datosVariables['generacionB']['kw']) ? $datosVariables['generacionB']['kw'] : 0 }}</td>
                <td align="right">{{ isset($datosVariables['generacionB']['kwh']) ? $datosVariables['generacionB']['kwh'] : 0 }}</td>
                <td align="right">{{ isset($datosVariables['generacionB']['tot']) ? $datosVariables['generacionB']['tot'] : 0 }}</td>
              </tr>
              <tr>
                <td>Generación I</td>
                <td align="center">{{ isset($datosVariables['generacionI']['impte']) ? $datosVariables['generacionI']['impte'] : 0 }}</td>
                <td align="right">{{ isset($datosVariables['generacionI']['kw']) ? $datosVariables['generacionI']['kw'] : 0 }}</td>
                <td align="right">{{ isset($datosVariables['generacionI']['kwh']) ? $datosVariables['generacionI']['kwh'] : 0 }}</td>
                <td align="right">{{ isset($datosVariables['generacionI']['tot']) ? $datosVariables['generacionI']['tot'] : 0 }}</td>
              </tr>
              <tr>
                <td>Generación P</td>
                <td align="center">{{ isset($datosVariables['generacionP']['impte']) ? $datosVariables['generacionP']['impte'] : 0 }}</td>
                <td align="right">{{ isset($datosVariables['generacionP']['kw']) ? $datosVariables['generacionP']['kw'] : 0 }}</td>
                <td align="right">{{ isset($datosVariables['generacionP']['kwh']) ? $datosVariables['generacionP']['kwh'] : 0 }}</td>
                <td align="right">{{ isset($datosVariables['generacionP']['tot']) ? $datosVariables['generacionP']['tot'] : 0 }}</td>
              </tr>
              <tr>
                <td>Capacidad</td>
                <td align="center">{{ $datosVariables['capacidad']['impte'] }}</td>
                <td align="right">{{ $datosVariables['capacidad']['kw'] }}</td>
                <td align="right">{{ $datosVariables['capacidad']['kwh'] }}</td>
                <td align="right">{{ $datosVariables['capacidad']['tot'] }}</td>
              </tr>
              <tr>
                <td>SCnMEM</td>
                <td align="center">{{ $datosVariables['scnmem']['impte'] }}</td>
                <td align="right">{{ $datosVariables['scnmem']['kw'] }}</td>
                <td align="right">{{ $datosVariables['scnmem']['kwh'] }}</td>
                <td align="right">{{ $datosVariables['scnmem']['tot'] }}</td>
              </tr>
              <tr>
                <td><strong>Total</strong></td>
                <td align="center"><strong>{{ number_format($datosVariables['impteTotal'], 2, '.', ',') }}</strong></td>
                <td align="right"><strong>{{ number_format($datosVariables['kwTotal'], 2, '.', ',') }}</strong></td>
                <td align="right"><strong>{{ number_format($datosVariables['kwhTotal'], 2, '.', ',') }}</strong></td>
                <td align="right"><strong>{{ number_format($datosVariables['totTotal'], 2, '.', ',') }}</strong></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
        <div class="col-xs-4 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr bgcolor="#1dd1a1">
                <th style="text-align: center;">Concepto</th>
                <th style="text-align: center;">Importe (MXN)</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($datosVariables['importe'] as $key => $item)
              <tr>
                @if ($item['concepto'] === 'Total')
                <td align="right"><strong>{{ $item['concepto'] }}</strong></td>
                <td align="right"><strong>${{ number_format( $item['Importe'], 2, '.', ',') }}</strong></td>
                @else
                <td align="right">{{ $item['concepto'] }}</td>
                <td align="right">{{ number_format( $item['Importe'], 2, '.', ',') }}</td>
                @endif
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.end box -->
  </br>
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="#" class="btn btn-info" onClick="window.print()"><i class="fa fa-print"></i> Imprimir</a>
      @if ($recibo->nombre_archivo_pdf)
      <a href={{ asset('/facturas/pdf/'.$recibo->nombre_archivo_pdf) }} class="btn btn-primary pull-right" style="margin-right: 5px;"
        target="_blank"><i class="fa fa-download"></i> Recibo PDF</a>
      @endif
    </div>
  </div>
</section>
@endsection