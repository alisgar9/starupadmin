@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Recibos<small>Administrador de municipios</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="{{ url('recibos') }}">Recibos</a></li>
        <li class="active">Agregar</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Nuevo Recibo</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{ Form::open(array('url' => 'recibos','files' => true)) }}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::Label('item', 'Municipios') !!}
                        {!! Form::select('municipios', $municipios, null, ['class' => 'form-control','placeholder' =>
                        'seleccione uno...']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::Label('item', 'Áreas') !!}
                        {!! Form::select('areas', $areas, null, ['class' => 'form-control','placeholder' => 'seleccione
                        uno...']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::Label('item', 'Centro de Costos') !!}
                        {!! Form::select('costos', $costos, null, ['class' => 'form-control','placeholder' => 'seleccione
                        uno...']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::Label('item', 'Ubicaciones') !!}
                        {!! Form::select('ubicaciones', $ubicaciones, null, ['class' => 'form-control','placeholder' => 'seleccione
                        uno...']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::Label('item', 'Empresas') !!}
                        {!! Form::select('empresas', $empresas, null, ['class' => 'form-control','placeholder' => 'seleccione
                        uno...']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Archivo en Formato XML') !!}
                        {!! Form::file('reciboxml', null) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Recibo en Formato PDF') !!}
                        {!! Form::file('recibopdf', null) !!}
                    </div>

                </div>

                <div class="box-footer">
                    {{ Form::button('<i class="fa fa-save" aria-hidden="true"></i> Guardar', ['class' => 'btn
                    btn-primary', 'type' => 'submit']) }}
                </div>
                {{ Form::close() }}
            </div>
            <!-- /.box -->

        </div>
        <!--/.col (left) -->

    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection