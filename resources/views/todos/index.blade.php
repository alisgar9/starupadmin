@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Clientes<small>Club América</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Clientes Club América</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">



  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-ame"><i class="fa fa-users"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Clientes Club América</span>
          <span class="info-box-number">{{$americanistas}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-ame"><i class="fa fa-download" aria-hidden="true"></i></span>
        <div class="info-box-content">
          <a href="{{ route('get.download.clientes') }}">
            <span class="info-box-text">Árchivo</span>
            <span class="info-box-number">Descargar</span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Especificaciones de lectura para el documento de Clientes Club América.</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-md-3">
            <div class="card border border-primary">
              <div class="card-header">
                <strong class="card-title text text-center">Valor</strong>
              </div>
              <div class="card-body text-center">
                <h4><i class="fa fa-check" aria-hidden="true"></i> 0</h4>
                <h4><i class="fa fa-check" aria-hidden="true"></i> 1</h4>
                <h4><i class="fa fa-check" aria-hidden="true"></i> 2</h4>
                <h4><i class="fa fa-check" aria-hidden="true"></i> 3</h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card border border-primary">
              <div class="card-header">
                <strong class="card-title text text-center">Status</strong>
              </div>
              <div class="card-body">
                <ul>
                  <h4>SIN ACCESO</h4>
                  <h4>ACTIVO</h4>
                  <h4>CAMBIO DE CONTRASEÑA</h4>
                  <h4>BAJA</h4>
                </ul>
              </div>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->

  </div>

</section>

@endsection