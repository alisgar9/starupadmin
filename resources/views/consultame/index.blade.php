@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Consulta de<small>Clientes</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Consulta de Clientes</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

  
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Búsqueda por:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body text-center">
         
         @if(session()->has('message'))
            <div class="alert alert-success">{{ session()->get('message') }} </div>
         @endif 
            <form id="filtrar">
               <div class="row">
                  <div class="col-md-3">
                     <div class="input-group input-group-sm">
                        <span class="input-group-addon"><i class="fa fa-id-card-o" aria-hidden="true"></i> </span>
                        <input type="text" class="form-control text-uppercase" required="required" placeholder="Nº de Afiliación" name="Afiliacion" id="Afiliacion" autocomplete="off" >
                        <span class="input-group-btn">
                        <button type="button" id="btnFiltro" data-id="SAF" class="btn btn-info btn-flat"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </span>
                     </div>
                  </div>
                  
                  <div class="col-md-3">
                     <div class="input-group input-group-sm">
                        <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                        <input type="text" class="form-control text-uppercase" placeholder="Nombre" name="Nombre" id="Nombre" autocomplete="off" >
                        <span class="input-group-btn">
                        <button type="button" id="btnFiltro" data-id="SNO" class="btn btn-info btn-flat"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </span>
                     </div>
                  </div>

                  <div class="col-md-3">
                     <div class="input-group input-group-sm">
                        <span class="input-group-addon"><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                        <input type="text" class="form-control text-uppercase" placeholder="A. Paterno" name="Apaterno" id="Apaterno" autocomplete="off">
                        <span class="input-group-btn">
                        <button type="button" id="btnFiltro" data-id="SAP" class="btn btn-info btn-flat"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </span>
                     </div>
                  </div>

                  <div class="col-md-3">
                     <div class="input-group input-group-sm">
                        <span class="input-group-addon"><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                        <input type="text" class="form-control text-uppercase" placeholder="A. Materno" name="Amaterno" id="Amaterno" autocomplete="off" >
                        <span class="input-group-btn">
                        <button type="button" id="btnFiltro" data-id="SAM" class="btn btn-info btn-flat"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </span>
                     </div>
                  </div>                  
               </div>
            </form>

        </div>
      </div>
    <!-- /.col -->
    </div>

      <div class="row" id="resultado">
      
      </div>    


    </div>


<div id="confirmDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <!-- header modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">Activar Descuento</h4>
        </div>

        <!-- body modal -->
        <div class="modal-body text-center">
          ¿Por favor, confirme que desea activar el descuento <b><span id="fav-title"></span></b>?
          <hr>
          <form action="{{ url('clientes/activar/descuento') }}" id="delForm" method="post">
            <div class="row">
                <div class="col-md-12">
                <label>Descripción de Producto, para aplicar descuento.</label> 
                </div>
                <div class="col-md-12">
                <input name="descripcion" id="descripcion" autocomplete="off" type="text" class="form-control" required />
                </div>
            </div>
            <div class="row">
              <input name="id_americanista" id="id_emp" type="hidden" />
              <input name="_token" type="hidden" value="{{ csrf_token() }}" />
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
              <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


<div class="modal fade in" id="modal-danger" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">* Campo Requerido</h4>
              </div>
              <div class="modal-body">
                <h4 class="text-center">El campo es requerido para ejecutar la consulta.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Intentar de Nuevo</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
  </section>
<script type="text/javascript">
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var id_americanista = $(e.relatedTarget).data('idemp');
      modal.find('.modal-body #id_emp').val(id_americanista);
    });
  });
  $(document).ready( function() {
    $("#resultado").hide();  
    
    
    $("button[id=btnFiltro]").on('click',function () {
      $("#resultado").empty();
        var Afiliacion = $("#Afiliacion").val();
        var Nombre = $("#Nombre").val();
        var Apaterno = $("#Apaterno").val();
        var Amaterno = $("#Amaterno").val();
        var Filtro = $(this).data("id");

      if( Afiliacion.length > 0 || Nombre.length > 0 || Apaterno.length > 0 || Amaterno.length > 0){  
        $("#resultado").show();
        $("#resultado").append('<div class="col-md-12"><div class="box box-primary"><div class="box-header with-border"><h3 class="box-title"></h3></div><div class="box-body text-center"><table id="americanista-table" class="table table-striped table-bordered dt-responsive"></table></div></div></div>');

        $('#americanista-table').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
              url: "{{ url('clientes/buscar') }}",
              type: "POST",
              data: function (d) {
                    d.Afiliacion = Afiliacion;
                    d.Nombre = Nombre;
                    d.Apaterno = Apaterno;
                    d.Amaterno = Amaterno;
                    d.Filtro = Filtro;
                    d._token = "{{ csrf_token() }}";
              },
          },
          language: {
            "search": "Buscar",
            "lengthMenu": "Mostar _MENU_ registros por página",
            "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
            "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
            "infoEmpty": "Registros no encontrados",
            "infoFiltered": "(Filtrado en _MAX_ registros totales)",
            "paginate": {
              "previous": "Anterior",
              "next": "Siguiente",
            },
          },
          columns: [
            { data: 'numeroAmericanista', name: 'numeroAmericanista', title: 'Número Americanista' },
            { data: 'nombre', name: 'nombre', title: 'Nombre' },
            { data: 'paterno', name: 'paterno', title: 'Apellido Paterno' },
            { data: 'materno', name: 'materno', title: 'Apellido Materno' },
            { data: 'fechaVigencia', name: 'fechaVigencia', title: 'Fecha Vigencia',"render": function (data) {
                  var date = new Date(data);
                  var month = date.getMonth() + 1;
                  return date.getFullYear() + "-" + (month.length > 1 ? month :  month) + "-" + date.getDate()  ;
                } 
            },
            {
              data: 'activo', name: 'activo', title: 'Estado', "render": function (data, type, full, meta) {
                if (data == 1) {
                  return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Activo';
                } else {
                  return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Inactivo';
                }
              }
            },
            { data: 'action', name: 'action', title: 'Acciones', orderable: false, searchable: false }
          ]
        });
     }else{
            $('#modal-danger').modal('show');
          }


    });    
});

</script>
@endsection