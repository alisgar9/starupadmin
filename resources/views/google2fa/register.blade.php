@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Set up Google Authenticator</div>

                <div class="card-body">

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">Please scan the barcode below. Alternatively, you can  use the code {{$secret}}</label>

                            <div class="col-md-6">
                                <img src="{{ $QR_Image }}">
                            </div>
                            <p>You need to set up your Google Authenticator app before continuing. You will be unable to login otherwise </p>
                        </div>

                        <div class="form-group row">
                          <a href="{{url(/)}}/complete-registration"><button class="btn-success btn">continue</button></a>                            
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
