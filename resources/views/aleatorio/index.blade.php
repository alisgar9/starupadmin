@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Consulta de Clientes<small> Aleatorios</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Consulta de Clientes Aleatorios</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Cantidad de Clienes por Mostrar:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body text-center">
         
         @if(session()->has('message'))
            <div class="alert alert-success">{{ session()->get('message') }} </div>
         @endif 
            <form id="filtrar">
               <div class="row">
                  <div class="col-md-12">
                     <div class="input-group input-group-sm">
                        <span class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i> Cantidad de Clienes por Mostrar:</span>
                        <input type="number" class="form-control" placeholder="Cantidad" name="cantidad" id="cantidad" autocomplete="off" required="required" >
                        <span class="input-group-btn">
                        <button type="button" id="btnFiltro" class="btn btn-info btn-flat"><i class="fa fa-eye" aria-hidden="true"></i> Mostrar!</button>
                        </span>
                     </div>
                     <!-- /input-group -->
                  </div>
                  <!-- /.col-lg-6 -->
               </div>
            </form>

        </div>
      </div>
    <!-- /.col -->
    </div>

      <div class="row" id="resultado">
      
      </div>    


    </div>


  <div class="modal fade in" id="modal-danger" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">* Campo Requerido</h4>
              </div>
              <div class="modal-body">
                <h4 class="text-center">Se requiere la cantidad de clientes a consultar.</h4>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Intentar de Nuevo</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
  </section>
<script type="text/javascript">
  $(document).ready(function () {
    // se activa cuando el modal está a punto de ser mostrado.
    $('#confirmDeleteComment').on('show.bs.modal', function (e) {
      var modal = $(this);

      // obtener el atributo idEmpresa del elemento pulsado.
      $("#fav-title").html($(e.relatedTarget).data('title'));
      var id_americanista = $(e.relatedTarget).data('idemp');
      modal.find('.modal-body #id_emp').val(id_americanista);
    });
  });
  $(document).ready( function() {
    $("#resultado").hide();  
    
    
    $("button[id=btnFiltro]").on('click',function () {
      $("#resultado").empty();
        var cantidad = $("#cantidad").val();
        if(cantidad.length > 0){ 
              $("#resultado").show();
              $("#resultado").append('<div class="col-md-12"><div class="box box-primary"><div class="box-header with-border"><h3 class="box-title"></h3></div><div class="box-body text-center"><table id="americanista-table" class="table table-striped table-bordered dt-responsive"></table></div></div></div>');

              $('#americanista-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('clientes/aleatorio/buscar') }}",
                    type: "POST",
                    data: function (d) {
                          d.cantidad = cantidad;
                          d._token = "{{ csrf_token() }}";
                    },
                },
                language: {
                  "search": "Buscar",
                  "lengthMenu": "Mostar _MENU_ registros por página",
                  "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
                  "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
                  "infoEmpty": "Registros no encontrados",
                  "infoFiltered": "(Filtrado en _MAX_ registros totales)",
                  "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente",
                  },
                },
                columns: [
                  { data: 'numeroAmericanista', name: 'numeroAmericanista', title: 'Número Americanista' },
                  { data: 'nombre', name: 'nombre', title: 'Nombre' },
                  { data: 'paterno', name: 'paterno', title: 'Apellido Paterno' },
                  { data: 'materno', name: 'materno', title: 'Apellido Materno' },
                  { data: 'correo', name: 'correo', title: 'Correo Electrónico' },
                  { data: 'telefono', name: 'telefono', title: 'Teléfono' },
                  {
                    data: 'activo', name: 'activo', title: 'Estado', "render": function (data, type, full, meta) {
                      if (data == 1) {
                        return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Activo';
                      } else {
                        return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Inactivo';
                      }
                    }
                  },
                ]
              });

          }else{
            $('#modal-danger').modal('show');
          }

    });    
});

</script>
@endsection