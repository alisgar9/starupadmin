@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Eliminar<small>Americanista por Mes</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Eliminar Americanista por Mes</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fa fa-ban"></i> ¡Alerta!</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
    </div>
    @endif

    @if(session()->has('message'))
            <div class="alert alert-success">{{ session()->get('message') }} </div>
    @endif 

  <div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-ame"><i class="fa fa-users"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Americanistas X Eliminar</span>
          <span class="info-box-number" id="bloque">{{$total}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
   <!--
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-ame"><i class="fa fa-share" aria-hidden="true"></i></span>
        <div class="info-box-content">
          <a href="{{ route('get.download') }}">
            <span class="info-box-text">Árchivo del Mes</span>
            <span class="info-box-number">Descargar</span></a>
        </div>
      </div>
    </div>
  </div>-->

  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Selecciona el Mes y Año para Eliminar Americanista.</h3>
        </div>
        <div class="box box-default">
        {{ Form::open(array('url' => 'mantenimiento/elimina')) }}
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <form id="data">
            <div class="col-md-6">
              <div class="form-group">
                <label>Mes</label>
                {{ Form::selectMonth('month', $month, array('class' => 'form-control select2 select2-hidden-accessible', 'style' => 'width: 100%;')) }}
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                <label>Año</label>
                {{ Form::selectYear('year', 2013, $Year, $Year, array('class' => 'form-control select2 select2-hidden-accessible', 'style' => 'width: 100%;')) }}
        <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div id="msg">
            <h3 class="box-title text-center"><i class="fa fa-trash-o"></i>  <span id="front">{{$total}}</span> <span id="dinamico" style="visibility: none"></span>,  total de Americanistas por eliminar. Haga clic para continuar el proceso de eliminación.</h3>
            <p class="text-center">Los registros por eliminar, se borraran de la base de datos. </p>
            <p class="text-center"><button type="submit" class="btn btn-info" onclick="return confirm('¿Estás seguro que quieres eliminar los registros Filtrados.?');" id="enviar"><i class="fa fa-ban" aria-hidden="true"></i> Iniciar eliminación de Americanistas</button></p>
          </div>
          <div id="msgVacio" style="display:none;">
            <h3 class="box-title text-center"> No se encontraron Registros por Eliminar.</h3>
          </div>

        </div>
       </form> <!-- /.box-body -->
      </div>
      <!-- /.box -->
      {{ Form::close() }}
    </div>
    <!-- /.col -->

  </div>

</section>

<script type="text/javascript">
  
  $(document).ready( function() {
    
    $("#msgVacio").prop('disabled', true);

    $("select[name=month]").change(function(){
      $("#front").empty();
      $("#dinamico").empty();
      $("#bloque").empty();
      
        var month = $(this).val();
        var year = $("select[name=year]").val();

        $("#dinamico").show();

           $.ajax({  
                url:"{{ url('mantenimiento/elimina/buscar') }}",  
                method:"POST",  
                data:{
                  "month":month,
                  "year":year,
                  "_token":"{{ csrf_token() }}"
                },  
                success:function(data){ 

                     console.log(data);
                     //$('#result').html(data);
                     $("#dinamico").append('<b>'+data.total+'</b>');  
                     $("#bloque").append(data.total);
                     
                     if(data.total > 0){ 
                      $("#enviar").prop('disabled', false);
                     }else{ 
                      $("#enviar").prop('disabled', true);
                      $("#msgVacio").prop('disabled', false);
                      $("#msg").prop('disabled', true);

                     }
                     
                }  
           });  
    });

    $("select[name=year]").change(function(){
      $("#front").empty();
      $("#dinamico").empty();
      $("#bloque").empty();
        var year = $(this).val();
        var month = $("select[name=month]").val();

        $("#dinamico").show();

           $.ajax({  
                url:"{{ url('mantenimiento/elimina/buscar') }}",  
                method:"POST",  
                data:{
                  "month":month,
                  "year":year,
                  "_token":"{{ csrf_token() }}"
                },  
                success:function(data){ 
                     //$('#result').html(data);
                     $("#dinamico").append('<b>'+data.total+'</b>');  
                     $("#bloque").append(data.total);

                     if(data.total > 0){ 
                      $("#enviar").prop('disabled', false);
                     }else{ 
                      $("#enviar").prop('disabled', true);
                     }
                     
                }  
           });  
    });

});

</script>
@endsection