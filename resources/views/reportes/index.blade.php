@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Clientes<small>Activos</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Clientes  Activos</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

  @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Éxito!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Clientes Activos</span>
          <span class="info-box-number">{{$americanistas}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div> 
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-download" aria-hidden="true"></i></span>
        <div class="info-box-content">
          <a href="{{ route('get.download') }}">
          <span class="info-box-text">Árchivo</span>
          <span class="info-box-number">Descargar</span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Clientes Activos.</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body text-center">
              <table id="americanista-table" class="table table-striped table-bordered dt-responsive"></table>
              

         
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    
  </div>

</section>

<script>
  

  $(function () {
    $('#americanista-table').DataTable({
      processing: true,
      serverSide: true,
      type: 'GET',
      ajax: "{{ route('get.americanistas') }}",
      language: {
        "search": "Buscar",
        "lengthMenu": "Mostar _MENU_ registros por página",
        "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
        "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
        "infoEmpty": "Registros no encontrados",
        "infoFiltered": "(Filtrado en _MAX_ registros totales)",
        "paginate": {
          "previous": "Anterior",
          "next": "Siguiente",
        },
      },
      columns: [
        { data: 'numeroAmericanista', name: 'numeroAmericanista', title: 'Numero Americanista' },
        { data: 'nombre', name: 'nombre', title: 'Nombre' },
        { data: 'paterno', name: 'paterno', title: 'Apellido Paterno' },
        { data: 'materno', name: 'materno', title: 'Apellido Materno' },
        {
          data: 'activo', name: 'activo', title: 'Estado', "render": function (data, type, full, meta) {
            if (data == 1) {
              return '<div class="iradio_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Habilitada';
            } else {
              return '<div class="iradio_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="radio" name="r3" class="flat-red" checked="" style="position: absolute; opacity: 0;"></div> Eliminada';
            }
          }
        }
      ]
    });
  });
</script>
</section>
@endsection