@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Permisos<small>Administrador de permisos</small></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Permisos</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->


<!-- Main content -->
<section class="content">

@if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Éxito!</h4>
    {{ session()->get('flash_message') }}
  </div>
@endif

	<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-key"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Permisos</span>
              <span class="info-box-number"><h3>{{ count($permissions) }}</h3></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    </div>
	<!-- /.row -->    
  <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Lista de Permisos</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Nombre</th>
                  <th>Asignados a Permisos</th>
                </tr>
                @foreach ($permissions as $key=>$permission)
                <tr>
                  <td>{{ ++$key }}.</td>
                  <td>{{ $permission->name }}</td> 
                  <td>{{ $permission->count }}</td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
</section>
@endsection