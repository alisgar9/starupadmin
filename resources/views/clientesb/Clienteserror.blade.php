@extends('adminlte::layouts.errors')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.pagenotfound') }}
@endsection

@section('main-content')

    <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Ha ocurrido un error.</h3>
            <p>Contacte al administrador del sistema</p>
            <p class="text-red"><b>Error:</b> {{$message}}</p>
            <form class='search-form'>
                <div class='input-group'>
                    <div class="input-group-btn">
                        <a href="{{ url('home') }}" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                        Inicio </a>
                    </div>
                </div><!-- /.input-group -->
            </form>
        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
@endsection



