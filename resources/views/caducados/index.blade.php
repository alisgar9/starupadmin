@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Pagos<small>Caducados</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Pagos Caducados</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">



  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-ame"><i class="fa fa-users"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Pagos Caducados</span>
          <span class="info-box-number">{{$pagosCaducados}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-ame"><i class="fa fa-download" aria-hidden="true"></i></span>
        <div class="info-box-content">
          <a href="{{ route('get.download.pagos') }}">
            <span class="info-box-text">Árchivo</span>
            <span class="info-box-number">Descargar</span></a>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- ./col -->
  </div>
  <!-- /.row -->
  


</section>

@endsection