@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Perfiles<small>Control de perfiles para usuarios</small></h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Perfiles</li>
    </ol>
</section>
<!-- /.Content Header (Page header) -->


<!-- Main content -->
<section class="content">

@if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Éxito!</h4>
    {{ session()->get('flash_message') }}
  </div>
@endif

	<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-lock"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Perfiles</span>
            <span class="info-box-number"><h3>{{ count($roles) }}</h3></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
    </div>
	<!-- /.row -->    
  <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Lista de Perfiles</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Perfil</th>
                  <th>Permisos</th>
                  <th>Creado</th>
                  <th>Actualizado</th>
                  <th>Acciones</th>

                </tr>
                @foreach ($roles as $key=>$role)
                <tr>
                  <td>{{ ++$key }}.</td>
                  <td>{{ $role->name }}</td>
                  <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                  <td>{{ $role->created_at }}</td>
                  <td>{{ $role->updated_at }}</td>
                  <td>
                    @if ($role->id != 1)
                      <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;"><i class="fa fa-edit"></i></a>

                      {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                      {{ Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', ['class' => 'btn btn-danger', 'type' => 'submit']) }}
                      {!! Form::close() !!}
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

   
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li>
              </ul>
            </div>
          </div>
          <!-- /.box -->

          <a href="{{ URL::to('roles/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Agregar Perfil</a>
        </div>
        <!-- /.col -->
      </div>
</section>
@endsection