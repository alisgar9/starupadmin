@extends('adminlte::layouts.app')

@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Bitácora<small>Bitácora de Eventos</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Bitácora</li>
  </ol>
</section>
<!-- /.Content Header (Page header) -->

<!-- Main content -->
<section class="content">

  @if(session()->has('flash_message'))
  <div class="alert alert-success alert-dismissible fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-check"></i> Éxito!</h4>
    {{ session()->get('flash_message') }}
  </div>
  @endif

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Lista de Actividades</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="eventos-table" class="table table-striped table-bordered dt-responsive nowrap"></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
  </div>

  <script>
    $(function () {
      $('#eventos-table').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        type: 'GET',
        order: [[0, 'desc']],
        deferRender: true,
        scrollX: true,
        scrollCollapse: true,
        scroller: true,
        ajax: "{{ route('get.eventos') }}",
        language: {
          "search": "Buscar",
          "lengthMenu": "Mostar _MENU_ registros por página",
          "zeroRecords": "Lo sentimos, no encontramos lo que estas buscando",
          "info": "Mostrando página _PAGE_ de _PAGES_ de _TOTAL_ Registros",
          "infoEmpty": "Registros no encontrados",
          "infoFiltered": "(Filtrado en _MAX_ registros totales)",
          "paginate": {
            "previous": "Anterior",
            "next": "Siguiente",
          },
        },
        columns: [
          { data: 'id', name: 'id', title: '# No' },
          { data: 'subject', name: 'subject', title: 'Evento' },
          { data: 'url', name: 'url', title: 'URL', className: 'text-success', width: '25%' },
          {
            data: 'method', name: 'method', title: 'Método', "render": function (data, type, full, meta) {
              return '<label class="label label-info">' + data + '</label>';
            }
          },
          { data: 'ip', name: 'ip', title: 'Ip', className: 'text-warning' },
          { data: 'agent', name: 'agent', title: 'Navegador', className: 'text-danger' },
          { data: 'user_id', name: 'user_id', title: 'Id Usuario' },
          { data: 'created_at', name: 'created_at', title: 'Fecha / Hora' },
        ]
      });
    });
  </script>
</section>
@endsection