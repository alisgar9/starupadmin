@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-danger mb-3">
                <div class="card-header">Cuenta por Aprobar</div>
                <div class="card-body text-danger">
                    <h5 class="card-title">Acceso en proceso de validación</h5>
                    Su cuenta está esperando la aprobación de nuestro administrador.
                    <br />
                    
                </div>
                <div class="card-footer bg-transparent border-danger">Por favor, intente más tarde.</div>
            </div>
        </div>
    </div>
</div>
@endsection