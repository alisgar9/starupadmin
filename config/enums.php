<?php

return [
    'filter_types' => [
        'AREA' => "Entidad",
        'MUNICIPIO' => "Banco",
        'EMPRESA' => "Clasificacion",
    ],
    'report_format' => [
        'PDF' => "PDF",
        'EXCEL' => "Excel",
    ],
    'company_add' => [
        1 => "MANUAL",
        2 => "XML",
    ]
];