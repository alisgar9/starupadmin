<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperacionDetails extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'adm_operaciones_contenedor';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'operacion_id',
      'container_number',
      'container_seal',
      'mark',
      'container_type',
      'weight',
      'volume',
      'activo',
      'empty_container'
  ];

// establecer un valor predeterminado.
protected $attributes = array(
  'activo' => 1,
);

public function contenedor_tipo()
{
    return $this->belongsTo('App\Contenedor','container_type');
}


  
}
