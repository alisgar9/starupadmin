<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username', 'admin', 'approved_at', 'token_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        // check if the value is already a hash (Regex: String begins with '$2y$##$' followed by at least 50 characters)
        if ( preg_match('/^\$2y\$[0-9]*\$.{50,}$/', $password) ) {
            // if it is so, set the attribute without hashing again
            $this->attributes['password'] = $password;
        }
        else {
            // else hash the password and set as attribute
            $this->attributes['password'] = bcrypt($password);
        }
    }

      // establecer un valor predeterminado.
        protected $attributes = array(
            'status' => 1,
        );

      //tablas relacionales  
        public function Userdetails()
        {
            return $this->belongsTo('App\Userdetails','user_id');
        }

        public function detalleUser()
        {
            return $this->belongsTo('App\Userdetails','user_id');
        }
        
        
}
