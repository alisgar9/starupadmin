<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Puerto;
use App\Pais;
use App\Entidades;
use App\Operacion;
use App\Liberacion;
use App\Incoterm;
use App\Contenedor;
use App\OperacionDetails;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;
Use Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use DB;
use Carbon\Carbon;

class OperationDetails extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $operacion_id = decrypt($request['id_entidad']);

        //Validate data fields
        $this->validate($request, [
            'tipo_operacion' => 'required|max:1',
            "container_number"  => "required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,3",
            "container_type"  => "max:3",
            "chargable_weight"  => "required_if:tipo_operacion,==,2|between:0,10",
            "weight"  => "required|between:0,10",
            "volume"  => "required|between:0,10",
            
        ],[
            'tipo_operacion.required' => 'Requerid field Operation!',
            'container_number.required_if' => 'Requerid field #Ctnr  or #Box!',
            'chargable_weight.required_if' => 'Requerid field Chargable Weight!',
            'weight.required' => 'Requerid field Weight!',
            'volume.required' => 'Requerid field Volume!',
            'container_number.unique' => 'Field #Ctnr  or #Box duplicate!',
            
        ]);


        $operacion = new OperacionDetails();
        $operacion->operacion_id = $operacion_id;
        $operacion->container_number = isset($request['container_number']) ? Str::upper($request['container_number']) : '';
        $operacion->container_seal = isset($request['container_seal']) ? Str::upper($request['container_seal']) : '';
        $operacion->mark = isset($request['mark']) ? Str::upper($request['mark']) : '';
        $operacion->container_type = isset($request['container_type']) ? $request['container_type'] : '0';
        $operacion->weight = isset($request['weight']) ? Str::upper($request['weight']) : '0';
        $operacion->chargable_weight = isset($request['chargable_weight']) ? Str::upper($request['chargable_weight']) : '0';
        $operacion->volume = isset($request['volume']) ? Str::upper($request['volume']) : '0';
        $operacion->save();

        // Registro en log.
        \Log::info('|::| Detalle contenedor .store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Operacion' => $operacion]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$operacion->container_number);


        return Redirect::back()->with('success', 'Container detail added successfully! '.$operacion->container_number);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        

        //Validate data fields
        $this->validate($request, [
            'tipo_operacion' => 'required|max:1',
            "container_type"  => "max:3",
            "chargable_weight"  => "required_if:tipo_operacion,==,2|between:0,10",
            "weight"  => "required|between:0,10",
            "volume"  => "required|between:0,10",
            
        ],[
            'tipo_operacion.required' => 'Requerid field Operation!',
            'chargable_weight.required_if' => 'Requerid field Chargable Weight!',
            'weight.required' => 'Requerid field Weight!',
            'volume.required' => 'Requerid field Volume!',
            'container_number.unique' => 'Field #Ctnr  or #Box duplicate!',
            
        ]);

        
        $id = decrypt($request['id_detailo']);
        $detalle = OperacionDetails::findOrFail($id);
        $detalle->update(
        array(
        'container_number' => isset($request['container_number']) ? Str::upper($request['container_number']) : '',
        'container_seal' => isset($request['container_seal']) ? Str::upper($request['container_seal']) : '','mark' => isset($request['mark']) ? Str::upper($request['mark']) : '', 
        'container_type' => isset($request['container_type']) ? $request['container_type'] : '0', 
        'weight' => isset($request['weight']) ? Str::upper($request['weight']) : '0', 
        'volume' => $request['volume'],
        'chargable_weight' => isset($request['chargable_weight']) ? Str::upper($request['chargable_weight']) : '0',
        )
        );
        // Registro en Bitácora.
        \LogActivity::addToLog('Detail Update: '.$request->container_number);

        return Redirect::back()->with('success', 'Detail '.$request->container_number.' update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        /** Status(activo) de Contenedor son:
         * 0 Eliminado
         * 1 Activo
         * 2 Contenedor retornado
         * 3 Cancelado
        **/

        //Validate data fields
        $this->validate($request, [
            'status' => 'required',
            "id_detail_contr"  => "required"
            
        ],[
            'id_detail_contr.required' => 'Error try again',
            'status.required' => 'Select Cancel or Remove'
        ]);

        if($request->comments != NULL){
            $mytime = Carbon::now();
            $comments = array();
            $comments['date']= $mytime->toDateTimeString();
            $comments['comment']=$request->comments;
            $data = serialize($comments);

        }else{ $data = "";}

        
        $operaciondetails = OperacionDetails::findOrFail(decrypt($request->id_detail_contr));
        $operaciondetails->container_number = 'D'.date("Ymds").$operaciondetails->container_number;
        $operaciondetails->activo   = decrypt($request->status);
        $operaciondetails->comments = $data ;
        $operaciondetails->save();

        //$res=OperacionDetails::where('id',decrypt($request->id_detail_contr))->delete();

        // Registro en log.
        \Log::info('|::| Detalle Contenedor.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Contenedor' => $operaciondetails]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$operaciondetails->container_number);

        return Redirect::back()->with('success', 'Detail '.$operaciondetails->container_number.' Removed!');
        
    }


    public function search()
    {
        // Registro de log.
        \Log::info('|::| Operaciones filtrado de contenedores.', ['Usuario' => Auth::user()]);

         //Get all Operaciones and pass it to the view

         $IdCurrentUser = Auth::id();
         $contenedores = DB::table('adm_operaciones_contenedor as A')
            ->select('A.operacion_id', 'A.container_number','A.container_seal','A.mark','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.tipo_operacion','P.operacion')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where(function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                }
            })
            ->count();

                    
         $contenedoreEnabled = DB::table('adm_operaciones_contenedor as A')
            ->select('A.operacion_id', 'A.container_number','A.container_seal','A.mark','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.tipo_operacion','P.operacion')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([[function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                }
            }],['A.activo','=',1]])
            ->count();

         $contenedoreDisabled = DB::table('adm_operaciones_contenedor as A')
            ->select('A.operacion_id', 'A.container_number','A.container_seal','A.mark','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.tipo_operacion','P.operacion')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([[function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                }
            }],['A.activo','=',0]])
            ->count();

         $contenedorReturnEmpty = DB::table('adm_operaciones_contenedor as A')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([[function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                }
            }],['A.activo','=',2]])
            ->count();

            return view('operaciones.search')->with('contenedores', $contenedores)->with('contenedoreEnabled', $contenedoreEnabled)->with('contenedoreDisabled', $contenedoreDisabled)->with('contenedorReturnEmpty', $contenedorReturnEmpty);
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getDataDetails()
    {
        
       
        $IdCurrentUser = Auth::id();
        $showDeleted  = Input::get('showDeleted');
        
        
       //$detallesoperacion = OperacionDetails::where([['activo','=',1]] )->get();
        $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
        ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.mbl','P.ETD', 'P.ETA', 'P.PO', 'P.PI', 'E.nombre as customer', 'PU1.puerto as POL', 'PU2.puerto as POD','SL.nombre AS shippingline')
        ->leftJoin('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
        ->leftJoin('adm_entidad as E', 'E.id', '=', 'P.consignee')
        ->leftJoin('adm_entidad as SL', 'SL.id', '=', 'P.shipping_line')
        ->leftJoin('adm_puertos as PU1', 'PU1.id', '=', 'P.POL')
        ->leftJoin('adm_puertos as PU2', 'PU2.id', '=', 'P.POD')
        ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
        ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
        ->where([[function($query) use ($IdCurrentUser) {
            if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
            }else{
                $query->where([['U.type','=',1]]);
            }
        }],['U.activo','=',1],['A.activo','=',$showDeleted]])
        ->orderby('P.hbl')
        ->get();


        return Datatables::of($detallesoperacion)
            
            ->addColumn('action', function ($detallesoperacion) {

                $result = '';

                if ($detallesoperacion->activo == 1){
                    if($this->uShare($detallesoperacion->operacion_id) != 0)
                    {

                            if (Auth::user()->hasPermissionTo('Editar'))
                            $result .= " <a href='".route('operaciones.edit', encrypt($detallesoperacion->operacion_id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>
                            <button type='submit' class='btn btn-sm btn-warning' data-toggle='modal' data-cont='".encrypt($detallesoperacion->id)."' data-title='".$detallesoperacion->hbl."' data-ncont='".encrypt($detallesoperacion->container_number)."' data-neta='".encrypt($detallesoperacion->ETA)."' data-eta='".$detallesoperacion->ETA."' data-netd='".encrypt($detallesoperacion->ETD)."'  data-target='#confirmContainerComment'><i class='fa fa-refresh' aria-hidden='true'></i></button>";
                    }
                }
                else if ($detallesoperacion->activo == 2){
                    if(Auth::user()->admin == 2){
                        if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= "<button type='submit' class='btn btn-sm btn-warning' data-toggle='modal' data-cont='".encrypt($detallesoperacion->id)."' data-title='".$detallesoperacion->hbl."' data-ncont='".encrypt($detallesoperacion->container_number)."' data-neta='".encrypt($detallesoperacion->ETA)."' data-eta='".$detallesoperacion->ETA."' data-netd='".encrypt($detallesoperacion->ETD)."'  data-target='#confirmContainerComment'><i class='fa fa-refresh' aria-hidden='true'></i></button>";  
                    }
                }else{ $result .= ''; }   

                return $result;
            })
            ->addIndexColumn()
            ->editColumn('ETD', '{{ Carbon\Carbon::parse($ETD)->format("d-m-y") }}')
            ->editColumn('ETA', '{{ Carbon\Carbon::parse($ETA)->format("d-m-y") }}')
            ->editColumn('tipo_operacion', function ($query) {

                return Carbon::parse($query->empty_container)->format("d-m-y");
            })
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    public function emptyContainer(Request $request){

        //Validate data fields
        $this->validate($request, [
            'empty_container' => 'required',
            'idContainer' => 'required',
            'ETA' => 'required',
            'ETD' => 'required'
            
        ],[
            'empty_container.required' => 'Requerid field Date!',
            'idContainer.required' => 'There is an error, try again!',
            'ETA.required' => 'There is an error, try again!',
            'ETD.required' => 'There is an error, try again!'
        ]);


        $dias = $this->isDateBetweenDates(decrypt($request->ETA), $request->empty_container);
        $NContainer = decrypt($request->NContainer);
        
        if($dias == true)
        {
            $detalle = OperacionDetails::findOrFail(decrypt($request->idContainer));
             

                

                $detalle->update(
                    array(
                        'empty_container' => $request->empty_container,
                        'activo' => 2
    
                    ));

                    //$message = $this->sendMailNotification($detalle->operacion_id, decrypt($request->idContainer));
                
                
            // Registro en log.
            \Log::info('|::| Operacion.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'HBL' => $request->Nhbl]);

            // Registro en Bitácora.
            \LogActivity::addToLog('Updated Return empty container: '.$NContainer.' of HBL: '.$request->Nhbl);

            return redirect()->back()->with('flash_message','Success'.$NContainer.' Empty Container Return');
        }else{
            return redirect()->back()->with('error_message', $NContainer.' Empty Container Date Error!'); 
            //return Redirect::back()->with('Error', 'sin cambios '.$request->hbl.' Operation!');
        }


    }

    public function sendMailNotification ($idoperacion, $contenedor){

        $data = array();
        $operacion = Operacion::where("id",$idoperacion)->first();
        $detallesoperacion = OperacionDetails::where("id",$contenedor)->first();
        $dataConsignee = Entidades::where("id",$operacion->consignee)->first();
        $cc = $dataConsignee->email_cc;
        $data['number'] = $detallesoperacion->container_number;
        $data['parrafo_1']  = 'Notification of return date of empty container';
        $data['parrafo_2']  = 'The container details are:';
        $data['details']    = '{{<b style="color:#2652a1;">Container #:</b> '.$detallesoperacion->container_number.' <br/><b style="color:#2652a1;">Weight:</b> '.$detallesoperacion->weight.' <br/><b style="color:#2652a1;">Volume #:</b> '.$detallesoperacion->volume.'<br/>}}';
        $data['consignee'] = $operacion->consigneeDesc->nombre;
        $data['nombrecontacto'] = $operacion->consigneeDesc->nombrecontacto;

        
        $message =  app('App\Http\Controllers\EmailsController')->notificationGeneral($data, $dataConsignee->email, $dataConsignee->nombrecontacto, $detallesoperacion->container_number, 'Return Empty Container', $cc);
        
        if($message == 1){
            $mess = ". Empty container return notification, was sent success";
        }else{
            $mess = ". There was an error sending the email notification";
        }
        
        return $mess;
        //return view('mail.msgoperation', compact('data','contenedor'));

    }
    

    public function isDateBetweenDates($eta, $retorno) {
        
        $hoy = Carbon::now()->format("Y-m-d");
        $eta = strtotime($eta);
        $retorno = strtotime($retorno);
        $fecha = strtotime($hoy);

        if(($eta < $retorno) && ($retorno <= $fecha)) {

            return true;

        } else {

            return false;

        }
        
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
       
        $showDeleted  = Input::get('showDeleted');
        
       

        $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
        ->select('A.id', 'A.operacion_id', 'A.container_number','A.mark','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.mbl','P.ETD', 'P.ETA', 'P.PO', 'P.PI','P.free_demurrages_days_cust', 'C.descripcion AS tipo_operacion_desc')
        ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
        ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
        ->where([['A.operacion_id','=',$showDeleted],['A.activo','>',0]] )
        ->get();

        return Datatables::of($detallesoperacion)
            ->addIndexColumn()
            ->addColumn('action', function ($detallesoperacion) {

                $result = '';

                if ($detallesoperacion->activo == 1){

                    if (Auth::user()->hasPermissionTo('Editar'))
                    $result .= "&nbsp;&nbsp;<button type='submit' class='btn btn-sm btn-info' data-toggle='modal' data-idoped='".encrypt($detallesoperacion->id)."' data-cn='".$detallesoperacion->container_number."' id='btn' data-cs='".$detallesoperacion->container_seal."' data-m='".$detallesoperacion->mark."' data-ct='".$detallesoperacion->container_type."' data-w='".$detallesoperacion->weight."' data-v='".$detallesoperacion->volume."' data-title='".$detallesoperacion->container_number."' data-target='#myModalEdit'><i class='fa fa-pencil' aria-hidden='true'></i></button>&nbsp;&nbsp;<button type='submit' class='btn btn-sm btn-warning' data-toggle='modal' data-cont='".encrypt($detallesoperacion->id)."' data-title='".$detallesoperacion->hbl."' data-ncont='".encrypt($detallesoperacion->container_number)."' data-neta='".encrypt($detallesoperacion->ETA)."' data-eta='".$detallesoperacion->ETA."' data-netd='".encrypt($detallesoperacion->ETD)."' data-target='#exampleModal'><i class='fa fa-refresh' aria-hidden='true'></i></button>";

                    if (Auth::user()->hasPermissionTo('Eliminar'))
                                    
                        $result .= "&nbsp;&nbsp;<button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idoped='".encrypt($detallesoperacion->id)."' data-operation='".encrypt($detallesoperacion->operacion_id)."'   data-title='".$detallesoperacion->container_number."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    
}
