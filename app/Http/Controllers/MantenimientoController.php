<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Americanista;
use App\Eliminados;
use PdfReport;
use ExcelReport;
use Config;
use DB;
use Response;
use Carbon;


class MantenimientoController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }
            // Registro de log.
            \Log::info('|::| Baja de Americanista.', ['Usuario' => Auth::user()]);
            $Year = date("Y");
            $Month = date("m");
            $month = intval($Month); 
            $total = Americanista::whereIn('activo', [1, 2])
            ->whereMonth('fechaVigencia', $Month)
            ->whereYear('fechaVigencia', $Year)
            ->count(); 
              $total = number_format($total);

             return view('mantenimiento.baja.index', compact('total','Year','month')); 
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }           
    }
    public function validaFecha($year, $month){
            $monthyear = date("Y-m"); 
            $comparar_fecha = $year."-".$month;
            $finish = date("Y-m",strtotime($comparar_fecha));
            if($finish < $monthyear)
             {
                 $data = 1;
             }else {
                 $data = 2;
             }

             return $data;
    }    

    public function getData(Request $request)
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }


            $this->validate($request, [
                'month' => 'required',
                'year' => 'required'
            ],[
                'month.required' => 'El Mes es requerido!',
                'year.required' => 'El Año es requerido!',
                
            ]);
            $comparar_fecha = $this->validaFecha($request->year, $request->month);    
            $total = Americanista::whereIn('activo', [1, 2])
                ->whereMonth('fechaVigencia', $request->month)
                ->whereYear('fechaVigencia', $request->year)
                ->count(); 
                $total = number_format($total);

            // Registro en log.
            \Log::info('|::| Mantenimiento: Americanistas Filtrados, por mes y año '.$request->month.'/'.$request->year.' .', ['Usuario' => Auth::user()]);

            // Registro en Bitácora.
            \LogActivity::addToLog('Mantenimiento: Americanistas Filtrados, por mes y año '.$request->month.'/'.$request->year.' .');
            
            $data = array('total' => $total,  'validar' => $comparar_fecha);
            return $data;
            
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }    
    }
    public function bajames(Request $request)
    {
        //dd($request);
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }


            $this->validate($request, [
                'month' => 'required',
                'year' => 'required'
            ],[
                'month.required' => 'El Mes es requerido!',
                'year.required' => 'El Año es requerido!',
                
            ]);
            
             
            $comparar_fecha = $this->validaFecha($request->year, $request->month);
            if($comparar_fecha == 1)
                {
                    Americanista::whereMonth('fechaVigencia', $request->month)
                    ->whereYear('fechaVigencia', $request->year)
                    ->whereIn('activo', [1, 2])
                    ->update(['activo'=>3]);
        
                    // Registro en log.
                    \Log::info('|::| Mantenimiento: Americanistas con baja, por mes.', ['Usuario' => Auth::user()]);
        
                    // Registro en Bitácora.
                    \LogActivity::addToLog('Mantenimiento: Americanistas con baja, por mes');
        
                    return redirect()->back()->with('message', 'Baja Americanistas aplicado con Éxito!!.');
                }else
                    {
                        return redirect()->back()->with('message', 'Baja de Americanistas no permitida, aún faltan días.');
                    }


           
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }     

    }

    /**
     * Eliminar Americanistas
     *
     * 
     */

    public function eliminaview()
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }
            // Registro de log.
            \Log::info('|::| Eliminar Americanistas.', ['Usuario' => Auth::user()]);
            $Year = date("Y");
            $Year = date("Y",strtotime($Year."- 2 year")); 
            $Month = date("m");
            $month = intval($Month); 
            $total = Americanista::whereMonth('fechaVigencia', $Month)
            ->whereYear('fechaVigencia', $Year)
            ->count(); 
              $total = number_format($total);

             return view('mantenimiento.elimina.index', compact('total','Year','month')); 
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }           
    }

    public function getDataElimina(Request $request)
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }


            $this->validate($request, [
                'month' => 'required',
                'year' => 'required'
            ],[
                'month.required' => 'El Mes es requerido!',
                'year.required' => 'El Año es requerido!',
                
            ]);
            $comparar_fecha = $this->validaFecha($request->year, $request->month);    
            $total = Americanista::whereMonth('fechaVigencia', $request->month)
                ->whereYear('fechaVigencia', $request->year)
                ->count(); 
                $total = number_format($total);

            // Registro en log.
            \Log::info('|::| Mantenimiento: Eliminar Americanistas Filtrados, por mes y año '.$request->month.'/'.$request->year.' .', ['Usuario' => Auth::user()]);

            // Registro en Bitácora.
            \LogActivity::addToLog('Mantenimiento: Eliminar Americanistas Filtrados, por mes y año '.$request->month.'/'.$request->year.' .');
            
            $data = array('total' => $total,  'validar' => $comparar_fecha);
            return $data;
            
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }    
    }
    public function dateClear($date){
        
        if($date == "0000-00-00" || $date == "0000-00-00 00:00:00" || $date == "NULL"){
            $date = "";
        }
        else{
            $date = date('Y-m-d', strtotime($date));
            $date = $date;
        }

        return $date;
    }
    public function deleteAmericanista(Request $request)
    {
        //dd($request);
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }


            $this->validate($request, [
                'month' => 'required',
                'year' => 'required'
            ],[
                'month.required' => 'El Mes es requerido!',
                'year.required' => 'El Año es requerido!',
                
            ]);
            
             
            $comparar_fecha = $this->validaFecha($request->year, $request->month);
            if($comparar_fecha == 1)
                {
                  $total =  Americanista::whereMonth('fechaVigencia', $request->month)
                    ->whereYear('fechaVigencia', $request->year)
                    ->get();

                    foreach ($total as $k => $v) {
                        $adds[] = [
                          'numeroAmericanista' => $v->numeroAmericanista,'numerosocio' => $v->numerosocio,'noafiliacion' => $v->noafiliacion,
                          'nombre' => $v->nombre,'paterno' => $v->paterno,'materno' => $v->materno,'correo' => $v->correo,
                          'contrasena' => $v->contrasena,'fechaRegistro' => $this->dateClear($v->fechaRegistro),
                          'fechaVigencia' => $this->dateClear($v->fechaVigencia), 'codigo' => $v->codigo,'sexo' => $v->sexo,
                          'ocupacion' => $v->ocupacion,'estado' => $v->estado, 'ciudad' => $v->ciudad,'telefono' => $v->telefono,'direccion' => $v->direccion,'colonia' => $v->colonia,'municipio' => $v->municipio,
                          'cp' => $v->cp,'fuenteSubscripcion' => $v->fuenteSubscripcion,'activo' => $v->activo,'idPais' => $v->idPais,
                          'extra' => $v->extra, 'punto_referencia' => $v->punto_referencia, 'comentarios' => $v->comentarios
                        ];

                        Americanista::where('id',$v->id)->delete();
                    }
                    
                    DB::table('eliminados')->insert($adds);
        
                    // Registro en log.
                    \Log::info('|::| Mantenimiento: Americanistas Eliminados.', ['Usuario' => Auth::user()]);
        
                    // Registro en Bitácora.
                    \LogActivity::addToLog('Mantenimiento: Americanistas Eliminados');
        
                    return redirect()->back()->with('message', 'Americanistas Eliminados con Éxito!!.');
                }else
                    {
                        return redirect()->back()->with('message', 'Bloque de Americanistas por Eliminar no permitida, aún faltan días.');
                    }


           
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }     

    }

    
}
