<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Estado;
use App\Pais;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class EstadoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| State Index.', ['Usuario' => Auth::user()]);

         //Get all puertos and pass it to the view
         $estados = Estado::count();
         $estadosEnabled = Estado::where('activo','=',1)->count();
         $estadosDisabled = Estado::where('activo','=',0)->count();
         return view('estado.index')->with('estados', $estados)->with('estadosEnabled', $estadosEnabled)->with('estadosDisabled', $estadosDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario State.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        
        $pais = Pais::orderBy('nombre', 'ASC')->get()->pluck('nombre', 'id')->toArray();

        //Get all municipios and show view
        return view('estado.create', ['pais' => $pais]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate fields
        $this->validate($request, [
            'nombre' => 'required|max:50|unique:adm_estados',
            'pais' => 'required|max:3',
        ],[
            'nombre.required' => 'The Name field is required!',
            'pais.required' => 'The Country field is required!',
        ]);

        $estado = new Estado();
        $estado->nombre = Str::upper($request['nombre']);
        $estado->pais = $request['pais'];
        $estado->clave = Str::upper($request['clave']);
        $estado->save();

        // Registro en log.
        \Log::info('|::| Recurso State.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'State' => $estado]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$estado->nombre);

        return redirect()->route('states.index')
            ->with('flash_message',
             'State: '. $estado->nombre.' added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'You cannot access directly.']);
        }

        // Registro en log.
        \Log::info('|::| State.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $estado = Estado::findOrFail(decrypt($id));
        return view('estado.show', compact('estado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| State.edit - Show the form to edit the specified resource.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $estado = Estado::findOrFail(decrypt($id));
        $pais = Pais::orderBy('nombre', 'ASC')->get()->pluck('nombre', 'id')->toArray();
        return view('estado.edit', compact('estado','pais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        //Validate name, email and password fields
        $this->validate($request, [
            'nombre' => 'required|max:50|unique:adm_estados,nombre,'.$id,
            'pais' => 'required|max:3',
        ],[
            'nombre.required' => 'The Name field is required!',
            'pais.required' => 'The Country field is required!',
        ]);


        $estado = Estado::findOrFail($id);
        $input = array(
            'nombre' => Str::upper($request['nombre']),
            'pais' => $request['pais'],
            'clave' => Str::upper($request['clave'])
        );
        
        $request->all();
        $estado->fill($input)->save();

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso actualizado: '.$estado->nombre);
        
        return redirect()->route('states.index')
            ->with('flash_message',
             'Updated '. $estado->nombre.' State!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $estado = Estado::findOrFail(decrypt($request->idstate));
        $estado->activo = 0;
        $estado->save();

        // Registro en log.
        \Log::info('|::| State.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Contenedor' => $estado]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$estado->nombre);

        return redirect()->route('states.index')
            ->with('flash_message',
             'State successfully removed!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $showDeleted  = Input::get('showDeleted');

        $estados = Estado::where('activo','=',$showDeleted )->orderby('id','desc')->get();
        return Datatables::of($estados)
            ->addColumn('action', function ($estados) {

                $result = '';

                if ($estados->activo == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('states.show', encrypt($estados->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('states.edit', encrypt($estados->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-puerto='".encrypt($estados->id)."' data-title='".$estados->nombre."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addIndexColumn()
            ->addColumn('paisDesc', function ($query) {
                return $query->Pais['nombre'];
            })
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    

}
