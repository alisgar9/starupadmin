<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Americanista;
use PdfReport;
use ExcelReport;
use Config;
use DB;
use Response;
use Carbon\Carbon;

class PagosCaducadosController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }
            // Registro de log.
            \Log::info('|::| Pagos Caducados.', ['Usuario' => Auth::user()]);
            
            $pagosCaducados = DB::select('select * from view_caducados ');
            $pagosCaducados = count($pagosCaducados);
             return view('caducados.index', compact('pagosCaducados')); 
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }           
    }
    
    public function generateReportall()
    {
      try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }

                
               $americanistas = DB::select('select * from view_caducados ');
               $file = fopen('pagos_caducados.csv', 'w');
               $titles = [
                   'uuid','nombre', 'a_materno','a_paterno','email','telefono','celular','fecha_nacimiento','sexo',
                   'referencia','tienda','created_at','status'
               ];
               fputcsv($file, $titles);
                foreach ($americanistas as $row) {

                        
                        $array = array(
                            $row->uuid,$row->nombre,$row->a_materno,$row->a_paterno,$row->email,
                            $row->telefono,$row->celular,$row->fecha_nacimiento,$row->sexo,$row->referencia,$row->tienda,$row->created_at,
                            $row->status);
                        
                        $array = array_map("utf8_decode", $array);
                        fputcsv($file,$array);
                    }
                    fclose($file);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );

                    return Response::download('pagos_caducados.csv', 'pagos_caducados.csv', $headers);
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }                      

    }

}
