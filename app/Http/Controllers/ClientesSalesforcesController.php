<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Americanista;
use PdfReport;
use ExcelReport;
use Config;
use DB;
use Response;
use Carbon\Carbon;

class ClientesSalesforcesController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }
            // Registro de log.
            \Log::info('|::| Reportes Salesforce.', ['Usuario' => Auth::user()]);
            $americanistas = Americanista::count(); 
            $americanistas = number_format($americanistas);
             return view('salesforce.index', compact('americanistas')); 
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }           
    }
    /**
     * Limpiar Espacios en Blanco.
     *
     */
    public function stringClear($string){
        
        $string = preg_replace("[\s+]",'', $string);

        return $string;
    }
     /**
     * Limpiar Fecha de vigencia y fecha de registro.
     *
     */
    public function dateClear($date){
        
        if($date == "0000-00-00" || $date == "0000-00-00 00:00:00" || $date == "NULL"){
            $date = "";
        }
        else{
            $date = $date;
        }

        return $date;
    }
    /**
     * Limpiar Fecha de Nacimiento y validar la misma.
     *
     */
    public function dateClearBirthday($date){
        
        if($date == "0000-00-00" || $date == "0000-00-00 00:00:00" || $date == "NULL"){
            $date = "";
        }
        else{
            $fechaComoEntero = strtotime($date);
            $anio = date("Y", $fechaComoEntero);
            $mes = date("m", $fechaComoEntero);
            $d = date("d", $fechaComoEntero);
            $Year = Carbon::now();
            $Year = $Year->format('Y');

            if($anio>1901 && $anio<$Year)
            {
                $date = $date;
            }else{
                $date = "1900-".$mes."-".$d;
                $date = Carbon::parse($date)->format('d/m/Y');
                
            }
            
        }

        return $date;
    }
   

    public function generateReportall()
    {
      try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }

                set_time_limit(0);
                ini_set('max_execution_time', 100000);
                ini_set('memory_limit','512m');
               $americanistas = DB::select('select * from americanista ');
               $file = fopen('salesforce.csv', 'w');
               $titles = [
                   'id', 'numeroAmericanista', 'numerosocio', 'noafiliacion','nombre', 'paterno',
                   'materno','correo','contrasena','fechaRegistro','fechaVigencia','codigo',
                   'sexo','ocupacion','estado','ciudad','telefono','fechanacorg',
                   'fechanac','direccion','colonia','municipio','cp','fuenteSubscripcion',
                   'activo','idPais','extra','punto_referencia','comentarios'
               ];
               fputcsv($file, $titles);
                foreach ($americanistas as $row) {

                        
                        $array = array(
                            $row->id,$this->stringClear($row->numeroAmericanista),$row->numerosocio,$row->noafiliacion,$row->nombre,
                            $row->paterno,$row->materno,$row->correo,$row->contrasena,$this->dateClear($row->fechaRegistro),
                            $this->dateClear($row->fechaVigencia),$row->codigo,$row->sexo,$row->ocupacion,$row->estado,
                            $row->ciudad,$row->telefono,$row->fechanacorg, $this->dateClearBirthday($row->fechanac), $row->direccion,
                            $row->colonia,$row->municipio,$row->cp,$row->fuenteSubscripcion,$row->activo,$row->idPais, $row->extra,
                            $row->punto_referencia,$row->comentarios);
                        
                        $array = array_map("utf8_decode", $array);
                        fputcsv($file,$array);
                    }
                    fclose($file);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );

                    return Response::download('salesforce.csv', 'salesforce.csv', $headers);
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }                      

    }

}
