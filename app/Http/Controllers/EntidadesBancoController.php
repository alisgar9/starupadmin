<?php

namespace App\Http\Controllers;

use App\Entidades;
use App\Entidadesbanco;
use App\Banco;
use Auth;
use Charts;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Form;
use Collective\Html\FormFacade;


//Importing laravel-permission models

//Enables us to output flash messaging
use Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;

class EntidadesBancoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Entidades Index.', ['Usuario' => Auth::user()]);

        //Get all users and pass it to the view
        $users = Entidades::all();

        //Get count users this month
        $usersThisMonth = Entidades::where(DB::raw('MONTH(created_at)'), '=', date('n'))->get();

        //Get last 6 users
        $lastUsers = $users->sortByDesc('created_at')->take(6);

        //Get Count users deleted
        $delUsers = $users->where('status', 0);

        $countUsers = DB::table('adm_entidad')
            ->select(DB::raw('MONTH(created_at) as month'), DB::raw('count(*) as records'))
            ->whereRaw('year(`created_at`) = ?', array(date('Y')))
            ->groupBy('month')
            ->get()->toArray();

        $serieChart = array();

        for ($i = 1; $i <= 12; $i++) {
            $searchR = array_search($i, array_column($countUsers, 'month'));

            if (is_int($searchR)) {
                array_push($serieChart, $countUsers[$searchR]->records);
            } else {
                array_push($serieChart, 0);
            }
        }

        $chartPline = Charts::multi('areaspline', 'highcharts')
            ->title('Analítica ' . date("Y"))
            ->colors(['#ff0000', '#ffffff'])
            ->elementLabel("Registros")
            ->labels(['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'])
            ->dataset('Usuarios', $serieChart);

        return view('entidades.index')->with('users', $users)->with('chartPline', $chartPline)->with('lastUsers', $lastUsers)->with('usersThisMonth', $usersThisMonth)->with('delUsers', $delUsers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email fields
        $this->validate($request, [
            'cuentabancaria' => 'required|max:120',
            'cinterbancaria' => 'required|max:18',
            'id_banco' => 'required|max:2',
        ], [
            'cuentabancaria.required' => 'El campo Cuenta Bancaria es requerido!',
            'cinterbancaria.required' => 'El campo Clabe Interbancaria es requerido!',
            'id_banco.required' => 'El campo Banco es requerido!',
            
        ]);

        $id_banco_inter = isset($request['id_banco_inter']) ? $request['id_banco_inter'] : '';
        $swift = isset($request['swift']) ? $request['swift '] : '';

        $Ebanco = new Entidadesbanco();
        $Ebanco->id_entidad = decrypt($request->id_entidad);
        $Ebanco->cuentabancaria = $request['cuentabancaria'];
        $Ebanco->cinterbancaria = $request['cinterbancaria'];
        $Ebanco->id_banco = $request['id_banco'];
        $Ebanco->id_banco_inter = $id_banco_inter;
        $Ebanco->swift = $swift;
        $Ebanco->save();
    

        // Registro en log.
        \Log::info('|::| Recurso Entidad detalle Banco.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Entidad detalle banco' => $Ebanco]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$Ebanco->cuentabancaria);

        //Redirect to the users.index view and display message
        return redirect()->route('entidades.edit', $request->id_entidad)
            ->with('flash_message',
                'Entidad detalle Banco agregada con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        //Find  Entidad detalle Banco with a given id and delete
        $entidadBanco = Entidadesbanco::findOrFail(decrypt($request->id_entidad_banco));
        $entidadBanco->activo = "0";
        $entidadBanco->save();

        // Registro en log.
        \Log::info('|::| Entidad detalle Banco .destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'User' => $entidadBanco]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$entidadBanco->cuentabancaria);

        return redirect()->route('entidades.edit', $request->id_entidad)
            ->with('flash_message',
                'Entidad Detalle Banco eliminada con éxito.');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        $users = Entidades::all();

        return Datatables::of($users)
            ->addColumn('action', function ($users) {

                $result = '';

                if ($users->status == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('entidades.show', encrypt($users->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('entidades.edit', encrypt($users->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";

                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($users->id)."' data-title='".$users->id."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    public function getDataBanco()
    {
        $showDeleted  = Input::get('showDeleted');
        
        $detallesbancos = Entidadesbanco::where('id_entidad','=',$showDeleted )->get();


        return Datatables::of($detallesbancos)
            ->addColumn('action', function ($detallesbancos) {

                $result = '';

                if ($detallesbancos->activo == 1){

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('entidades.edit', encrypt($detallesbancos->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";

                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($detallesbancos->id)."' data-title='".$detallesbancos->id."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addColumn('banco', function ($query) {
                return $query->banco['descripcion'];
            })
            ->addColumn('bancointer', function ($query) {
                return $query->bancointer['descripcion'];
            })
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    /** Vista de Bancos**/
    public function getDataBancoView()
    {
        $showDeleted  = Input::get('showDeleted');
        
        $detallesbancos = Entidadesbanco::where('id_entidad','=',$showDeleted )->get();


        return Datatables::of($detallesbancos)
            ->addColumn('banco', function ($query) {
                return $query->banco['descripcion'];
            })
            ->addColumn('bancointer', function ($query) {
                return $query->bancointer['descripcion'];
            })
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    
}
