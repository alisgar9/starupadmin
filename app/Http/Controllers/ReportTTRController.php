<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Puerto;
use App\Estado;
use App\Aeropuerto;
use App\Pais;
use App\Entidades;
use App\Operacion;
use App\Liberacion;
use App\Incoterm;
use App\Contenedor;
use App\OperacionDetails;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;
use Session;
Use Redirect;
use DB;
use PdfReport;
use ExcelReport;
use Config;
use Response;





class ReportTTRController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Demurrage(Request $request)
    {
        
        //catalogo Consignee
        $entidad = Entidades::where([['status', 1]])->orderBy('nombre')->pluck('nombre', 'id');

        
        return view('reportes.reportTTR')->with('entidad', $entidad);
        
    }

    public function DiasTranscurridos($inicio,$final){

        $inicio = Carbon::parse($inicio);
        $final = Carbon::parse($final);

        return $diasDiferencia = $final->diffInDays($inicio);
    }


    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData(Request $request)
    {
       $IdCurrentUser = Auth::id();
       
      //$detallesoperacion = OperacionDetails::where([['activo','=',1]] )->get();

       $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
       ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo', 'P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA','P.POL','P.POD','P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal','C.descripcion AS tipo_operacion_desc')
       ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
       ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
       ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
       ->where([[function($query) use ($IdCurrentUser) {
           if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
               $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
           }else{
               $query->where([['U.type','=',1]]);
           }
       }],['U.activo','=',1],['P.consignee','=',$request->custom],[
        function($query){
            $query->where('A.activo', '=', 1)
              ->orWhere('A.activo', '=', 2);
        }
       ],[
        function($query){
            $query->where('P.tipo_operacion', '=', 1)
              ->orWhere('P.tipo_operacion', '=', 2);
        }
       ]])
       ->whereBetween('P.ETA', array($request->etas, $request->etast))
       ->orderby('P.id', 'asc')
       ->get();

       return Datatables::of($detallesoperacion)
           
          
            ->addIndexColumn()
            ->editColumn('shipper', function ($query) {
                if($query->shipper) {                    
                    $shipper = Entidades::where("id","=",$query->shipper)->first();
                    return $shipper->nombre;
                }else{
                    return 'No Apply';
                }
            })
            ->addColumn('ttr', function ($detallesoperacion) {
                return $this->DiasTranscurridos($detallesoperacion->ETA,$detallesoperacion->ETD);
            })
           ->editColumn('SUMEX', '{{ "SUMEX0".$SUMEX }}')
           ->editColumn('ETA', '{{ Carbon\Carbon::parse($ETA)->format("Y/m/d") }}')
           ->editColumn('ATD', '{{ Carbon\Carbon::parse($ATD)->format("Y/m/d") }}')
           ->editColumn('ETD', '{{ Carbon\Carbon::parse($ETD)->format("Y/m/d") }}')
           ->editColumn('ATA', '{{ Carbon\Carbon::parse($ETD)->format("Y/m/d") }}')
           ->make(true);
    }

    

    /**
     * Limpiar Espacios en Blanco.
     *
     */
    public function stringClear($string){
        
        $string = preg_replace("[\s+]",'', $string);

        return $string;
    }


    public function Descentidad($id){
        
        $entidad = Entidades::where("id","=",$id)->first();

        return $entidad;
    }

    public function DescPuerto($id, $type){

        if($type == 1){
            $result = Puerto::where('id', '=', $id)->first();
            $name = $result->puerto;
            
        }
        elseif($type == 2){
            $result = Aeropuerto::where('id', '=', $id)->first();
            $name = $result->nombre;
        }
        elseif($type == 3){
            $result = Estado::where('id', '=', $id)->first();
            $name = $result->nombre;
        }else{
            $name = 'not apply ';
        }
      
        
 
        return $name;
    }

    public function exportGenerate($custom, $dateStart, $dateFinish)
    {        
        $IdCurrentUser = Auth::id();     
        $data =  DB::table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA', 'P.POL','P.POD', 'P.tipo_operacion', 'P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal','C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([[function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
                }else{
                    $query->where([['U.type','=',1]]);
                }
            }],['U.activo','=',1],['P.consignee','=',$custom],[
                function($query){
                    $query->where('A.activo', '=', 1)
                      ->orWhere('A.activo', '=', 2);
                }
            ],[
                function($query){
                    $query->where('P.tipo_operacion', '=', 1)
                      ->orWhere('P.tipo_operacion', '=', 2);
                }
               ]])
            ->whereBetween('P.ETA', array($dateStart, $dateFinish))
            ->orderby('P.id', 'asc')->get()->toArray();
            //dd($data);
                
            $customer = Entidades::where("id","=",$custom)->first();

        
        //$data = Entidades::get()->toArray();
        
        return \Excel::create('ReportTTR', function($excel) use ($data, $customer) {
            $excel->sheet('mySheet', function($sheet) use ($data, $customer)
            {

                $sheet->cell('A1', function($cell) {$cell->setValue('FOLIO');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('# Container');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('Shipper');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('Transit Time');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('ETD');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('ATD');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('ETA');   });
                $sheet->cell('H1', function($cell) {$cell->setValue('ATA');   });
                $sheet->cell('I1', function($cell) {$cell->setValue('# PO');   });
                $sheet->cell('J1', function($cell) {$cell->setValue('# PI');   });
                $sheet->setColumnFormat(array('A' => '@', 'B' => '@','C' => '@', 'D' => '@','E' => NumberFormat::FORMAT_DATE_DDMMYYYY,'F' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                    'G' => NumberFormat::FORMAT_DATE_DDMMYYYY,'H' => NumberFormat::FORMAT_DATE_DDMMYYYY, 'I' => '@', 'j' => '@'));
                


                if (!empty($data)) {

                    
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $folio = 'SUMEX0'.$value->SUMEX;

                        
                        $sheet->cell('A'.$i, $folio);
                        $sheet->cell('B'.$i, $value->container_number);
                        $sheet->cell('C'.$i, $this->Descentidad($value->shipper)->nombre);
                        $sheet->cell('D'.$i, $this->DiasTranscurridos($value->ETA,$value->ETD));
                        $sheet->cell('E'.$i, Date::stringToExcel($value->ETD));
                        $sheet->cell('F'.$i, Date::stringToExcel($value->ATD)); 
                        $sheet->cell('G'.$i, Date::stringToExcel($value->ETA));
                        $sheet->cell('H'.$i, Date::stringToExcel($value->ATA));
                        $sheet->cell('I'.$i, isset($value->PO) ? json_decode($value->PO) : NULL);
                        $sheet->cell('J'.$i, isset($value->PI) ? json_decode($value->PI) : NULL);
                    }
                }
            });
        })->export('xlsx');
    }

    public function importOperation(){

        \Excel::create('Users', function($excel) {
 
            $users = Entidades::all();
         
            $excel->sheet('Users', function($sheet) use($users) {
         
            $sheet->fromArray($users);
         
        });
         
        })->export('xlsx');

    }

    public function transformDate($value, $format = 'Y/m/d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }


    public function exportUserData()
    {
        $data = Entidades::get()->toArray();
        
        return \Excel::create('laravelcode', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('ID');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('NOMBRE');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('RAZON SOCIAL');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('CLASIFICACION');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('NOMBRE CONTACTO');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('DIRECCION');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('TELEFONO');   });
                $sheet->cell('H1', function($cell) {$cell->setValue('EMAIL');   });
                $sheet->cell('I1', function($cell) {$cell->setValue('TAXID');   });
                $sheet->cell('J1', function($cell) {$cell->setValue('STATUS');   });
                $sheet->cell('K1', function($cell) {$cell->setValue('EMAIL CC');   });
                $sheet->cell('L1', function($cell) {$cell->setValue('FECHA');   });
                $sheet->cell('M1', function($cell) {$cell->setValue('FECHA AT');   });                
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'D' => '@',
                    'F' => '@',
                    'L' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                    'M' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                ));
                $sheet->setStyle('A1',array(
                    'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  12,
                        'bold'      =>  true
                    ),
                ));


                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $sheet->cell('A'.$i, $value['id']); 
                        $sheet->cell('B'.$i, $value['nombre']); 
                        $sheet->cell('C'.$i, $value['razonsocial']);
                        $sheet->cell('D'.$i, $value['id_clasificacion']); 
                        $sheet->cell('E'.$i, $value['nombrecontacto']); 
                        $sheet->cell('F'.$i, $value['direccion']); 
                        $sheet->cell('G'.$i, $value['telefono']); 
                        $sheet->cell('H'.$i, $value['email']);
                        $sheet->cell('I'.$i, $value['taxid']); 
                        $sheet->cell('J'.$i, $value['status']);
                        $sheet->cell('K'.$i, $value['email_cc']);
                        $sheet->cell('L'.$i, Date::stringToExcel($value['created_at']));
                        $sheet->cell('M'.$i, Date::stringToExcel($value['updated_at']));
                    }
                }
            });
        })->export('xlsx');
    }
    

    

    


}
