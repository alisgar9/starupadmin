<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect,Response,DB,Config;
use Mail;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Auth;

class EmailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function vista()
    {
        return view('mail.msgoperation');
    }

    public function build($data,$contenedor,$email,$namecontact, $operation, $cc)
    {
      $email = 'no-reply@starup.com.mx';
      $emailOpe = Auth::user()->email;
      $nameOpe = Auth::user()->name;

      if($cc != NULL)
      {
        $con_c = json_decode($cc);
        $con_c = explode(',', $con_c);
      }else{
        $con_c = '';
      }
      

      Mail::send('mail.msgoperation', ["data"=>$data,"contenedor"=>$contenedor], function($message) use ($email, $namecontact, $operation, $emailOpe, $nameOpe, $con_c) {
            
          $message->to($email, $namecontact)
                    ->subject('We are working on your operation #'.$operation)
                    ->replyTo($emailOpe, $nameOpe)
                    ->bcc('jade@starup.com.mx', 'JADE ATZIN CARBAJAL GUTIERREZ');

            if($con_c != NULL){
              $message->cc($con_c);
            }

        });
        
         if (Mail::failures()) {
           //return response()->Fail('Sorry! Please try again latter');
           return 0;
         }else{
           return 1;
         }

    }

    public function notification($data,$email,$namecontact, $operation, $asunto, $cc)
    {
          $email = 'no-reply@starup.com.mx';
          $emailOpe = Auth::user()->email;
          $nameOpe = Auth::user()->name;
          if($cc != NULL)
          {
            $con_c = json_decode($cc);
            $con_c = explode(',', $con_c);
          }else{
            $con_c = '';
          }

        Mail::send('mail.msgnotification', ["data"=>$data, 'asunto'=>$asunto], function($message) use ($email, $namecontact, $operation, $asunto, $emailOpe, $nameOpe, $con_c) {
          
            $message->from($emailOpe, $asunto)
                    ->to($email, $namecontact)
                    ->subject($asunto.' '.$operation)
                    ->replyTo($emailOpe, $nameOpe)
                    ->bcc('jade@starup.com.mx', 'JADE ATZIN CARBAJAL GUTIERREZ');

                    if($con_c != NULL){
                      $message->cc($con_c);
                    }

        });
 
         if (Mail::failures()) {
           //return response()->Fail('Sorry! Please try again latter');
           return 0;
         }else{
           return 1;
         }

    }


    public function notificationGeneral($data,$email,$namecontact, $operation, $asunto, $cc)
    {
          $email = 'no-reply@starup.com.mx';
          $emailOpe = Auth::user()->email;
          $nameOpe = Auth::user()->name;
          if($cc != NULL)
          {
            $con_c = json_decode($cc);
            $con_c = explode(',', $con_c);
          }else{
            $con_c = '';
          }

        Mail::send('mail.msgGeneral', ["data"=>$data, 'asunto'=>$asunto], function($message) use ($email, $namecontact, $operation, $asunto, $emailOpe, $nameOpe, $con_c) {
          
            $message->from($emailOpe, $asunto)
                    ->to($email, $namecontact)
                    ->subject($asunto.' '.$operation)
                    ->replyTo($emailOpe, $nameOpe)
                    ->bcc('jade@starup.com.mx', 'JADE ATZIN CARBAJAL GUTIERREZ');
                    if($con_c != NULL){
                      $message->cc($con_c);
                    }

        });
 
         if (Mail::failures()) {
           //return response()->Fail('Sorry! Please try again latter');
           return 0;
         }else{
           return 1;
         }

    }

    
}
