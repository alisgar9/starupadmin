<?php

namespace App\Http\Controllers;

use App\Clasificacion;
use App\Entidades;
use App\Entidadesbanco;
use App\Banco;
use Auth;
use Charts;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Form;
use Collective\Html\FormFacade;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;


//Importing laravel-permission models

//Enables us to output flash messaging
use Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario customer.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Get all municipios and show view
        return view('customer.create');
    }


    public function RequestAPI($tipo, $url){
        $client = new Client(['base_uri' => 'https://starup.com.mx/clients/']);  
        $response = $client->request($tipo, $url);
        $body = $response->getBody();
        $content =$body->getContents();
        $array = json_decode($content, true);

        return $array;
    }

    public function postGuzzleRequest($url, $body)
    {
        $client = new Client(['base_uri' => 'https://starup.com.mx/clients/']); 
        $response = $client->request('POST', $url, [
            'form_params' => $body
        ]);
        
        
        $body = $response->getBody();
        

        return $body;
    
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // Registro de log.
        \Log::info('|::| Modulo Acceso a Portal Clientes Index.', ['Usuario' => Auth::user()]);

        //Get all users and pass it to the view
        $users = $this->RequestAPI('GET', 'api/usuarios/card');

        return view('customer.index')->with('users', $users);
    }


    /** Generamos Contraseña **/
    public function GenerarContrasena(){
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$password = "";
		$longitud = 8;
		for($i=0;$i<$longitud;$i++) {
			$password .= substr($str,rand(0,62),1);
		}

		return $password;
	}
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
         
        $this->validate($request, [
            'id_user' => 'required',
        ],[
            'id_user.required' => 'Customer Requerid!',
            
        ]);

        // Registro de log.
         \Log::info('|::| Eliminar Cliente de Portal Clientes Index.', ['Usuario' => Auth::user()]);

         //Get all users and pass it to the view

         $id = decrypt($request->id_user);
         $users = $this->RequestAPI('GET', 'api/user/delete/'.$id);
        
        

        return redirect()->route('customer.index')
            ->with('flash_message',
                'User Deleted Successfully.');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        $users = $this->RequestAPI('GET', 'api/usuarios/1');
        
        //dd($users);
        return Datatables::of($users)
            ->addColumn('action', function ($users) {

                $result = '';
               
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        if($users['status'] == 1){
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($users['id'])."' data-title='".$users['name']."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";}
                

                return $result;
            })
            ->editColumn('consignee', function ($users) {
               $data =  $this->SearchEntidad($users['consignee']);
               return $data['razonsocial'];
            })
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y") }}')
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    public function SearchEntidad($id){
      
        $dataConsignee = Entidades::where("id",$id)->first();
        return $dataConsignee;

    }

    public function add(Request $request){

        //Validate name, email fields
        $this->validate($request, [
            'name' => 'required',
            'custom' => 'required',
            'email.*' => 'required|email',
        ], [
            'name.required' => 'A problem occurred try again!',
            'custom.required' => 'Select a Custom',
            'email.required' => 'Email required!',
            'email.email' => 'Include an "@" sign in the email address!',
            
        ]);

        
        $client = new Client();
        $response = $client->request('POST', 'https://starup.com.mx/clients/api/add', [
            'form_params' => [
                'email' => $request->email,
                'name' => $request->name,
                'custom' => $request->custom,
            ]
        ]);



        $data = $response->getBody()->getContents();

        return redirect()->route('customer.index')
        ->with('flash_message', $data);
    }
   
}
