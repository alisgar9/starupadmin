<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Ubicacion;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use App\Role;
use Session;

class UbicacionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        // Registro de log.
        \Log::info('|::| Ubicaciones Index.', ['Usuario' => Auth::user()]);

        //Get all ubicaciones and pass it to the view
        $ubicaciones = Ubicacion::count();
        return view('ubicaciones.index')->with('ubicaciones', $ubicaciones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario ubicaciones.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]); 

        //Get all municipios and show view
        return view('ubicaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'ubicacion' => 'required|max:200',
        ],[
            'ubicacion.required' => 'El campo Ubicación es requerido!',
        ]);

        $ubicacion = new Ubicacion();
        $ubicacion->ubicacion = $request['ubicacion'];
        $ubicacion->save();
    
        // Registro en log.
        \Log::info('|::| Recurso Ubicacion.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Ubicacion' => $ubicacion]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$ubicacion->ubicacion);

        return redirect()->route('ubicaciones.index')
            ->with('flash_message',
             'Ubicación '. $ubicacion->ubicacion.' agregada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| UbicacionController.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $ubicacion = Ubicacion::findOrFail(decrypt($id));
        return view('ubicaciones.edit', compact('ubicacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'ubicacion' => 'required|max:200',
        ],[
            'ubicacion.required' => 'El campo Ubicación es requerido!',
        ]);

        $ubicacion = Ubicacion::findOrFail(decrypt($id));
        $input = $request->all();
        $ubicacion->fill($input)->save();

        \Log::info('Ubicación Editada.', ['Usuario' => Auth::user(),'Ubicación' => $ubicacion]);

        return redirect()->route('ubicaciones.index')
            ->with('flash_message',
                'Ubicación '. $ubicacion->ubicacion.' actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ubicacion = Ubicacion::findOrFail(decrypt($request->id_empresa));
        $ubicacion->estado = 0;
        $ubicacion->save();

        return redirect()->route('ubicaciones.index')
            ->with('flash_message',
             'Ubicación eliminada con éxito!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $ubicaciones = Ubicacion::all(); 
        return Datatables::of($ubicaciones)
            ->addColumn('action', function ($ubicacion) {

                $result = '';

                if ($ubicacion->estado == 1){

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('ubicaciones.edit', encrypt($ubicacion->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($ubicacion->id)."' data-title='".$ubicacion->ubicacion."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }
}
