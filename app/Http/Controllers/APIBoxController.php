<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Puerto;
use App\Estado;
use App\Aeropuerto;
use App\Pais;
use App\Operacion;
use App\Contenedor;
use App\OperacionDetails;
use App\Liberacion;
use App\Incoterm;
use Illuminate\Http\Request;
Use Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Response;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class APIBoxController extends Controller
{
    public function datos(){
        echo "alicia";
    }
    public function data($consignee){
        echo "alicia";
    }
    public function ActiveContainers($consignee){
        dd($consignee);
      
        if (Operacion::where('consignee', $consignee)->exists()) { 
        
            
            $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->where([['A.activo','=',1],[function ($query) {
                $query->where('P.pre_operation','=',0)
                    ->orWhereNull('P.pre_operation');
            }],['P.consignee','=',$consignee]])->count();

        return response()->json($detallesoperacion, 200);

        }else {
            return response()->json([
              "message" => "Conteiners not found"
            ], 404);
          }
    }
    /** 
     * contenedores de consignatario Retornados
    */
    public function ReturnsContainers($consignee){
      
        if (Operacion::where('consignee', $consignee)->exists()) { 
        $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
        ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
        ->leftJoin('adm_entidad as E', 'E.id', '=', 'P.consignee')
        ->leftJoin('adm_entidad as SL', 'SL.id', '=', 'P.shipping_line')
        ->leftJoin('adm_puertos as PU1', 'PU1.id', '=', 'P.POL')
        ->leftJoin('adm_puertos as PU2', 'PU2.id', '=', 'P.POD')
        ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
        ->where([['A.activo','=',2],[function ($query) {
            $query->where('P.pre_operation','=',0)
                ->orWhereNull('P.pre_operation');
        }],['P.consignee','=',$consignee]])->count();

        return response()->json($detallesoperacion, 200);

        }else {
            return response()->json([
              "message" => "Conteiners not found"
            ], 404);
          }
    }

    
}
