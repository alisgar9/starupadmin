<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Recibo;
use App\Tarifa;
use App\Municipio;
use App\Area;
use App\Costo;
use App\Ubicacion;
use App\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Yajra\Datatables\Datatables;


class ReciboController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Recibos Index.', ['Usuario' => Auth::user()]);

        //Get all municipios and pass it to the view
        $recibos = Recibo::with('Areas','Municipios')->get();
        //dd($recibos[1]->cfdiComprobante);
        $recibosCargados = $recibos->where('tipo', 1);
        $recibosCapturados= $recibos->where('tipo', 2);

        return view('recibos.index')->with([
            'recibos' => $recibos,
            'recibosCargados' => $recibosCargados,
            'recibosCapturados' => $recibosCapturados,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario recibos.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        $municipios = Municipio::pluck('municipio', 'id');
        $areas = Area::pluck('nombre', 'id');
        $costos = Costo::pluck('centro', 'id');
        $ubicaciones = Ubicacion::pluck('ubicacion', 'id');
        $empresas = Empresa::pluck('empresa', 'id');

        //Get all municipios and show view
        return view('recibos.create', compact('municipios','areas','costos','ubicaciones','empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        //Validate name, email and password fields
        $this->validate($request, [
            'reciboxml'  => 'required',
            'municipios' => 'required',
            'areas' => 'required',
            'costos' => 'required',
            'ubicaciones' => 'required',
            'empresas' => 'required'
        ],[
            'reciboxml.required'  => 'El campo Archivo XML es obligatorio.',
            'municipios.required'  => 'El campo Municipio es obligatorio.',
            'areas.required'  => 'El campo Área es obligatorio.', 
            'costos.required'  => 'El campo Centro de Costos es obligatorio.', 
            'ubicaciones.required'  => 'El campo Ubicaciones es obligatorio.',
            'empresas.required'  => 'El campo Empresas es obligatorio.'    
        ]);

        $fileNameUploadXML = "XML-".$request->municipios."-".$request->areas."-".hash('md5', $request->file('reciboxml')->getClientOriginalName()).".".$request->file('reciboxml')->getClientOriginalExtension();
        $request->file('reciboxml')->move('facturas/xml', $fileNameUploadXML);

        if($request->file('recibopdf') !== null)
        {
            $fileNameUploadPDF = "PDF-".$request->municipios."-".$request->areas."-".hash('md5', $request->file('recibopdf')->getClientOriginalName()).".".$request->file('recibopdf')->getClientOriginalExtension();
            $request->file('recibopdf')->move('facturas/pdf', $fileNameUploadPDF);
        }

        $recibo = new Recibo();
        $recibo->id_area = $request->areas;
        $recibo->id_municipio = $request->municipios;
        $recibo->id_costo = $request->costos;
        $recibo->id_ubicacion = $request->ubicaciones;
        $recibo->id_empresa = $request->empresas;
        $recibo->tipo = '1';

        try {
            $xml = simplexml_load_file('facturas/xml/'.$fileNameUploadXML); 
        } catch (\Exception $ex){
            error_log($ex); 
            return redirect()->route('recibos.index')
            ->with('flash_message_error',
             'Hubo un Problema con el Archivo: '.  $request->file('reciboxml')->getClientOriginalName().'! Tiene que ser un archivo con extensión XML.');
        }
        
        $ns = $xml->getNamespaces(true);
        $xml->registerXPathNamespace('c', $ns['cfdi']);
        $xml->registerXPathNamespace('t', $ns['tfd']);

        //EMPIEZO A LEER LA INFORMACION DEL CFDI E IMPRIMIRLA 
        foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante){ 
            $recibo->total = $cfdiComprobante[0]->attributes()->{'Total'};
        } 
        
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor') as $Emisor){ 
            $recibo->cfdiEmisor = json_encode($Emisor);
        } 

        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor') as $Receptor){ 
            $recibo->cfdiReceptor = json_encode($Receptor);
            $cfdiReceptor = json_decode($recibo->cfdiReceptor,true);
            $recibo->nombre = $Receptor[0]->attributes()->{'Nombre'}->__toString(); // Obtenemos el Nombre del receptor del XML
            $recibo->rfc = $Receptor[0]->attributes()->{'Rfc'}->__toString(); // Obtenemos el Rfc del receptor del XML
        }

        $Conceptos = array();
        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto){ 
            array_push($Conceptos, $Concepto);
        }
        $recibo->cfdiConcepto = json_encode($Conceptos);


         foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado') as $Traslado){ 
            $recibo->cfdiTraslado = json_encode($Traslado);
         } 
          
         //ESTA ULTIMA PARTE ES LA QUE GENERABA EL ERROR
         foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {
            $recibo->tfdtimbrefiscaldigital = json_encode($tfd);
         } 

        foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Addenda') as $Addenda){ 
            $recibo->cfdiAddenda = json_encode($Addenda);
            $cfdiAddenda = json_decode($recibo->cfdiAddenda, true)['clsRegArchFact'];

            //dd($cfdiAddenda );

            // Obtenemos datos de la dirección.
            $recibo->direccion = isset($cfdiAddenda['DIRECC']) ? $cfdiAddenda['DIRECC'] :  ''; // Obtenemos DIRECC del XML
            $recibo->calle1 = isset($cfdiAddenda['CALLE1']) ? $cfdiAddenda['CALLE1'] :  ''; // Obtenemos CALLE1 del XML
            $recibo->colonia = isset($cfdiAddenda['COLONIA']) ? $cfdiAddenda['COLONIA'] :  ''; // Obtenemos COLONIA del XML
            $recibo->nompob = isset($cfdiAddenda['NOMPOB']) ? $cfdiAddenda['NOMPOB'] :  ''; // Obtenemos NOMPOB del XML
            $recibo->nomest = isset($cfdiAddenda['NOMEST']) ? $cfdiAddenda['NOMEST'] :  ''; // Obtenemos NOMEST del XML
            $recibo->cp = isset($cfdiAddenda['CODIGO_POSTAL']) ? $cfdiAddenda['CODIGO_POSTAL'] :  ''; // Obtenemos NOMEST del XML

            // Obtenemos datos fijos de la factura
            $recibo->no_cuenta = isset($cfdiAddenda['Importes']['NumCta']) ? $cfdiAddenda['Importes']['NumCta'] :  ''; // Obtenemos NumCta del XML
            $recibo->rmu = isset($cfdiAddenda['RMU']) ? $cfdiAddenda['RMU'] :  ''; // Obtenemos RMU del XML
            $recibo->rpu = isset($cfdiAddenda['RPU']) ? $cfdiAddenda['RPU'] :  ''; // Obtenemos RPU del XML 
            $recibo->no_medidor = isset($cfdiAddenda['NUMMED1']) ? $cfdiAddenda['NUMMED1'] :  ''; // Obtenemos NUMMED1 del 
            $recibo->tarifa = isset($cfdiAddenda['TARIFA_REG']) ? $cfdiAddenda['TARIFA_REG'] :  ''; // Obtenemos TARIFA_REG del XML 
            $recibo->multiplo = isset($cfdiAddenda['MULTI1']) ? $cfdiAddenda['MULTI1'] :  ''; // Obtenemos MULTI1 del XML 
            $recibo->carga_conectada = isset($cfdiAddenda['CARGA_CONECTADA']) ? $cfdiAddenda['CARGA_CONECTADA'] :  ''; // Obtenemos CARGA_CONECTADA del XML 
            $recibo->demanda_contratada = isset($cfdiAddenda['CARGA_CONTRATADA']) ? $cfdiAddenda['CARGA_CONTRATADA'] :  ''; // Obtenemos CARGA_CONTRATADA del XML 

            // Obtener Datos variables de la factura (energia)
            $recibo->mes_facturado = isset($cfdiAddenda['FECDESDE']) ? date('Y-m-d',strtotime($this->getFecha($cfdiAddenda['FECDESDE'].'+ 15 day'))) :  null; // Mes facturado sumando 15 dias
            $recibo->fecdesde = isset($cfdiAddenda['FECDESDE']) ? date('Y-m-d',strtotime($this->getFecha($cfdiAddenda['FECDESDE']))) :  null; // Obtenemos FECDESDE del XML 
            $recibo->fechasta = isset($cfdiAddenda['FECHASTA']) ? date('Y-m-d',strtotime($this->getFecha($cfdiAddenda['FECHASTA']))) :  null; // Obtenemos FECHASTA del XML 

            $recibo->feclimite = isset($cfdiAddenda['FECLIMITE']) ? date('Y-m-d',strtotime($this->getFecha($cfdiAddenda['FECLIMITE']))) :  null; // Obtenemos FECLIMITE del XML
            $recibo->fecorte = isset($cfdiAddenda['FECORTE']) ? date('Y-m-d',strtotime($this->getFecha($cfdiAddenda['FECORTE']))) :  null; // Obtenemos FECORTE del XML  
            $recibo->monto_apagar_enletras = isset($cfdiAddenda['MontoAPagarEnLetras']) ? $cfdiAddenda['MontoAPagarEnLetras'] :  ''; // Obtenemos MONTOAPAGARENLETRAS del XML 
           
            $recibo->kwh_energia_base = isset($cfdiAddenda['CONSUMO3F']) ? $cfdiAddenda['CONSUMO3F'] :  '0';  // kwh energía base
            $recibo->kwh_energia_intermedia = isset($cfdiAddenda['CONSUMO2F']) ?  $cfdiAddenda['CONSUMO2F'] :  '0'; // kwh energía intermedia
            $recibo->kwh_energia_punta  = isset($cfdiAddenda['CONSUMO1F']) ?   $cfdiAddenda['CONSUMO1F'] :  '0'; // kwh energía intermedia
            $recibo->kwh_total = $cfdiAddenda['CONSUMO_R']; // kwh total

            $recibo->kw_demanda_base  = isset($cfdiAddenda['DEMANDA1P']) ? $cfdiAddenda['DEMANDA1P'] : '0'; // kw demanda base
            $recibo->kw_demanda_intermedia  = isset( $cfdiAddenda['DEMANDA2P']) ?  $cfdiAddenda['DEMANDA2P'] : '0'; // kw demanda intermedia
            $recibo->kw_demanda_punta  = isset($cfdiAddenda['DEMANDA3P']) ?   $cfdiAddenda['DEMANDA3P'] : '0'; // kw demanda punta
            $aDemanda = array($recibo->kw_demanda_base, $recibo->kw_demanda_intermedia, $recibo->kw_demanda_punta);
            $recibo->kw_demanda_max = max($aDemanda); // kw demanda max

            $recibo->kvarh_total  = $cfdiAddenda['KVARH']; // kvarh total
            $recibo->periodo_facturado = $cfdiAddenda['FECDESDE'].' - '.$cfdiAddenda['FECHASTA']; // Periodo Facturado 

            // Bloque de codigo para obtener Suministro
            $claveES1 = array_search('ES1', $cfdiAddenda);

            if ($claveES1 != false)
            {
                $indice =  preg_replace('/\D/', '', $claveES1);
                
                // datos de distribucion
                $output['suministro']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            }

            // Bloque de codigo para obtener Distribucion
            $claveED1 = array_search('ED1', $cfdiAddenda);

            if ($claveED1 != false)
            {
                $indice =  preg_replace('/\D/', '', $claveED1);
                 
                // datos de distribucion
                $output['distribucion']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            }

            // Bloque de codigo para obtener Transmision
            $claveET1 = array_search('ET1', $cfdiAddenda);
            $claveETB = array_search('ETB', $cfdiAddenda);

            if ($claveET1 != false)
            {
                $indice =  preg_replace('/\D/', '', $claveET1);
                 
                // datos de transmision
                $output['transmision']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            } else if ($claveETB != false){
                $indice =  preg_replace('/\D/', '', $claveETB);
                 
                // datos de transmision
                $output['transmision']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            }

            // Bloque de codigo para obtener CENACE
            $claveECB = array_search('ECB', $cfdiAddenda);
            $claveEC1 = array_search('EC1', $cfdiAddenda);

            if ($claveECB != false){
                $indice =  preg_replace('/\D/', '', $claveECB);

                // datos de cenace
                $output['cenace']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );

            } else if ($claveEC1 != false){
                $indice =  preg_replace('/\D/', '', $claveEC1);

                // datos de cenace
                $output['cenace']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            }

             // Bloque de codigo para obtener Energia
             $claveEG1 = array_search('EG1', $cfdiAddenda);

             if ($claveEG1 != false)
             {
                 $indice =  preg_replace('/\D/', '', $claveEG1);
                 
                  // datos de energia
                 $output['energia']= array(
                     "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                     "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                     "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                     "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                 );
             }

            // Bloque de codigo para obtener GeneracionB
            $claveEGB = array_search('EGB', $cfdiAddenda);

            if ($claveEGB != false)
            {
                $indice =  preg_replace('/\D/', '', $claveEGB);
                
                 // datos de generacionB
                $output['generacionB']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            }

            // Bloque de codigo para obtener GeneracionI
            $claveEGI = array_search('EGI', $cfdiAddenda);

            if ($claveEGI != false)
            {
                $indice =  preg_replace('/\D/', '', $claveEGI);
                
                 // datos de generacionI
                $output['generacionI']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            }

            // Bloque de codigo para obtener GeneracionP
            $claveEGP = array_search('EGP', $cfdiAddenda);

            if ($claveEGP != false)
            {
                $indice =  preg_replace('/\D/', '', $claveEGP);
                
                 // datos de generacionP
                $output['generacionP']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            }

            // Bloque de codigo para obtener Capacidad
            $claveEID = array_search('EID', $cfdiAddenda);
            $claveEIK = array_search('EIK', $cfdiAddenda);

            if ($claveEID != false)
            {
                $indice =  preg_replace('/\D/', '', $claveEID);
                
                 // datos de capacidad
                $output['capacidad']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            } else if ($claveEIK != false) {
                $indice =  preg_replace('/\D/', '', $claveEIK);
                
                // datos de capacidad
               $output['capacidad']= array(
                   "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                   "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                   "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                   "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
               );
            }

            // Bloque de codigo para obtener SCNMEM
            $claveEMB = array_search('EMB', $cfdiAddenda);
            $claveEM1 = array_search('EM1', $cfdiAddenda);

            if ($claveEMB != false){
                $indice =  preg_replace('/\D/', '', $claveEMB);

                // datos de scnmem
                $output['scnmem']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );

            } else if ($claveEM1 != false){
                $indice =  preg_replace('/\D/', '', $claveEM1);

                // datos de scnmem
                $output['scnmem']= array(
                    "impte" => isset($cfdiAddenda['IMPTE_FIJO_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_FIJO_REG_'.$indice] :  0,
                    "kw" => isset($cfdiAddenda['IMPTE_KW_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KW_REG_'.$indice] :  0,
                    "kwh" => isset($cfdiAddenda['IMPTE_KWH_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_KWH_REG_'.$indice] :  0,
                    "tot" => isset($cfdiAddenda['IMPTE_TOT_REG_'.$indice]) ? (float)$cfdiAddenda['IMPTE_TOT_REG_'.$indice] :  0,
                );
            }

            // datos calculados
            $kwhTotal = (int)$recibo->kwh_energia_base + (int)$recibo->kwh_energia_intermedia + (int)$recibo->kwh_energia_punta;
            $kwDemMax = max((int)$recibo->kw_demanda_base, (int)$recibo->kw_demanda_intermedia, (int)$recibo->kw_demanda_punta );

            // validamos que el kwhTotal no sea 0 por que es para recibos donde no tiene intermedia y base
            if ($kwhTotal != 0){

                $factorPotencia = cos(atan( (int)$recibo->kvarh_total / ($kwhTotal)));
                $recibo->fp = number_format(($factorPotencia*100), 2); // Factor de Potencia %
                
                $fecdesde = new DateTime($recibo->fecdesde);
                $fechasta = new DateTime($recibo->fechasta);
                $diff = $fechasta->diff($fecdesde);
                $recibo->fc = number_format(( $kwhTotal / ($kwDemMax * $diff->days * 24)*100), 2); // Factor Carga %
            }

            $datosVariablesImporte = array('Cargo Fijo', 'Energía', 'Bonificación Factor de Potencia', 'Subtotal', 'IVA 16%', 'Facturación del Periodo', 'Derecho de Alumbrado Público 5.00%', 'DAP', 'Adeudo Anterior', 'Su Pago', 'Total');
            $conceptos = [];

            foreach ($cfdiAddenda['Conceptos'] as $clave => $valor) {
                if (in_array($valor, $datosVariablesImporte)){

                    $nameImporte = 'Importe'.preg_replace('/[^0-9]/', '', $clave);
                    
                    array_push($conceptos,[
                        "concepto" => $valor,
                        "Importe" => isset($cfdiAddenda['Importes'][$nameImporte]) ? (float)$cfdiAddenda['Importes'][$nameImporte] :  0
                        ]
                    );
                    
                    //$conceptos[$clave]= array(
                    //    "concepto" => $valor,
                    //    "Importe" => isset($cfdiAddenda['Importes'][$nameImporte]) ? (float)$cfdiAddenda['Importes'][$nameImporte] :  0,
                    //);
                }
            }

            // Desglose del importe a pagar
            //$output['importe']= array(
            //    "cargofijo" => isset($cfdiAddenda['Importes']['Importe1']) ? (float)$cfdiAddenda['Importes']['Importe1'] :  0,
            //    "energia" => isset($cfdiAddenda['Importes']['Importe2']) ? (float)$cfdiAddenda['Importes']['Importe2'] :  0,
            //    "medbt" => isset($cfdiAddenda['Importes']['Importe3']) ? (float)$cfdiAddenda['Importes']['Importe3'] :  0,
            //    "fpbonificacion" => isset($cfdiAddenda['Importes']['Importe4']) ? (float)$cfdiAddenda['Importes']['Importe4'] :  0,
            //    "subtotalsiniva" => isset($cfdiAddenda['Importes']['Importe5']) ? (float)$cfdiAddenda['Importes']['Importe5'] :  0,
            //    "iva" => isset($cfdiAddenda['Importes']['Importe6']) ? (float)$cfdiAddenda['Importes']['Importe6'] :  0,
            //    "fpcargo" => isset($cfdiAddenda['Importes']['Importe7']) ? (float)$cfdiAddenda['Importes']['Importe7'] :  0,
            //    "dap" => isset($cfdiAddenda['Importes']['Importe8']) ? (float)$cfdiAddenda['Importes']['Importe8'] :  0,
            //    "adeudoanterior" => isset($cfdiAddenda['Importes']['Importe9']) ? (float)$cfdiAddenda['Importes']['Importe9'] :  0,
            //    "supago" => isset($cfdiAddenda['Importes']['Importe10']) ? (float)$cfdiAddenda['Importes']['Importe10'] :  0,
            //    
            //    );
            $output['importe']=  $conceptos;
            
            // Totales costo energia en el mercado
            $output['impteTotal'] = array_sum(array_column($output,'impte'));
            $output['kwTotal'] = array_sum(array_column($output,'kw'));
            $output['kwhTotal'] = array_sum(array_column($output,'kwh'));
            $output['totTotal'] = array_sum(array_column($output,'tot'));
            $output['total'] = isset($cfdiAddenda['Importes']['Importe11']) ? (float)$cfdiAddenda['Importes']['Importe11'] :  0;
    
            $recibo->datos_variables_fimporte = json_encode($output);        
        }

         $recibo->nombre_archivo_xml = $fileNameUploadXML;
         $recibo->nombre_archivo_pdf = isset($fileNameUploadPDF) ? $fileNameUploadPDF : '';
         $recibo->save();

        // Registro en log.
        \Log::info('|::| Recurso Recibo.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Recibo' => $recibo->nombre]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$recibo->nombre);

        return redirect()->route('recibos.index')
            ->with('flash_message',
             'Recibo '. $recibo->nombre.' agregado con éxito!');
    }

    /**
     * Funcion para retornar la fecha en el formato adecuado.
     *
     * @param  date $fecha
     * @return string $fecha
     */
    private function getFecha($fecha){

        $months = [
            'ENE' => 'Jan',
            'FEB' => 'Feb',
            'MAR' => 'Mar',
            'ABR' => 'Apr',
            'MAY' => 'May',
            'JUN' => 'June',
            'JUL' => 'July',
            'AGO' => 'Aug',
            'SEP' => 'Sept',
            'OCT' => 'Oct',
            'NOV' => 'Nov',
            'DIC' => 'Dec'
        ];

        $date = str_replace(array_keys($months), $months, $fecha);
        return $date;
    }

    /**
     * Funcion para retornar la fecha del mes facturado.
     *
     * @param  date $fecha
     * @return string $fecha
     */
    private function getMesFacturado($fecha){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        // Registro en log.
        \Log::info('|::| ReciboController.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $recibo = Recibo::findOrFail(decrypt($id));

        $cfdiReceptor = json_decode($recibo->cfdiReceptor, true)['@attributes'];
        $cfdiAddenda = json_decode($recibo->cfdiAddenda, true)['clsRegArchFact'];
        $datosVariables = json_decode($recibo->datos_variables_fimporte, true);

        return view('recibos.show', compact('cfdiReceptor','cfdiAddenda','recibo','datosVariables'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        // Registro en log.
        \Log::info('|::| ReciboController.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $recibo = Recibo::findOrFail(decrypt($id));
        
        $municipios = Municipio::pluck('municipio', 'id');
        $areas = Area::pluck('nombre', 'id');
        $tarifas = Tarifa::pluck('descripcion', 'nombre');
        $datosVariables = json_decode($recibo->datos_variables_fimporte, true);

        // Obtenemos importes del desglose a pagar.
        $keyIva = array_search('IVA 16%', array_column($datosVariables['importe'], 'concepto'));
        $iva = $keyIva != false ? $datosVariables['importe'][$keyIva]['Importe'] : 0;

        $keyFP = array_search('Bonificación Factor de Potencia', array_column($datosVariables['importe'], 'concepto'));
        $fp = $keyFP != false ? $datosVariables['importe'][$keyFP]['Importe'] : 0;

        $keyFPC = array_search('Facturación del Periodo', array_column($datosVariables['importe'], 'concepto'));
        $fpc = $keyFPC != false ? $datosVariables['importe'][$keyFPC]['Importe'] : 0;

        $keySubtotal = array_search('Subtotal', array_column($datosVariables['importe'], 'concepto'));
        $subtotal = $keySubtotal != false ? $datosVariables['importe'][$keySubtotal]['Importe'] : 0;

        $keyDAP = array_search('Derecho de Alumbrado Público 5.00%', array_column($datosVariables['importe'], 'concepto'));
        $dap = $keyDAP != false ? $datosVariables['importe'][$keyDAP]['Importe'] : 0;

        $keyTotal = array_search('Total', array_column($datosVariables['importe'], 'concepto'));
        $total = $keyTotal != false ? $datosVariables['importe'][$keyTotal]['Importe'] : 0;

        $medBT = 0;

        return view('recibos.edit', compact('recibo','municipios','areas','tarifas','datosVariables','iva','medBT','fp','fpc','subtotal','dap','total'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        $recibo = Recibo::findOrFail(decrypt($id));
        $input = $request->all();

        $modifyDatosVariables = json_decode($recibo->datos_variables_fimporte, true);
        $modifyDatosVariables['suministro']['tot'] = (float)$request->suministro;
        $modifyDatosVariables['distribucion']['tot'] = (float)$request->distribucion;
        $modifyDatosVariables['transmision']['tot'] = (float)$request->transmision;
        $modifyDatosVariables['cenace']['tot'] = (float)$request->cenace;
        $modifyDatosVariables['generacionB']['tot'] = (float)$request->generacionB;
        $modifyDatosVariables['generacionI']['tot'] = (float)$request->generacionI;
        $modifyDatosVariables['generacionP']['tot'] = (float)$request->generacionP;
        $modifyDatosVariables['capacidad']['tot'] = (float)$request->capacidad;
        $modifyDatosVariables['scnmem']['tot'] = (float)$request->scnmem;

        // recorremos el json para actualizr los valores si es que existio un cambio
        foreach($modifyDatosVariables['importe'] as $key=>$value) {

            if ($value['concepto'] == 'IVA 16%'){
                $value['Importe'] = (float)$request->iva;
            } else if ($value['concepto'] == 'Energía'){
                $value['Importe'] =  (float)$request->energia;
            } else if ($value['concepto'] == 'Subtotal'){
                $value['Importe'] = (float)$request->subtotalsiniva;
            } else if ($value['concepto'] == 'Bonificación Factor de Potencia'){
                $value['Importe'] = (float)$request->fpbonificacion;
            } else if ($value['concepto'] == 'Derecho de Alumbrado Público 5.00%'){
                $value['Importe'] = (float)$request->dap;
            } else if ($value['concepto'] == 'Total'){
                $value['Importe'] = (float)$request->total;
            } 
        }
     
        //$modifyDatosVariables['importe']['energia'] = (float)$request->energia;
        //$modifyDatosVariables['importe']['medbt'] = (float)$request->medbt;
        //$modifyDatosVariables['importe']['fpbonificacion'] = (float)$request->fpbonificacion;
        //$modifyDatosVariables['importe']['fpcargo'] = (float)$request->fpcargo;
        //$modifyDatosVariables['importe']['subtotalsiniva'] = (float)$request->subtotalsiniva;
        //$modifyDatosVariables['importe']['iva'] = (float)$request->iva;
        //$modifyDatosVariables['importe']['dap'] = (float)$request->dap;

       

        $modifyDatosVariables['total'] = (float)$request->total;
        $modifyDatosVariables['totalkw'] = (float)$request->totalkw;
        $modifyDatosVariables['totalkwh'] = (float)$request->totalkwh;

        $input['datos_variables_fimporte'] = json_encode($modifyDatosVariables); 
        $recibo->fill($input);
        $recibo->datos_variables_fimporte =  json_encode($modifyDatosVariables); 
        dd($recibo->datos_variables_fimporte);     
       
       
        $recibo->save();
        
        return redirect()->route('recibos.index')
            ->with('flash_message',
             'Recibo '. $recibo->nombre.' Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $recibo = Recibo::findOrFail(decrypt($request->id_recibo));
        $recibo->delete();

        // Registro en log.
        \Log::info('|::| Recibo.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Recibo' => $recibo]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$recibo->nombre);

        return redirect()->route('recibos.index')
            ->with('flash_message',
             'Recibo eliminada con éxito!');
    }

    public function capture(){
        $municipios = Municipio::pluck('municipio', 'id');
        $areas = Area::pluck('nombre', 'id');
        $tarifas = Tarifa::pluck('descripcion', 'nombre');
        $costos = Costo::pluck('centro', 'id');
        $ubicaciones = Ubicacion::pluck('ubicacion', 'id');
        $empresas = Empresa::pluck('empresa', 'id');

        //Get all municipios and show view
        return view('recibos.capture', compact('municipios','areas','tarifas','costos','ubicaciones','empresas'));
    }

    public function saveCapture(Request $request){
        //dd($request->all());

        // Alta de recibo en forma manual.
        $recibo = new Recibo();
        $recibo->nombre = $request->nombre;
        $recibo->direccion = $request->direccion;
        $recibo->calle1 = $request->calle;
        $recibo->colonia = $request->colonia;
        $recibo->nompob = $request->nompob;
        $recibo->nomest = $request->nomest;
        $recibo->cp = $request->codigoPostal;
        $recibo->rfc = $request->rfc;
        $recibo->total = $request->totalPagar;
        $recibo->monto_apagar_enletras = $request->totalLetra;
        $recibo->multiplo = $request->multiplo;
        $recibo->demanda_contratada = $request->demanda_contratada;
        $recibo->carga_conectada = $request->carga_conectada;
        $recibo->feclimite = $request->feclimite;
        $recibo->fecorte = $request->fecorte;
        $recibo->fecdesde = $request->fecdesde;
        $recibo->fechasta = $request->fechasta;
        $recibo->mes_facturado = date('Y-m-d',strtotime($request->mes_facturado));
        $recibo->periodo_facturado = $request->fecdesde.' - '.$request->fechasta;

        $recibo->kwh_energia_base = $request->kwh_energia_base;
        $recibo->kwh_energia_intermedia = $request->kwh_energia_intermedia;
        $recibo->kwh_energia_punta = $request->kwh_energia_punta;
        $recibo->kw_demanda_base = $request->kw_demanda_base;
        $recibo->kw_demanda_intermedia = $request->kw_demanda_intermedia;
        $recibo->kw_demanda_punta = $request->kw_demanda_punta;

        // Generamos Datos Variables de la Factura (Importe) en formato json.

        // datos de distribucion
        $output['suministro']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->suministro,
        );

        // datos de distribucion
        $output['distribucion']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->distribucion,
        );

        // datos de distribucion
        $output['transmision']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->transmision,
        );

        // datos de distribucion
        $output['cenace']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->cenace,
        );

        // datos de distribucion
        $output['generacionB']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->generacionB,
        );

          // datos de distribucion
          $output['generacionI']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->generacionI,
        );

          // datos de distribucion
          $output['generacionP']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->generacionP,
        );

        // datos de distribucion
        $output['capacidad']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->capacidad,
        );

        // datos de distribucion
        $output['scnmem']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->scnmem,
        );

        // datos de distribucion
        $output['energia']= array(
            "impte" => 0,
            "kw" => 0,
            "kwh" => 0,
            "tot" => (float)$request->energia,
        );

        // Importes
        $output['importe']= array(
            [
                "concepto" => 'Cargo Fijo',
                "Importe" => 0
            ],
            [
                "concepto" => 'Energía',
                "Importe" => 0
            ],
            [
                "concepto" => 'Bonificación Factor de Potencia',
                "Importe" => (float)$request->fpbonificacion
            ],
            [
                "concepto" => 'Subtotal',
                "Importe" => (float)$request->subtotalsiniva
            ],
            [
                "concepto" => 'IVA 16%',
                "Importe" => (float)$request->iva
            ],
            [
                "concepto" => 'Facturación del Periodo',
                "Importe" => (float)$request->fpcargo
            ],
            [
                "concepto" => 'Derecho de Alumbrado Público 5.00%',
                "Importe" => (float)$request->dap
            ],
            [
                "concepto" => 'Adeudo Anterior',
                "Importe" => 0
            ],
            [
                "concepto" => 'Su Pago',
                "Importe" => 0
            ],
            [
                "concepto" => 'Total',
                "Importe" => (float)$request->total
            ],
        );

        // Totales costo energia en el mercado
        $output['impteTotal'] = 0;
        $output['kwTotal'] = (float)$request->totalkw;
        $output['kwhTotal'] = (float)$request->totalkwh;
        $output['totTotal'] = 0;
        $output['total'] = 0;

        $datos_variables_importe = array();
        $datos_variables_importe['suministro'] = (float)$request->suministro;
        $datos_variables_importe['distribucion'] = (float)$request->distribucion;
        $datos_variables_importe['transmision'] = (float)$request->transmision;
        $datos_variables_importe['cenace'] = (float)$request->cenace;
        $datos_variables_importe['generacionB'] = (float)$request->generacionB;
        $datos_variables_importe['generacionI'] = (float)$request->generacionI;
        $datos_variables_importe['generacionP'] = (float)$request->generacionP;
        $datos_variables_importe['capacidad'] = (float)$request->capacidad;
        $datos_variables_importe['scnmem'] = (float)$request->scnmem;
        $datos_variables_importe['energia'] = (float)$request->energia;
        $datos_variables_importe['medbt'] = (float)$request->medbt;
        $datos_variables_importe['fpbonificacion'] = (float)$request->fpbonificacion;
        $datos_variables_importe['fpcargo'] = (float)$request->fpcargo;
        $datos_variables_importe['subtotalsiniva'] = (float)$request->subtotalsiniva;
        $datos_variables_importe['iva'] = (float)$request->iva;
        $datos_variables_importe['dap'] = (float)$request->dap;
        $datos_variables_importe['total'] = (float)$request->total;
        $datos_variables_importe['totalkw'] = (float)$request->totalkw;
        $datos_variables_importe['totalkwh'] = (float)$request->totalkwh;

        $recibo->datos_variables_fimporte = json_encode($output);

        $recibo->kw_max_movil = $request->kw_max_movil;
        $recibo->kvarh_total = $request->kvarh_total;
        $recibo->fp = $request->fp;
        $recibo->fc = isset($request->fc) ? $request->fc : 0;

        $recibo->rpu = $request->rpu;
        $recibo->rmu = $request->rmu;
        $recibo->no_medidor = $request->no_medidor;
        $recibo->no_cuenta = $request->no_cuenta;
        $recibo->tarifa = $request->id_tarifa;
      
        $recibo->id_area = $request->id_area;
        $recibo->id_municipio = $request->id_municipio;
        $recibo->id_ubicacion = $request->id_ubicacion;
        $recibo->id_empresa = $request->id_empresa;
        $recibo->id_costo = $request->id_costo;
        $recibo->tipo = '2';
        $recibo->save();
    
        return redirect()->route('recibos.index')
            ->with('flash_message',
             'Recibo '. $recibo->nombre.' agregado con éxito!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $recibos = Recibo::all(); 
        return Datatables::of($recibos)
            ->addColumn('action', function ($recibo) {

                $result = '';

                if (Auth::user()->hasPermissionTo('Ver'))
                    $result .= " <a href='".route('recibos.show', encrypt($recibo->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                if (Auth::user()->hasPermissionTo('Editar'))
                    $result .= " <a href='".route('recibos.edit', encrypt($recibo->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";

                if (Auth::user()->hasPermissionTo('Eliminar'))
                    $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($recibo->id)."' data-title='".$recibo->nombre."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
            
                return $result;
            })
            ->addColumn('municipio', function ($recibo) {
                return $recibo->Municipios->municipio;
            })
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }
}
