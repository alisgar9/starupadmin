<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Puerto;
use App\Pais;
use App\Entidades;
use App\Operacion;
use App\Liberacion;
use App\Incoterm;
use App\Contenedor;
use App\OperacionDetails;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;
use Session;
Use Redirect;
use DB;
use PdfReport;
use ExcelReport;
use Config;
use Response;





class ReportContainersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        
        //catalogo Consignee
        $entidad = Entidades::where([['status', 1]])->orderBy('nombre')->pluck('nombre', 'id');

        
        return view('reportes.reportContainers')->with('entidad', $entidad);
        
    }


    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData(Request $request)
    {
       
       
       dd($request);
       
       
        $IdCurrentUser = Auth::id();
       
      //$detallesoperacion = OperacionDetails::where([['activo','=',1]] )->get();
      
       $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
       ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.mbl','P.ETA','P.ATA','P.free_demurrages_days_cust','P.tipo_operacion','P.operacion', 'P.precio_demora', 'P.iva','C.descripcion AS tipo_operacion_desc', 'P.PI', 'P.PO')
       ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
       ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
       ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
       ->where([[function($query) use ($IdCurrentUser) {
           if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
               $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
           }else{
               $query->where([['U.type','=',1]]);
           }
       }],['U.activo','=',1],['P.consignee','=',$request->custom],['A.activo', '=', '1'], ['P.operacion', '=', '1']])
       ->whereBetween('P.ETA', array($request->etas, $request->etast))
       ->orWhere('P.operacion','=',2)
       ->get();

       

       return Datatables::of($detallesoperacion)
           
          
           ->addIndexColumn()
           ->addColumn('difdays', function ($query) {

                $fechaEmision = Carbon::parse($query->ATA);
                $dias_demora = $query->free_demurrages_days_cust - 1;               
                $newDateETA = $fechaEmision->addDays($dias_demora);
                $newDateETA2 = Carbon::parse($newDateETA);
                $rango = $this->isDateBetweenDates($query->empty_container, $query->ATA, $newDateETA2);

                return $rango;
            })
            ->addColumn('etaplus', function ($query) {

                
                $fechaEmision = Carbon::parse($query->ATA);
                $free_demurrages_days_cust = $query->free_demurrages_days_cust - 1;
                $newDateETA = $fechaEmision->addDays($free_demurrages_days_cust);
                $newDateETA2 = Carbon::parse($newDateETA);

                return Carbon::parse($newDateETA2)->format("d-M-y");

               
            })

           ->editColumn('empty_container', '{{ Carbon\Carbon::parse($empty_container)->format("d-M-y") }}')
           ->editColumn('ETA', '{{ Carbon\Carbon::parse($ETA)->format("d-M-y") }}')
           ->rawColumns(['estado', 'action'])
           ->make(true);
    }

    public function isDateBetweenDates($date, $startDate, $endDate) {
        
        $diasDiferencia = $endDate->diffInDays($date);

        if(($date >= $startDate) && ($date <= $endDate))
        {
              return 'Not Apply';
        }
         else
         {
              return $diasDiferencia;
         }
    }

    /**
     * Limpiar Espacios en Blanco.
     *
     */
    public function stringClear($string){
        
        $string = preg_replace("[\s+]",'', $string);

        return $string;
    }
    public function LastFreeDay($eta,$freeDay){

        $free_demurrages_days_cust = $freeDay - 1;           
        //$newDateETA = $eta->addDays($free_demurrages_days_cust);
        $newDateETA =  (new Carbon($eta))->addDays($free_demurrages_days_cust);
        $newDateETA_clear = Carbon::parse($newDateETA);
                    
        return $newDateETA_clear;

    }

    public function Descentidad($id){
        
        $entidad = Entidades::where("id","=",$id)->first();

        return $entidad;
    }

    public function generateReportxls($custom, $dateStart, $dateFinish)
    {        
        $IdCurrentUser = Auth::id();     
        $data =  DB::table('adm_operaciones_contenedor as A')
        ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.id AS SUMEX','P.hbl','P.mbl','P.ETA','P.ETD', 'P.ATA','P.free_demurrages_days_cust','P.tipo_operacion','P.operacion', 'P.precio_demora','P.PO','P.PI', 'P.iva', 'P.agente','C.descripcion AS tipo_operacion_desc')
        ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
        ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
        ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
        ->where([[function($query) use ($IdCurrentUser) {
            if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
            }else{
                $query->where([['U.type','=',1]]);
            }
        }],['U.activo','=',1],['P.consignee','=',$custom],['A.activo', '=', '1'], ['P.operacion', '=', '1']])
        ->whereBetween('P.ETA', array($dateStart, $dateFinish))
        ->orWhere('P.operacion','=',2)
        ->orderby('P.id', 'asc')->get()->toArray();
            
        $customer = Entidades::where("id","=",$custom)->first();

        //dd($data);
        //$data = Entidades::get()->toArray();
        
        return \Excel::create('ReportContainer', function($excel) use ($data, $customer) {
            $excel->sheet('mySheet', function($sheet) use ($data, $customer)
            {

                $sheet->cell('A1', function($cell) {$cell->setValue('FOLIO');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('HBL');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('MBL');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('ETD');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('ATA');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('Last Free Day');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('# Container');   });
                $sheet->cell('H1', function($cell) {$cell->setValue('Customs Broker');   });
                $sheet->setColumnFormat(array('A' => '@', 'B' => '@','C' => '@', 'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,'F' => NumberFormat::FORMAT_DATE_DDMMYYYY,'G' => '@','H' => '@'));
                


                if (!empty($data)) {

                    
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $folio = 'SUMEX0'.$value->SUMEX;

                        

                        //dd($folio);
                        $sheet->cell('A'.$i, $folio);
                        $sheet->cell('B'.$i, $value->hbl); 
                        $sheet->cell('C'.$i, $value->mbl);
                        $sheet->cell('D'.$i, Date::stringToExcel($value->ETD)); 
                        $sheet->cell('E'.$i, Date::stringToExcel($value->ATA));
                        $sheet->cell('F'.$i, Date::stringToExcel($this->LastFreeDay($value->ATA,$value->free_demurrages_days_cust)));
                        $sheet->cell('G'.$i, $value->container_number);
                        $sheet->cell('H'.$i, isset($value->agente) ? $this->Descentidad($value->agente)->nombre : NULL); 
                    }
                }
            });
        })->export('xlsx');
    }

    
    


}
