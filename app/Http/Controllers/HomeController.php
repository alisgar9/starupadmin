<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use App\Municipio;
use App\Area;
use App\Recibo;
use Illuminate\Http\Request;
use Charts;
use DB;
use Auth;
use Yajra\Datatables\Datatables;
use App\Operacion;
use App\Liberacion;
use App\Incoterm;
use App\Contenedor;
use App\OperacionDetails;
use App\OperacionUsers;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        
        return view('home.index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function logActivity()
    {
        // Registro de log.
        \Log::info('|::| Bitácora Index.', ['Usuario' => Auth::user()]);

        $logs = \LogActivity::logActivityLists();
        return view('eventos.index',compact('logs'));
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $logs = \LogActivity::logActivityLists();
        return Datatables::of($logs)
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    /**
     * Show the application approval.
     *
     * @return Response
     */
    public function approval()
    {
        return view('approval');
    }

    public function home()
    {

        $IdCurrentUser = Auth::id();
         if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {

            
            $operacionesList = DB::table('adm_operaciones_usuarios AS U')
            ->Join('adm_operaciones AS O', 'O.id', '=', 'U.id_operacion')
            ->where([[function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
                }
            }],['O.activo','=',1]])
            ->groupBy('U.id_operacion')
            ->orderBy('O.id', 'DESC')
            ->take(8)
            ->get();


            $operaciones = DB::table('adm_operaciones AS O')
                ->join('adm_operaciones_usuarios AS U', 'O.id', '=', 'U.id_operacion')
                ->where(function($query) use ($IdCurrentUser) {
                    if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                        $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                    }
                })->count();


            $operacionesPre = DB::table('adm_operaciones AS O')
                ->join('adm_operaciones_usuarios AS U', 'O.id', '=', 'U.id_operacion')
                ->where([[function($query) use ($IdCurrentUser) {
                    if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                        $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                    }
                }], ['O.pre_operation',1],['O.activo',1]])->count();


            $operacionesEnabled = DB::table('adm_operaciones AS O')
                ->join('adm_operaciones_usuarios AS U', 'O.id', '=', 'U.id_operacion')
                ->where([[function($query) use ($IdCurrentUser) {
                    if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                        $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                    }
                }],['O.activo','=',1]])->count();
            
            $operacionesDisabled = DB::table('adm_operaciones AS O')
                ->join('adm_operaciones_usuarios AS U', 'O.id', '=', 'U.id_operacion')
                ->where([[function($query) use ($IdCurrentUser) {
                    if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                        $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                    }
                }],['O.activo','=',0]])->count();

            $operacionesShared = Operacion::where([['activo', 1],['share', '>',0]])->count();

            $contenedores = DB::table('adm_operaciones_contenedor as A')
            ->select('A.operacion_id', 'A.container_number','A.container_seal','A.mark','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.tipo_operacion','P.operacion')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where(function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                }
            })
            ->count();

                    
         $contenedoreEnabled = DB::table('adm_operaciones_contenedor as A')
            ->select('A.operacion_id', 'A.container_number','A.container_seal','A.mark','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.tipo_operacion','P.operacion')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([[function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                }
            }],['A.activo','=',1]])
            ->count();

         $contenedorReturnEmpty = DB::table('adm_operaciones_contenedor as A')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([[function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                }
            }],['A.activo','=',2]])
            ->count();

        }else{
            
            $operaciones = DB::table('adm_operaciones')->count();
            $operacionesPre = DB::table('adm_operaciones')->where([['pre_operation',1],['activo',1]])->count();
            $operacionesEnabled = DB::table('adm_operaciones')->where('activo','=', 1)->count();
            $operacionesDisabled = DB::table('adm_operaciones')->where('activo','=', 0)->count();
            $contenedoreEnabled = DB::table('adm_operaciones_contenedor as A')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where('A.activo','=',1)
            ->count();
            $contenedorReturnEmpty = DB::table('adm_operaciones_contenedor as A')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where('A.activo','=',2)
            ->count();

        }

        return view('home.index')
        ->with('operacionesShared', $operacionesShared)
        ->with('operaciones', $operaciones)
        ->with('operacionesEnabled', $operacionesEnabled)
        ->with('operacionesDisabled', $operacionesDisabled)
        ->with('operacionesPre', $operacionesPre)
        ->with('contenedoreEnabled', $contenedoreEnabled)
        ->with('operacionesList', $operacionesList)
        ->with('contenedorReturnEmpty', $contenedorReturnEmpty);
    }
}
