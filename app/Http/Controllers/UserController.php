<?php

namespace App\Http\Controllers;

use App\User;
use App\Cargo;
use App\Userdetails;
use Auth;
use Charts;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

//Importing laravel-permission models

//Enables us to output flash messaging
use Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Usuarios Index.', ['Usuario' => Auth::user()]);

        //Get all users and pass it to the view
        $users = User::all();

        //Get count users this month
        $usersThisMonth = User::where(DB::raw('MONTH(created_at)'), '=', date('n'))->get();

        //Get last 6 users
        $lastUsers = $users->sortByDesc('created_at')->take(6);

        //Get Count users deleted
        $delUsers = $users->where('approved_at', null);

        $countUsers = DB::table('users')
            ->select(DB::raw('MONTH(created_at) as month'), DB::raw('count(*) as records'))
            ->whereRaw('year(`created_at`) = ?', array(date('Y')))
            ->groupBy('month')
            ->get()->toArray();

        $serieChart = array();

        for ($i = 1; $i <= 12; $i++) {
            $searchR = array_search($i, array_column($countUsers, 'month'));

            if (is_int($searchR)) {
                array_push($serieChart, $countUsers[$searchR]->records);
            } else {
                array_push($serieChart, 0);
            }
        }

        //Grafica de Recibos Facturados por Municipio
        $chartPline = Charts::multi('areaspline', 'highcharts')
            ->title('Analítica ' . date("Y"))
            ->colors(['#ff0000', '#ffffff'])
            ->elementLabel("Registros")
            ->labels(['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'])
            ->dataset('Usuarios', $serieChart);

        return view('users.index')->with('users', $users)->with('chartPline', $chartPline)->with('lastUsers', $lastUsers)->with('usersThisMonth', $usersThisMonth)->with('delUsers', $delUsers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario users.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Get all roles and pass it to the view
        $roles = Role::get();
        $cargos = Cargo::where('activo', 1)->get()->pluck('descripcion', 'idcargo')->toArray();
        
        //$cargos = Cargo::pluck('descripcion', 'idcargo')->where("activo",1);
        return view('users.create', ['roles' => $roles, 'cargos' => $cargos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'name' => 'required|max:120',
            'username' => 'sometimes|required|max:255|unique:users',
            'email' => 'required|email|unique:users',
            'roles' => 'required',
            'roles.*' => 'numeric',
            'password' => 'required|min:6|confirmed',
        ], [
            'name.required' => 'El campo Nombre es requerido!',
            'username.required' => 'El campo Nombre de usuario es requerido!',
            'email.required' => 'El campo Correo es requerido!',
            'email.email' => 'Incluye un signo "@" en la dirección de correo!',
            'roles.required' => 'El campo Rol es requerido!',
            'password.required' => 'El campo Contraseña es requerido!',
            'password.min' => 'La contraseña debe tener al menos 6 caracteres!',
            'password.confirmed' => 'La confirmación de contraseña no coincide!',
        ]);

        
        $user = User::create($request->only('email', 'name', 'password', 'username')); //Retrieving only the email and password data

        $Userdetails = new Userdetails();
        $Userdetails->user_id = $user->id;
        $Userdetails->fechaingreso = $request['fechaingreso'];
        $Userdetails->fechasalida = $request['fechasalida'];
        $Userdetails->idcargo = $request['idcargo'];
        $Userdetails->salario = $request['salario'];
        $Userdetails->rfc = $request['rfc'];
        $Userdetails->curp = $request['curp'];
        $Userdetails->domicilio = $request['domicilio'];
        $Userdetails->telefono = $request['telefono'];
        $Userdetails->nss = $request['nss'];
        $Userdetails->save();
        
        
        $roles = $request['roles']; //Retrieving the roles field
        //Checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r); //Assigning role to user
            }
        }

        // Registro en log.
        \Log::info('|::| Recurso User.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Usuario' => $user]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$user->email);

        //Redirect to the users.index view and display message
        return redirect()->route('users.index')
            ->with('flash_message',
                'Usuario agregado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| UserController.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $user = User::findOrFail(decrypt($id)); //Get user with specified id
        $userdetail = Userdetails::where('user_id', decrypt($id))->first(); 
        $roles = Role::get(); //Get all roles
        $cargos = Cargo::where('activo', 1)->get()->pluck('descripcion', 'idcargo')->toArray();

        return view('users.edit', compact('user', 'roles', 'cargos', 'userdetail')); //pass user and roles data to view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'name' => 'required|max:120',
            'email' => 'required|email|unique:users,email,' . decrypt($id),
            'password' => 'confirmed',
        ], [
            'password.required' => 'El campo Contraseña es requerido!',
            'password.min' => 'La contraseña debe tener al menos 6 caracteres!',
            'password.confirmed' => 'La confirmación de contraseña no coincide!',
        ]);

        $user = User::findOrFail(decrypt($id)); //Get role specified by id

        $userdetail = Userdetails::findOrFail(decrypt($request->details_id)); 
        $userdetail->update(
            array(
                'fechaingreso' => $request->fechaingreso, 
                'fechasalida' => $request->fechasalida, 
                'idcargo' => $request->idcargo, 
                'salario' => $request->salario, 
                'rfc' => $request->rfc, 
                'curp' => $request->curp, 
                'domicilio' => $request->domicilio, 
                'telefono' => $request->telefono, 
                'nss' => $request->nss
            )
        );

        $newPassword = $request->get('password');

        if(empty($newPassword)){
            $user->update($request->except('password'));
        }else{
            $user->update($request->all());
        }
    
        $roles = $request['roles']; //Retreive all roles
  
        if (isset($roles)) {
            $user->roles()->sync($roles); //If one or more role is selected associate user to roles
        } else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }

        \LogActivity::addToLog('Se actualizo el usuario : ' . $user->email);

        return redirect()->route('users.index')
            ->with('flash_message',
                'Usuario editado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        //Find a user with a given id and delete
        $user = User::findOrFail(decrypt($request->id_user));
        $user->approved_at = null;
        $user->status = "0";
        $user->save();

        // Registro en log.
        \Log::info('|::| User.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'User' => $user]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$user->name);

        return redirect()->route('users.index')
            ->with('flash_message',
                'Usuario eliminado con éxito.');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        $users = User::all();

        return Datatables::of($users)
            ->addColumn('action', function ($user) {

                $result = '';

                if (Auth::user()->hasPermissionTo('Editar')) {
                    $result .= " <a href='" . route('users.edit', encrypt($user->id)) . "' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
                }

                if (Auth::user()->id != $user->id) {
                    if (Auth::user()->hasPermissionTo('Eliminar')) {
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='" . encrypt($user->id) . "' data-title='".$user->name."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                    }
                }
                return $result;
            })
            ->addColumn('perfil', function ($user) {

                $array = array();

                foreach ($user->roles as $role) {
                    array_push($array, $role->name);
                }

                return $array;
            })
            ->addColumn('estado', function ($user) {

                $result = '';

                if (Auth::user()->hasPermissionTo('Administer roles & permissions')) {
                    if ($user->approved_at) {
                        $result .= '<button type="button" class="btn btn-success">Aprobado</button><button type="button" class="btn btn-default" disabled>Aprobar</button>';
                    } else {
                        $result .= "<button type='button' class='btn btn-default' disabled>Aprobado</button><a href='" . route('admin.users.approve', encrypt($user->id)) . "' type='button' class='btn btn-danger'>Aprobar</a>";
                    }

                } else {
                    $result .= '<button type="button" class="btn btn-default" disabled>Aprobado</button><button type="button" class="btn btn-disabled" disabled>Aprobar</button>';
                }

                return $result;
            })
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    /**
     * Process ajax approve user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve($user_id)
    {
        $user = User::findOrFail(decrypt($user_id));
        $user->update(['approved_at' => now()]);

        return redirect()->route('users.index')->with('flash_message', 'Usuario aprobado exitosamente!!');
    }
}
