<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Contenedor;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class ContenedorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Contenedor Index.', ['Usuario' => Auth::user()]);

         //Get all contenedores and pass it to the view
         $contenedores = Contenedor::count();
         $contenedoresEnabled = Contenedor::where('activo','=',1)->count();
         $contenedoresDisabled = Contenedor::where('activo','=',0)->count();
         return view('contenedores.index')->with('contenedores', $contenedores)->with('contenedoresEnabled', $contenedoresEnabled)->with('contenedoresDisabled', $contenedoresDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario contenedores.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Get all municipios and show view
        return view('contenedores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'descripcion' => 'required|max:50|unique:adm_contenedor',
        ],[
            'descripcion.required' => 'El campo Nombre es requerido!',
            
        ]);

        $contenedor = new Contenedor();
        $contenedor->descripcion = Str::upper($request['descripcion']);
        $contenedor->save();

        // Registro en log.
        \Log::info('|::| Recurso Contenedor.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Contenedor' => $contenedor]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$contenedor->descripcion);

        return redirect()->route('contenedores.index')
            ->with('flash_message',
             'Contenedor '. $contenedor->descripcion.' agregada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        // Registro en log.
        \Log::info('|::| Contenedores.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $contenedor = Contenedor::findOrFail(decrypt($id));
        return view('contenedores.show', compact('contenedor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| Contenedores.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $contenedor = Contenedor::findOrFail(decrypt($id));
        return view('contenedores.edit', compact('contenedor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        
        

        
        //Validate name, email and password fields
        $this->validate($request, [
            'descripcion' => 'required|max:50',
        ],[
            'descripcion.required' => 'El campo Nombre es requerido!',
            
        ]);

        $contenedor = Contenedor::findOrFail($id);
        $input = array(
            'descripcion' => Str::upper($request['descripcion'])
        );
        $contenedor->fill($input)->save();

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$contenedor->descripcion);
        
        return redirect()->route('contenedores.index')
            ->with('flash_message',
             'Contenedores '. $contenedor->descripcion.' actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $contenedor = Contenedor::findOrFail(decrypt($request->idcontenedor));
        $contenedor->activo = 0;
        $contenedor->save();

        // Registro en log.
        \Log::info('|::| Contenedor.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Contenedor' => $contenedor]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$contenedor->descripcion);

        return redirect()->route('contenedores.index')
            ->with('flash_message',
             'Contenedor eliminado con éxito!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $showDeleted  = Input::get('showDeleted');

        $contenedores = Contenedor::where('activo','=',$showDeleted )->get();
        return Datatables::of($contenedores)
            ->addColumn('action', function ($contenedores) {

                $result = '';

                if ($contenedores->activo == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('contenedores.show', encrypt($contenedores->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('contenedores.edit', encrypt($contenedores->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-clave='".encrypt($contenedores->id)."' data-title='".$contenedores->descripcion."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addIndexColumn()
            ->editColumn('descripcion', '{{ $descripcion }}')
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    

}
