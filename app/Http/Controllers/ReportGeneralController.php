<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Puerto;
use App\Estado;
use App\Aeropuerto;
use App\Pais;
use App\Entidades;
use App\Operacion;
use App\Liberacion;
use App\Incoterm;
use App\Contenedor;
use App\OperacionDetails;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;
use Session;
Use Redirect;
use DB;
use PdfReport;
use ExcelReport;
use Config;
use Response;





class ReportGeneralController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Demurrage(Request $request)
    {
        
        //catalogo Consignee
        $entidad = Entidades::where([['status', 1]])->orderBy('nombre')->pluck('nombre', 'id');

        
        return view('reportes.reportGeneral')->with('entidad', $entidad);
        
    }


    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData(Request $request)
    {
       $IdCurrentUser = Auth::id();
       
      //$detallesoperacion = OperacionDetails::where([['activo','=',1]] )->get();

       $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
       ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo', 'P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA', 'P.free_demurrages_days_cust', 'P.POL','P.POD','P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal','C.descripcion AS tipo_operacion_desc')
       ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
       ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
       ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
       ->where([[function($query) use ($IdCurrentUser) {
           if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
               $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
           }else{
               $query->where([['U.type','=',1]]);
           }
       }],['U.activo','=',1],['P.consignee','=',$request->custom],[
        function($query){
            $query->where('A.activo', '=', 1)
              ->orWhere('A.activo', '=', 2);
        }
       ]])
       ->whereBetween('P.ETA', array($request->etas, $request->etast))
       ->orderby('P.id', 'asc')
       ->get();

       return Datatables::of($detallesoperacion)
           
          
            ->addIndexColumn()
            ->editColumn('POL', function ($query) {

                $puerto = Puerto::where("id","=",$query->POL)->first();
                
                return $puerto->puerto;
            })
            ->editColumn('POD', function ($query) {

                $puerto = Puerto::where("id","=",$query->POD)->first();
                
                return $puerto->puerto;
            })
            ->editColumn('shipper', function ($query) {
                if($query->shipper) {                    
                    $shipper = Entidades::where("id","=",$query->shipper)->first();
                    return $shipper->nombre;
                }else{
                    return 'No Apply';
                }
            })
            ->editColumn('telex', function ($query) {
                if($query->telex) {                    
                    return $query->telex;
                }else{
                    return 'Not';
                }
            })
           ->editColumn('SUMEX', '{{ "SUMEX0".$SUMEX }}')
           ->editColumn('ETA', function ($query) {
                if($query->ETA) {                    
                    return Carbon::parse($query->ETA)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })
            ->editColumn('ETD', function ($query) {
                if($query->ETD) {                    
                    return Carbon::parse($query->ETD)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })
            ->editColumn('ATD', function ($query) {
                if($query->ATD) {                    
                    return Carbon::parse($query->ATD)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })
            ->editColumn('ATA', function ($query) {
                if($query->ATA) {                    
                    return Carbon::parse($query->ATA)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })
            ->editColumn('bbb', function ($query) {
                if($query->ATA) {                    
                    return Carbon::parse($query->ATA)->format("Y/m/d");
                }else{
                    return 'Not';
                }
            })

           ->make(true);
    }

    //$this->DiasTranscurridos($value->ETA,$value->ETD)

    /**
     * Limpiar Espacios en Blanco.
     *
     */
    public function stringClear($string){
        
        $string = preg_replace("[\s+]",'', $string);

        return $string;
    }


    public function Descentidad($id){
        
        $entidad = Entidades::where("id","=",$id)->first();

        return $entidad;
    }

    public function searchCountry($id){
        $pais = Pais::where("id","=",$id)->first();
        return $pais['nombre'];
    }

    public function totalContainers($operacion_id){
        $containers = OperacionDetails::where([["operacion_id","=",$operacion_id],[
                function($query){
                    $query->where('activo', '=', 1)
                      ->orWhere('activo', '=', 2)
                      ->orWhere('activo', '=', 3);
                }
               ]])->get();
        if($containers == NULL){
            $container = 0;
        }else{
            $container = count($containers);
        }
        return $container;
    }

    public function DescPuerto($id, $type){

        if($type == 1){
            $result = Puerto::where('id', '=', $id)->first();
            $name = $result->puerto;
            
        }
        elseif($type == 2){
            $result = Aeropuerto::where('id', '=', $id)->first();
            $name = $result->nombre;
        }
        elseif($type == 3){
            $result = Estado::where('id', '=', $id)->first();
            $name = $result->nombre;
        }else{
            $name = ' ';
        }
      
        
 
        return $name;
    }

    public function DiasTranscurridos($inicio,$final){

        $inicio = Carbon::parse($inicio);
        $final = Carbon::parse($final);

        return $diasDiferencia = $final->diffInDays($inicio);
    }


    public function exportGenerate($custom, $dateStart, $dateFinish)
    {        
        $IdCurrentUser = Auth::id();     
        $data =  DB::table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA', 'P.vessel','P.free_demurrages_days_cust', 'P.POL','P.POD', 'P.tipo_operacion', 'P.operacion', 'P.PI', 'P.PO', 'P.final_dest','P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal', 'P.shipping_line', 'P.booking','P.origin_contry','P.destination_contry','P.booking_status','P.expense_bl','P.expense_contr','P.tarifa', 'C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([[function($query) use ($IdCurrentUser) {
                if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                    $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
                }else{
                    $query->where([['U.type','=',1]]);
                }
            }],['U.activo','=',1],['P.consignee','=',$custom],[
                function($query){
                    $query->where('A.activo', '=', 1)
                      ->orWhere('A.activo', '=', 2)
                      ->orWhere('A.activo', '=', 3);
                }
               ]])
            ->whereBetween('P.ETA', array($dateStart, $dateFinish))
            ->orderby('P.id', 'asc')->get()->toArray();
            //dd($data);
                
            $customer = Entidades::where("id","=",$custom)->first();

        
        //$data = Entidades::get()->toArray();
        
        return \Excel::create('ReportGeneral', function($excel) use ($data, $customer) {
            $excel->sheet('mySheet', function($sheet) use ($data, $customer)
            {

                $sheet->cell('A1', function($cell) {$cell->setValue('FOLIO');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('Shipper');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('# PO');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('# PI');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('# Booking');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('Origin Port');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('Origin Country');   });
                $sheet->cell('H1', function($cell) {$cell->setValue('Destination Port');   });
                $sheet->cell('I1', function($cell) {$cell->setValue('Destination Country');   });
                $sheet->cell('J1', function($cell) {$cell->setValue('Final Destination');   });
                $sheet->cell('K1', function($cell) {$cell->setValue('Vessel');   });
                $sheet->cell('L1', function($cell) {$cell->setValue('ETD');   });
                $sheet->cell('M1', function($cell) {$cell->setValue('ATD');   });
                $sheet->cell('N1', function($cell) {$cell->setValue('ETA');   });
                $sheet->cell('O1', function($cell) {$cell->setValue('ATA');   });
                $sheet->cell('P1', function($cell) {$cell->setValue('HBL');   });
                $sheet->cell('Q1', function($cell) {$cell->setValue('MBL');   });
                $sheet->cell('R1', function($cell) {$cell->setValue('# Container');   });
                $sheet->cell('S1', function($cell) {$cell->setValue('40H');   });
                $sheet->cell('T1', function($cell) {$cell->setValue('Status Booking');   });
                $sheet->cell('U1', function($cell) {$cell->setValue('Free Days');   });
                $sheet->cell('V1', function($cell) {$cell->setValue('comments');  });
                $sheet->cell('W1', function($cell) {$cell->setValue('Rate');  });
                $sheet->cell('X1', function($cell) {$cell->setValue('Expense BL');  });
                $sheet->cell('Y1', function($cell) {$cell->setValue('Expense Contr');  });
                $sheet->cell('Z1', function($cell) {$cell->setValue('Invoice Number');  });
                $sheet->cell('AA1', function($cell) {$cell->setValue('TELEX');   });
                $sheet->cell('AB1', function($cell) {$cell->setValue('TERMINAL');   });
                $sheet->cell('AC1', function($cell) {$cell->setValue('Shipping Lineeee');   });
                $sheet->cell('AD1', function($cell) {$cell->setValue('Transit Time');   });
               
                $sheet->setColumnFormat(array('A' => '@', 'B' => '@','C' => '@', 'D' => '@','E' => '@','F' => '@', 'G' => '@', 'H' => '@', 'I' => '@','J' => '@', 'K' => '@','L' => NumberFormat::FORMAT_DATE_DDMMYYYY,'M' => NumberFormat::FORMAT_DATE_DDMMYYYY, 'N' => NumberFormat::FORMAT_DATE_DDMMYYYY,'O' => NumberFormat::FORMAT_DATE_DDMMYYYY,'P' => '@','Q' => '@','R' => '@','S' => '@','T' => '@','U' => '@','V' => '@','W' => '@','X' => '@','Y' => '@','Z' => '@'));
                


                if (!empty($data)) {

                    
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $folio = 'SUMEX0'.$value->SUMEX;

                        if($value->telex) {                    
                            $telex = $value->telex;
                        }else{
                            $telex = 'Not';
                        }

                        //dd($folio);

                     $sheet->cell('A'.$i, $folio);
                     $sheet->cell('B'.$i, isset($value->shipper) ? $this->Descentidad($value->shipper)->nombre : NULL); 
                     $sheet->cell('C'.$i, isset($value->PO) ? json_decode($value->PO) : NULL);
                     $sheet->cell('D'.$i, isset($value->PI) ? json_decode($value->PI) : NULL);
                     $sheet->cell('E'.$i, isset($value->booking) ? json_decode($value->booking) : NULL);
                     $sheet->cell('F'.$i, isset($value->POL) ? $this->DescPuerto($value->POL, $value->tipo_operacion) : NULL );
                     $sheet->cell('G'.$i, isset($value->origin_contry) ? $this->searchCountry($value->origin_contry) : NULL);
                     $sheet->cell('H'.$i, isset($value->POD) ? $this->DescPuerto($value->POD, $value->tipo_operacion) : NULL );
                     $sheet->cell('I'.$i, isset($value->destination_contry) ? $this->searchCountry($value->destination_contry) : NULL);
                     $sheet->cell('J'.$i, isset($value->final_dest) ? $value->final_dest : NULL);
                     $sheet->cell('K'.$i, isset($value->vessel) ? $value->vessel : NULL);
                     $sheet->cell('L'.$i, isset($value->ETD) ? Date::stringToExcel($value->ETD) : NULL);
                     $sheet->cell('M'.$i, isset($value->ATD) ? Date::stringToExcel($value->ATD) : NULL);
                     $sheet->cell('N'.$i, isset($value->ETA) ? Date::stringToExcel($value->ETA) : NULL);
                     $sheet->cell('O'.$i, isset($value->ETA) ? Date::stringToExcel($value->ATA) : NULL);
                     $sheet->cell('P'.$i, isset($value->hbl) ? $value->hbl : NULL); 
                     $sheet->cell('Q'.$i, isset($value->mbl) ? $value->mbl : NULL); 
                     $sheet->cell('R'.$i, isset($value->container_number) ? $value->container_number : NULL);
                     $sheet->cell('S'.$i,isset($value->SUMEX) ? $this->totalContainers($value->SUMEX) : NULL);
                     $sheet->cell('T'.$i,  isset($value->booking_status) ? $value->booking_status : NULL);
                     $sheet->cell('U'.$i, isset($value->free_demurrages_days_cust) ? $value->free_demurrages_days_cust : NULL);
                     $sheet->cell('V'.$i, isset($value->comments) ? $value->comments : NULL);
                     $sheet->cell('W'.$i, isset($value->tarifa) ? $value->tarifa : NULL);
                     $sheet->cell('X'.$i, isset($value->expense_bl) ? $value->expense_bl : NULL);
                     $sheet->cell('Y'.$i, isset($value->expense_contr) ? $value->expense_contr : NULL);
                     $sheet->cell('Z'.$i, isset($value->factura) ? $value->factura : NULL);
                     $sheet->cell('AA'.$i, isset($value->telex) ? $value->telex : NULL);
                     $sheet->cell('AB'.$i, isset($value->terminal) ? $value->terminal : NULL);
                     $sheet->cell('AC'.$i, isset($value->shipping_line) ? $this->Descentidad($value->shipping_line)->nombre : NULL);
                     $sheet->cell('AD'.$i, $this->DiasTranscurridos($value->ETA,$value->ETD));
                    
                    }
                }
            });
        })->export('xlsx');
    }

    public function importOperation(){

        \Excel::create('Users', function($excel) {
 
            $users = Entidades::all();
         
            $excel->sheet('Users', function($sheet) use($users) {
         
            $sheet->fromArray($users);
         
        });
         
        })->export('xlsx');

    }

    public function transformDate($value, $format = 'Y/m/d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }


    public function exportUserData()
    {
        $data = Entidades::get()->toArray();
        
        return \Excel::create('laravelcode', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('ID');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('NOMBRE');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('RAZON SOCIAL');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('CLASIFICACION');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('NOMBRE CONTACTO');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('DIRECCION');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('TELEFONO');   });
                $sheet->cell('H1', function($cell) {$cell->setValue('EMAIL');   });
                $sheet->cell('I1', function($cell) {$cell->setValue('TAXID');   });
                $sheet->cell('J1', function($cell) {$cell->setValue('STATUS');   });
                $sheet->cell('K1', function($cell) {$cell->setValue('EMAIL CC');   });
                $sheet->cell('L1', function($cell) {$cell->setValue('FECHA');   });
                $sheet->cell('M1', function($cell) {$cell->setValue('FECHA AT');   });                
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'D' => '@',
                    'F' => '@',
                    'L' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                    'M' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                ));
                $sheet->setStyle('A1',array(
                    'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  12,
                        'bold'      =>  true
                    ),
                ));


                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $sheet->cell('A'.$i, $value['id']); 
                        $sheet->cell('B'.$i, $value['nombre']); 
                        $sheet->cell('C'.$i, $value['razonsocial']);
                        $sheet->cell('D'.$i, $value['id_clasificacion']); 
                        $sheet->cell('E'.$i, $value['nombrecontacto']); 
                        $sheet->cell('F'.$i, $value['direccion']); 
                        $sheet->cell('G'.$i, $value['telefono']); 
                        $sheet->cell('H'.$i, $value['email']);
                        $sheet->cell('I'.$i, $value['taxid']); 
                        $sheet->cell('J'.$i, $value['status']);
                        $sheet->cell('K'.$i, $value['email_cc']);
                        $sheet->cell('L'.$i, Date::stringToExcel($value['created_at']));
                        $sheet->cell('M'.$i, Date::stringToExcel($value['updated_at']));
                    }
                }
            });
        })->export('xlsx');
    }
    

    

    


}
