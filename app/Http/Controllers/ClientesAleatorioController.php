<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Americanista;
use App\Aleatorio;
use PdfReport;
use ExcelReport;
use Config;
use DB;
use Carbon;

class ClientesAleatorioController extends Controller
{
    private $Xexportar;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }
            // Registro de log.
            \Log::info('|::| Consulta de Clientes Aleatorio.', ['Usuario' => Auth::user()]); 
             return view('aleatorio.index'); 
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }           
    }


    public function getData(Request $request)
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }

            $this->validate($request, [
                'cantidad' => 'required|max:5',
            ],[
                'cantidad.required' => 'El campo Cantidad es requerido!',
                
            ]);

            // Registro de log.
            \Log::info('|::| Consulta Realizada de Clientes Aleatorio.', ['Usuario' => Auth::user()]); 
            
            $americanista = Americanista::take($request->cantidad)
            ->inRandomOrder()
            ->where('activo', '=', '1')
            ->where(function($query) {
                  $query->where('numeroAmericanista', 'like', 'AZ%')
                      ->orWhere('numeroAmericanista', 'like', 'AB%');
              })->get();
              
              $adds = array();
              foreach ($americanista as  $v) {
                $nombre = $v->nombre." ".$v->paterno." ".$v->materno;
                $adds[] = ['idamericanista' => $v->id,'nombre' => $nombre];
              }  
                DB::table('aleatorio')->insert($adds);

            return Datatables::of($americanista)->toJson();
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }     
    }

    public function generateReportActivos()
    {
      try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }

               $queryBuilder = Americanista::where('activo','=','1');
               
               
                $columns = [
                    'Número Americanista' => 'numeroAmericanista',
                    'Nombre' => 'nombre',
                    'Apellido Paterno' => 'paterno',
                    'Apellido Materno' => 'materno',
                ];
                $title = 'Reporte de Clientes Activos';
                $hoy = date("d/m/Y-H:i:s"); 
                $meta = [ // For displaying filters description on header
                    'Fecha Elaboración: ' => $hoy
                ];

                ExcelReport::of($title, $meta, $queryBuilder, $columns)
                 ->simple()
                 ->download('Reporte-'.$hoy);
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }                      

    }

}
