<?php

namespace App\Http\Controllers;

use App\Clasificacion;
use App\Entidades;
use App\Entidadesbanco;
use App\Banco;
use Auth;
use Charts;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Form;
use Collective\Html\FormFacade;
use Illuminate\Support\Str;


//Importing laravel-permission models

//Enables us to output flash messaging
use Spatie\Permission\Models\Role;
use Yajra\Datatables\Datatables;

class EntidadesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        
        // Registro de log.
        \Log::info('|::| Entidades Index.', ['Usuario' => Auth::user()]);

        //Get all users and pass it to the view
        $users = Entidades::all();

        //Get count users this month
        $usersThisMonth = Entidades::where(DB::raw('MONTH(created_at)'), '=', date('n'))->get();

        //Get last 6 users
        $lastUsers = $users->sortByDesc('created_at')->take(6);

        //Get Count users deleted
        $delUsers = $users->where('status', 0);

        $countUsers = DB::table('adm_entidad')
            ->select(DB::raw('MONTH(created_at) as month'), DB::raw('count(*) as records'))
            ->whereRaw('year(`created_at`) = ?', array(date('Y')))
            ->groupBy('month')
            ->get()->toArray();

        $serieChart = array();

        for ($i = 1; $i <= 12; $i++) {
            $searchR = array_search($i, array_column($countUsers, 'month'));

            if (is_int($searchR)) {
                array_push($serieChart, $countUsers[$searchR]->records);
            } else {
                array_push($serieChart, 0);
            }
        }

        $chartPline = Charts::multi('areaspline', 'highcharts')
            ->title('Analítica ' . date("Y"))
            ->colors(['#ff0000', '#ffffff'])
            ->elementLabel("Registros")
            ->labels(['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'])
            ->dataset('Usuarios', $serieChart);

        return view('entidades.index')->with('users', $users)->with('chartPline', $chartPline)->with('lastUsers', $lastUsers)->with('usersThisMonth', $usersThisMonth)->with('delUsers', $delUsers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario Entidad.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Clasificacion en la vista
        $clasificaciones = Clasificacion::where('activo', 1)->get()->pluck('descripcion', 'id')->toArray();
        $banco = Banco::where('activo', 1)->get()->pluck('descripcion', 'id')->toArray();
        
        return view('entidades.create', ['clasificaciones' => $clasificaciones,'banco' => $banco]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email fields
        $this->validate($request, [
            'nombre' => 'required|max:120|unique:adm_entidad',
            'razonsocial' => 'required|max:255',
            'id_clasificacion' => 'required|max:3',
            'email' => 'required|email|unique:users,email',
        ], [
            'nombre.required' => 'El campo Nombre(Alias) es requerido!',
            'razonsocial.required' => 'El campo Razón Social es requerido!',
            'id_clasificacion.required' => 'El campo Clasificación es requerido!',
            'email.required' => 'El campo Correo es requerido!',
            'email.email' => 'Incluye un signo "@" en la dirección de correo!',
            
        ]);
       
        
            $entidad = new Entidades();
            $entidad->nombre = isset($request['nombre']) ? Str::upper($request['nombre']) : '';
            $entidad->razonsocial = isset($request['razonsocial']) ? Str::upper($request['razonsocial']) : '';
            $entidad->id_clasificacion = json_encode($request['id_clasificacion']);
            $entidad->nombrecontacto = isset($request['nombrecontacto']) ? Str::upper($request['nombrecontacto']) : '';
            $entidad->direccion = isset($request['direccion']) ? Str::upper($request['direccion']) : '';
            $entidad->telefono = isset($request['telefono']) ? $request['telefono'] : '';
            $entidad->email = isset($request['email']) ? Str::lower($request['email']) : 'empresa@empresa.com';
            $entidad->taxid = isset($request['taxid']) ? Str::upper($request['taxid']) : '';
            $entidad->email_cc = isset($request['email_cc']) ? json_encode($request['email_cc']) : '';
            $entidad->save();
        
        
        if(!empty($request['cuentabancaria'])){
            $Ebanco = new Entidadesbanco();
            $Ebanco->id_entidad = $entidad->id;
            $Ebanco->cuentabancaria = $request['cuentabancaria'];
            $Ebanco->cinterbancaria = $request['cinterbancaria'];
            $Ebanco->id_banco = $request['id_banco'];
            $Ebanco->id_banco_inter = $request['id_banco_inter'];
            $Ebanco->swift = $request['swift'];
            $Ebanco->save();
        }    
    

        // Registro en log.
        \Log::info('|::| Recurso Entidad.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Entidad' => $entidad]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$entidad->nombre);

        //Redirect to the users.index view and display message
        return redirect()->route('entidades.index')
            ->with('flash_message',
                'Entidad agregada con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Registro en log.
        \Log::info('|::| UserController.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $entidades = Entidades::findOrFail(decrypt($id)); //Get user with specified id
        //Clasificacion en la vista
        $clasificaciones = Clasificacion::where('activo', 1)->get()->pluck('descripcion', 'id')->toArray();
        $clasificacion = json_decode($entidades->id_clasificacion);
        $banco = Banco::where('activo', 1)->get()->pluck('descripcion', 'id')->toArray();
        return view('entidades.show', compact('entidades','clasificaciones','banco','clasificacion')); //pass user and roles data to view

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| UserController.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $entidades = Entidades::findOrFail(decrypt($id)); //Get user with specified id
        //Clasificacion en la vista
        $clasificaciones = Clasificacion::where('activo', 1)->get()->pluck('descripcion', 'id')->toArray();

        $clasificacion = json_decode($entidades->id_clasificacion);

        $banco = Banco::where('activo', 1)->get()->pluck('descripcion', 'id')->toArray();
        return view('entidades.edit', compact('entidades','clasificaciones','banco','id', 'clasificacion')); //pass user and roles data to view
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $id = decrypt($request['idEntidad']);

        //Validate name, email fields
        $this->validate($request, [
            'nombre' => 'required|max:120',
            'razonsocial' => 'required|max:255',
            'id_clasificacion' => 'required|max:3',
            'email' => 'required|email',
        ], [
            'nombre.required' => 'El campo Nombre(Alias) es requerido!',
            'razonsocial.required' => 'El campo Razón Social es requerido!',
            'id_clasificacion.required' => 'El campo Clasificación es requerido!',
            'email.required' => 'El campo Correo es requerido!',
            'email.email' => 'Incluye un signo "@" en la dirección de correo!',
            
        ]);

        
        $entidad = Entidades::findOrFail($id);
        $entidad->update(
            array(
                'nombre' => isset($request['nombre']) ? Str::upper($request['nombre']) : '', 
                'razonsocial' => isset($request['razonsocial']) ? Str::upper($request['razonsocial']) : '',  
                'nombrecontacto' => isset($request['nombrecontacto']) ? Str::upper($request['nombrecontacto']) : '', 
                'direccion' => isset($request['direccion']) ? Str::upper($request['direccion']) : '', 
                'telefono' => isset($request['telefono']) ? $request['telefono'] : '', 
                'email' =>  Str::lower($request['email']), 
                'taxid' => isset($request['taxid']) ? Str::upper($request['taxid']) : '', 
                'id_clasificacion' => json_encode($request['id_clasificacion']),
                'email_cc' => isset($request['email_cc']) ? json_encode($request['email_cc']) : '',
            )
        );

        
        \LogActivity::addToLog('Se actualizo la entidad : ' . $entidad->id);
        
        return redirect()->route('entidades.index')
            ->with('flash_message',
                'Entidad '.$entidad->razonsocial.' editada exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        //Find a Entidad with a given id and delete
        $entidad = Entidades::findOrFail(decrypt($request->id_entidad));
        $entidad->status = "0";
        $entidad->save();

        // Registro en log.
        \Log::info('|::| Entidad.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'User' => $entidad]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$entidad->name);

        return redirect()->route('entidades.index')
            ->with('flash_message',
                'Entidad eliminada con éxito.');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        $users = Entidades::orderby('id','DESC')->get();

        return Datatables::of($users)
            ->addColumn('action', function ($users) {

                $result = '';

                if ($users->status == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('entidades.show', encrypt($users->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('entidades.edit', encrypt($users->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";

                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($users->id)."' data-title='".$users->id."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    public function getDataBanco()
    {
        $showDeleted  = Input::get('showDeleted');
        
        $detallesbancos = Entidadesbanco::where('id_entidad','=',$showDeleted )->get();


        return Datatables::of($detallesbancos)
            ->addColumn('action', function ($detallesbancos) {

                $result = '';

                if ($detallesbancos->activo == 1){

                    
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($detallesbancos->id)."' data-ident='".encrypt($detallesbancos->id_entidad)."' data-title='".$detallesbancos->id."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addColumn('banco', function ($query) {
                return $query->banco['descripcion'];
            })
            ->addColumn('bancointer', function ($query) {
                return $query->bancointer['descripcion'];
            })
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    
}
