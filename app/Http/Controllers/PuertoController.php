<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Puerto;
use App\Pais;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class PuertoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Puerto Index.', ['Usuario' => Auth::user()]);

         //Get all puertos and pass it to the view
         $puertos = Puerto::count();
         $puertosEnabled = Puerto::where('activo','=',1)->count();
         $puertosDisabled = Puerto::where('activo','=',0)->count();
         return view('puertos.index')->with('puertos', $puertos)->with('puertosEnabled', $puertosEnabled)->with('puertosDisabled', $puertosDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario puertos.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        
        $pais = Pais::orderBy('nombre', 'ASC')->get()->pluck('nombre', 'id')->toArray();

        //Get all municipios and show view
        return view('puertos.create', ['pais' => $pais]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate fields
        $this->validate($request, [
            'puerto' => 'required|max:50|unique:adm_puertos',
            'pais' => 'required|max:3',
        ],[
            'puerto.required' => 'El campo Nombre de Puerto es requerido!',
            'pais.required' => 'El campo País es requerido!',
            
        ]);

        $puerto = new Puerto();
        $puerto->puerto = Str::upper($request['puerto']);
        $puerto->pais = $request['pais'];
        $puerto->clave_puerto = Str::upper($request['clave_puerto']);
        $puerto->save();

        // Registro en log.
        \Log::info('|::| Recurso Puerto.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Puerto' => $puerto]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$puerto->puerto);

        return redirect()->route('puertos.index')
            ->with('flash_message',
             'Puertos '. $puerto->puerto.' agregada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        // Registro en log.
        \Log::info('|::| Puerto.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $puerto = Puerto::findOrFail(decrypt($id));
        return view('puertos.show', compact('puerto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| Puerto.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $puerto = Puerto::findOrFail(decrypt($id));
        $pais = Pais::orderBy('nombre', 'ASC')->get()->pluck('nombre', 'id')->toArray();
        return view('puertos.edit', compact('puerto','pais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        //Validate name, email and password fields
        $this->validate($request, [
            'puerto' => 'required|max:50',
            'pais' => 'required|max:3',
        ],[
            'puerto.required' => 'El campo Nombre de Puerto es requerido!',
            'pais.required' => 'El campo País es requerido!',
             
        ]);

        $puerto = Puerto::findOrFail($id);
        $input = array(
            'puerto' => Str::upper($request['puerto']),
            'pais' => $request['pais'],
            'clave_puerto' => Str::upper($request['clave_puerto'])
        );
        
        $request->all();
        $puerto->fill($input)->save();

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso actualizado: '.$puerto->puerto);
        
        return redirect()->route('puertos.index')
            ->with('flash_message',
             'Puerto '. $puerto->puerto.' actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $puerto = Puerto::findOrFail(decrypt($request->idpuerto));
        $puerto->activo = 0;
        $puerto->save();

        // Registro en log.
        \Log::info('|::| Puerto.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Contenedor' => $puerto]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$puerto->puerto);

        return redirect()->route('puertos.index')
            ->with('flash_message',
             'Puerto eliminado con éxito!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $showDeleted  = Input::get('showDeleted');

        $puertos = Puerto::where('activo','=',$showDeleted )->get();
        return Datatables::of($puertos)
            ->addColumn('action', function ($puertos) {

                $result = '';

                if ($puertos->activo == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('puertos.show', encrypt($puertos->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('puertos.edit', encrypt($puertos->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-puerto='".encrypt($puertos->id)."' data-title='".$puertos->puerto."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addIndexColumn()
            ->editColumn('puerto', '{{ $puerto }}')
            ->addColumn('paisDesc', function ($query) {
                return $query->Pais['nombre'];
            })
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    

}
