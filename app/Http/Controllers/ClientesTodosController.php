<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Americanista;
use PdfReport;
use ExcelReport;
use Config;
use DB;
use Response;

class ClientesTodosController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }
            // Registro de log.
            \Log::info('|::| Reportes Ticket Master.', ['Usuario' => Auth::user()]);
            $americanistas = Americanista::count(); 
            $americanistas = number_format($americanistas);
             return view('todos.index', compact('americanistas')); 
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }           
    }

    /**
     * Limpiar Espacios en Blanco.
     *
     */
    public function stringClear($string){
        
        $string = preg_replace("[\s+]",'', $string);

        return $string;
    }

    public function generateReportall()
    {
      try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }

               $queryBuilder = DB::select('select * from view_all ');

               $user = Auth::user()->admin;
            
                if($user == 1 ){
                    $titles = ['NUMERO AMERICANISTA', 'NOMBRE', 'A. PATERNO', 'A. MATERNO','CORREO', 'ESTADO'];
                }
                else{
                    $titles = ['NUMERO AMERICANISTA', 'NOMBRE', 'A. PATERNO', 'A. MATERNO','ESTADO'];
                } 
               $file = fopen('ticket-master.csv', 'w');
               
               fputcsv($file, $titles);
                foreach ($queryBuilder as $row) {

                        if($user == 1 ){
                            $array = array($this->stringClear($row->numeroAmericanista), $row->nombre, $row->paterno, $row->materno,$row->correo, $row->activo);
                        }
                        else{
                            $array = array($this->stringClear($row->numeroAmericanista), $row->nombre, $row->paterno, $row->materno, $row->activo);
                        }    

                        
                        $array = array_map("utf8_decode", $array);
                        fputcsv($file,$array);
                    }
                    fclose($file);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );

                    return Response::download('ticket-master.csv', 'ticket-master.csv', $headers);
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }                      

    }

}
