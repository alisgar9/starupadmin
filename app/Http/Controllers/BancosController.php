<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Banco;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Input;

class BancosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Bancos Index.', ['Usuario' => Auth::user()]);

         //Get all cargos and pass it to the view
         $bancos = Banco::count();
         $bancosEnabled = Banco::where('activo','=',1)->count();
         $bancosDisabled = Banco::where('activo','=',0)->count();
         return view('banco.index')->with('bancos', $bancos)->with('bancosEnabled', $bancosEnabled)->with('bancosDisabled', $bancosDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario banco.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Get all municipios and show view
        return view('banco.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'descripcion' => 'required|max:50|unique:adm_bancos',
        ],[
            'descripcion.required' => 'El campo Nombre es requerido!',
            
        ]);

        $banco = new Banco();
        $banco->descripcion = $request['descripcion'];
        $banco->save();

        // Registro en log.
        \Log::info('|::| Recurso Banco.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Banco' => $banco]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$banco->descripcion);

        return redirect()->route('bancos.index')
            ->with('flash_message',
             'Cargo '. $banco->descripcion.' agregada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        // Registro en log.
        \Log::info('|::| Banco.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $banco = Banco::findOrFail(decrypt($id));
        return view('banco.show', compact('banco'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| Banco.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $banco = Banco::findOrFail(decrypt($id));
        return view('banco.edit', compact('banco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'descripcion' => 'required|max:50',
        ],[
            'descripcion.required' => 'El campo Nombre es requerido!',
            
        ]);

        $banco = Banco::findOrFail($id);
        $input = $request->all();
        $banco->fill($input)->save();

        return redirect()->route('bancos.index')
            ->with('flash_message',
             'Banco '. $banco->descripcion.' actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $banco = Banco::findOrFail(decrypt($request->idbanco));
        $banco->activo = 0;
        $banco->save();

        // Registro en log.
        \Log::info('|::| Banco.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Banco' => $banco]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$banco->descripcion);

        return redirect()->route('bancos.index')
            ->with('flash_message',
             'Banco eliminado con éxito!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $showDeleted  = Input::get('showDeleted');

        $bancos = Banco::where('activo','=',$showDeleted )->get();
        return Datatables::of($bancos)
            ->addColumn('action', function ($bancos) {

                $result = '';

                if ($bancos->activo == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('bancos.show', encrypt($bancos->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('bancos.edit', encrypt($bancos->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($bancos->id)."' data-title='".$bancos->id."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addIndexColumn()
            ->editColumn('descripcion', '{{ $descripcion }}')
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    

}
