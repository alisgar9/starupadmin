<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Puerto;
use App\Pais;
use App\Entidades;
use App\Operacion;
use App\Liberacion;
use App\Incoterm;
use App\Contenedor;
use App\OperacionDetails;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;
use Session;
Use Redirect;
use DB;
use PdfReport;
use ExcelReport;
use Config;
use Response;





class ReportDemurrageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Demurrage(Request $request)
    {
        
        //catalogo Consignee
        $entidad = Entidades::where([['status', 1]])->orderBy('nombre')->pluck('nombre', 'id');

        
        return view('reportes.reportDemorrage')->with('entidad', $entidad);
        
    }


    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData(Request $request)
    {
       $IdCurrentUser = Auth::id();
       
      //$detallesoperacion = OperacionDetails::where([['activo','=',1]] )->get();

       $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
       ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.mbl','P.ETA','P.free_demurrages_days_cust','P.tipo_operacion','P.operacion', 'P.precio_demora', 'P.iva','C.descripcion AS tipo_operacion_desc')
       ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
       ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
       ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
       ->where([[function($query) use ($IdCurrentUser) {
           if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
               $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
           }else{
               $query->where([['U.type','=',1]]);
           }
       }],['U.activo','=',1],['P.consignee','=',$request->custom],['A.activo', '=', '2']])
       ->whereBetween('P.ETA', array($request->etas, $request->etast))
       ->get();

       return Datatables::of($detallesoperacion)
           
          
           ->addIndexColumn()
           ->addColumn('difdays', function ($query) {

                $fechaEmision = Carbon::parse($query->ETA);
                $dias_demora = $query->free_demurrages_days_cust - 1;               
                $newDateETA = $fechaEmision->addDays($dias_demora);
                $newDateETA2 = Carbon::parse($newDateETA);
                $rango = $this->isDateBetweenDates($query->empty_container, $query->ETA, $newDateETA2);

                return $rango;
            })
            ->addColumn('demoratotal', function ($query) {
                $total = $this->TotalDemoras($query->ETA, $query->free_demurrages_days_cust, $query->empty_container,$query->precio_demora, $query->iva);

                return $total;
                
            })
            ->addColumn('etaplus', function ($query) {

                
                $fechaEmision = Carbon::parse($query->ETA);
                $free_demurrages_days_cust = $query->free_demurrages_days_cust - 1;
                $newDateETA = $fechaEmision->addDays($free_demurrages_days_cust);
                $newDateETA2 = Carbon::parse($newDateETA);

                return Carbon::parse($newDateETA2)->format("d-M-y");

               
            })

           ->editColumn('empty_container', '{{ Carbon\Carbon::parse($empty_container)->format("d-M-y") }}')
           ->editColumn('ETA', '{{ Carbon\Carbon::parse($ETA)->format("d-M-y") }}')
           ->rawColumns(['estado', 'action'])
           ->make(true);
    }

    public function isDateBetweenDates($date, $startDate, $endDate) {
        
        $diasDiferencia = $endDate->diffInDays($date);

        if(($date >= $startDate) && ($date <= $endDate))
        {
              return 'Not Apply';
        }
         else
         {
              return $diasDiferencia;
         }
    }

    /**
     * Limpiar Espacios en Blanco.
     *
     */
    public function stringClear($string){
        
        $string = preg_replace("[\s+]",'', $string);

        return $string;
    }
    public function LastFreeDay($eta,$freeDay){

        $free_demurrages_days_cust = $freeDay - 1;           
        //$newDateETA = $eta->addDays($free_demurrages_days_cust);
        $newDateETA =  (new Carbon($eta))->addDays($free_demurrages_days_cust);
        $newDateETA_clear = Carbon::parse($newDateETA);
                    
        return $newDateETA_clear;

    }
    public function TotalDemoras($ETA, $FDC, $empty_container,$precio_demora, $iva){

        $DemmurrageDays = $this->searchDemurrage($ETA, $FDC, $empty_container);

                    if($DemmurrageDays != "Not Apply"){
                        $iva = $iva/100; 
                        $TotalIVA = $precio_demora * $iva;
                        $precioConIVA = $precio_demora + $TotalIVA;  
                        $DTotal = $precioConIVA * $DemmurrageDays;
                        //$DemmurrageTotal = $iva; 
                        $DemmurrageTotal = "$".number_format($DTotal,2)." USD";
                    }else{ $DemmurrageTotal = "0.00"; }
                    
                    return $DemmurrageTotal;
    }

    public function generateReportxls($custom, $dateStart, $dateFinish)
    {        
        $IdCurrentUser = Auth::id();     
        $data =  DB::table('adm_operaciones_contenedor as A')
        ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.id AS SUMEX','P.hbl','P.mbl','P.ETA','P.ETD','P.free_demurrages_days_cust','P.tipo_operacion','P.operacion', 'P.precio_demora','P.PO','P.PI', 'P.iva','C.descripcion AS tipo_operacion_desc')
        ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
        ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
        ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
        ->where([[function($query) use ($IdCurrentUser) {
            if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
            }else{
                $query->where([['U.type','=',1]]);
            }
        }],['U.activo','=',1],['P.consignee','=',$custom],['A.activo', '=', '2']])
        ->whereBetween('P.ETA', array($dateStart, $dateFinish))
        ->orderby('P.id', 'asc')->get()->toArray();
            
        $customer = Entidades::where("id","=",$custom)->first();

        //dd($data);
        //$data = Entidades::get()->toArray();
        
        return \Excel::create('ReportDemurrage', function($excel) use ($data, $customer) {
            $excel->sheet('mySheet', function($sheet) use ($data, $customer)
            {

                $sheet->cell('A1', function($cell) {$cell->setValue('FOLIO');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('HBL');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('MBL');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('ETD');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('ETA');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('Last Free Day');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('Free Days');   });
                $sheet->cell('H1', function($cell) {$cell->setValue('# Container');   });
                $sheet->cell('I1', function($cell) {$cell->setValue('Date Return Empty');   });
                $sheet->cell('J1', function($cell) {$cell->setValue('Demurrage Days');   });
                $sheet->cell('K1', function($cell) {$cell->setValue('Demurrage Total');   });
                $sheet->cell('L1', function($cell) {$cell->setValue('# PI');   });
                $sheet->cell('M1', function($cell) {$cell->setValue('# PO');   });
                $sheet->setColumnFormat(array('A' => '@', 'B' => '@','C' => '@', 'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,'F' => '@','G' => '@','H' => '@', 'I' => NumberFormat::FORMAT_DATE_DDMMYYYY,'J' => '@','K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2,
                    'L' => '@', 'M' => '@','N' => '@'));
                


                if (!empty($data)) {

                    
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $folio = 'SUMEX0'.$value->SUMEX;

                        

                        //dd($folio);
                        $sheet->cell('A'.$i, $folio);
                        $sheet->cell('B'.$i, $value->hbl); 
                        $sheet->cell('C'.$i, $value->mbl);
                        $sheet->cell('D'.$i, Date::stringToExcel($value->ETD)); 
                        $sheet->cell('E'.$i, Date::stringToExcel($value->ETA));
                        $sheet->cell('F'.$i, $this->LastFreeDay($value->ETA,$value->free_demurrages_days_cust));
                        $sheet->cell('G'.$i, $value->free_demurrages_days_cust);
                        $sheet->cell('H'.$i, $value->container_number);
                        $sheet->cell('I'.$i, $value->empty_container);
                        $sheet->cell('J'.$i, $this->searchDemurrage($value->ETA,$value->free_demurrages_days_cust,$value->empty_container));
                        $sheet->cell('K'.$i, $this->TotalDemoras($value->ETA, $value->free_demurrages_days_cust, $value->empty_container,$value->precio_demora,$value->iva));
                        $sheet->cell('L'.$i, isset($value->PI) ? json_decode($value->PI) : NULL); 
                        $sheet->cell('M'.$i, isset($value->PO) ? json_decode($value->PO) : NULL);
                    }
                }
            });
        })->export('xlsx');
    }

    public function generateReportxls1($custom, $dateStart, $dateFinish)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            return redirect()->back()->with('message', 'No puedes acceder directamente.');
        }

        try{
        
                $IdCurrentUser = Auth::id();       
                $queryBuilder =  DB::table('adm_operaciones_contenedor as A')
                    ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.mbl','P.ETA','P.free_demurrages_days_cust','P.tipo_operacion','P.operacion', 'P.precio_demora','P.PO','P.PI', 'C.descripcion AS tipo_operacion_desc')
                    ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
                    ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
                    ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
                    ->where([[function($query) use ($IdCurrentUser) {
                        if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                            $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
                        }else{
                            $query->where([['U.type','=',1]]);
                        }
                    }],['U.activo','=',1],['P.consignee','=',$custom],['A.activo', '=', '2']])
                    ->whereBetween('P.ETA', array($dateStart, $dateFinish));
                        
                    $customer = Entidades::where("id","=",$custom)->first();
                    $meta = [ // For displaying filters description on header
                            'Customer' => $customer->nombre,
                            'Date of Elaboration ' => Carbon::now()->format("d-M-y H:i"),
                        ];
                    
                // Manipulacion de datos y creacion de columnas con formato
                $columnsa = array();        
                $columnsa['HBL'] = 'hbl';
                $columnsa['MBL'] = 'mbl';
                $columnsa['ETA'] = 'ETA';
                $columnsa['Last Free Day'] = function($result) {

                    $fechaEmision = Carbon::parse($result->ETA); 
                    $free_demurrages_days_cust = $result->free_demurrages_days_cust - 1;           
                    $newDateETA = $fechaEmision->addDays($free_demurrages_days_cust);
                    $newDateETA_clear = Carbon::parse($newDateETA);
                    
                    return $newDateETA_clear;
                };

                $columnsa['Free Days'] = 'free_demurrages_days_cust';
                $columnsa['# Container'] = 'container_number';
                $columnsa['Date Return Empty'] = 'empty_container';
                $columnsa['Demurrage Days'] = function($result) {

                    $Days = $this->searchDemurrage($result->ETA, $result->free_demurrages_days_cust, $result->empty_container);
                    return $Days;
                };
                $columnsa['Demurrage Total'] =function($result) {

                    $DemmurrageDays = $this->searchDemurrage($result->ETA, $result->free_demurrages_days_cust, $result->empty_container);

                    if($DemmurrageDays != "Not Apply"){

                        $DTotal = $result->precio_demora * $DemmurrageDays;
                        $DemmurrageTotal = "$".number_format($DTotal,2)." USD";
                    }else{ $DemmurrageTotal = ""; }
                    
                    return $DemmurrageTotal;

                };
                $columnsa['PO'] = 'PO';
                $columnsa['PI'] = 'PI';

                        ExcelReport::of("Demorras", $meta, $queryBuilder, $columnsa)
                        ->editColumn('Fecha de Registro', [ // Change column class or manipulate its data for displaying to report
                            'displayAs' => function($result) {
                                return $result->created_at->format('d/M/Y H:i');
                            },
                            'class' => 'left'
                        ])
                        ->simple()
                        ->download(utf8_decode("ReportDemurrage").'-'.date("d/m/Y-H:i"));
            
                    }
                    catch(\Exception $e){
                        return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
                    }                                         

    }
    public function search()
    {
          
        // Registro de log.
        \Log::info('|::| Operaciones filtrado de contenedores.', ['Usuario' => Auth::user()]);

         //Get all Operaciones and pass it to the view

         $IdCurrentUser = Auth::id();
         $contenedores = DB::table('adm_operaciones_contenedor as A')
            ->select('A.operacion_id', 'A.container_number','A.container_seal','A.mark','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.tipo_operacion','P.operacion')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([['U.id_user', $IdCurrentUser],['U.activo','=',1]])
            ->count();

                    
         $contenedoreEnabled = DB::table('adm_operaciones_contenedor as A')
            ->select('A.operacion_id', 'A.container_number','A.container_seal','A.mark','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.tipo_operacion','P.operacion')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([['U.id_user', $IdCurrentUser],['A.activo','=',1],['U.activo','=',1]])
            ->count();
         $contenedoreDisabled = DB::table('adm_operaciones_contenedor as A')
            ->select('A.operacion_id', 'A.container_number','A.container_seal','A.mark','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.tipo_operacion','P.operacion')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([['U.id_user', $IdCurrentUser],['A.activo','=',0],['U.activo','=',1]])
            ->count();
        
         return view('operaciones.search')->with('contenedores', $contenedores)->with('contenedoreEnabled', $contenedoreEnabled)->with('contenedoreDisabled', $contenedoreDisabled);

    }

    public function searchDemurrage($ETA, $free_demurrages_days_cust, $empty_container){
            
            
        $fechaEmision = Carbon::parse($ETA);
        $free_demurrages_days_cust = $free_demurrages_days_cust - 1;             
        $newDateETA = $fechaEmision->addDays($free_demurrages_days_cust);
        $newDateETA2 = Carbon::parse($newDateETA);
        $DemmurrageDays = $this->isDateBetweenDates($empty_container, $ETA, $newDateETA2);

        return $DemmurrageDays;

    }

    


}
