<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Area;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;

class AreaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Áreas Index.', ['Usuario' => Auth::user()]);

         //Get all areas and pass it to the view
         $areas = Area::count();
         $areasEnabled = Area::where('estado','=',1)->count();
         $areasDisabled = Area::where('estado','=',0)->count();
         return view('areas.index')->with('areas', $areas)->with('areasEnabled', $areasEnabled)->with('areasDisabled', $areasDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario areas.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Get all municipios and show view
        return view('areas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'nombre' => 'required|max:50',
            'descripcion' => 'required|max:100'
        ],[
            'nombre.required' => 'El campo Nombre es requerido!',
            'descripcion.required' => 'El campo Descripción es requerido!',
            
        ]);

        $area = new Area();
        $area->nombre = $request['nombre'];
        $area->descripcion = $request['descripcion'];
        $area->save();

        // Registro en log.
        \Log::info('|::| Recurso Area.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Area' => $area]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$area->nombre);

        return redirect()->route('areas.index')
            ->with('flash_message',
             'Área '. $area->nombre.' agregada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        // Registro en log.
        \Log::info('|::| AreaController.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $area = Area::findOrFail(decrypt($id));
        return view('areas.show', compact('area'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| AreaController.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $area = Area::findOrFail(decrypt($id));
        return view('areas.edit', compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'nombre' => 'required|max:50',
            'descripcion' => 'required|max:100'
        ],[
            'nombre.required' => 'El campo Nombre es requerido!',
            'descripcion.required' => 'El campo Descripción es requerido!',
            
        ]);

        $area = Area::findOrFail($id);
        $input = $request->all();
        $area->fill($input)->save();

        return redirect()->route('areas.index')
            ->with('flash_message',
             'Área '. $area->nombre.' actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $area = Area::findOrFail(decrypt($request->id_area));
        $area->estado = 0;
        $area->save();

        // Registro en log.
        \Log::info('|::| Area.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Area' => $area]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$area->nombre);

        return redirect()->route('areas.index')
            ->with('flash_message',
             'Área eliminada con éxito!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $areas = Area::all(); 
        return Datatables::of($areas)
            ->addColumn('action', function ($area) {

                $result = '';

                if ($area->estado == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('areas.show', encrypt($area->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('areas.edit', encrypt($area->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($area->id)."' data-title='".$area->nombre."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }
}
