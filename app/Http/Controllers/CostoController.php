<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Costo;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;

class CostoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Centro de costo Index.', ['Usuario' => Auth::user()]);

        //Get all costos and pass it to the view
        $costos = Costo::count();
        $costosEnabled = Costo::where('estado','=',1)->count();
        $costosDisabled = Costo::where('estado','=',0)->count();
        return view('costos.index')->with('costos', $costos)->with('costosEnabled', $costosEnabled)->with('costosDisabled', $costosDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         // Registro de log.
         \Log::info('|::| Muestra el formulario costos.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Get all municipios and show view
        return view('costos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'centro' => 'required|max:200',
        ],[
            'centro.required' => 'El campo Nombre es requerido!',
        ]);

        $costo = new Costo();
        $costo->centro = $request['centro'];
        $costo->save();

        // Registro en log.
        \Log::info('|::| Recurso Costo.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Costo' => $costo]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$costo->centro);

        return redirect()->route('costos.index')
            ->with('flash_message',
             'Centro de Costos '. $costo->centro.' agregada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| CostoController.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $costo = Costo::findOrFail(decrypt($id));
        return view('costos.edit', compact('costo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'centro' => 'required|max:200',
        ],[
            'centro.required' => 'El campo Nombre es requerido!',
        ]);

        $costo = Costo::findOrFail(decrypt($id));
        $input = $request->all();
        $costo->fill($input)->save();

        return redirect()->route('costos.index')
            ->with('flash_message',
                'Centro de Costos '. $costo->centro.' actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $costo = Costo::findOrFail(decrypt($request->id_costo));
        $costo->estado = 0;
        $costo->save();

        // Registro en log.
        \Log::info('|::| Costo.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Costo' => $costo]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$costo->centro);

        return redirect()->route('costos.index')
            ->with('flash_message',
             'Centro de Costos eliminado con éxito!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $costos = Costo::all(); 
        return Datatables::of($costos)
            ->addColumn('action', function ($costo) {

                $result = '';

                if ($costo->estado == 1){

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= "<a href='".route('costos.edit', encrypt($costo->id))."' class='btn btn-sm btn-info'><i class='fa fa-edit'></i></a>";

                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($costo->id)."' data-title='".$costo->centro."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }
}
