<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Session;

class RoleController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'isAdmin']);//isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        // Registro de log.
        \Log::info('|::| Perfiles Index.', ['Usuario' => Auth::user()]);

        $roles = Role::all();//Get all roles
        return view('roles.index')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        // Registro de log.
        \Log::info('|::| Muestra el formulario roles.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]); 

        $permissions = Permission::all();//Get all permissions
        return view('roles.create', ['permissions'=>$permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
    //Validate name and permissions field
        $this->validate($request, [
            'name'=>'required|unique:roles|max:10',
            'permissions' =>'required',
        ],[
            'name.required'  => 'El campo de nombre es obligatorio.',
            'permissions.required'  => 'El campo de permisos es obligatorio.',
            'name.max'  => 'El nombre no puede tener más de 10 caracteres.'
        ]);

        $name = $request['name'];
        $role = new Role();
        $role->name = $name;

        $permissions = $request['permissions'];

        $role->save();
    //Looping thru selected permissions
        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); 
         //Fetch the newly created role and assign permission
            $role = Role::where('name', '=', $name)->first(); 
            $role->givePermissionTo($p);
        }

        // Registro en log.
        \Log::info('|::| Perfile agregado.', ['Usuario' => Auth::user(), 'Perfil' => $role]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Se agrego el perfil: '.$role->name);

        return redirect()->route('roles.index')
            ->with('flash_message',
             'Perfil '. $role->name.' agregado con éxito!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return redirect('roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        // Registro de log.
        \Log::info('|::| Perfiles Edit.', ['Usuario' => Auth::user()]);

        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return view('roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $role = Role::findOrFail($id);//Get role with the given id
    //Validate name and permission fields
        $this->validate($request, [
            'name'=>'required|max:10|unique:roles,name,'.$id,
            'permissions' =>'required',
        ],[
            'name.required'  => 'El campo de nombre es obligatorio.',
            'permissions.required'  => 'El campo de permisos es obligatorio.',
            'name.max'  => 'El nombre no puede tener más de 10 caracteres.'
        ]);

        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();

        $p_all = Permission::all();//Get all permissions

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
            $role->givePermissionTo($p);  //Assign permission to role
        }

        // Registro en log.
        \Log::info('|::| Perfile actualizado.', ['Usuario' => Auth::user(), 'Perfil' => $role]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Se actualizo el perfil: '.$role->name);

        return redirect()->route('roles.index')
            ->with('flash_message',
             'Perfil '. $role->name.' Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        // Registro en log.
        \Log::info('|::| Role.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Role' => $role]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$role->name);

        return redirect()->route('roles.index')
            ->with('flash_message',
             'Perfil eliminado!');
    }
}
