<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Americanista;
use PdfReport;
use ExcelReport;
use Config;
use DB;
use Response;

class ClientesAzulcremaController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }
            // Registro de log.
            \Log::info('|::| Reportes Clientes Azulcrema.', ['Usuario' => Auth::user()]);
            $americanistas = Americanista::where(function($query) {
                  $query->where('activo', '=', '1')
                      ->orWhere('activo', '=', '2');
              })
              ->where('numeroAmericanista', 'like', 'AZ%')
              ->count(); 
              $americanistas = number_format($americanistas);
             return view('azulcrema.index', compact('americanistas')); 
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }           
    }
    public function getData()
    {
        $americanista = Americanista::where((function($query) {
            $query->where('activo', '=', '1')
                ->where('numeroAmericanista', 'like', 'AZ%')
                ->orWhere('activo', '=', '2');
        })); 

        return Datatables::of($americanista)->toJson();
    }

    /**
     * Limpiar Espacios en Blanco.
     *
     */
    public function stringClear($string){
        
        $string = preg_replace("[\s+]",'', $string);

        return $string;
    }

    public function generateReportazulcrema()
    {
      try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }

               $queryBuilder = DB::select('select * from view_azulcremas ');

                $titles = ['NUMERO AMERICANISTA', 'NUMERO DE AFILIACION', 'NOMBRE', 'A. PATERNO', 'A. MATERNO', 'CORREO ELECTRONICO', 'TELEFONO', 'FECHA VIGENCIA','DIRECCION','COLONIA','MUNICIPIO','ESTADO','C.P.','STATUS'];
                $file = fopen('azulcremas.csv', 'w');
                
                fputcsv($file, $titles);
                foreach ($queryBuilder as $row) {
                  if($row->activo == 1 || $row->activo == 2 ){
                    $array = array($this->stringClear($row->numeroAmericanista), $row->noafiliacion,$row->nombre,$row->paterno,$row->materno,$row->correo,$row->telefono,$row->fechaVigencia,$row->direccion,$row->colonia,$row->municipio,$row->estado,$row->cp,$row->activo);
                    $array = array_map("utf8_decode", $array);
                    fputcsv($file, $array);                  
                  }
                    }
                    fclose($file);

                    $headers = array(
                        'Content-Type' => 'text/csv',
                    );

                    return Response::download('azulcremas.csv', 'azulcremas.csv', $headers);
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }                      

    }

}
