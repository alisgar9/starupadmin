<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use App\Puerto;
use App\Estado;
use App\Aeropuerto;
use App\Pais;
use App\Entidades;
use App\Operacion;
use App\Liberacion;
use App\Incoterm;
use App\Contenedor;
use App\OperacionDetails;
use App\OperacionUsers;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;
Use Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use DB;
use Response;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class OperacionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        
        // Registro de log.
        \Log::info('|::| Operaciones Index.', ['Usuario' => Auth::user()]);

         //Get all Operaciones and pass it to the view

         $IdCurrentUser = Auth::id();
         if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {

                $operaciones = DB::table('adm_operaciones AS O')
                ->leftjoin('adm_operaciones_usuarios AS U', 'O.id', '=', 'U.id_operacion')
                ->where(function($query) use ($IdCurrentUser) {
                    if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                        $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                    }
                })->count();

                $operacionesEnabled = DB::table('adm_operaciones AS O')
                ->leftjoin('adm_operaciones_usuarios AS U', 'O.id', '=', 'U.id_operacion')
                ->where([[function($query) use ($IdCurrentUser) {
                    if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                        $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                    }
                }],['O.activo','=',1]])->count();

                $operacionesDisabled = DB::table('adm_operaciones AS O')
                ->leftjoin('adm_operaciones_usuarios AS U', 'O.id', '=', 'U.id_operacion')
                ->where([[function($query) use ($IdCurrentUser) {
                    if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                        $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo',"=",1]]);
                    }
                }],['O.activo','=',0]])->orWhere('O.activo','=',3)->count();
        }else{
            $operaciones = DB::table('adm_operaciones')->count();
            $operacionesEnabled = DB::table('adm_operaciones')->where('activo','=', 1)->count();
            $operacionesDisabled = DB::table('adm_operaciones')->where('activo','=', 0)->count();
        }


            
         $operacionesShared = Operacion::where([['activo', 1],['share', '>',0]])->count();

         return view('operaciones.index')->with('operacionesShared', $operacionesShared)->with('operaciones', $operaciones)->with('operacionesEnabled', $operacionesEnabled)->with('operacionesDisabled', $operacionesDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd(Auth::user()->email);
        
        // Registro de log.
        \Log::info('|::| Muestra el formulario operacion.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //catalogo liberacion MBL
        $LMBL = Liberacion::where([['activo', 1],['tipo', 'MBL']])->orderBy('descripcion')->pluck('descripcion', 'id');
        //catalogo liberacion HBL
        $LHBL = Liberacion::where([['activo', 1],['tipo', 'HBL']])->orderBy('descripcion')->pluck('descripcion', 'id');
        //catalogo liberacion HBL
        $Incoterm = Incoterm::where('activo', 1)->orderBy('id')->pluck('nombre', 'id');

        $pais = Pais::orderBy('nombre', 'ASC')->get()->pluck('nombre', 'id')->toArray();
        //catalogo de operadores
        $operadores = User::whereHas(
            'roles', function($q){
                $q->where(function($query) {
                    $query->where('name', '=', 'Operador')
                        ->orWhere('name', '=', 'GnteOpe');
                });
            }
        )->orderBy('name')->pluck('name', 'id');
        unset($operadores[1],$operadores[Auth::id()]);

        //Get all operacion and show view
        return view('operaciones.create', ['LMBL' => $LMBL, 'LHBL' => $LHBL, 'Incoterm' => $Incoterm, 'operadores' =>$operadores, 'pais' => $pais]);
    }

    public function add_user($share, $id_operacion, $type, $activo){
        $user = new OperacionUsers();
        $user->id_user = $share;
        $user->id_operacion = $id_operacion;
        $user->activo = $activo;
        $user->type =  $type;
        $user->save();
    }

    public function disable_user_share($id_operacion){
        $user = DB::table('adm_operaciones_usuarios')
                ->where([['id_operacion',$id_operacion],['type', '0']])
                ->update(['activo' => 0]);
    }

    public function add_user_share($share, $id_operacion, $type, $activo){
                
                $user = DB::table('adm_operaciones_usuarios')
                ->where([['id_operacion',$id_operacion],['type', '0']])
                ->update(['activo' => 0]);  
                
            
                foreach ($share as $v) {
                
                    $user = OperacionUsers::firstOrNew(array('id_user'=> $v,'id_operacion'=> $id_operacion));
                    $user->id_user = $v;
                    $user->id_operacion = $id_operacion;
                    $user->activo = $activo;
                    $user->type =  $type;
                    $user->save();
                }
    }


    
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

       // dd($request);
        $this->validate($request, [
            'tipo_operacion' => 'required|max:1',
            'operacion' => 'required|max:1',
            'pre_operation' => 'max:1',
            'hbl' => 'required_if:pre_operation,==,0|max:50|unique:adm_operaciones',
            'operacion_detail' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,3|max:25',
            'ETD' => 'required',
            'ETA' => 'required',
            'ATD' => 'required',
            'ATA' => 'required',
            'vessel' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,3|max:50',
            'cy' => 'required|max:3',
            'cy_door' => 'required_if:cy,==,2|max:3',
            'POL' => 'required|max:3',
            'POD' => 'required|max:3',
            'free_demurrages_days_cust' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,2|max:2|not_in:0',
            'free_demurrages_days_sl' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,2|max:2|not_in:0',
            'final_dest' => 'max:80',
            'shipper' => 'required|max:5',
            'consignee' => 'required|max:5',
            'agente' => 'max:5',
            "mark"  => "max:50",
            'INCOTERM' => 'max:4',
            "precio_demora"  => "required|between:0,10",
            "iva"  => "required|between:0,10",
            
            
        ],[
            'operacion.required' => 'Requerid field Operation Type!',
            'tipo_operacion.required' => 'Requerid field Operation!',
            'hbl.required_if' => 'Requerid field House (HBL)!',
            'operacion_detail.required_if' => 'Requerid field Operation Profile!',
            'ETD.required' => 'Requerid field ETD!',
            'ETA.required' => 'Requerid field ETA!',
            'ATD.required' => 'Requerid field ATD!',
            'ATA.required' => 'Requerid field ATA!',
            'vessel.required_if' => 'Requerid field Vessel or Unit No.!',
            'POL.required' => 'Requerid field POL!',
            'POD.required' => 'Requerid field POD!',
            'free_demurrages_days_cust.required_if' => 'Requerid field Free dem days Customer!',
            'free_demurrages_days_sl.required_if' => 'Requerid field Free dem days SL!',
            'cy.required' => 'Requerid field Service Mode!',
            'cy_door.required_if'  => 'Requerid field CY Door Type!',
            'precio_demora.required_if'  => 'Requerid field Demurrage Price Type!',
            'iva.required_if'  => 'Requerid field IVA%!',
            'consignee.required' => 'Requerid field consignee!',
            'shipper.required' => 'Requerid field Shipper!'

        ]);

        $userId = Auth::id();

        $operacion = new Operacion();
        $operacion->tipo_operacion = $request['tipo_operacion'];
        $operacion->operacion_detail = $request['operacion_detail'];
        $operacion->hbl = isset($request['hbl']) ? Str::upper($request['hbl']) : NULL;
        $operacion->mbl = $request['mbl'];
        $operacion->house_release = isset($request['house_release']) ? $request['house_release'] : '0000-00-00';
        $operacion->master_release = isset($request['master_release']) ? $request['master_release'] : '0000-00-00';
        $operacion->ETD = $request['ETD'];
        $operacion->ETA = $request['ETA'];
        $operacion->ATD = $request['ATD'];
        $operacion->ATA = $request['ATA'];
        $operacion->vessel = isset($request['vessel']) ? Str::upper($request['vessel']) : NULL;
        $operacion->vogage = isset($request['vogage']) ? Str::upper($request['vogage']) : NULL;
        $operacion->POL = $request['POL'];
        $operacion->POD = $request['POD'];
        $operacion->final_dest = isset($request['final_dest']) ? Str::upper($request['final_dest']) : NULL;
        $operacion->shipper = $request['shipper'];
        $operacion->consignee = $request['consignee'];
        $operacion->agente = $request['agente'];
        $operacion->agente_envio = isset($request['agente_envio']) ? $request['agente_envio'] : NULL;
        $operacion->shipping_line = isset($request['shipping_line']) ? $request['shipping_line'] : NULL;
        $operacion->terminal = isset($request['terminal']) ? $request['terminal'] : NULL;
        $operacion->INCOTERM = $request['INCOTERM'];
        $operacion->commodity = isset($request['commodity']) ? Str::upper($request['commodity']) : NULL;
        $operacion->operacion = $request['operacion'];
        $operacion->share =  isset($request['share']) ? count($request['share']) : NULL;
        $operacion->cy = isset($request['cy']) ? $request['cy'] : NULL;
        $operacion->cy_door = isset($request['cy_door']) ? $request['cy_door'] : NULL;
        $operacion->precio_demora= $request['precio_demora'];
        $operacion->iva= $request['iva'];
        $operacion->free_demurrages_days_cust= $request['free_demurrages_days_cust'];
        $operacion->free_demurrages_days_sl= $request['free_demurrages_days_sl'];
        $operacion->PO = isset($request['PO']) ? json_encode(Str::upper($request['PO'])) : NULL;
        $operacion->PI = isset($request['PI']) ? json_encode(Str::upper($request['PI'])) : NULL;
        $operacion->factura = isset($request['factura']) ? $request['factura'] : NULL;
        $operacion->sc = isset($request['sc']) ? $request['sc'] : NULL;
        $operacion->pre_operation = isset($request['pre_operation']) ? $request['pre_operation'] : NULL;
        $operacion->comments = isset($request['comments']) ? $request['comments'] : NULL;
        $operacion->booking = isset($request['booking']) ? $request['booking'] : NULL;
        $operacion->origin_contry = isset($request['origin_contry']) ? $request['origin_contry'] : NULL;
        $operacion->destination_contry = isset($request['destination_contry']) ? $request['destination_contry'] : NULL;
        $operacion->booking_status = isset($request['booking_status']) ? $request['booking_status'] : NULL;
        $operacion->expense_bl = isset($request['expense_bl']) ? $request['expense_bl'] : NULL;
        $operacion->expense_contr = isset($request['expense_contr']) ? $request['expense_contr'] : NULL;
        $operacion->tarifa = isset($request['tarifa']) ? $request['tarifa'] : NULL;
        $operacion->save();

        if($operacion->pre_operation == 1)
        {
            $operationSend = Operacion::find($operacion->id);
            $operationSend->update([
                'hbl' => 'SUMEX0'.$operacion->id
            ]);
        }

        $this->add_user($userId, $operacion->id, 1, 1);

        if($request['share']){
            $this->add_user_share($request['share'], $operacion->id, 0, 1);
        }

        
        
        // Registro en log.
        \Log::info('|::| Recurso Operacion .store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Operacion' => $operacion]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$operacion->hbl);

        
        return redirect()->route('operaciones.dcontainer',[encrypt($operacion->id), encrypt($operacion->tipo_operacion), encrypt($operacion->operacion_detail)]);
    }

    public function createdetails($idoperation, $tipo_operacion, $operacion_detail)
    {
        //catalogo de Contenedores
        $contenedores = Contenedor::where('activo', 1)->orderBy('id')->pluck('descripcion', 'id');
        return view('operaciones.createdetail', ['operationID' => $idoperation, 'contenedores' => $contenedores, 'tipo_operacion' => $tipo_operacion, 'operacion_detail' =>$operacion_detail]);
    }

    public function addDetails(Request $request)
    {
        
        //Validate fields
        $this->validate($request, [
            'operationID' => 'required',
            'tipo_operacion' => 'required',
            "container_number.*"  => "required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,3|distinct",
            "chargable_weight.*"  => "required_if:tipo_operacion,==,2|between:0,10",
            "weight.*"  => "required|between:0,10",
            "volume.*"  => "required|between:0,10",
            
        ],[
            'operationID.required' => 'There was a problem with the operation, Edit Here',
            'tipo_operacion.required' => 'There was a problem with the operation, Edit Here',
            'container_number.required_if' => 'Requerid field #Ctnr  or #Box!',
            'chargable_weight.required_if' => 'Requerid field Chargable Weight!',
            'weight.required' => 'Requerid field Weight!',
            'volume.required' => 'Requerid field Volume!',
            'container_number.distinct' => 'Field #Ctnr  or #Box duplicate!',

        ]);


        $container_number = $request['container_number']; //Retrieving the container_number field
        $operacion_id = decrypt($request->operationID);

        //Checking if add details container
        if (isset($container_number)) {

            for($i=0;$i<=count($container_number)-1;$i++){
                    //echo $request['container_number'][$i]."<br>";

               
                $operacion_details = new OperacionDetails();
                $operacion_details->operacion_id = $operacion_id;
                $operacion_details->container_number = isset($request['container_number'][$i]) ? Str::upper($request['container_number'][$i]) : '';
                $operacion_details->container_seal = isset($request['container_seal'][$i]) ? Str::upper($request['container_seal'][$i]) : '';
                $operacion_details->mark = isset($request['mark'][$i]) ? Str::upper($request['mark'][$i]) : '';
                $operacion_details->container_type = isset($request['container_type'][$i]) ? Str::upper($request['container_type'][$i]) : '0';
                $operacion_details->weight = isset($request['weight'][$i]) ? Str::upper($request['weight'][$i]) : '0';
                $operacion_details->chargable_weight = isset($request['chargable_weight'][$i]) ? Str::upper($request['chargable_weight'][$i]) : '0';
                $operacion_details->volume = isset($request['volume'][$i]) ? Str::upper($request['volume'][$i]) : '0';
                $operacion_details->save();

            }
        }

        $operacion = Operacion::findOrFail($operacion_id);
        if($operacion->send_mail == 0 && $operacion->pre_operation == 0)
        {
            $mail_send = $this->sendMailCreate($operacion_id);
            
        }else{ $mail_send =''; }
        

        return redirect()->route('operaciones.index')
            ->with('flash_message',
             'Operation tracking number: SUMEX0'. $operacion_id.' added successfully, '.$mail_send);

        
        
    }

    public function storedetails(Request $request)
    {
        
        //Validate fields
        $this->validate($request, [
            'tipo_operacion' => 'required|max:1',
            'operacion' => 'required|max:1',
            'hbl' => 'required|max:50|unique:adm_operaciones',
            'house_release' => 'required_if:tipo_operacion,==,1|max:25',
            'operacion_detail' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,3|max:25',
            'ETD' => 'required',
            'ETA' => 'required',
            'vessel' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,3|max:50',
            'vogage' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,2|max:25',
            'cy' => 'required|max:3',
            'cy_door' => 'required_if:cy,==,2|max:3',
            'POL' => 'required|max:3',
            'POD' => 'required|max:3',
            'commodity' => 'required',
            'free_demurrages_days_cust' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,2|max:2|not_in:0',
            'free_demurrages_days_sl' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,2|max:2|not_in:0',
            'final_dest' => 'max:80',
            'shipper' => 'max:5',
            'consignee' => 'required|max:5',
            'agente' => 'max:5',
            'agente_envio'  => 'required',
            'shipping_line' => 'max:5',
            "mark"  => "max:50",
            'INCOTERM' => 'max:4',
            "container_number.*"  => "required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,3|distinct",
            "container_seal.*"  => "required_if:tipo_operacion,==,1",
            "container_type.*"  => "max:3",
            "chargable_weight.*"  => "required_if:tipo_operacion,==,2|between:0,10",
            "weight.*"  => "required|between:0,10",
            "volume.*"  => "required|between:0,10",
            "precio_demora"  => "required|between:0,10",
            "iva"  => "required|between:0,10",
            
        ],[
            'operacion.required' => 'Requerid field Operation Type!',
            'tipo_operacion.required' => 'Requerid field Operation!',
            'hbl.required' => 'Requerid field House (HBL)!',
            'house_release.required_if'  => 'Requerid field House Release Mode!',
            'operacion_detail.required_if' => 'Requerid field Operation Profile!',
            'ETD.required' => 'Requerid field ETD!',
            'ETA.required' => 'Requerid field ETA!',
            'vessel.required_if' => 'Requerid field Vessel or Unit No.!',
            'vogage.required_if' => 'Requerid field Voyage or Flight!',
            'POL.required' => 'Requerid field POL!',
            'POD.required' => 'Requerid field POD!',
            'commodity.required' => 'Requerid field Commodity!',
            'free_demurrages_days_cust.required_if' => 'Requerid field Free dem days Customer!',
            'free_demurrages_days_sl.required_if' => 'Requerid field Free dem days SL!',
            'container_number.required_if' => 'Requerid field #Ctnr  or #Box!',
            'container_seal.required_if' => 'Requerid field Ctnr Seal!',
            'chargable_weight.required_if' => 'Requerid field Chargable Weight!',
            'weight.required' => 'Requerid field Weight!',
            'volume.required' => 'Requerid field Volume!',
            'container_number.distinct' => 'Field #Ctnr  or #Box duplicate!',
            'cy.required' => 'Requerid field Container Yard!',
            'cy_door.required_if'  => 'Requerid field CY Door Type!',
            'precio_demora.required_if'  => 'Requerid field Demurrage Price Type!',
            'iva.required_if'  => 'Requerid field IVA%!',
            'consignee.required' => 'Requerid field consignee!',

        ]);


        $userId = Auth::id();

        $operacion = new Operacion();
        $operacion->tipo_operacion = $request['tipo_operacion'];
        $operacion->operacion_detail = $request['operacion_detail'];
        $operacion->hbl = Str::upper($request['hbl']);
        $operacion->mbl = $request['mbl'];
        $operacion->house_release = isset($request['house_release']) ? $request['house_release'] : '0000-00-00';
        $operacion->ETD = $request['ETD'];
        $operacion->ETA = $request['ETA'];
        $operacion->vessel = Str::upper($request['vessel']);
        $operacion->vogage = Str::upper($request['vogage']);
        $operacion->POL = $request['POL'];
        $operacion->POD = $request['POD'];
        $operacion->final_dest = Str::upper($request['final_dest']);
        $operacion->shipper = $request['shipper'];
        $operacion->consignee = $request['consignee'];
        $operacion->agente = $request['agente'];
        $operacion->agente_envio = $request['agente_envio'];
        $operacion->shipping_line = $request['shipping_line'];
        $operacion->INCOTERM = $request['INCOTERM'];
        $operacion->commodity = Str::upper($request['commodity']);
        $operacion->operacion = $request['operacion'];
        $operacion->share = $request['share'];
        $operacion->cy = isset($request['cy']) ? $request['cy'] : NULL;
        $operacion->cy_door = isset($request['cy_door']) ? $request['cy_door'] : NULL;
        $operacion->precio_demora= $request['precio_demora'];
        $operacion->iva = $request['iva'];
        $operacion->PO = isset($request['PO']) ? $request['PO'] : NULL;
        $operacion->PI = isset($request['PI']) ? $request['PI'] : NULL;
        $operacion->factura = isset($request['factura']) ? $request['factura'] : NULL;
        $operacion->save();

        $this->add_user($userId, $operacion->id, 1, 1);

        if($request['share'])
        {
            $this->add_user($request['share'], $operacion->id, 0, 1);
        }
        
        
        
        $container_number = $request['container_number']; //Retrieving the container_number field
        //Checking if add details container
        if (isset($container_number)) {

            for($i=0;$i<=count($container_number)-1;$i++){
                    //echo $request['container_number'][$i]."<br>";
                $operacion_details = new OperacionDetails();
                $operacion_details->operacion_id = $operacion->id;
                $operacion_details->container_number = isset($request['container_number'][$i]) ? Str::upper($request['container_number'][$i]) : '';
                $operacion_details->container_seal = isset($request['container_seal'][$i]) ? Str::upper($request['container_seal'][$i]) : '';
                $operacion_details->mark = isset($request['mark'][$i]) ? Str::upper($request['mark'][$i]) : '';
                $operacion_details->container_type = isset($request['container_type'][$i]) ? Str::upper($request['container_type'][$i]) : '0';
                $operacion_details->weight = isset($request['weight'][$i]) ? Str::upper($request['weight'][$i]) : '0';
                $operacion_details->chargable_weight = isset($request['chargable_weight'][$i]) ? Str::upper($request['chargable_weight'][$i]) : '0';
                $operacion_details->volume = isset($request['volume'][$i]) ? Str::upper($request['volume'][$i]) : '0';
                $operacion_details->save();

            }
        }


        // Registro en log.
        \Log::info('|::| Recurso Operacion .store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Operacion' => $operacion]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$operacion->hbl);

        return redirect()->route('operaciones.index')
            ->with('flash_message',
             'Operation: '. $operacion->hbl.' added successfully!');

        
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        
        // Registro en log.
        \Log::info('|::| Operacion.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $operacion = Operacion::findOrFail(decrypt($id));
        
        //catalogo liberacion MBL
        $LMBL = Liberacion::where([['activo', 1],['tipo', 'MBL']])->orderBy('descripcion')->pluck('descripcion', 'id');
        //catalogo liberacion HBL
        $LHBL = Liberacion::where([['activo', 1],['tipo', 'HBL']])->orderBy('descripcion')->pluck('descripcion', 'id');
        
        //catalogo liberacion HBL
        $Incoterm = Incoterm::where('activo', 1)->orderBy('id')->pluck('nombre', 'id');

        //catalogo de Contenedores
        $contenedores = Contenedor::where('activo', 1)->orderBy('descripcion')->pluck('descripcion', 'id');

        
        return view('operaciones.show', compact('operacion','LMBL', 'LHBL', 'Incoterm','contenedores'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // Registro en log.
        \Log::info('|::| Operacion.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $operacion = Operacion::findOrFail(decrypt($id));

        $shareuser = OperacionUsers::where([['id_operacion', decrypt($id)],['activo',1],['type',0]])->pluck('id_user');

        $typeShareuser = OperacionUsers::where([['id_operacion', decrypt($id)],['id_user',Auth::id()], ['activo',1]])->first();


        
        //catalogo liberacion MBL
        $LMBL = Liberacion::where([['activo', 1],['tipo', 'MBL']])->orderBy('descripcion')->pluck('descripcion', 'id');
        //catalogo liberacion HBL
        $LHBL = Liberacion::where([['activo', 1],['tipo', 'HBL']])->orderBy('descripcion')->pluck('descripcion', 'id');
        //catalogo liberacion HBL
        $Incoterm = Incoterm::where('activo', 1)->orderBy('id')->pluck('nombre', 'id');
        $pais = Pais::orderBy('nombre', 'ASC')->get()->pluck('nombre', 'id')->toArray();
        //catalogo de operadores
        $operadores = User::whereHas(
            'roles', function($q){
                $q->where(function($query) {
                    $query->where('name', '=', 'Operador')
                        ->orWhere('name', '=', 'GnteOpe');
                });
            }
        )->orderBy('name')->pluck('name', 'id');
        unset($operadores[1],$operadores[Auth::id()]);


        if($operacion->tipo_operacion == 1){
            $Pol_Desc =isset($operacion->POLdesc->puerto)?$operacion->POLdesc->puerto:null;
            $Pod_Desc =isset($operacion->PODdesc->puerto)?$operacion->PODdesc->puerto:null;
        }else if($operacion->tipo_operacion == 2){ 
            $Pol_Desc = isset($operacion->POLdescAir->nombre)?$operacion->POLdescAir->nombre:null;
            $Pod_Desc = isset($operacion->PODdescAir->nombre)?$operacion->PODdescAir->nombre:null;
        }else if ($operacion->tipo_operacion == 3) {
            $Pol_Desc = isset($operacion->POLdescLine->nombre)?$operacion->POLdescLine->nombre:null;
            $Pod_Desc = isset($operacion->PODdescLine->nombre)?$operacion->PODdescLine->nombre:null;
        }else{ 
            $Pol_Desc = "No Apply";
        } 

        //catalogo de Contenedores
        $contenedores = Contenedor::where('activo', 1)->orderBy('id')->pluck('descripcion', 'id');
        
        if($typeShareuser)
        {
            return view('operaciones.edit', compact('typeShareuser','operacion', 'shareuser','LMBL', 'LHBL', 'Incoterm','contenedores', 'operadores', 'Pol_Desc', 'Pod_Desc','pais'));
        }else{
            return redirect()->route('traking')
            ->with('Error',
             'No permissions to manage this operation!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate fields
        $this->validate($request, [
            'operacion' => 'required|max:1',
            'ETD' => 'required',
            'ETA' => 'required',
            'ATD' => 'required',
            'ATA' => 'required',
            'POL' => 'required|max:3',
            'POD' => 'required|max:3',
            'free_demurrages_days_cust' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,2|max:2|not_in:0',
            'free_demurrages_days_sl' => 'required_if:tipo_operacion,==,1|required_if:tipo_operacion,==,2|max:2|not_in:0',
            'final_dest' => 'max:80',
            'shipper' => 'required|max:5',
            'consignee' => 'required|max:5',
            'agente' => 'max:5',
            "mark"  => "max:50",
            'INCOTERM' => 'max:4',
            'cy' => 'required|max:3',
            'cy_door' => 'required_if:cy,==,2|max:3',
            "precio_demora"  => "required|between:0,10",
            "iva"  => "required|between:0,10",
            
        ],[
            'operacion.required' => 'Requerid field Operation Type!',
            'ETD.required' => 'Requerid field ETD!',
            'ETA.required' => 'Requerid field ETA!',
            'ATD.required' => 'Requerid field ATD!',
            'ATA.required' => 'Requerid field ATA!',
            'POL.required' => 'Requerid field POL!',
            'POD.required' => 'Requerid field POD!',
            'free_demurrages_days_cust.required_if' => 'Requerid field Free dem days Customer!',
            'free_demurrages_days_sl.required_if' => 'Requerid field Free dem days SL!',
            'cy.required' => 'Requerid field Container Yard!',
            'cy_door.required_if'  => 'Requerid field CY Door Type!',
            'precio_demora.required_if'  => 'Requerid field Demurrage Price Type!',
            'iva.required_if'  => 'Requerid field IVA%!',
            'consignee.required' => 'Requerid field consignee!',
            'shipper.required' => 'Requerid field Shipper!',
            
        ]);
        
        $detalle = Operacion::findOrFail($id);
        $actualConsignee = $detalle->consignee;
        $actualETA = Carbon::parse($detalle->ETA)->format("Y-m-d");
        $newETA = Carbon::parse($request['ETA'])->format("Y-m-d");

        $data = array(
            'share' => isset($request['share']) ? count($request['share']) : NULL,
            'mbl' => isset($request['mbl']) ? Str::upper($request['mbl']) : '',
            'house_release' => isset($request['house_release']) ? $request['house_release'] : null,
            'master_release' => isset($request['master_release']) ? $request['master_release'] : null,
            'ETD' => isset($request['ETD']) ? $request['ETD'] : '',
            'ETA' => isset($request['ETA']) ? $request['ETA'] : '',
            'ATD' => isset($request['ATD']) ? $request['ATD'] : '',
            'ATA' => isset($request['ATA']) ? $request['ATA'] : '',
            'vessel' => isset($request['vessel']) ? Str::upper($request['vessel']) : '',
            'vogage' =>  isset($request['vogage']) ? Str::upper($request['vogage']) : '',
            'POL' => isset($request['POL']) ? $request['POL'] : '',
            'POD' => isset($request['POD']) ? $request['POD'] : '',
            'final_dest' => isset($request['final_dest']) ? Str::upper($request['final_dest']) : '',
            'shipper' => isset($request['shipper']) ? $request['shipper'] : ' ',
            'consignee' => isset($request['consignee']) ? $request['consignee'] : ' ',
            'agente' => isset($request['agente']) ? $request['agente'] : ' ',
            'shipping_line' => isset($request['shipping_line']) ? $request['shipping_line'] : ' ',
            'terminal' => isset($request['terminal']) ? $request['terminal'] : ' ',
            'INCOTERM' => isset($request['INCOTERM']) ? $request['INCOTERM'] : '',
            'commodity' => isset($request['commodity']) ? Str::upper($request['commodity']) : '',
            'free_demurrages_days_cust' => $request['free_demurrages_days_cust'],
            'free_demurrages_days_sl' => $request['free_demurrages_days_sl'],
            'house_release_date'=> isset($request['house_release_date']) ? $request['house_release_date'] : null,
            'master_release_date'=> isset($request['master_release_date']) ? $request['master_release_date'] : null,
            'operacion' => isset($request['operacion']) ? $request['operacion'] : null,
            'agente_envio' => isset($request['agente_envio']) ? Str::upper($request['agente_envio']) : '',
            'cy' => isset($request['cy']) ? $request['cy'] : NULL,
            'cy_door' => isset($request['cy_door']) ? $request['cy_door'] : NULL,
            'PO' => isset($request['PO']) ? json_encode(Str::upper($request['PO'])) : NULL,
            'PI' => isset($request['PI']) ? json_encode(Str::upper($request['PI'])) : NULL,
            'factura' => isset($request['factura']) ? $request['factura'] : NULL,
            'precio_demora' => $request['precio_demora'],
            'iva' => $request['iva'],
            'sc' => isset($request['sc']) ? $request['sc'] : NULL,
            'comments' => isset($request['comments']) ? $request['comments'] : NULL,
            'booking' => isset($request['booking']) ? $request['booking'] : NULL,
            'origin_contry' => isset($request['origin_contry']) ? $request['origin_contry'] : NULL,
            'destination_contry' => isset($request['destination_contry']) ? $request['destination_contry'] : NULL,
            'booking_status' => isset($request['booking_status']) ? $request['booking_status'] : NULL,
            'expense_bl' => isset($request['expense_bl']) ? $request['expense_bl'] : NULL,
            'expense_contr' => isset($request['expense_contr']) ? $request['expense_contr'] : NULL,
            'tarifa' => isset($request['tarifa']) ? $request['tarifa'] : NULL,
        );
        $update = Operacion::where('id','=',$id);
        $update->update($data);

        
/*
        if ( $actualETA != $newETA || $actualConsignee != $request['consignee']){
            $message = $this->sendMailNotification ($id);
        }else{ $message =''; }*/
        

        if($request->input('share'))
        {
            if(count($request['share']) > 0){ $this->add_user_share($request['share'], $id, 0, 1); }
            else{ $this->disable_user_share($id); }
        }


        
        $operacion = Operacion::findOrFail($id);
        /*if($operacion->send_mail == 0 && $operacion->pre_operation == 0)
        {
            $mail_send = $this->sendMailCreate($id);
            
        }else{ $mail_send =''; }*/

           


        // Registro en Bitácora.
        \LogActivity::addToLog('Updated Operation: '.$detalle->hbl);

        //return Redirect::back()->with('success', 'Updated '.$detalle->hbl.' Operation'.$message.$mail_send);
        return redirect()->route('operaciones.edit', encrypt($id))->with('success', 'Updated '.$detalle->hbl.' Operation');
        //return redirect()->route('operaciones.edit', encrypt($id))->with('success', 'Updated '.$detalle->hbl.' Operation'.$message.$mail_send);
        //return redirect()->route('operaciones.index')->with('flash_message','Operacion '. $detalle->hbl.' actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
         /** Status(activo) de Contenedor son:
         * 0 Eliminado
         * 1 Activo
         * 2 Contenedor retornado
         * 3 Cancelado
        **/

        //Validate data fields
        $this->validate($request, [
            'status' => 'required',
            "idoperacion"  => "required"
            
        ],[
            'idoperacion.required' => 'Error try again',
            'status.required' => 'Select Cancel or Remove'
        ]);
        
        $IdCurrentUser = Auth::id();
        $operacionUser = OperacionUsers::where([['id_operacion', decrypt($request->idoperacion)],['id_user', $IdCurrentUser],['type', 1]])->count();

        if($operacionUser == 1){


        $operacion = Operacion::findOrFail(decrypt($request->idoperacion));
        $operacion->hbl = 'D'.date("Ymds").$operacion->hbl;
        $operacion->activo = decrypt($request->status);
        $operacion->comments = $request->comments;
        $operacion->save();
        /** Desactivamos  los contenedores de la operacion eliminada */
        $operacionConteiner = OperacionDetails::where('operacion_id', '=', decrypt($request->idoperacion))->update(array('activo' => 0));

            // Registro en log.
        \Log::info('|::| Operacion.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Operacion' => $operacion]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$operacion->hbl);

        return redirect()->route('operaciones.index')
            ->with('flash_message',
             'Operation successfully removed!');
        
        }else{
            return redirect()->route('operaciones.index')
            ->with('Error',
             'No permissions  to remove!');  
        }

        
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $showDeleted  = Input::get('showDeleted');
        $IdCurrentUser = Auth::id();

        $operaciones = DB::table('adm_operaciones_usuarios AS U')
        ->Join('adm_operaciones AS O', 'O.id', '=', 'U.id_operacion')
        ->where([[function($query) use ($IdCurrentUser) {
            if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
            }
        }],[function($query) use ($showDeleted) {
           if($showDeleted == 0) {    
             $query->where('O.activo','=',$showDeleted)->orWhere('O.activo', '=', 3);
            }else{
             $query->where('O.activo','=',$showDeleted);  
            }
        }]])->groupBy('U.id_operacion')->orderBy('O.id', 'DESC')->paginate(500);

        
        
        return Datatables::of($operaciones)
            ->addColumn('action', function ($operaciones) {

                $result = '';

                if ($operaciones->activo == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('operaciones.show', encrypt($operaciones->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if($this->uShare($operaciones->id) != 0)
                    {
                        if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('operaciones.edit', encrypt($operaciones->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
                        if($operaciones->pre_operation == 0 && $operaciones->telex == "Not" || $operaciones->telex == NULL){
                            $result .= " <button type='submit' class='btn btn-sm bg-navy' data-toggle='modal' data-operacionid='".encrypt($operaciones->id)."' data-hbl='".$operaciones->hbl."' data-target='#confirmTelexComment'><b>T</b></button>";
                        }
                        if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-operacion='".encrypt($operaciones->id)."' data-title='".$operaciones->hbl."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";

                        if($operaciones->pre_operation == 1)
                        $result .= " <button title='Edit HBL' type='submit' class='btn btn-sm btn-primary' data-toggle='modal' data-operacion='".encrypt($operaciones->id)."' data-title='Edit HBL ".$operaciones->hbl."' data-target='#confirmHBLEdit'><i class='fa fa-header' aria-hidden='true'></i></button>";
                
                    }
                }
                if($operaciones->activo == 0 || $operaciones->activo == 3 && $operaciones->pre_operation == 1){
                        $result .= " <button type='submit' title='Restore pre Operation' class='btn btn-sm btn-success' data-toggle='modal' data-operacionid='".encrypt($operaciones->id)."' data-title='Restore pre Operation ".$operaciones->hbl."' data-target='#confirmRestore'><i class='fa fa-window-restore' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addIndexColumn()
            ->addColumn('folio', function ($query) {
                return 'SUMEX0'.$query->id;
            })
            ->editColumn('tipo_operacion', function ($query) {

                if( $query->tipo_operacion == 1)
                {
                    $operacion = "Ocean";

                }else if( $query->tipo_operacion == 2)
                {
                    $operacion = "Air";                    
                }else if( $query->tipo_operacion == 3)
                {
                    $operacion = "In land"; 
                }else{
                    $operacion = "Not Apply";
                }
                return $operacion;
            })
            ->editColumn('operacion', function ($query) {

                if( $query->operacion == 1)
                {
                    $operacion = "Import";

                }else if( $query->operacion == 2)
                {
                    $operacion = "Export";                    
                }else if( $query->operacion == 3)
                {
                    $operacion = "Domestic"; 
                }else{
                    $operacion = "Not Apply";
                }
                return $operacion;
            })
            ->editColumn('telex', function ($query) {

                if( $query->telex == NULL)
                {
                    $telex = "Not";

                }else{
                    $telex = $query->telex;
                }
                return $telex;
            })
            ->editColumn('ETD', '{{ Carbon\Carbon::parse($ETD)->format("d-M-y") }}')
            ->editColumn('ETA', '{{ Carbon\Carbon::parse($ETA)->format("d-M-y") }}')
            ->editColumn('id', '{{$id}}')
            ->addColumn('creator', function ($query) {

                return $this->usersShare($query->id);

            })
            ->addColumn('consignee_name', function ($query) {

                return $this->conssigne_name($query->consignee);

            })
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y") }}')
            ->make(true);
    }

    

    public function puertoSearch(Request $request)
    {
        
        $type = $request->type ?: '';
        $search = trim($request->term) ?: '';

        if($type == 1){
            $result = Puerto::where([['puerto', 'LIKE', '%'. $search. '%'], ['activo', 1]])->get();
        }
        elseif($type == 2){
            $result = Aeropuerto::select('id', 'nombre AS puerto', 'pais', 'clave', 'activo')
            ->where([['nombre', 'LIKE', '%'. $search. '%'], ['activo', 1]])->get();
        }
        elseif($type == 3){
            $result = Estado::select('id', 'nombre AS puerto', 'pais', 'clave', 'activo')
            ->where([['nombre', 'LIKE', '%'. $search. '%'], ['activo', 1]])->get();
        }else{
            $result =  'Error, try again, select the operation';
        }
      
        
 
        return response()->json($result);
        
    }

    public function entidadSearch(Request $request)
    {
        
       
        $search = trim($request->term) ?: '';
        $tipo = trim($request->tipo) ?: '';

        $tip = trim($request->tipo) ?: '';

       

        if($tipo == "shipper"){
            $tipo = 2;
        }else if($tipo == "consignee"){
            $tipo = 4;
        }else if($tipo == "agente"){
            $tipo = 5;
        }else if($tipo == "shipping_line"){
            $tipo = 1;
        }else if($tipo == "air_line"){
            $tipo = 8;
        }else if($tipo == "carrier_line"){
            $tipo = 9;
        }else if($tipo == "agente_envio"){
            $tipo = 10;
        }else{
            $tipo = 2; 
        }
        //dd($tipo);

        $result = Entidades::where([['nombre', 'LIKE', '%'.$search.'%'], ['status', '=', '1'], [
            function($query) use ($tipo) {
                if ($tipo != 2 || $tipo != 4){ 
                    $query->where('id_clasificacion', 'LIKE', '["'.$tipo.'"]');
                }
            }]])->get();    
        
 
        return response()->json($result);
        
    }
    

    public function files(Request $request)
    {
          //Validate fields
          $this->validate($request, [
            'file_hbl' => 'mimetypes:application/pdf|nullable|max:2048',
            'file_mbl' => 'mimetypes:application/pdf|nullable|max:2048',
            
            ],[
                'file_hbl.mimetypes' => 'Requerid Only files with PDF!',
                'file_hbl.size' => 'size!',
                'file_hbl.max' => 'sssss!',
                'file_mbl.mimetypes'  => 'Requerid Only files with PDF!',
            ]);

            
        if($request->hasFile('file_hbl') != '' || $request->hasFile('file_mbl') != ''){
            $operacion = Operacion::findOrFail(decrypt($request->operation));
        
            if ($request->hasFile('file_hbl')) {
                
                $hbl = $request->file('file_hbl')->store('hbl');
                //Borramos aarchivo eistente del disco
                if($operacion->file_hbl){
                    Storage::delete('app/' . $operacion->file_hbl);
                }

            }

            if($request->hasFile('file_mbl')) {
                
                $mbl = $request->file('file_mbl')->store('mbl');

                //Borramos aarchivo eistente del disco
                if($operacion->file_mbl){
                    Storage::delete('app/' . $operacion->file_mbl);
                }


            }

            
            
            
            $operacion->file_hbl = isset($hbl) ? $hbl : null;
            $operacion->file_mbl = isset($mbl) ? $mbl : null;
            $operacion->save();

            // Registro en Bitácora.
            \LogActivity::addToLog('Updated Files: '.$request->hbl);

            return Redirect::back()->with('success', 'Updated Files '.$request->hbl.' Operation!');

        }else{

            return Redirect::back()->with('Error', 'sin cambios '.$request->hbl.' Operation!');
        } 
    }

    public function viewFile($url){
        $filename = decrypt($url);
        return response()->download(storage_path('app/' . $filename));
      
    }


    public function usersShare($id){

        $operaciones = DB::table('adm_operaciones_usuarios AS U')
        ->Join('users AS Us', 'U.id_user', '=', 'Us.id')
        ->where([['U.id_operacion',$id],['U.activo',1]])->get();

            $users = "";
            foreach($operaciones as $v => $l){
                $name = $l->name.', ';
                $users.= $name;
            }
                return $users;
    }

    public function conssigne_name($id){

        $operacion = Entidades::findOrFail($id);

        return $operacion->nombre;
    }

    public function uShare($id){

        $operaciones = DB::table('adm_operaciones_usuarios AS U')
        ->Join('users AS Us', 'U.id_user', '=', 'Us.id')
        ->where([['U.id_operacion',$id],['U.id_user',Auth::id()],['U.activo',1]])->count();
        
        return $operaciones;

    }
    public function isDateBetweenDates($date, $startDate, $endDate) {
        echo 'date '.$date.'<br>';
        echo 'ENDDATE '.$endDate.'<br>';
        echo 'START '.$startDate.'<br>';
        //$diasDiferencia = $date->diffInDays($endDate);

        //$date = Carbon::parse('2016-09-17 11:00:00');
$now = Carbon::now();

$diff = $date->diffInDays($now);

        

        
              return $diasDiferencia;
         
    }
    public function searchDemurrage($ETA, $free_demurrages_days_cust, $empty_container){
            
            
        $fechaEmision = Carbon::parse($ETA);
        $free_demurrages_days_cust = $free_demurrages_days_cust - 1;             
        $newDateETA = $fechaEmision->addDays($free_demurrages_days_cust);
        $newDateETA2 = Carbon::parse($newDateETA);
//$DemmurrageDays = $this->isDateBetweenDates($empty_container, $ETA, $newDateETA2);
        echo $free_demurrages_days_cust;
        echo " ETA ".$fechaEmision." dias libres ".$newDateETA2;

    }

    public function diasLibres($eta, $daysFree){
        $dias_demora = $daysFree - 1;
        $data =  (new Carbon($eta))->addDays($dias_demora);
        $ldate = date('Y-m-d H:i:s');
        $diasDiferencia = $data->diffInDays($ldate);
        if($diasDiferencia >= $daysFree){
            $daysDiferencia = $daysFree;
        }else{
            $daysDiferencia = $diasDiferencia;
        }

        return $daysDiferencia;
    }

    public function allContainers($idOperacion){
        
        $data  = OperacionDetails::where([["operacion_id", "=", $idOperacion],['activo', '=', 1]])
        ->get();
        $datos = array();

        $arrNewSku = array();
        $incI = 0;
        foreach($data AS $arrKey => $arrData){
            $arrNewSku[$incI]['id'] = encrypt($arrData['id']);
            $arrNewSku[$incI]['container_number'] = $arrData['container_number'];
            $incI++;
        }

        $encodedSku = json_encode($arrNewSku);
        return $encodedSku;

        

    }

    public function getContainer()
    {
        
       
        $IdCurrentUser = Auth::id();
        $showDeleted  = Input::get('showDeleted');
        
       //$detallesoperacion = OperacionDetails::where([['activo','=',1]] )->get();
        $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
        ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.mbl','P.ETD', 'P.ETA', 'P.PO', 'P.PI','P.free_demurrages_days_cust', 'E.nombre as customer', 'PU1.puerto as POL', 'PU2.puerto as POD','SL.nombre AS shippingline')
        ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
        ->leftJoin('adm_entidad as E', 'E.id', '=', 'P.consignee')
        ->leftJoin('adm_entidad as SL', 'SL.id', '=', 'P.shipping_line')
        ->leftJoin('adm_puertos as PU1', 'PU1.id', '=', 'P.POL')
        ->leftJoin('adm_puertos as PU2', 'PU2.id', '=', 'P.POD')
        ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
        ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
        ->where([[function($query) use ($IdCurrentUser) {
            if (Auth::user()->admin != 1 && Auth::user()->admin != 2) {
                $query->where([['U.id_user',"=",$IdCurrentUser],['U.activo','=',1]]);
            }else{
                $query->where([['U.type','=',1]]);
            }
        }],['U.activo','=',1],['A.activo','=',$showDeleted]])
        ->orderby('P.hbl')
        ->orderBy('C.id', 'DESC')
        ->get();


        return Datatables::of($detallesoperacion)
            
            ->addColumn('action', function ($detallesoperacion) {

                $result = '';

                if ($detallesoperacion->activo == 1){
                    if($this->uShare($detallesoperacion->operacion_id) != 0)
                    {

                            if (Auth::user()->hasPermissionTo('Editar'))
                            $result .= " <a href='".route('operaciones.edit', encrypt($detallesoperacion->operacion_id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>
                            <button type='submit' class='btn btn-sm btn-warning' data-toggle='modal' data-cont='".encrypt($detallesoperacion->id)."' data-title='".$detallesoperacion->hbl."' data-ncont='".encrypt($detallesoperacion->container_number)."' data-neta='".encrypt($detallesoperacion->ETA)."' data-eta='".$detallesoperacion->ETA."' data-netd='".encrypt($detallesoperacion->ETD)."'  data-all='".$this->allContainers($detallesoperacion->operacion_id)."' data-target='#confirmContainerComment'><i class='fa fa-refresh' aria-hidden='true'></i></button>";
                    }
                }
                else if ($detallesoperacion->activo == 2){
                    if(Auth::user()->admin == 2){
                        if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= "<button type='submit' class='btn btn-sm btn-warning' data-toggle='modal' data-cont='".encrypt($detallesoperacion->id)."' data-title='".$detallesoperacion->hbl."' data-ncont='".encrypt($detallesoperacion->container_number)."' data-neta='".encrypt($detallesoperacion->ETA)."' data-eta='".$detallesoperacion->ETA."' data-netd='".encrypt($detallesoperacion->ETD)."' data-target='#myModalReturn'><i class='fa fa-refresh' aria-hidden='true'></i></button>";  
                    }
                }else{ $result .= ''; }   

                return $result;
            })
            ->addIndexColumn()
            ->editColumn('ETD', '{{ Carbon\Carbon::parse($ETD)->format("d-m-y") }}')
            ->editColumn('ETA', '{{ Carbon\Carbon::parse($ETA)->format("d-m-y") }}')
            ->addColumn('freeDays', function ($query) { 
                return $this->diasLibres($query->ETA, $query->free_demurrages_days_cust);   
            })
            ->editColumn('tipo_operacion', function ($query) {

                return Carbon::parse($query->empty_container)->format("d-m-y");
            })
            ->rawColumns(['estado', 'action'])
            ->make(true);
    }

    public function sendMailCreate($idoperacion){

        $detallesoperacion = Operacion::where("id",$idoperacion)->first();
        $contenedor  = OperacionDetails::where("operacion_id",$idoperacion)->get();
        $dataConsignee = Entidades::where("id",$detallesoperacion->consignee)->first();
        $cc = $dataConsignee->email_cc;

        

        $data = array();

        $data['id'] = $detallesoperacion->id;
        $data['consignee'] = $detallesoperacion->consigneeDesc->nombre;
        $data['mbl'] = $detallesoperacion->mbl;
        $data['hbl'] = $detallesoperacion->hbl;
        $data['tipo_operacion'] = $detallesoperacion->tipo_operacion;
        $data['vogage'] = $detallesoperacion->vogage;
        $data['vessel'] = $detallesoperacion->vessel;
        $data['ETD'] = $detallesoperacion->ETD;
        $data['ETA'] = $detallesoperacion->ETA;
        $data['POL'] = $detallesoperacion->POLdesc->puerto;
        $data['POD'] = $detallesoperacion->PODdesc->puerto;
        $data['final_dest'] = $detallesoperacion->final_dest;
        $data['shipping_line'] = isset($detallesoperacion->slineDesc->nombre) ? $detallesoperacion->slineDesc->nombre : '';
        $data['nombrecontacto'] = $detallesoperacion->consigneeDesc->nombrecontacto;

        
        $message =  app('App\Http\Controllers\EmailsController')->build($data, $contenedor, $dataConsignee->email, $dataConsignee->nombrecontacto, 'SUMEX0'.$idoperacion, $cc);
        
        if($message == 1){
            $operationSend = Operacion::find($idoperacion);
            $operationSend->update([
                'send_mail' => 1
            ]);

            $mess = "The operation notification was sent successfully";
        }else{
            $mess = "An error occurred while sending the operation notification";
        }
        
        return $mess;
        //return view('mail.msgoperation', compact('data','contenedor'));

    }

    public function sendMailNotification ($idoperacion){

        $detallesoperacion = Operacion::where("id",$idoperacion)->first();
        $dataConsignee = Entidades::where("id",$detallesoperacion->consignee)->first();
        $cc = $dataConsignee->email_cc;

        $data = array();

        $data['id'] = $detallesoperacion->id;
        $data['consignee'] = $detallesoperacion->consigneeDesc->nombre;
        $data['mbl'] = $detallesoperacion->mbl;
        $data['hbl'] = $detallesoperacion->hbl;
        $data['tipo_operacion'] = $detallesoperacion->tipo_operacion;
        $data['vogage'] = $detallesoperacion->vogage;
        $data['vessel'] = $detallesoperacion->vessel;
        $data['ETD'] = $detallesoperacion->ETD;
        $data['ETA'] = $detallesoperacion->ETA;
        $data['POL'] = $detallesoperacion->POLdesc->puerto;
        $data['POD'] = $detallesoperacion->PODdesc->puerto;
        $data['final_dest'] = $detallesoperacion->final_dest;
        $data['shipping_line'] = isset($detallesoperacion->slineDesc->nombre) ? $detallesoperacion->slineDesc->nombre : '';
        $data['nombrecontacto'] = $detallesoperacion->consigneeDesc->nombrecontacto;

        
        $message =  app('App\Http\Controllers\EmailsController')->notification($data, $dataConsignee->email, $dataConsignee->nombrecontacto, 'SUMEX0'.$idoperacion, 'ETA date Update', $cc);
        
        if($message == 1){
            $mess = ", ETA notification was sent";
        }else{
            $mess = ", An error occurred while sending the operation notification";
        }
        
        return $mess;
        //return view('mail.msgoperation', compact('data','contenedor'));

    }
    public function updateHBL(Request $request){

        $this->validate($request, [
            'hbl' => 'required|max:50|unique:adm_operaciones',
            'operationid' => 'required',
        ],[
            'hbl.required' => 'Requerid field HBL!',
            'operationid.required' => 'There was an error, try again!',
            'hbl.unique' => 'HBL entered already exists, try again!',
        ]);

        $idoperacion = decrypt($request['operationid']); 
        $operacion = Operacion::where("id",$idoperacion)->first();

        if($operacion->pre_operation == 1){

            $operacion->update([
                'pre_operation' => 0,
                'hbl' => isset($request['hbl']) ? Str::upper($request['hbl']) : NULL,
            ]);

              /*  if($operacion->send_mail == 0 && $operacion->pre_operation == 0)
                {
                    $mail_send = $this->sendMailCreate($idoperacion);
                    
                }else{ $mail_send =''; }*/

            // Registro en log.
            \Log::info('|::| Recurso Operacion edicion de HBL.', ['Usuario' => Auth::user(), 'Operacion' => $operacion]);

            // Registro en Bitácora.
            \LogActivity::addToLog('Operacion Edicion de: '.$operacion->hbl);

            
            return redirect()->route('operaciones.index')
                ->with('flash_message',
                'Operation HBL : '. $operacion->hbl.' edit successfully!');
        }        

    }

    public function telex(Request $request){

        $this->validate($request, [
            'operationid' => 'required',
        ],[
            'operationid.required' => 'There was an error, try again!',
        ]);

        $idoperacion = decrypt($request['operationid']); 
        $operacion = Operacion::where("id",$idoperacion)->first();

        if($operacion->pre_operation == NULL && $operacion->activo == 1){

            $operacion_update = DB::table('adm_operaciones')
                ->where([['id',$idoperacion]])
                ->update(['telex' => 'Yes']); 

            // Registro en log.
            \Log::info('|::| Recurso Operacion edicion de Telex.', ['Usuario' => Auth::user(), 'Operacion' => $operacion]);

            // Registro en Bitácora.
            \LogActivity::addToLog('Operacion Edicion de: '.$operacion->hbl);

            
            return redirect()->route('operaciones.index')
                ->with('flash_message',
                'Operation HBL : '. $operacion->hbl.' Telex edit successfully!');
        }        

    }

    public function restorePreOperation(Request $request){

        $this->validate($request, [
            'operacionid' => 'required',
        ],[
            'operacionid.required' => 'There was an error, try again!',
        ]);

        

        $idoperacion = decrypt($request['operacionid']); 
        $operacion = Operacion::where("id",$idoperacion)->first();
        $hbl = "SUMEX0".$operacion->id;
            
        if($operacion->pre_operation == 1){

            
            $operacion_update = DB::table('adm_operaciones')
                ->where([['id',$idoperacion]])
                ->update(['hbl' => $hbl,'activo'=> 1]); 

                $operacion = Operacion::where("id",$idoperacion)->first();
            // Registro en log.
            \Log::info('|::| Recurso Pre Operacion Restaurada.', ['Usuario' => Auth::user(), 'Operacion' => $operacion]);

            // Registro en Bitácora.
            \LogActivity::addToLog('Operacion Edicion de: '.$hbl);

            
            return redirect()->route('operaciones.index')
                ->with('flash_message','Operation HBL : '. $hbl.' Restored Pre Operation!');
        }else{
            return redirect()->route('operaciones.index')->with('Error','There was a problem, cannot be restored!');  
        }        

    }    

}
