<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Cargo;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Input;

class CargoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Cargos Index.', ['Usuario' => Auth::user()]);

         //Get all cargos and pass it to the view
         $cargos = Cargo::count();
         $cargosEnabled = Cargo::where('activo','=',1)->count();
         $cargosDisabled = Cargo::where('activo','=',0)->count();
         return view('cargo.index')->with('cargos', $cargos)->with('cargosEnabled', $cargosEnabled)->with('cargosDisabled', $cargosDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario cargo.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Get all municipios and show view
        return view('cargo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'nombre' => 'required|max:50',
        ],[
            'nombre.required' => 'El campo Nombre es requerido!',
            
        ]);

        $cargo = new Cargo();
        $cargo->descripcion = $request['nombre'];
        $cargo->save();

        // Registro en log.
        \Log::info('|::| Recurso Cargo.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Cargo' => $cargo]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$cargo->nombre);

        return redirect()->route('cargos.index')
            ->with('flash_message',
             'Cargo '. $cargo->nombre.' agregada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        // Registro en log.
        \Log::info('|::| Cargo.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $cargo = Cargo::findOrFail(decrypt($id));
        return view('cargo.show', compact('cargo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| Cargo.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $cargo = Cargo::findOrFail(decrypt($id));
        return view('cargo.edit', compact('cargo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'descripcion' => 'required|max:50',
        ],[
            'descripcion.required' => 'El campo Nombre es requerido!',
            
        ]);

        $cargo = Cargo::findOrFail($id);
        $input = $request->all();
        $cargo->fill($input)->save();

        return redirect()->route('cargos.index')
            ->with('flash_message',
             'Cargo '. $cargo->descripcion.' actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $cargo = Cargo::findOrFail(decrypt($request->idcargo));
        $cargo->activo = 0;
        $cargo->save();

        // Registro en log.
        \Log::info('|::| Cargo.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Cargo' => $cargo]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$cargo->descripcion);

        return redirect()->route('cargos.index')
            ->with('flash_message',
             'Cargo eliminado con éxito!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $showDeleted  = Input::get('showDeleted');

        $cargos = Cargo::where('activo','=',$showDeleted )->get();
        return Datatables::of($cargos)
            ->addColumn('action', function ($cargos) {

                $result = '';

                if ($cargos->activo == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('cargos.show', encrypt($cargos->idcargo))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('cargos.edit', encrypt($cargos->idcargo))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($cargos->idcargo)."' data-title='".$cargos->descripcion."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addIndexColumn()
            ->editColumn('descripcion', '{{ $descripcion }}')
            ->editColumn('id', '{{$idcargo}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    

}
