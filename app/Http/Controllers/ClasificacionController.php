<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Clasificacion;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Input;

class ClasificacionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Registro de log.
        \Log::info('|::| Clasificación Index.', ['Usuario' => Auth::user()]);

         //Get all cargos and pass it to the view
         $classification = Clasificacion::count();
         $classificationEnabled = Clasificacion::where('activo','=',1)->count();
         $classificationDisabled = Clasificacion::where('activo','=',0)->count();
         return view('clasificaciones.index')->with('clasificaciones', $classification)->with('classificationEnabled', $classificationEnabled)->with('classificationDisabled', $classificationDisabled);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Registro de log.
        \Log::info('|::| Muestra el formulario Clasificación.create para crear un nuevo recurso.', ['Usuario' => Auth::user()]);

        //Get all municipios and show view
        return view('clasificaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'nombre' => 'required|max:50',
        ],[
            'nombre.required' => 'El campo Nombre es requerido!',
            
        ]);

        $clasificacion = new Clasificacion();
        $clasificacion->descripcion = $request['nombre'];
        $clasificacion->save();

        // Registro en log.
        \Log::info('|::| Recurso Clasificación.store recién creado y almacenado.', ['Usuario' => Auth::user(), 'Clasificación' => $clasificacion]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién creado: '.$clasificacion->nombre);

        return redirect()->route('clasificaciones.index')
            ->with('flash_message',
             'Clasificación '. $clasificacion->nombre.' agregada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!isset($_SERVER['HTTP_REFERER'])){
            Auth::logout();
            return view('errors.forbidden', ['message' => 'No puedes acceder directamente.']);
        }

        // Registro en log.
        \Log::info('|::| Clasificación.show - Mostrar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);
        
        $clasificacion = Clasificacion::findOrFail(decrypt($id));
        return view('clasificaciones.show', compact('clasificacion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Registro en log.
        \Log::info('|::| Clasificación.edit - Muestra el formulario para editar el recurso especificado.', ['Usuario' => Auth::user(), 'Id' => decrypt($id)]);

        $clasificacion = Clasificacion::findOrFail(decrypt($id));
        return view('clasificaciones.edit', compact('clasificacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validate name, email and password fields
        $this->validate($request, [
            'descripcion' => 'required|max:50',
        ],[
            'descripcion.required' => 'El campo Nombre es requerido!',
            
        ]);

        $clasificacion = Clasificacion::findOrFail($id);
        $input = $request->all();
        $clasificacion->fill($input)->save();

        return redirect()->route('clasificaciones.index')
            ->with('flash_message',
             'Clasificación '. $clasificacion->descripcion.' actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $cargo = Clasificacion::findOrFail(decrypt($request->id));
        $cargo->activo = 0;
        $cargo->save();

        // Registro en log.
        \Log::info('|::| Clasificación.destroy Elimina el recurso especificado.', ['Usuario' => Auth::user(), 'Clasificación' => $cargo]);

        // Registro en Bitácora.
        \LogActivity::addToLog('Recurso recién eliminado: '.$cargo->descripcion);

        return redirect()->route('clasificaciones.index')
            ->with('flash_message',
             'Clasificación eliminada con éxito!');
    }

    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getData()
    {
        $showDeleted  = Input::get('showDeleted');

        $Clasificaciones = Clasificacion::where('activo','=',$showDeleted )->get();
        return Datatables::of($Clasificaciones)
            ->addColumn('action', function ($Clasificacion) {

                $result = '';

                if ($Clasificacion->activo == 1){

                    if (Auth::user()->hasPermissionTo('Ver'))
                        $result .= " <a href='".route('clasificaciones.show', encrypt($Clasificacion->id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Ver'><i class='fa fa-eye'></i></a> ";

                    if (Auth::user()->hasPermissionTo('Editar'))
                        $result .= " <a href='".route('clasificaciones.edit', encrypt($Clasificacion->id))."' class='btn btn-sm btn-info' data-toggle='tooltip' title='Editar'><i class='fa fa-edit'></i></a>";
    
                    if (Auth::user()->hasPermissionTo('Eliminar'))
                        $result .= " <button type='submit' class='btn btn-sm btn-danger' data-toggle='modal' data-idemp='".encrypt($Clasificacion->id)."' data-title='".$Clasificacion->descripcion."' data-target='#confirmDeleteComment'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
                }

                return $result;
            })
            ->addIndexColumn()
            ->editColumn('descripcion', '{{ $descripcion }}')
            ->editColumn('id', '{{$id}}')
            ->editColumn('created_at', '{{ Carbon\Carbon::parse($created_at)->format("d-M-y H:i:s") }}')
            ->editColumn('updated_at', '{{ Carbon\Carbon::parse($updated_at)->format("d-M-y H:i:s") }}')
            ->make(true);
    }

    

}
