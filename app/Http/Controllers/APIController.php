<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Puerto;
use App\Estado;
use App\Aeropuerto;
use App\Pais;
use App\Operacion;
use App\Contenedor;
use App\OperacionDetails;
use App\Liberacion;
use App\Incoterm;
use App\Entidades;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
Use Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use DB;
use Response;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class APIController extends Controller
{
    /** 
     *Funcion valida variable nula
    */
    public function VariableNull($data){
        if (is_null($data) || $data == ""){
            $data = "Does not apply";
        }else{
            $data = $data;
        }
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Operacion::where('consignee', $id)->exists()) {
            $consignee = Operacion::where([["consignee",$id],['activo', '=',1],['pre_operation', '=',0]])
            ->get();
            
            return response($consignee, 200);

        }else {
            return response()->json([
              "message" => "Operations not found"
            ], 404);
          }

    }
    public function datos($consignee, $status){
        if (Operacion::where('consignee', $consignee)->exists()) {
            
            $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->where([['A.activo','=',$status],['P.activo','=',1],['P.consignee','=',$consignee]])->count();

            return response()->json($detallesoperacion, 200);
        }else {
            return response()->json([
              "message" => "Conteiners not found"
            ], 404);
          }
    }

    public function Descentidad($id){
        if($id != NULL){
            $entidad = Entidades::where("id","=",$id)->first();
        }else{
            $entidad = "Does not apply";
        }

        return $entidad;
    }


    public function DescPuerto($id, $type){

        if($type == 1){
            $result = Puerto::where('id', '=', $id)->first();
            $name = $result->puerto;
            
        }
        elseif($type == 2){
            $result = Aeropuerto::where('id', '=', $id)->first();
            $name = $result->nombre;
        }
        elseif($type == 3){
            $result = Estado::where('id', '=', $id)->first();
            $name = $result->nombre;
        }else{
            $name = 'not apply ';
        }
      
        
 
        return $name;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showOperation($id)
    {

        if (Operacion::where('id', $id)->exists()) {

            //$consignee = Operacion::where('id','=',$id)->first();
            $consignee  = Operacion::where('id','=',$id)->first();
            $containers = OperacionDetails::where([['operacion_id','=',$id],['activo','>',0]] )->get();

            $consig=array(
                'id'             => $consignee['id'],
                'hbl'            => $consignee['hbl'],
                'mbl'            => $consignee['mbl'],
                'house_release'  => $this->VariableNull(isset($consignee->libhblDesc->descripcion)?$consignee->libhblDesc->descripcion:null),
                'master_release' => $this->VariableNull(isset($consignee->libmblDesc->descripcion)?$consignee->libmblDesc->descripcion:null),
                'ETD'            => Carbon::parse($consignee['ETD'])->format("d-M-y"),
                'ETA'            => Carbon::parse($consignee['ETA'])->format("d-M-y"),
                'vessel'         => $this->VariableNull($consignee['vessel']),
                'vogage'         => $this->VariableNull($consignee['vogage']),
                'POL'            => $this->VariableNull($consignee->POLdesc->puerto),
                'POD'            => $this->VariableNull($consignee->PODdesc->puerto),
                'final_dest'     => $this->VariableNull($consignee['final_dest']),
                'shipper'        => $consignee->shipperDesc->nombre,
                'consignee'      => $consignee->consigneeDesc->nombre,
                'agente'         =>  $this->VariableNull(isset($consignee->agenteDesc->nombre)?$consignee->agenteDesc->nombre:null),
                'agente_envio'   => $consignee->AEnDesc->nombre,
                'shipping_line'  => $this->VariableNull(isset($consignee->slineDesc->nombre)?$consignee->slineDesc->nombre:null),
                'INCOTERM'       => $this->VariableNull(isset($consignee->incoDesc->nombre)?$consignee->incoDesc->nombre:null),
                'commodity'      => $this->VariableNull($consignee['commodity']),
                'free_demurrages_days_cust' => $this->VariableNull($consignee['free_demurrages_days_cust']),
                'house_release_date'        => $this->VariableNull($consignee['house_release_date']),
                'master_release_date'       => $this->VariableNull($consignee['master_release_date']),
                'tipo_operacion'            => $this->VariableNull($consignee['tipo_operacion']),
                'operacion_detail'          => $this->VariableNull($consignee['operacion_detail']),
                'operacion'                 => $this->VariableNull($consignee['operacion']),
                'activo'                    => $this->VariableNull($consignee['activo']),
                'cy'                        => $this->VariableNull($consignee['cy']),
                'cy_door'                   => $this->VariableNull($consignee['cy_door']),
                'PO'                        => $this->VariableNull(isset($consignee['PO'])?$consignee['PO']:null),
                'PI'                        => $this->VariableNull(isset($consignee['PI'])?$consignee['PI']:null),
                'factura'                   => $this->VariableNull($consignee['factura']),
                'sc'                        => $this->VariableNull(isset($consignee['sc'])?$consignee['sc']:null),
                'pre_operation'             => $this->VariableNull($consignee['pre_operation']),
                'comments'                  => $this->VariableNull($consignee['comments']),
                'terminal'                  => $this->VariableNull($consignee['terminal']),
                'telex'                     => $this->VariableNull(isset($consignee['telex'])?$consignee['telex']:null),
                'containers'                => $containers
            );

            
            return response()->json($consig, 200);

        }else {
            return response()->json([
              "message" => "Number of Operation not found"
            ], 404);
          }

    }

    public function typeOperation($tipo_operacion){

        if( $tipo_operacion == 1)
        {
            $operacion = "Ocean";

        }else if( $tipo_operacion == 2)
        {
            $operacion = "Air";                    
        }else if( $tipo_operacion == 3)
        {
            $operacion = "In land"; 
        }else{
            $operacion = "Not Apply";
        }
        return $operacion;

    }

    public function reportGeneral($etas, $etast, $consignee){
        if (Operacion::where('consignee', $consignee)->exists()) {    
            $data = DB::table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo', 'P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA', 'P.free_demurrages_days_cust', 'P.POL','P.POD','P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal', 'P.tipo_operacion', 'C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->where([['P.consignee','=',$consignee],[
                function($query){
                    $query->where('A.activo', '=', 1)
                    ->orWhere('A.activo', '=', 2);
                }
            ]])
            ->whereBetween('P.ETA', array($etas, $etast))
            ->orderby('P.id', 'DESC')
            ->get();

            $datas = array();

            foreach ($data as $v) {
                $datas[] = array(
                    'SUMEX' => 'SUMEX0'.$v->SUMEX,
                    'PI' => $this->VariableNull(isset($v->PI)?$v->PI:null),
                    'PO' => $this->VariableNull(isset($v->PO)?$v->PO:null),
                    'factura' => $this->VariableNull(isset($v->factura)?$v->factura:null),
                    'hbl' => $this->VariableNull(isset($v->hbl)?$v->hbl:null),
                    'mbl' => $this->VariableNull(isset($v->mbl)?$v->mbl:null),
                    'ETD' => Carbon::parse($v->ETD)->format("d-M-y"),
                    'ETA' => Carbon::parse($v->ETA)->format("d-M-y"),
                    'ATD' => Carbon::parse($v->ETD)->format("d-M-y"),
                    'ATA' => Carbon::parse($v->ATA)->format("d-M-y"),
                    'container_number' => $v->container_number,
                    'shipper' => $this->Descentidad($v->shipper)->nombre,
                    'POL' => $this->DescPuerto($v->POL, $v->tipo_operacion),
                    'POD' => $this->DescPuerto($v->POD, $v->tipo_operacion),
                    'terminal' => $this->VariableNull($v->terminal),
                    'telex' => $this->VariableNull($v->telex),
                    'comments' => $this->VariableNull(isset($v->comments)?$v->comments:'Not Comments'),
                    'free_demurrages_days_cust' => $this->VariableNull(isset($v->free_demurrages_days_cust)?$v->free_demurrages_days_cust:null), 
                );
                
            }


            return response()->json($datas, 200);

        }else {
            return response()->json([
              "message" => "Conteiners not found"
            ], 404);
          }
    }

    public function reportGeneralDownload(Request $request){
        
        if (Operacion::where('consignee', $request->consignee)->exists()) {    
            $data = DB::table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo', 'P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA', 'P.free_demurrages_days_cust', 'P.POL','P.POD', 'P.tipo_operacion','P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal','C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->where([['P.consignee','=',$request->consignee],[
                function($query){
                    $query->where('A.activo', '=', 1)
                    ->orWhere('A.activo', '=', 2);
                }
            ]])
            ->whereBetween('P.ETA', array($request->etas, $request->etast))
            ->orderby('P.id', 'DESC')
            ->get();

            return \Excel::create('ReportGeneral', function($excel) use ($data) {
                $excel->sheet('mySheet', function($sheet) use ($data)
                {
                    $sheet->cell('A1', function($cell) {$cell->setValue('FOLIO');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('# PI');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('# PO');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('# Invoice');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('HBL');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('MBL');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('ETD');   });
                $sheet->cell('H1', function($cell) {$cell->setValue('ATD');   });
                $sheet->cell('I1', function($cell) {$cell->setValue('ETA');   });
                $sheet->cell('J1', function($cell) {$cell->setValue('ATA');   });
                $sheet->cell('K1', function($cell) {$cell->setValue('# Container');   });
                $sheet->cell('L1', function($cell) {$cell->setValue('Shipper');   });
                $sheet->cell('M1', function($cell) {$cell->setValue('POL');   });
                $sheet->cell('N1', function($cell) {$cell->setValue('POD');   });
                $sheet->cell('O1', function($cell) {$cell->setValue('TERMINAL');   });
                $sheet->cell('P1', function($cell) {$cell->setValue('TELEX');   });
                $sheet->cell('Q1', function($cell) {$cell->setValue('Free Days');   });
                $sheet->cell('R1', function($cell) {$cell->setValue('comments');   });                
                $sheet->setColumnFormat(array('A' => '@', 'B' => '@','C' => '@', 'D' => '@','E' => '@','F' => '@',
                    'G' => NumberFormat::FORMAT_DATE_DDMMYYYY,'H' => NumberFormat::FORMAT_DATE_DDMMYYYY, 'I' => NumberFormat::FORMAT_DATE_DDMMYYYY,'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,'K' => '@',
                    'L' => '@', 'M' => '@','N' => '@','O' => '@','P' => '@','Q' => '@','R' => '@'));
                    
    
    
                    if (!empty($data)) {
    
                        
                        foreach ($data as $key => $v) {
                            $i= $key+2;
                            
    
                            $sheet->cell('A'.$i, 'SUMEX0'.$v->SUMEX); 
                            $sheet->cell('B'.$i, $this->VariableNull(isset($v->PI)?$v->PI:null)); 
                            $sheet->cell('C'.$i, $this->VariableNull(isset($v->PO)?$v->PO:null));
                            $sheet->cell('D'.$i, $this->VariableNull(isset($v->factura)?$v->factura:null)); 
                            $sheet->cell('E'.$i, $this->VariableNull(isset($v->hbl)?$v->hbl:null)); 
                            $sheet->cell('F'.$i, $this->VariableNull(isset($v->mbl)?$v->mbl:null)); 
                            $sheet->cell('G'.$i, isset($value->ETD) ? Date::stringToExcel($value->ETD) : NULL);
                            $sheet->cell('H'.$i, isset($value->ATD) ? Date::stringToExcel($value->ATD) : NULL);
                            $sheet->cell('I'.$i, isset($value->ATD) ? Date::stringToExcel($value->ETA) : NULL);
                            $sheet->cell('J'.$i, isset($value->ETA) ? Date::stringToExcel($value->ATA) : NULL);
                            $sheet->cell('K'.$i, $v->container_number);
                            $sheet->cell('L'.$i, $this->Descentidad($v->shipper)->nombre);
                            $sheet->cell('M'.$i, isset($value->POL) ? $this->DescPuerto($value->POL, $value->tipo_operacion) : NULL);
                            $sheet->cell('N'.$i, isset($value->POD) ? $this->DescPuerto($value->POD, $value->tipo_operacion) : NULL);
                            $sheet->cell('O'.$i, $this->VariableNull($v->terminal));
                            $sheet->cell('P'.$i, $this->VariableNull($v->telex));
                            $sheet->cell('Q'.$i, isset($value->free_demurrages_days_cust) ? $value->free_demurrages_days_cust : NULL);
                            $sheet->cell('R'.$i, $this->VariableNull(isset($v->comments)?$v->comments:'Not Comments'));
                        }
                    }
                });
            })->export('xlsx');

           

        }else {
            return response()->json([
              "message" => "Not Found"
            ], 404);
          } 

       
       // Storage::download('reports/'.$request->file);

    }
    /** 
     * Todos los contenedores de consignatario
    */
    public function showContainers($consignee,$status){

      
        if (Operacion::where('consignee', $consignee)->exists()) { 
        $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
        ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.mbl','P.ETD', 'P.ETA', 'P.PO', 'P.PI','P.free_demurrages_days_cust', 'P.pre_operation','E.nombre as customer', 'PU1.puerto as POL', 'PU2.puerto as POD','SL.nombre AS shippingline')
        ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
        ->leftJoin('adm_entidad as E', 'E.id', '=', 'P.consignee')
        ->leftJoin('adm_entidad as SL', 'SL.id', '=', 'P.shipping_line')
        ->leftJoin('adm_puertos as PU1', 'PU1.id', '=', 'P.POL')
        ->leftJoin('adm_puertos as PU2', 'PU2.id', '=', 'P.POD')
        ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
        ->where([['A.activo','=',$status],[function ($query) {
            $query->where('P.pre_operation','=',0)
                ->orWhereNull('P.pre_operation');
        }],['P.consignee','=',$consignee]])
        ->orderby('P.hbl')
        ->orderBy('C.id', 'DESC')
        ->get();

        return response()->json($detallesoperacion, 200);

        }else {
            return response()->json([
              "message" => "Conteiners not found"
            ], 404);
          }
    }

    public function isDateBetweenDates($date, $startDate, $endDate) {
        
        $diasDiferencia = $endDate->diffInDays($date);

        if(($date >= $startDate) && ($date <= $endDate))
        {
              return 'Not Apply';
        }
         else
         {
              return $diasDiferencia;
         }
    }

    /**
     * Limpiar Espacios en Blanco.
     *
     */
    public function stringClear($string){
        
        $string = preg_replace("[\s+]",'', $string);

        return $string;
    }

    public function LastFreeDay($eta,$freeDay){

        $free_demurrages_days_cust = $freeDay - 1;           
        //$newDateETA = $eta->addDays($free_demurrages_days_cust);
        $newDateETA =  (new Carbon($eta))->addDays($free_demurrages_days_cust);
        $newDateETA_clear = Carbon::parse($newDateETA);

        foreach ($newDateETA_clear as $k => $v) {
           $date =  $newDateETA_clear->date;
        }
                    
        return $date;

    }
    public function TotalDemoras($ETA, $FDC, $empty_container,$precio_demora, $iva){

        $DemmurrageDays = $this->searchDemurrage($ETA, $FDC, $empty_container);

                    if($DemmurrageDays != "Not Apply"){
                        $iva = $iva/100; 
                        $TotalIVA = $precio_demora * $iva;
                        $precioConIVA = $precio_demora + $TotalIVA;  
                        $DTotal = $precioConIVA * $DemmurrageDays;
                        //$DemmurrageTotal = $iva; 
                        $DemmurrageTotal = number_format($DTotal,2);
                    }else{ $DemmurrageTotal = "0.00"; }
                    
                    return $DemmurrageTotal;
    }

    public function searchDemurrage($ETA, $free_demurrages_days_cust, $empty_container){
            
            
        $fechaEmision = Carbon::parse($ETA);
        $free_demurrages_days_cust = $free_demurrages_days_cust - 1;             
        $newDateETA = $fechaEmision->addDays($free_demurrages_days_cust);
        $newDateETA2 = Carbon::parse($newDateETA);
        $DemmurrageDays = $this->isDateBetweenDates($empty_container, $ETA, $newDateETA2);

        return $DemmurrageDays;

    }

    /**
    * Process ajax request Reporte demoras.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getDataDemurrage($etas, $etast, $consignee){


        if (Operacion::where('consignee', $consignee)->exists()) { 
            
            $data = DB::table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo', 'P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA','P.POL','P.POD','P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal','P.tipo_operacion', 'P.free_demurrages_days_cust', 'P.precio_demora', 'P.iva', 'C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->where([['P.consignee','=',$consignee],['A.activo', '=', '2']])
            ->whereBetween('P.ETA', array($etas, $etast))
            ->orderby('P.id', 'DESC')
            ->get();

            $datas = array();

            foreach ($data as $v) {

                $fechaEmision = Carbon::parse($v->ETA);
                $dias_demora = $v->free_demurrages_days_cust - 1;               
                $newDateETA = $fechaEmision->addDays($dias_demora);
                $newDateETA2 = Carbon::parse($newDateETA);
                $rango = $this->isDateBetweenDates($v->empty_container, $v->ETA, $newDateETA2);
                $total = $this->TotalDemoras($v->ETA, $v->free_demurrages_days_cust, $v->empty_container,$v->precio_demora, $v->iva);

                $datas[] = array(
                    'SUMEX' => 'SUMEX0'.$v->SUMEX,
                    'hbl' => $this->VariableNull(isset($v->hbl)?$v->hbl:null),
                    'mbl' => $this->VariableNull(isset($v->mbl)?$v->mbl:null),
                    'ETA' => Carbon::parse($v->ETA)->format("d-M-y"),
                    'container_number' => $v->container_number,
                    'difdays' => $rango,
                    'demoratotal' => $total,
                    'etaplus' => Carbon::parse($newDateETA2)->format("d-M-y"),
                    'free_demurrages_days_cust' => $v->free_demurrages_days_cust,
                    'empty_container' => Carbon::parse($v->empty_container)->format("d-M-y")
                );
                
            }

            return response()->json($datas, 200);

        }else {
            return response()->json([
              "message" => "Conteiners not found"
            ], 404);
          }
    }

    
    

    public function reportDemurrageDownload(Request $request){
        
        if (Operacion::where('consignee', $request->consignee)->exists()) {    
            $data = DB::table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo', 'P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA','P.POL','P.POD','P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal','P.tipo_operacion', 'P.free_demurrages_days_cust', 'P.precio_demora', 'P.iva', 'C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->where([['P.consignee','=',$request->consignee],['A.activo', '=', '2']])
            ->whereBetween('P.ETA', array($request->etas, $request->etast))
            ->orderby('P.id', 'DESC')
            ->get();

            return \Excel::create('ReportDemurrage', function($excel) use ($data) {
                $excel->sheet('mySheet', function($sheet) use ($data)
                {
                    $sheet->cell('A1', function($cell) {$cell->setValue('# Trucker');   });
                    $sheet->cell('B1', function($cell) {$cell->setValue('HBL');   });
                    $sheet->cell('C1', function($cell) {$cell->setValue('MBL');   });
                    $sheet->cell('D1', function($cell) {$cell->setValue('ETD');   });
                    $sheet->cell('E1', function($cell) {$cell->setValue('ETA');   });
                    $sheet->cell('F1', function($cell) {$cell->setValue('Last Free Day');   });
                    $sheet->cell('G1', function($cell) {$cell->setValue('Free Days');   });
                    $sheet->cell('H1', function($cell) {$cell->setValue('# Container');   });
                    $sheet->cell('I1', function($cell) {$cell->setValue('Date Return Empty');   });
                    $sheet->cell('J1', function($cell) {$cell->setValue('Demurrage Days');   });
                    $sheet->cell('K1', function($cell) {$cell->setValue('Demurrage Total (USD)');   });
                    $sheet->cell('L1', function($cell) {$cell->setValue('# PI');   });
                    $sheet->cell('M1', function($cell) {$cell->setValue('# PO');   });
                    $sheet->setColumnFormat(array('A' => '@', 'B' => '@','C' => '@', 'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,'F' => NumberFormat::FORMAT_DATE_DDMMYYYY,'G' => '@','H' => '@', 'I' => NumberFormat::FORMAT_DATE_DDMMYYYY,'J' => '@','K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2,
                        'L' => '@', 'M' => '@','N' => '@'));
                    


                    if (!empty($data)) {

                        
                        foreach ($data as $key => $value) {
                            $i= $key+2;
                            $folio = 'SUMEX0'.$value->SUMEX;

                            

                            //dd($folio);
                            $sheet->cell('A'.$i, $folio);
                            $sheet->cell('B'.$i, $value->hbl); 
                            $sheet->cell('C'.$i, $value->mbl);
                            $sheet->cell('D'.$i, Date::stringToExcel($value->ETD)); 
                            $sheet->cell('E'.$i, Date::stringToExcel($value->ETA));
                            $sheet->cell('F'.$i, Date::stringToExcel($this->LastFreeDay($value->ETA,$value->free_demurrages_days_cust)));
                            $sheet->cell('G'.$i, $value->free_demurrages_days_cust);
                            $sheet->cell('H'.$i, $value->container_number);
                            $sheet->cell('I'.$i, Date::stringToExcel($value->empty_container));
                            $sheet->cell('J'.$i, $this->searchDemurrage($value->ETA,$value->free_demurrages_days_cust,$value->empty_container));
                            $sheet->cell('K'.$i, $this->TotalDemoras($value->ETA, $value->free_demurrages_days_cust, $value->empty_container,$value->precio_demora,$value->iva));
                            $sheet->cell('L'.$i, $value->PI); 
                            $sheet->cell('M'.$i, $value->PO);
                        }
                    }
                });
            })->export('xlsx');

           

        }else {
            return response()->json([
              "message" => "Not Found"
            ], 404);
          } 
    }
/**
 * Reporte Dias transcurridos 
*/
    public function DiasTranscurridos($inicio,$final){

        $inicio = Carbon::parse($inicio);
        $final = Carbon::parse($final);

        return $diasDiferencia = $final->diffInDays($inicio);
    }


    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getDataTT($etas, $etast, $consignee)
    {
        if (Operacion::where('consignee', $consignee)->exists()) { 

            $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo', 'P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA','P.POL','P.POD','P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal','C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([['U.activo','=',1],['P.consignee','=',$consignee],[
                function($query){
                    $query->where('A.activo', '=', 1)
                    ->orWhere('A.activo', '=', 2);
                }
            ]])
            ->whereBetween('P.ETA', array($etas, $etast))
            ->orderby('P.id', 'asc')
            ->get();
                
                $data = array();
                foreach ($detallesoperacion as $k => $v) {
                    $data[] = array(
                        'SUMEX'   => "SUMEX0".$v->SUMEX,
                        'container_number' => $v->container_number,
                        'shipper' =>$this->Descentidad($v->shipper)->nombre,
                        'ttr' =>$this->DiasTranscurridos($v->ETA,$v->ETD),
                        'ETD' =>Carbon::parse($v->ETD)->format("Y/m/d"),
                        'ATD' =>Carbon::parse($v->ATD)->format("Y/m/d"),
                        'ETA' =>Carbon::parse($v->ETA)->format("Y/m/d"),
                        'ATA' =>Carbon::parse($v->ATA)->format("Y/m/d"),
                        'PI' => $v->PI,
                        'PO' => $v->PO
                    );
                }

            return response()->json($data, 200);

        }else {
                return response()->json([
                  "message" => "Not Found"
                ], 404);
              } 
        
    }

    public function exportGenerateTT(Request $request)
    {

        //dd($request);
        
        if (Operacion::where('consignee', $request->consignee)->exists()) {
        $data =  DB::table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.id AS SUMEX','P.hbl','P.mbl', 'P.ETD', 'P.ETA', 'P.ATD', 'P.ATA', 'P.POL','P.POD', 'P.tipo_operacion', 'P.operacion', 'P.PI', 'P.PO', 'P.shipper', 'P.factura','P.comments', 'P.telex', 'P.terminal','C.descripcion AS tipo_operacion_desc')
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
            ->Join('adm_operaciones_usuarios as U', 'U.id_operacion', '=', 'P.id')
            ->where([['U.activo','=',1],['P.consignee','=',$request->consignee],[
                function($query){
                    $query->where('A.activo', '=', 1)
                      ->orWhere('A.activo', '=', 2);
                }
               ]])
            ->whereBetween('P.ETA', array($request->etas, $request->etast))
            ->orderby('P.id', 'asc')->get()->toArray();
            
        
        return \Excel::create('ReportTTR', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {

                $sheet->cell('A1', function($cell) {$cell->setValue('FOLIO');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('# Container');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('Shipper');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('Transit Time');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('ETD');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('ATD');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('ETA');   });
                $sheet->cell('H1', function($cell) {$cell->setValue('ATA');   });
                $sheet->cell('I1', function($cell) {$cell->setValue('# PO');  });
                $sheet->cell('J1', function($cell) {$cell->setValue('# PI');  });
                $sheet->setColumnFormat(array('A' => '@', 'B' => '@','C' => '@', 'D' => '@','E' => NumberFormat::FORMAT_DATE_DDMMYYYY,'F' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                    'G' => NumberFormat::FORMAT_DATE_DDMMYYYY,'H' => NumberFormat::FORMAT_DATE_DDMMYYYY, 'I' => '@', 'j' => '@'));                


                if (!empty($data)) {

                    
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $folio = 'SUMEX0'.$value->SUMEX;

                        
                        $sheet->cell('A'.$i, $folio);
                        $sheet->cell('B'.$i, $value->container_number);
                        $sheet->cell('C'.$i, $this->Descentidad($value->shipper)->nombre);
                        $sheet->cell('D'.$i, $this->DiasTranscurridos($value->ETA,$value->ETD));
                        $sheet->cell('E'.$i, Date::stringToExcel($value->ETD));
                        $sheet->cell('F'.$i, Date::stringToExcel($value->ATD)); 
                        $sheet->cell('G'.$i, Date::stringToExcel($value->ETA));
                        $sheet->cell('H'.$i, Date::stringToExcel($value->ATA));
                        $sheet->cell('I'.$i, isset($value->PO) ? json_decode($value->PO) : NULL);
                        $sheet->cell('J'.$i, isset($value->PI) ? json_decode($value->PI) : NULL);
                    }
                }
            });
        })->export('xlsx');

    }else {
        return response()->json([
          "message" => "Not Found"
        ], 404);
      } 
    }


    public function cards($consignee, $type){
        if (Operacion::where('consignee', $consignee)->exists()) {
           
           
                $operaciones = Operacion::where([['consignee', $consignee],['activo', 1]])->get();
                $dataOperaciones = $operaciones->count();

           
                $ServicioMaritimo = Operacion::where([['consignee', $consignee],['tipo_operacion', 1],['activo', 1]])->get();
                $dataMaritimo = $ServicioMaritimo->count();

            
                $ServicioAereo = Operacion::where([['consignee', $consignee],['tipo_operacion', 2],['activo', 1]])->get();
                $dataAereo = $ServicioAereo->count();

                
                $ServicioTerrestre = Operacion::where([['consignee', $consignee],['tipo_operacion', 3],['activo', 1]])->get();
                $dataTerrestre = $ServicioTerrestre->count();

                $containersActiveData  =  DB::table('adm_operaciones_contenedor as A')
                ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
                ->where([['P.consignee','=',$consignee],['A.activo', '=', 1]],['P.activo', '=', 1])
                ->get()->count();

                $ContainersTotal = array();

                $header = [
                    'Containers Status',
                    'Total'];
                array_push($ContainersTotal, $header);

                $containersActive = ['Containers Active', $containersActiveData];
                array_push($ContainersTotal, $containersActive);

                $containersReturnsData =  DB::table('adm_operaciones_contenedor as A')
                ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
                ->where([['P.consignee','=',$consignee],['A.activo', '=', 2],['P.activo', '=', 1]])
                ->get()->count();

                $containersReturns = ['Containers Returns', $containersReturnsData];

                array_push($ContainersTotal, $containersReturns);

               $chartData= DB::table('adm_operaciones')
               ->select(
                    DB::raw('COUNT( * ) AS total'),
                    DB::raw("DATE_FORMAT(ATD,'%m') as months")
                )
                    ->where([['consignee','=',$consignee],['activo', '=', 1]])
                    ->groupBy('months')
                    ->get();
                

            $data = array(
                'dataOperaciones'   => $dataOperaciones,
                'dataMaritimo'      => $dataMaritimo,
                'dataAereo'         => $dataAereo,
                'ServicioTerrestre' => $dataTerrestre,
                'containersData'    => $ContainersTotal,
                'chartData'         => $chartData
            );

            return response()->json($data, 200);
        }else {
            return response()->json([
              "message" => "Not Found"
            ], 404);
          } 
        
    }

    
    /**
    * Process ajax request.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function getDataContainers($etas, $etast, $consignee)
    {
       
        if (Operacion::where('consignee', $consignee)->exists()) {
            
            $detallesoperacion = DB::table('adm_operaciones_contenedor as A')
            ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.hbl','P.mbl','P.ETA','P.free_demurrages_days_cust','P.tipo_operacion','P.operacion', 'P.precio_demora', 'P.iva', 'P.PI', 'P.PO') 
            ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
            ->whereBetween('P.ETA', array($etas, $etast))
            ->where([['P.consignee','=',$consignee],['P.pre_operation','=',0],['P.operacion','=',1],['A.activo', '=', 1],['A.activo', '=', 1]])
            ->orWhere('P.operacion','=',2)
            ->get();

            $data = array();
            foreach ($detallesoperacion as $k => $v) {
                $etaplus = $this->LastFreeDay($v->ETA,$v->free_demurrages_days_cust);

                $data[] = array(
                    'hbl'       => $v->hbl,
                    'mbl'       => $v->mbl,
                    'ETA'       => Carbon::parse($v->ETA)->format("Y/m/d"),
                    'etaplus'   =>  Carbon::parse($etaplus)->format("Y/m/d"),
                    'container_number' => $v->container_number
                );
            }
            
            return response()->json($data, 200);
            
            }else {
                    return response()->json([
                      "message" => "Not Found"
                    ], 404);
            } 
    }


    public function generateReportContainersxls(Request $request)
    {        
        if (Operacion::where('consignee', $request->consignee)->exists()) {
     
        $data =  DB::table('adm_operaciones_contenedor as A')
        ->select('A.id', 'A.operacion_id', 'A.container_number','A.container_seal','A.empty_container','A.container_type','A.weight','A.chargable_weight','A.volume','A.activo','P.id AS SUMEX','P.hbl','P.mbl','P.ETA','P.ETD','P.free_demurrages_days_cust','P.tipo_operacion','P.operacion', 'P.precio_demora','P.PO','P.PI', 'P.iva','C.descripcion AS tipo_operacion_desc')
        ->join('adm_operaciones as P', 'P.id', '=', 'A.operacion_id')
        ->leftJoin('adm_contenedor as C', 'C.id', '=', 'A.container_type')
        ->where([['P.consignee','=',$request->consignee],['A.activo', '=', '1'], ['P.operacion', '=', '1']])
        ->whereBetween('P.ETA', array($request->etas, $request->etast))
        ->orWhere('P.operacion','=',2)
        ->orderby('P.id', 'asc')->get()->toArray();
            
        
        
        return \Excel::create('ReportContainer', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {

                $sheet->cell('A1', function($cell) {$cell->setValue('FOLIO');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('HBL');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('MBL');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('ETD');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('ETA');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('Last Free Day');   });
                $sheet->cell('G1', function($cell) {$cell->setValue('# Container');   });
                $sheet->setColumnFormat(array('A' => '@', 'B' => '@','C' => '@', 'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,'F' => NumberFormat::FORMAT_DATE_DDMMYYYY,'G' => '@'));


                if (!empty($data)) {

                    
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $folio = 'SUMEX0'.$value->SUMEX;

                        $etaplus = $this->LastFreeDay($value->ETA,$value->free_demurrages_days_cust);
                        $etaplus = Carbon::parse($etaplus)->format("Y-m-d");
                        //dd($etaplus);
                        $sheet->cell('A'.$i, $folio);
                        $sheet->cell('B'.$i, $value->hbl); 
                        $sheet->cell('C'.$i, $value->mbl);
                        $sheet->cell('D'.$i, Date::stringToExcel($value->ETD)); 
                        $sheet->cell('E'.$i, Date::stringToExcel($value->ETA));
                        $sheet->cell('F'.$i, Date::stringToExcel($etaplus));
                        $sheet->cell('G'.$i, $value->container_number);
                    }
                }
            });
        })->export('xlsx');
    
    }else {
        return response()->json([
          "message" => "Not Found"
        ], 404);
      } 
    }



    
    
}
