<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Americanista;
use App\AzulcremaCodes;
use App\Aleatorio;
use PdfReport;
use ExcelReport;
use Config;
use DB;
use Carbon;

class PagosEstadoController extends Controller
{
    private $Xexportar;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }
            // Registro de log.
            \Log::info('|::| Consulta de Estado de Pago.', ['Usuario' => Auth::user()]); 
             return view('pagos.index'); 
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }           
    }

    public function codeSearch($id)
    {
       

            $pagos = AzulcremaCodes::where('id_transaction_cash', '=', $id)->first();

            $pagos = $pagos->codigo;
             if($pagos == "")
              {
                $pagos = "0";
              }else{
                $pagos = $pagos;
              }

            return $pagos;

          

    }    
    public function getData(Request $request)
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }

            

            // Registro de log.
            \Log::info('|::| Consulta Realizada de Clientes Aleatorio.', ['Usuario' => Auth::user()]); 
            $pagos = DB::table('azulcrema')
            ->select('azulcrema.nombre', 'azulcrema.a_paterno', 'azulcrema.a_materno', 'azulcrema.email', 'azulcrema.telefono','azulcrema.celular','azulcrema.created_at AS fecha', 'transacciones_efectivo.uid','transacciones_efectivo.referencia',
            'transacciones_efectivo.created_at', 'transacciones_efectivo.status')
            ->leftJoin('transacciones_efectivo', 'transacciones_efectivo.user_id', '=', 'azulcrema.uuid')
            ->where('azulcrema.email', '=', $request->email)
            ->get();

             return Datatables::of($pagos)
              ->addColumn('action', function ($pago) {

                if($pago->status == 1)
                {
                  $result = "<button type='submit' class='btn btn-sm btn-warning' data-toggle='modal' title='Ver Detalle' data-idemp='".$this->codeSearch($pago->uid)."' data-title='Ver Detalle' data-target='#confirmDeleteComment'><i class='fa fa-eye' aria-hidden='true'></i> Ver Detalle.</button>";
                }else{
                  $result = "";
                }  
                  
                  return $result;
              })->make(true);

            //return Datatables::of($pagos)->toJson();
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }     
    }

    
}
