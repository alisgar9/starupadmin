<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Americanista;
use App\Descuentos;
use PdfReport;
use ExcelReport;
use Config;
use DB;

class ClientesConsultameController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }
            // Registro de log.
            \Log::info('|::| Reportes Consulta de Clientes .', ['Usuario' => Auth::user()]);
             return view('consultame.index'); 
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        }           
    }

    public function getData(Request $request)
    {
        
      
        if($request->Filtro == "SAF")
        {
            
            $this->validate($request, [
                'Afiliacion' => 'required',
            ],[
                'Afiliacion.required' => 'El Número Americanista es requerido!',
                
            ]);
            $numeroAmericanista = $request->Afiliacion;
            $americanistas = Americanista::where([
                ['activo', '=', '1'],
                ['numeroAmericanista', 'like', $numeroAmericanista . '%']
            ]); 
        }
        else if($request->Filtro == "SNO")
        {
            $this->validate($request, [
                'Nombre' => 'required',
            ],[
                'Nombre.required' => 'El Nombre Americanista es requerido!',
                
            ]);

            $Nombre = $request->Nombre;
            $americanistas = Americanista::where([
                ['activo', '=', '1'],
                ['nombre', 'like', '%' . $Nombre . '%']
            ]); 
        }
        else if($request->Filtro == "SAP")
        {
            $this->validate($request, [
                'Apaterno' => 'required',
            ],[
                'Apaterno.required' => 'El Apellido paterno Americanista es requerido!',
                
            ]);

            $Apaterno = $request->Apaterno;
            $americanistas = Americanista::where([
                ['activo', '=', '1'],
                ['paterno', 'like', $Apaterno . '%']
            ]); 
        }
        else if($request->Filtro == "SAM")
        {
            $this->validate($request, [
                'Amaterno' => 'required',
            ],[
                'Amaterno.required' => 'El Apellido materno Americanista es requerido!',
                
            ]); 

            $Amaterno = $request->Amaterno;
            $americanistas = Americanista::where([
                ['activo', '=', '1'],
                ['materno', 'like', $Amaterno . '%']
            ]); 
        }
        else
        {
            return redirect()->back()->with('message', 'Debes utilizar un filtro, para realizar tu consulta.');
        }            

   

        return Datatables::of($americanistas)
        ->addColumn('action', function ($americanista) {

            $result = "<button type='submit' class='btn btn-sm btn-warning' data-toggle='modal' title='Activar Descuento' data-idemp='".encrypt($americanista->id)."' data-title='Aplicar Descuento' data-target='#confirmDeleteComment'><i class='fa fa-percent' aria-hidden='true'></i> Activar Dscto.</button>";
            return $result;
        })->make(true);
    }

    public function discount(Request $request)
    {
        try{

            if(!isset($_SERVER['HTTP_REFERER'])){
                  return redirect()->back()->with('message', 'No puedes acceder directamente.');
            }


            $this->validate($request, [
                'id_americanista' => 'required',
                'descripcion' => 'required'
            ],[
                'id_americanista.required' => 'El Id Americanista es requerido!',
                'descripcion.required' => 'El campo Descripción es requerido!',
                
            ]);

            $descuento = new Descuentos();
            $descuento->idamericanista = decrypt($request->id_americanista);
            $descuento->descripcion = strtoupper($request->descripcion);
            $descuento->descuento = 0;
            $descuento->save();

            // Registro en log.
            \Log::info('|::| Descuento activado y almacenado.', ['Usuario' => Auth::user()]);

            // Registro en Bitácora.
            \LogActivity::addToLog('Descuento Activado: ');

            return redirect()->back()->with('message', 'Descuento aplicado con Éxito!!.');
        }
        catch(\Exception $e){
            return view('clientes.Clienteserror', ['message' => $e->getMessage()]);
        } 
    }


}
