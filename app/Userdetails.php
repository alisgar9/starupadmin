<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userdetails extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'adm_user_details';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'fechaingreso',
      'fechasalida',
      'idcargo',
      'salario',
      'rfc',
      'curp',
      'domicilio',
      'telefono',
      'nss',
  ];

  protected $dates = ['fechaingreso', 'fechasalida'];


  
}
