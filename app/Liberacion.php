<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liberacion extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'adm_liberacion';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
    'descripcion', 'tipo', 'activo'
    ];

  // establecer un valor predeterminado.
  protected $attributes = array(
    'status' => 1,
  );

       
}


