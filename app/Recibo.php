<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recibo extends Model
{
/**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'recibos';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'nombre',
	    'direccion',
	    'calle1',
	    'colonia',
	    'nompob',
	    'nomest',
      'cp',
      'rfc',
      'ubicacion',
      'mes_facturado',
      'periodo_facturado',
      'rpu',
      'rmu',
      'no_medidor',
      'no_cuenta',
      'tarifa',
      'multiplo',
      'demanda_contratada',
      'carga_conectada',
      'feclimite',
      'fecorte',
      'fecdesde',
      'fechasta',
      'total',
      'monto_apagar_enletras',
      'id_area',
      'id_municipio',
      'id_costo',
      'id_ubicacion',
      'id_empresa',
      'cfdiComprobante',
      'cfdiEmisor',
      'cfdiReceptor',
      'cfdiConcepto',
      'cfdiTraslado',
      'tfdtimbrefiscaldigital',
      'cfdiAddenda',
      'nombre_archivo_xml',
      'nombre_archivo_pdf',
      'tipo',
      'kwh_total',
      'kwh_energia_base',
      'kwh_energia_intermedia',
      'kwh_energia_punta',
      'kw_demanda_max',
      'kw_demanda_base',
      'kw_demanda_intermedia',
      'kw_demanda_punta',
      'kw_max_movil',
      'kvarh_total',
      'fp',
      'fc',
      'datos_variables_fimporte',
  ];

  // establecer un valor predeterminado.
  protected $attributes = array(
    'mes_facturado' => 'sin datos',
    'kwh_total' => '0',
    'kwh_energia_base' => '0',
    'kwh_energia_intermedia' => '0',
    'kwh_energia_punta' => '0',
    'kw_demanda_max' => '0',
    'kw_demanda_base' => '0',
    'kw_demanda_intermedia' => '0',
    'kw_demanda_punta' => '0',
    'kvarh_total' => '0',
    'kw_max_movil' => '0',
    'fp' => '0',
    'fc' => '0',
  );

  public function Areas()
  {
    return $this->belongsTo('App\Area','id_area');
  }

  public function Municipios()
  {
    return $this->belongsTo('App\Municipio','id_municipio');
  }

  public function Costos()
  {
    return $this->belongsTo('App\Costo','id_costo');
  }

  public function Ubicaciones()
  {
    return $this->belongsTo('App\Ubicacion','id_ubicacion');
  }

  public function Empresas()
  {
    return $this->belongsTo('App\Empresa','id_empresa');
  }
}
