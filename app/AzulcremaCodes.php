<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AzulcremaCodes extends Model
{
    /**
    * El nombre de la tabla donde se almacena los datos
    * @var String
    * @access protected
    */
    protected $table = 'azulcrema_codigos';

    /**
     * El nombre de la llave primaria
    * @var String
    * @access protected
    */
    protected $primaryKey = 'id';

    /**
     * Los atributos que pueden ingresarlos de forma masiva
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'id_transaction_card',
        'id_transaction_cash',
        'vendido',
        'status',
        'creado',
        'comprado'
    ];  
    
    
    
}
