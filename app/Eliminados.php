<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eliminados extends Model
{
    /**
    * El nombre de la tabla donde se almacena los datos
    * @var String
    * @access protected
    */
    protected $table = 'eliminados';

    /**
     * El nombre de la llave primaria
    * @var String
    * @access protected
    */
    protected $primaryKey = 'id';

    /**
     * Los atributos que pueden ingresarlos de forma masiva
     *
     * @var array
     */
    protected $fillable = [
        'numeroAmericanista',
        'numerosocio',
        'noafiliacion',
        'nombre',
        'paterno',
        'materno',
        'correo',
        'contrasena',
        'fechaRegistro',
        'fechaVigencia',
        'codigo',
        'sexo',
        'ocupacion',
        'estado',
        'ciudad',
        'telefono',
        'fechanacorg',
        'fechanac',
        'direccion',
        'colonia',
        'municipio',
        'cp',
        'fuenteSubscripcion',
        'activo',
        'idPais',
        'extra',
        'punto_referencia',
        'comentarios',
        'created_at',
        'updated_at'
    ];  
    
    
    
}
