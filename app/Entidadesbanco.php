<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entidadesbanco extends Model
{
  /**
  * El nombre de la tabla donde se almacena los datos
  * @var String
  * @access protected
  */
  protected $table = 'adm_entidad_bancos';

  /**
  * El nombre de la llave primaria
  * @var String
  * @access protected
  */
  protected $primaryKey = 'id';

  /**
   * Los atributos que pueden ingresarlos de forma masiva
   *
   * @var array
   */
  protected $fillable = [
      'id_entidad',
      'cuentabancaria',
      'cinterbancaria',
      'id_banco',
      'id_banco_inter',
      'swift',
  ];

// establecer un valor predeterminado.
protected $attributes = array(
  'activo' => 1,
);

public function banco()
{
    return $this->belongsTo('App\Banco','id_banco');
}

public function bancointer()
{
    return $this->belongsTo('App\Banco','id_banco_inter');
}

  
}
